#pragma once
#include <vector>
#include "../../Serializer/SerializeFactory.h"
#include "..//..//Graphics//FrameBuffer.h"  

#include "../Regions/Regions.h"
#include "../../EventSystem/TypeInfo.h"

class CameraComp;
class GameObject;
class renderable;

struct Space : public IBase
{
	Space(const char* name = "Default");
	~Space();

	void AddObject(const char* name);
	void AddObject(GameObject& object);
	void AddObjectToGrid(GameObject& object, int key);
	void DeleteObject(int Id);
	void RemoveObjFromAlive(int Id);
	void DeleteFromGrid(GameObject* target);
	void DestroyDeadObjects();
	GameObject* FindObject(std::string name);
	GameObject* FindObject(int id);
	GameObject* FindObject(Tags tag);
	std::vector<GameObject*> GetObjectsByTag(Tags tag);
	void SetDefault();
	void RemoveDefault();
	void Initialize();
	void InitializeCameras();
	void GetCorners(Vector2& topLeft, Vector2& bottomRight, Space* target);

	/* Serialize Space */
	void						ToJson(nlohmann::json & j);
	std::vector<GameObject *>	FromJson(nlohmann::json & j);

	/* Serializable variables */
	bool						mbVisible;
	bool						mbEditable;
	bool						mbDrawOutlines{false};
	bool						mbUseFrameBuffer{true};
	bool						mbDrawShadows{false};
	std::string					space_name_;
	unsigned					mRenderOrder;
	unsigned					mBlendMode;
	bool						mbIsDefault;


	/* Other variables */
	std::map<int, std::pair<std::vector<GameObject*>, std::map<TypeInfo, std::vector<IComp*>>>> grid_;//each grid is 5000x5000 max/min is 100000
	std::vector<GameObject*>	space_objects;
	std::vector<GameObject*>	dead_objects;
	FrameBuffer					mFrameBuffer;
};
