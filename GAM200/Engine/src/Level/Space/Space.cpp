#pragma once
#include "../../Serializer/json.hpp"
#include "../../Serializer/SerializeFactory.h"
#include "..//..//System/GameObjectManager/GameObject.h"
#include "../Editor/Editor.h"
#include "..//Space/Space.h"
#include "..//..//EventSystem/EventDispatcher.h"
#include "..//..//EventSystem/TypeInfo.h"
#include "..//..//Physics/Collision Event/Collision Event.h"
#include "../../Utilities/MyDebug/LeakDetection.h"
#include "../../Graphics/RenderManager.h"
#include "../../Graphics/RenderableComp.h"

using json = nlohmann::json;

Space::Space(const char * name)
{
	space_name_ = name;
	mbIsDefault = false;
}

Space::~Space()
{
	while (!dead_objects.empty())
	{
		delete dead_objects.back();
		dead_objects.pop_back();
	}
	while (!space_objects.empty())
	{
		delete space_objects.back();
		space_objects.pop_back();
	}
}

void Space::AddObject(const char * name)
{
	space_objects.push_back(DBG_NEW GameObject());
	space_objects.back()->mSceneSpace = space_name_;
}

void Space::AddObject(GameObject & object)
{
	Vector2 coords(floorf(object.mPosition.x / GRID_SIZE) * GRID_SIZE,
		floorf(object.mPosition.y / GRID_SIZE) * GRID_SIZE);

	int key = static_cast<int>(coords.x * 623.19 + coords.y);

	object.mSceneSpace = space_name_;

	grid_[key].first.push_back(&object);

	for (auto it = object.GetComps().begin(); it != object.GetComps().end(); it++)
	{
		grid_[key].second[type_of(*(*it))].push_back((*it));
	}

	space_objects.push_back(&object);
}

void Space::AddObjectToGrid(GameObject & object, int key)
{
	grid_[key].first.push_back(&object);

	for (auto it : object.GetComps())
	{
		grid_[key].second[type_of(*it)].push_back(it);
	}
}

GameObject * Space::FindObject(std::string name)
{
	for (auto i = space_objects.begin(); i != space_objects.end(); i++)
	{
		if ((*i)->GetName() == name)
			return (*i);
	}
	return nullptr;
}

GameObject * Space::FindObject(int id)
{
	for (auto i = space_objects.begin(); i != space_objects.end(); i++)
	{
		if ((*i)->GetUID() == id)
			return (*i);
	}
	return nullptr;

}

GameObject * Space::FindObject(Tags tag)
{
	for (auto i = space_objects.begin(); i != space_objects.end(); i++)
	{
		if ((*i)->mTag == tag)
			return (*i);
	}

	return nullptr;
}

std::vector<GameObject*> Space::GetObjectsByTag(Tags tag)
{
	std::vector<GameObject*> wanted;

	for (auto i = space_objects.begin(); i != space_objects.end(); i++)
	{
		if ((*i)->mTag == tag)
			wanted.push_back(*i);
	}

	return wanted;
}


void Space::DeleteObject(int _Id) {
	std::vector<GameObject * >::iterator it;
	for (it = space_objects.begin(); it != space_objects.end(); ++it) {
		if ((*it)->GetUID() == _Id) 
		{
			DeleteFromGrid((*it));
			dead_objects.push_back(*it);
			space_objects.erase(it, it + 1);
			break;
		}
	}
}
void Space::RemoveObjFromAlive(int Id)
{
	std::vector<GameObject * >::iterator it;
	for (it = space_objects.begin(); it != space_objects.end(); ++it) {
		if ((*it)->GetUID() == Id)
		{
			space_objects.erase(it, it + 1);
			break;
		}
	}
}

void Space::DeleteFromGrid(GameObject* target)
{
	Vector2 coords(floorf(target->mPosition.x / GRID_SIZE) * GRID_SIZE
		, floorf(target->mPosition.y / GRID_SIZE) * GRID_SIZE);

	int key = static_cast<int>(coords.x * 623.19 + coords.y);

	auto found = std::find(grid_[key].first.begin(), grid_[key].first.end(), target);

	if (found == grid_[key].first.end())
	{
		for (auto entry : grid_)
		{
			auto go_vec = entry.second.first;

			for (auto go : go_vec)
			{
				if (go == target)
				{
					key = entry.first;
					found = std::find(grid_[key].first.begin(), grid_[key].first.end(), target);
					break;
				}
			}
			if (found != grid_[key].first.end()) break;
		}
	}

	grid_[key].first.erase(found);

	for (auto it : target->GetComps())
		grid_[key].second[type_of((*it))].erase(std::find(grid_[key].second[type_of((*it))].begin(), grid_[key].second[type_of((*it))].end(), it));
}

void Space::SetDefault() {
	mbIsDefault = true;
}

void Space::RemoveDefault() {
	mbIsDefault = false;
}

void Space::Initialize()
{
	for (auto it : space_objects)
	{
		it->Initialize();
	}
}

void Space::InitializeCameras()
{
	for (auto it : space_objects)
	{
		it->InitializeCameras();
	}
}

void Space::GetCorners(Vector2 & topLeft, Vector2 & bottomRight, Space* target)
{
	CameraComp* camera = RenderMgr->GetCamera(target);

	Vector2 centre = camera->GetOwner()->mPosition;
	Vector2 size = camera->mViewSize * 2;

	Vector2 topLeftcamera = centre + Vector2(-(size.x / 2), size.y / 2);
	Vector2 botRightcamera = centre + Vector2(size.x / 2, -(size.y / 2));

	Vector2 topLeftClamp(floorf(topLeftcamera.x / GRID_SIZE), floorf(topLeftcamera.y / GRID_SIZE));
	topLeft = Vector2(topLeftClamp.x * GRID_SIZE, topLeftClamp.y * GRID_SIZE);

	Vector2 botRightClamp(floorf(botRightcamera.x / GRID_SIZE), floorf(botRightcamera.y / GRID_SIZE));
	bottomRight = Vector2(botRightClamp.x * GRID_SIZE, botRightClamp.y * GRID_SIZE);

}

void Space::ToJson(json & _j) {

	_j["space_name_"] = space_name_;
	_j["mbVisible"] = mbVisible;
	_j["mbEditable"] = mbEditable;
	_j["mRenderOrder"] = mRenderOrder;
	_j["mBlendMode"] = mBlendMode;
	_j["mbIsDefault"] = mbIsDefault;

	_j["mbDrawOutlines"] = mbDrawOutlines;
	_j["mbDrawShadows"] = mbDrawShadows;
	_j["mbUseFrameBuffer"] = mbUseFrameBuffer;



	std::map<std::string, Area*>::iterator it;

	std::vector <std::pair<Area*, json>> regionList;


	_j["Regions"];
	for (it = RegionInst->regions.begin(); it != RegionInst->regions.end(); it++) {
		json jRegions;
		json areaJ;
		regionList.push_back(std::pair<Area*, json>(it->second, areaJ));
		_j["Regions"].push_back(it->first);
	}
	json areaJ2;
	
	regionList.push_back(std::pair<Area*, json>(new Area("default",Vector2(1,1), Vector2(1, 1)), areaJ2));
	_j["Regions"].push_back("default");

	std::vector <std::pair<Area*, json>>::iterator regionListit;

	for (GameObject * go : space_objects) {
		bool wasInRegion = false;
		for (regionListit = regionList.begin(); regionListit != regionList.end(); regionListit++) {
			if (RegionInst->IsIn(go, regionListit->first)) {
				wasInRegion = true;
				json j;
				regionListit->second["Objects"];
				if (go->mbSerializable && !go->mbArchetype) {
					go->ToJson(j);
					regionListit->second["Objects"]["GameObjects"];
					regionListit->second["Objects"]["GameObjects"].push_back(j);
				} else if (go->mbArchetype) {
					json gameObj;
					go->ToJsonArchetype(gameObj);
					regionListit->second["Objects"]["Archetypes"][go->mArchetypeName];
					regionListit->second["Objects"]["Archetypes"][go->mArchetypeName].push_back(gameObj);
				}
				break;
			}
		}

		if(!wasInRegion){
			json j;
			regionList.back().second["Objects"];
			if (go->mbSerializable && !go->mbArchetype) {
				go->ToJson(j);
				regionList.back().second["Objects"]["GameObjects"];
				regionList.back().second["Objects"]["GameObjects"].push_back(j);
			}
			else if (go->mbArchetype) {
				json gameObj;
				go->ToJsonArchetype(gameObj);
				regionList.back().second["Objects"]["Archetypes"][go->mArchetypeName];
				regionList.back().second["Objects"]["Archetypes"][go->mArchetypeName].push_back(gameObj);
			}
		}

	}
	RegionInst->SaveRegions(regionList, space_name_);
}

std::vector<GameObject *> Space::FromJson(json & _j) {
	std::vector<GameObject *> goVect;
	space_name_ = _j["space_name_"].get<std::string>();
	mbVisible = _j["mbVisible"];
	mbEditable = _j["mbEditable"];
	mRenderOrder = _j["mRenderOrder"];
	mBlendMode = _j["mBlendMode"];
	if (_j.find("mbIsDefault") != _j.end())
		mbIsDefault = _j["mbIsDefault"];

	if (_j.find("mbDrawOutlines") != _j.end())
		mbDrawOutlines = _j["mbDrawOutlines"];
	if (_j.find("mbUseFrameBuffer") != _j.end())
		mbUseFrameBuffer = _j["mbUseFrameBuffer"];
	if (_j.find("mbDrawShadows") != _j.end())
		mbDrawShadows = _j["mbDrawShadows"];

	if (_j.find("GameObjects") != _j.end()) {
		json & gameObjects = *_j.find("GameObjects");
		for (auto it = gameObjects.begin(); it != gameObjects.end() && gameObjects.size() != 0; ++it) {
			GameObject * go = DBG_NEW GameObject();
			go->mSceneSpace = space_name_;

			go->FromJson((*it)["GameObject"]);
			AddObject(*go);
			goVect.push_back(go);
		}

	}else {

		for (std::string region : _j["Regions"]) {

			json objects = RegionInst->LoadRegion(region, space_name_); 
			if (!objects.empty()) {
				json gameObjects = objects["Objects"]["GameObjects"];//_j["Objects"]["GameObjects"];
				for (auto it = gameObjects.begin(); it != gameObjects.end() && gameObjects.size() != 0; ++it) {
					GameObject * go = DBG_NEW GameObject();
					go->mSceneSpace = space_name_;

					go->FromJson((*it));
					AddObject(*go);
					goVect.push_back(go);
					auto dsadsa = RenderMgr->GetEditorRenderables();
				}

				json & archetypeOjects = objects["Objects"]["Archetypes"];
				for (auto it = archetypeOjects.begin(); it != archetypeOjects.end() && archetypeOjects.size() != 0; ++it) {
					for (auto it2 = it.value().begin(); it2 < it.value().end(); it2++) {
						GameObject * go = DBG_NEW GameObject();
						serializer->LoadArchetype(it.key().data(), go);
						go->mSceneSpace = space_name_;
						go->FromJsonArchetype(it2.value());
						AddObject(*go);
						goVect.push_back(go);
						Editor::archetypeObjects[it.key().data()].push_back(go);
					}
				}	
			}
		}
	}

	auto dsadsa = RenderMgr->GetEditorRenderables();

	return goVect;
}

void Space::DestroyDeadObjects()
{
	while (!dead_objects.empty())
	{
		delete dead_objects.back();
		dead_objects.pop_back();
	}
}
