#pragma once
#pragma once
#include "..//Level.h"
#include "..//Scene/Scene.h"

struct EngineDemoLevel : public Level
{
	void Initialize();
	void Load();
	void Update();
	void Render();
	void Free();
	void Unload();

	//Scene* demo_scene;
};