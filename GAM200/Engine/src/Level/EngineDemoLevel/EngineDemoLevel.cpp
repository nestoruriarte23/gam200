#include "../../Engine/Engine.h"
#include "../../Utilities/MyDebug/LeakDetection.h"
#include "../Editor/Editor.h"
#include "EngineDemoLevel.h"

////////////////* Components *////////////////////

#include "../../GamePlay/src/LogicComponents/CameraSync.h"

//////////////////////////////////////////////////

void EngineDemoLevel::Load() {
}

void EngineDemoLevel::Initialize() {
	GSM->nextLevel = ENGINE_PROOF_LEVEL;
	TimeManager::Instance()->Initialize(true, 60.0);
	Camera::Instance()->Initialize();
	demo_scene = DBG_NEW Scene();
	Game->SetScene(demo_scene);

	RenderMgr->Reset();
	serializer->LoadLevel(Editor::GetLevel().c_str(), demo_scene);

	LogicManager::Instance()->Initialize();
	PhysMgr->Initialize();
	CollMgr->Initialize();
	gAudioMgr->PlaySounds();
}

void EngineDemoLevel::Update() 
{
	if (KeyDown(Key::B)) {
		GSM->nextLevel = EDITOR_LEVEL;
	}

	if (KeyTriggered(Key::P))
		GSM->setPause(!GSM->isPaused());

	f64 fps = TimeManager::Instance()->GetFPS();

	std::string wintitle = "Engine Proof - FPS: ";
	wintitle += std::to_string(fps);
	wintitle += "  Alive Particles: ";
	wintitle += std::to_string(ParticleMgr->GetActiveParticles());

	SDL_SetWindowTitle(Game->GetSDLWindow(), wintitle.c_str());
}

void EngineDemoLevel::Render()
{
	/* Render to texture (Takes care of its own cleaning) */
	RenderMgr->RenderScene(Game->GetCurrScene());			

	/* Set the target */
	FrameBuffer::SetDefaultRenderTarget();

	/* Draw the scene to the whole screen (clean it beforehand) */
	Viewport::WholeScreen().SetOpenGLViewport();
	RenderMgr->CleanViewport(Engine::Color::black);

	RenderManager::Instance()->DrawScreenQuad(demo_scene->mFrameBuffer.GetResultingTexture());
	RenderManager::Instance()->DrawRunTimeDebugLines();

	/* Swap */
	SDL_GL_SwapWindow(Game->GetSDLWindow());

	TimeManager::Instance()->EndFrame();
}

void EngineDemoLevel::Free() {
	RenderManager::Instance()->RemoveAllRenderables();
	RenderManager::Instance()->RemoveAllCameras();
	LogicManager::Instance()->Shutdown();
	gAudioMgr->StopAll();
	StMgr->Shutdown();
}

void EngineDemoLevel::Unload() {
	delete demo_scene;
	Game->SetScene(NULL);
}
