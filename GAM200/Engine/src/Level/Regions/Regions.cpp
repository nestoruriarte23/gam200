#include "Regions.h"
#include "../Editor/Editor.h"

Region::Region() {}

Area& Region::GetRegion(std::string _name) {
	return *regions[_name];
}

void Region::AddRegion(Area* _area) {
	if (!regions[_area->name]) {
		regions[_area->name] = _area;
	}
}

bool Region::IsIn(GameObject* _go, Area * _area) {
	float width = _area->first.x - _area->last.x;
	float height = _area->first.y - _area->last.y;

	Vector2 pCenter{ _area->first.x - (width / 2), _area->first.y - (height / 2) };

	float absWidth = abs(width);
	float absHeight = abs(height);

	if (StaticPointToStaticRect(&(_go->mPosition), &pCenter, absWidth, absHeight)) {
		
		return true;
		
	}
	return false;	
}


void Region::SaveRegions(std::vector <std::pair<Area*, nlohmann::json>> _json, std::string _spaceName) {
	
	for (std::pair<Area*, nlohmann::json> pair : _json) {
		std::string path;
		path += "../Resources/Levels/Regions/";
		_mkdir(path.data());
		path += Editor::LevelName;
		_mkdir(path.data());
		path += +"/";
		path += pair.first->name;
		path += _spaceName;
		path += ".json";

		std::ofstream outFile(path.c_str());
		if (outFile.good() && outFile.is_open()) {
			pair.second["AreaData"];
			pair.second["AreaData"]["name"] = pair.first->name;

			pair.second["AreaData"]["first"];
			pair.second["AreaData"]["first"]["x"] = pair.first->first.x;
			pair.second["AreaData"]["first"]["y"] = pair.first->first.y;
			pair.second["AreaData"]["last"];
			pair.second["AreaData"]["last"]["x"] = pair.first->last.x;
			pair.second["AreaData"]["last"]["y"] = pair.first->last.y;

			outFile << std::setw(4) << pair.second;
			outFile.close();
		}
	}
}

nlohmann::json Region::LoadRegion(std::string _region, std::string _spaceName) {


	nlohmann::json j;

	std::string path;
	path += "../Resources/Levels/Regions/";
	path += Editor::LevelName + "/";
	path += _region;
	path += _spaceName;
	path += ".json";
	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();
		if (!j.empty()) {
			nlohmann::json j2 = j["AreaData"];
			if (j2["name"] != "default") {
				Vector2 first(j2["first"]["x"], j2["first"]["y"]);
				Vector2 last(j2["last"]["x"], j2["last"]["y"]);
				Area* a = new Area(j2["name"], first, last);
				AddRegion(a);
			}
		}
		return j;
	}

	return nullptr;

}