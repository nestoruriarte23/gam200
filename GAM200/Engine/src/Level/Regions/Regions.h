#pragma once

#include <map>
#include <string>
#include <direct.h>
#include <iostream>
#include <fstream>
#include "../../Serializer/json.hpp"
#include "../../Serializer/SerializeFactory.h"
#include "..//..//Engine/Engine.h"
#include "../../Components/System.h"
#include "..//..//System/GameObjectManager/GameObject.h"
//#include "../Editor/Editor.h"

struct Area {
	Area(std::string _name, Vector2 _first, Vector2 _last) : name(_name), first(_first), last(_last){}
	std::string name;
	Vector2 first;
	Vector2 last;
};


struct Region {
		MAKE_SINGLETON(Region);
	public:
		Area& GetRegion(std::string _name);
		void AddRegion(Area * _area);
		std::map<std::string, Area*> regions;
		bool IsIn(GameObject* _go, Area * _area);
		void SaveRegions(std::vector <std::pair<Area*, nlohmann::json>>, std::string _spaceName);
		nlohmann::json LoadRegion(std::string _region, std::string _spaceName);

};


#define RegionInst (Region::Instance())