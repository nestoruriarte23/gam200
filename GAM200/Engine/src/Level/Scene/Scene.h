#pragma once

#include <vector>
#include "../../Serializer/SerializeFactory.h"
#include "..//..//Graphics/Graphics.h"  
#include "../Space/Space.h"

#define GRID_SIZE 2500

class GameObject;
class BoxCollider;
 
struct Scene
{
	~Scene();

	void AddSpace(Space* new_space);
	void AddSpace(const char* space_name);
	void RemoveSpace(const char* space_name);
	void Initialize();
	Space* FindSpace(const char* space_name);

	GameObject* FindObjectInSpace(const char* space_name, const char* object_name);
	GameObject* FindObjectInSpace(const char* space_name, int object_id);
	GameObject* FindObjectInSpace(Space* space, const char* object_name);
	GameObject* FindObjectInSpace(Space* space, int object_id);
	GameObject* FindObjectInAnySpace(const char* object_name);
	GameObject* FindObjectInAnySpace(int object_id);

	template <typename T>
	std::map<Space*, std::vector<std::vector<IComp*>*>> GetComponentsInScreen();

	template <typename T>
	std::map<Space*, std::vector<std::vector<IComp*>*>> GetComponentsInScreen(
		const Vector2& _topLeft, const Vector2& _botright);

	void ClearLists();
	void Update();
	Space* GetDefault();
	void SetDefault(std::string _spaceName);
	void SetDefault(Space& _spaceName);
	void SetDefault(Space* _spaceName);

	void ToJson(nlohmann::json & j);
	std::vector<GameObject *> FromJson(nlohmann::json & j);

	FrameBuffer			mFrameBuffer;
	FrameBuffer			mParticleRealm;

	void FreeDestroyedObjects();
	std::vector<Space*> scene_spaces;

};

template<typename T>
inline std::map<Space*, std::vector<std::vector<IComp*>*>> Scene::GetComponentsInScreen()
{
	std::map<Space*, std::vector<std::vector<IComp*>*>> components;

	auto actual_type = type_of<T>();

	for (auto it : scene_spaces)
	{
		Vector2 topLeft;
		Vector2 botright;

		it->GetCorners(topLeft, botright, it);

		for (auto it1 = topLeft.y; it1 >= botright.y; it1 -= GRID_SIZE)
		{
			for (auto it2 = topLeft.x; it2 <= botright.x; it2 += GRID_SIZE)
			{
				int value = static_cast<int>(it2 * 623.19 + it1);

				if (it->grid_.find(value) != it->grid_.end())
				{
					if (it->grid_[value].second.find(actual_type) != it->grid_[value].second.end())
					{
						components[it].push_back(&(it->grid_[value].second[actual_type]));
					}
				}
			}
		}
	}

	return components;
}

template<typename T>
inline std::map<Space*, std::vector<std::vector<IComp*>*>> Scene::GetComponentsInScreen(
	const Vector2& _topLeft, const Vector2& _botright)
{
	std::map<Space*, std::vector<std::vector<IComp*>*>> components;

	auto actual_type = type_of<T>();

	for (auto it : scene_spaces)
	{
		for (auto it1 = _topLeft.y; it1 >= _botright.y; it1 -= GRID_SIZE)
		{
			for (auto it2 = _topLeft.x; it2 <= _botright.x; it2 += GRID_SIZE)
			{
				int value = static_cast<int>(it2 * 623.19 + it1);

				if (it->grid_.find(value) != it->grid_.end())
				{
					if (it->grid_[value].second.find(actual_type) != it->grid_[value].second.end())
					{
						components[it].push_back(&(it->grid_[value].second[actual_type]));
					}
				}
			}
		}
	}

	return components;
}
