#include <vector>
#include "../../Serializer/json.hpp"
#include "../../Serializer/SerializeFactory.h"
#include "..//..//Utilities//MyDebug//MyDebug.h"
#include "..//..//System/GameObjectManager/GameObject.h"
#include "..//..//Physics/BoxCollider.h"
#include "..//..//Graphics/RenderManager.h"
#include "..//..//Graphics/RenderableComp.h"
#include "..//Space/Space.h"
#include "Scene.h"
#include "../../Graphics/RenderManager.h"

// TEMPORAL
#include "..//..//Graphics//Camera.h"

#include "../../Utilities/MyDebug/LeakDetection.h"

using json = nlohmann::json;

void Scene::AddSpace(const char * space_name)
{
	scene_spaces.push_back(DBG_NEW Space(space_name));
}

Scene::~Scene()
{
	while (!scene_spaces.empty())
	{
		delete scene_spaces.back();
		scene_spaces.pop_back();
	}
}

void Scene::AddSpace(Space * space_name)
{
	scene_spaces.push_back(space_name);
}

void Scene::Initialize()
{
	for (auto it : scene_spaces)
		it->InitializeCameras();
	
	for(auto it : scene_spaces)
		it->Initialize();
}

Space * Scene::FindSpace(const char * space_name)
{
	for (auto i = scene_spaces.begin(); i != scene_spaces.end(); i++)
	{
		if ((*i)->space_name_ == space_name)
			return *i;
	}
	return nullptr;
}

GameObject * Scene::FindObjectInSpace(const char* space_name, const char* object_name)
{
	Space* wanted_space = nullptr;

	for (auto i = scene_spaces.begin(); i != scene_spaces.end(); i++)
	{
		if((*i)->space_name_== space_name)
		{
			wanted_space = *i;
			break;
		}

	}

	ShowConsole(true);
	ASSERT(wanted_space == nullptr);

	wanted_space->FindObject(object_name);


	return nullptr;
}

GameObject * Scene::FindObjectInSpace(const char* space_name, int object_id)
{
	Space* wanted_space = nullptr;

	for (auto i = scene_spaces.begin(); i != scene_spaces.end(); i++)
	{
		if ((*i)->space_name_ == space_name)
		{
			wanted_space = *i;
			break;
		}

	}

	ASSERT(wanted_space == nullptr);

	return wanted_space->FindObject(object_id);
}

GameObject * Scene::FindObjectInSpace(Space * space, const char* object_name)
{
	return space->FindObject(object_name);
}

GameObject * Scene::FindObjectInSpace(Space * space, int object_id)
{
	return space->FindObject(object_id);
}

GameObject * Scene::FindObjectInAnySpace(const char* object_name)
{
	GameObject* target = nullptr;

	for (auto it : scene_spaces)
	{
		target = it->FindObject(object_name);

		if (target != nullptr)
			break;
	}
	
	return target;
}

GameObject * Scene::FindObjectInAnySpace(int object_id)
{
	for (Space * sp : scene_spaces) {
		for (GameObject * go : sp->space_objects) {
			if (go->GetUID() == object_id) {
				return go;
			}
		}
	}
	return nullptr;
}

Space* Scene::GetDefault() {
	for (Space * sp : scene_spaces) {
			if (sp->mbIsDefault) {
				return sp;
			}
	}
	return nullptr;
}

void Scene::SetDefault(std::string _spaceName) {

	if(GetDefault())
		GetDefault()->RemoveDefault();

	FindSpace(_spaceName.data())->SetDefault();
}

void Scene::SetDefault(Space& _space) {
	SetDefault(_space.space_name_);
}

void Scene::SetDefault(Space* _space) {
	SetDefault(_space->space_name_);
}

void Scene::ClearLists()
{
	//while (!arenaObj.first.empty())
	//	arenaObj.first.remove(arenaObj.first.back());
	//while (!arenaObj.second.empty())
	//	arenaObj.second.remove(arenaObj.second.back());
	//
	//arenaObj.first.clear();
	//arenaObj.second.clear();
}

void Scene::ToJson(json & _j) {
	_j["Spaces"];
	for (Space * sp : scene_spaces) {
		json j;
		sp->ToJson(j);
		_j["Spaces"].push_back(j);
	}

	_j["Properties"];
	RenderMgr->ToJson(_j["Properties"]["RenderManager"]);
}

static std::string temp;

std::vector<GameObject *> Scene::FromJson(json & _j) {
	std::vector<GameObject *> go;

	if (_j.find("Spaces") != _j.end())
	{
		for (auto it = _j["Spaces"].begin(); it != _j["Spaces"].end(); ++it) {
			Space * s = DBG_NEW Space();
			AddSpace(s);
			std::vector<GameObject*> goTmp = s->FromJson(*it);
			go.insert(go.end(), goTmp.begin(), goTmp.end());
		}
	}
	else {
		for (auto it = _j.begin(); it != _j.end(); ++it) {
			Space * s = DBG_NEW Space();
			AddSpace(s);
			std::vector<GameObject*> goTmp = s->FromJson(*it);
			go.insert(go.end(), goTmp.begin(), goTmp.end());
		}
	}

	if (scene_spaces.size() > 0 && !GetDefault()) scene_spaces[0]->mbIsDefault = true;

	if (_j.find("Properties") != _j.end())
	{
		auto j = _j["Properties"];

		if (j.find("RenderManager") != j.end())
			RenderMgr->FromJson(_j["Properties"]["RenderManager"]);
	}

	return go;
}

void Scene::FreeDestroyedObjects()
{
	std::for_each(scene_spaces.begin(), scene_spaces.end(),
		[&](Space* mSpace)
		{
			mSpace->DestroyDeadObjects();
		});
}