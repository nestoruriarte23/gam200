#pragma once
#include "Scene/Scene.h"

class Window;

struct Level
{
	virtual void Initialize() {}
	virtual void Load() {}
	virtual void Update() {}
	virtual void Render() {}
	virtual void Free() {}
	virtual void Unload() {}

	Scene* demo_scene;
};