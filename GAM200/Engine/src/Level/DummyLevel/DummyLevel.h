#pragma once
#include "..//Level.h"

struct AudioTest : public Level
{
	void Initialize();
	void Load();
	void Update();
	void Render();
	void Free();
	void Unload();

	unsigned r = 0;
	unsigned g = 0;
	unsigned b = 0;
};