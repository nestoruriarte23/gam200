#include "../../Engine/src/GameStateManager/GameStateManager.h"
#include "../../Engine/src/Input/Input.h"
#include "../../Utilities/Time/FrameTime.h"
#include "../../Engine/src/Sound/Audio.h"
#include "..//..//ResourceManager/ActualResourceManager.h"

#include "DummyLevel.h"

static Sound * sound;
static Sound * sound2;
static Voice * voice;
static Voice * voice2;
int i = 0;

void AudioTest::Load()
{
	gResourceMgr.LoadResource("..//Resources//Sound//route217.ogg");

	sound = (gResourceMgr.GetResource<Sound>("..//Resources//Sound//route217.ogg"))->Get();

	sound2 = gAudioMgr->CreateSound("..//Resources//Sound//bidoofcry.ogg");
}

void AudioTest::Initialize()
{
	/* Load single sound */
	voice = gAudioMgr->Loop(sound);

	/* Initialize the frame rate controler */
	TimeManager::Instance()->Initialize(false, 60.0);
}	

void AudioTest::Update()
{
	TimeManager::Instance()->StartFrame();

	/* Test sound */
	if (MouseTriggered(MouseInput::LEFT))
	{
		voice2 = gAudioMgr->Play(sound2);
	}

	if (KeyTriggered('P'))
		voice->SetPause(!voice->IsPaused());
	if (KeyTriggered('T'))
		gAudioMgr->Shutdown();

	gAudioMgr->Update();
	GSM->HandleEvents();
}

void AudioTest::Render()
{
	/* ON/OFF Lock FrameRate */
	if (KeyTriggered('K')) TimeManager::Instance()->InvertLockState();

	/* Change the FPS */
	//std::string wintitle = "Press E to Continue - K to lock Frame Rate - FPS: ";
	//wintitle += std::to_string(TimeManager::Instance()->GetFPS());
	//SDL_SetWindowTitle(game->GetSDLWindow(), wintitle.c_str());

	/* Frame time */
	TimeManager::Instance()->EndFrame();
}

void AudioTest::Free()
{
	gAudioMgr->StopAll();
	gAudioMgr->Shutdown();
}

void AudioTest::Unload()
{
	gResourceMgr.UnloadAllResources();
	gAudioMgr->FreeSound(sound);
	gAudioMgr->FreeSound(sound2);
}
