#pragma once
#include "..//Level.h"
#include "../../Components/Components.h"
#include "../../../src/System/GameObjectManager/GameObject.h"
enum ColType2 { cOBB2, cAABB2, cCIRCLE2 };

class collision_comp : public IComp {
	RTTI_DECLARATION_INHERITED(collision_comp, IComp);
public:
	int colType;
	Vector2 mPosition;
	void Init() {
		mPosition.x = 0;
		mPosition.y = 0;
		colType = cCIRCLE2;
	}

	void Update() {
		//xPos++;
		//yPos++;
	}

	void SetPos(int x, int y) {
		mPosition.x = 0;
		mPosition.y = 0;
		colType = cCIRCLE2;
	}
	collision_comp * Clone() { return new collision_comp(*this); }
	void Edit();
};

class collider : public IComp {
	RTTI_DECLARATION_INHERITED(collider, IComp);
public:
	int colType;
	Vector2 mPosition;
	void Init() {
		mPosition.x = 0;
		mPosition.y = 0;
		colType = cCIRCLE2;
	}

	void Update() {
		//xPos++;
		//yPos++;
	}

	void SetPos(int x, int y) {
		mPosition.x = 0;
		mPosition.y = 0;
		colType = cCIRCLE2;
	}
	collider * Clone() { return new collider(*this); }

	void Edit();

};


class poitions : public IComp {
	RTTI_DECLARATION_INHERITED(poitions, IComp);
public:
	int colType;
	Vector2 mPosition;
	void Init() {
		mPosition.x = 0;
		mPosition.y = 0;
		colType = cCIRCLE2;
	}

	void Update() {
		//xPos++;
		//yPos++;
	}

	void SetPos(int x, int y) {
		mPosition.x = 0;
		mPosition.y = 0;
		colType = cCIRCLE2;
	}
	poitions * Clone() { return new poitions(*this); }

	void Edit();
};

struct PhysicsTesting : public Level
{
	void Initialize();
	void Load();
	void Update();
	void Render();
	void Free();
	void Unload();
	void MyEditor(std::vector<GameObject *> & _goVect);
	void ConcreteGO(GameObject * _go);
	void SearchComponents(std::string search);
	//std::map<std::string, IComp*> SearchComponents(std::string search);
	void AddComponent(const char *, IComp *);

private:
	std::vector<const char *> compNames;
	std::vector<IComp*> compConstructor;
};