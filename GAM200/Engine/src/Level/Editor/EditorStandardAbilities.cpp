#include <filesystem> 
#include <algorithm>
#include "Archetype.h"
#include "../../Utilities/MyDebug/LeakDetection.h"
#include "../../UndoRedo/UndoRedo.h"
#include "../../../Engine.h"
#include "ResourceImporter.h"
#include "ImGUI_Basics.h"
#include "ImGui/imgui.h"
#include "Editor.h"






void Editor::KeyboardShortcuts() {


	if (KeyDown(Key::Control) && KeyTriggered(Key::Z)) {
		Undo();
	}

	if (KeyDown(Key::Control) && KeyTriggered(Key::Y)) {
		Redo();
	}

	if (KeyDown(Key::Control) && KeyTriggered(Key::C)) {
		CopyObj();
	}

	if (KeyDown(Key::Control) && KeyTriggered(Key::V)) {
		PasteObj();
	}

	if (KeyDown(Key::Control) && KeyTriggered(Key::S)) {
		serializer->SaveLevel(Editor::LevelName.c_str(), Editor::demo_scene);
	}

	ImGuiIO& io = ImGui::GetIO();
	if (!io.WantCaptureKeyboard)
	{
		if (KeyTriggered(Key::Supr)) {
			if (!is_in_multi_edit) {
				if (selected_object)
					DeleteObject(selected_object, selected_object->GetSpace());
			}
			else {
				DeleteMultipleObjects();
			}
		}
	}

	if (KeyTriggered(Key::F1)) {
		if (show_all_colliders)
			show_all_colliders = false;
		else
			show_all_colliders = true;
	}

	if (KeyTriggered(Key::F2)) {
		show_all_regions = !show_all_regions;
	}

}






//////////////////////////Undo/Redo//////////////////////////
void Editor::Undo() {
	if (undo->GetUndoStackSize() != NULL) {
		int action = undo->GetAction();
		if (action != (int)Action::MULTIPLEEDIT && action != (int)Action::MULTIPLEDELETE && action != (int)Action::MULTIPLEADD) {
			int UndoID;
			int action;
			GameObject * go = undo->UndoChange(UndoID, action);
			if (UndoID != NULL && action == (int)Action::NONE) {
				GameObject * undoGo = demo_scene->FindObjectInAnySpace(UndoID);
				if (undoGo) {
					redo->SaveOldObject(undoGo, undoGo->GetUID(), action);
					(*undoGo) = *go;
					delete go;
					delete game_object_for_undo;
					game_object_for_undo = nullptr;
				}
			} else if (action == (int)Action::DELETEOBJECT) {
				go->Initialize();
				Editor::demo_scene->FindSpace(go->mSceneSpace.data())->AddObject(*go);
				Editor::goVect.push_back(go);
				redo->SaveOldObject(go, go->GetUID(), (int)Action::ADDOBJECT);
			} else if (UndoID != NULL && action == (int)Action::ADDOBJECT) {
				GameObject* delGo;
				std::for_each(Editor::goVect.begin(), Editor::goVect.end(), [&delGo, UndoID](GameObject * _go) { if (_go->GetUID() == UndoID) { delGo = _go; } });
				Editor::DeleteObject(delGo, Editor::demo_scene->FindSpace(go->mSceneSpace.data()), true);
				redo->SaveOldObject(go, go->GetUID(), (int)Action::DELETEOBJECT);
				delete go;
			}
		} else if(action == (int)Action::MULTIPLEEDIT) {
			std::vector<std::pair<int, Vector2>> newPositions;
			std::vector<std::pair<int, Vector2>> oldPositions;
			undo->UndoMultiStackChange(newPositions);
			for (std::pair<int, Vector2> pos : newPositions) {
				GameObject * go = demo_scene->FindObjectInAnySpace(pos.first);
				oldPositions.push_back(std::pair<int, Vector2>(go->GetUID(), go->mPosition));
				go->mPosition = pos.second;
			}
			redo->RedoSaveMultiStack(oldPositions);
		} else if (action == (int)Action::MULTIPLEDELETE) {
			std::vector<GameObject*> objs;
			undo->UndoMultiStackChangeObj(objs);
			for (GameObject * go : objs) {
				demo_scene->FindSpace(go->mSceneSpace.data())->AddObject(*go);
				Editor::goVect.push_back(go);
			}
			redo->RedoSaveMultiStackObj(objs, (int)Action::MULTIPLEADD);
		}
	}
}

void Editor::Redo() {
	if (redo->GetRedoStackSize() != 0) {
		int action = redo->GetAction();
		if (action != (int)Action::MULTIPLEEDIT && action != (int)Action::MULTIPLEDELETE && action != (int)Action::MULTIPLEADD) {
			int RedoID;
			int action;
			GameObject * go = redo->RedoChange(RedoID, action);
			if (RedoID != NULL && action == (int)Action::NONE) {
				GameObject * redoGo = demo_scene->FindObjectInAnySpace(RedoID);
				if (redoGo) {
					(*redoGo) = *go;
					delete go;
					delete game_object_for_undo;
					game_object_for_undo = nullptr;
				}
			} else if (action == (int)Action::DELETEOBJECT) {
				go->Initialize();
				Editor::demo_scene->FindSpace(go->mSceneSpace.data())->AddObject(*go);
				Editor::goVect.push_back(go);
			} else if (RedoID != NULL && action == (int)Action::ADDOBJECT) {
				GameObject* delGo;
				std::for_each(Editor::goVect.begin(), Editor::goVect.end(), [&delGo, RedoID](GameObject * _go) { if (_go->GetUID() == RedoID) { delGo = _go; } });
				Editor::DeleteObject(delGo, Editor::demo_scene->FindSpace(go->mSceneSpace.data()), true);
				delete go;
			}
		} else if (action == (int)Action::MULTIPLEEDIT) {

			std::vector<std::pair<int, Vector2>> oldPositions;

			redo->RedoMultiStackChange(oldPositions);

			for (std::pair<int, Vector2> pos : oldPositions) {

				demo_scene->FindObjectInAnySpace(pos.first)->mPosition = pos.second;
			}
		}/* else if(action == (int)Action::MULTIPLEADD) {
			std::vector<GameObject*> objs;
			redo->RedoMultiStackChangeObj(objs);
			for (GameObject* go : objs) {
				Editor::DeleteObject(go, demo_scene->FindSpace(go->mSceneSpace.data()), false);
			}
		}*/
	}
}

void Editor::SaveToUndoStack(int _action) {
	if (!is_in_multi_edit) {
		if (!selected_object) return;
		undo->SaveOldObject(game_object_for_undo, selected_object->GetUID(), _action);
		delete game_object_for_undo;
		game_object_for_undo = nullptr;
	}else {
		undo->UndoSaveMultiStack(multipleOldPositions);
	}
}

//////////////////////////AutoSave//////////////////////////
void Editor::AutoSave()
{
	SaveTime += (f32)TimeManager::Instance()->GetDt();
	if (SaveTime > TimeToSave)
	{
		SaveTime = 0.0f;
		serializer->SaveLevel(Editor::LevelName.c_str(), Editor::demo_scene);
	}
}

//////////////////////////Copy/Paste//////////////////////////
void Editor::CopyObj()
{
	if (selected_object)
		ObjectCopied = selected_object;
	else
		ObjectCopied = nullptr;

	if (is_in_multi_edit && multipleSelectionGameObjectVector.size() != 0) {

		multipleSelectionGameObjectVectorCopy = multipleSelectionGameObjectVector;
	}
}

void Editor::PasteObj()
{
	if (ObjectCopied) {
		GameObject* newGo = ObjectCopied->Clone();
		newGo->mPosition = Vector3(InputManager->GetMouse().x, InputManager->GetMouse().y, 0);
		Editor::demo_scene->FindSpace(newGo->mSceneSpace.c_str())->AddObject(*newGo);
		goVect.push_back(newGo);
		if (newGo->mbArchetype) {
			Archetype::AddToArchetype(newGo, newGo->mArchetypeName);
		}
	} else if (multipleSelectionGameObjectVectorCopy.size()) {
		Vector2 center(InputManager->GetMouse().x, InputManager->GetMouse().y);

		float minx, miny, maxx, maxy;

		minx = Editor::multipleSelectionGameObjectVectorCopy.front()->mPosition.x;

		miny = Editor::multipleSelectionGameObjectVectorCopy.front()->mPosition.y;

		maxx = Editor::multipleSelectionGameObjectVectorCopy.front()->mPosition.x;

		maxy = Editor::multipleSelectionGameObjectVectorCopy.front()->mPosition.y;



		for (GameObject* obj : Editor::multipleSelectionGameObjectVectorCopy) {

			minx = min(minx, obj->mPosition.x);

			miny = min(miny, obj->mPosition.y);

			maxx = max(maxx, obj->mPosition.x);

			maxy = max(maxy, obj->mPosition.y);

		}



		Vector2 first(maxx, maxy);

		Vector2 last(minx, miny);

		float width = first.x - last.x;

		float height = first.y - last.y;



		Vector2 pCenter{ first.x - (width / 2), first.y - (height / 2) };

		for (GameObject* obj : Editor::multipleSelectionGameObjectVectorCopy) {
			Vector2 distFrmOldCnt(obj->mPosition.x - pCenter.x, obj->mPosition.y - pCenter.y);
			
			GameObject * newGo = obj->Clone();

			newGo->mPosition = Vector2(center.x + distFrmOldCnt.x, center.y + distFrmOldCnt.y);

			Editor::demo_scene->FindSpace(newGo->mSceneSpace.c_str())->AddObject(*newGo);
			goVect.push_back(newGo);
			if (newGo->mbArchetype) {
				Archetype::AddToArchetype(newGo, newGo->mArchetypeName);
			}
		}
	}
}