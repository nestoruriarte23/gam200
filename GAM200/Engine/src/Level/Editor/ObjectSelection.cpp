#include "..//..//Engine/Engine.h"
#include "Editor.h"
#include "ObjectSelection.h"

GameObject*					Editor::selected_object = nullptr;

namespace ObjSelection
{
	/******      Object Selection: Variables     ******/

	enum ScaleMode
	{
		None,
		Horizontal,
		Vertical,
		Diagonal
	};

	static bool				previous_visual_rotator = false;
	static bool				previous_visual_scalator = false;
	static bool				previous_dragging = false;
	static bool				visual_rotator = false;
	static bool				visual_scalator = false;

	static ScaleMode		scale_mode = ScaleMode::None;
	static ScaleMode		previous_scale_mode = ScaleMode::None;

	static bool				dragging = false;
	static Vector2			scalator_origin;
	static Vector2			original_scale;
	static Vector2			selection_point;
	static Vector2			original_position;

	/**************************************************/

	GameObject* FindFirstObject(const Vector2& _pos,
		std::vector<GameObject *> & _goVect)
	{
		if (Editor::selected_object && StaticPointToOrientedRect(&_pos, &Editor::selected_object->mPosition, Editor::selected_object->mScale.x,
			Editor::selected_object->mScale.y, Editor::selected_object->mRotation))
			return  Editor::selected_object;

		for (auto space : Game->GetCurrScene()->scene_spaces)
		{
			std::sort(space->space_objects.begin(), space->space_objects.end(),
				[](GameObject* obj1, GameObject* obj2)
			{return obj1->mPosition.y < obj2->mPosition.y; });

			if (space->mbEditable)
				for (auto go : space->space_objects)
				{
					Vector2 pos(go->mPosition.x, go->mPosition.y);
					if (Editor::show_all_colliders)
					{
						BoxCollider* mBox = go->get_component_type<BoxCollider>();
						if (!mBox) continue;

						Vector2 collider_pos(go->mPosition + mBox->mOffset);

						if (StaticPointToOrientedRect(&_pos, &collider_pos, mBox->mCollSize.x,
							mBox->mCollSize.y, go->mRotation))
						{
							Editor::selected_object = go;
							Editor::slected = true;
							return go;
						}
					}
					else if (StaticPointToOrientedRect(&_pos, &pos, go->mScale.x,
						go->mScale.y, go->mRotation))
					{
						Editor::selected_object = go;
						Editor::slected = true;
						return go;
					}
				}
		}

		return NULL;
	}

	void DrawSelection(GameObject* _obj)
	{
		/* Safety check if an object is selected */
		if (!_obj) return;

		float ratio = (Camera::Instance()->mViewScale.x / Game->GetWindowSize().x);

		/* Some components do things when objects are selected on the editor */
		auto comps = _obj->GetComps();
		for (auto it = comps.begin(); it != comps.end(); ++it)
			(*it)->Selected();

		Vector2 pos(_obj->mPosition.x, _obj->mPosition.y);

		/* Draw basic frame */
		RenderManager::Instance()->DrawOrientedRectangleAt(
			pos, _obj->mScale.x,
			_obj->mScale.y, _obj->mRotation, Engine::Color::cyan);

		Matrix33 rotate = Matrix33::RotRad(_obj->mRotation);

		/* Useful points */
		Vector2 rotate_visual = rotate * Vector2(0, _obj->mScale.y * 0.5f + 20 * ratio);
		Vector2 obj_top = rotate * Vector2(0, _obj->mScale.y * 0.5f);
		Vector2 obj_right = rotate * Vector2(_obj->mScale.x * 0.5f, 0);
		Vector2 obj_corner = rotate * Vector2(Editor::selected_object->mScale.x * 0.5f, Editor::selected_object->mScale.y * 0.5f);

		/* Draw Rotator */
		RenderManager::Instance()->DrawCircleAt(rotate_visual + pos,
			35, 10 * ratio, Engine::Color::white);

		/* Draw Scalators */
		RenderManager::Instance()->DrawOrientedRectangleAt(obj_top + pos,
			16 * ratio, 4 * ratio, _obj->mRotation, Engine::Color::white);
		RenderManager::Instance()->DrawOrientedRectangleAt(obj_right + pos,
			4 * ratio, 16 * ratio, _obj->mRotation, Engine::Color::white);
		/*RenderManager::Instance()->DrawOrientedRectangleAt(pos,
			20 * ratio, 20 * ratio, _obj->mRotation, Engine::Color::red);*/

		RenderManager::Instance()->DrawOrientedRectangleAt(obj_corner + pos,
			12 * ratio, 12 * ratio, _obj->mRotation, Engine::Color::white);
		/*RenderManager::Instance()->DrawOrientedRectangleAt(obj_corner + pos,
			4 * ratio, 16 * ratio, _obj->mRotation, Engine::Color::white);*/
	}

	void SelectObject()
	{
		ImGuiIO& io = ImGui::GetIO();
		if (!io.WantCaptureMouse)
		{
			if (MouseTriggered(MouseInput::LEFT))
			{
				if (Editor::selected_object)
				{
					float ratio = (Camera::Instance()->mViewScale.x / Game->GetWindowSize().x);

					Vector2 pos(Editor::selected_object->mPosition.x, Editor::selected_object->mPosition.y);

					Matrix33 rotate = Matrix33::RotRad(Editor::selected_object->mRotation);
					Vector2 rotate_visual = rotate * Vector2(0, Editor::selected_object->mScale.y * 0.5f + 20 * ratio);
					Vector2 obj_top = rotate * Vector2(0, Editor::selected_object->mScale.y * 0.5f);
					Vector2 obj_right = rotate * Vector2(Editor::selected_object->mScale.x * 0.5f, 0);
					Vector2 obj_corner = rotate * Vector2(Editor::selected_object->mScale.x * 0.5f, Editor::selected_object->mScale.y * 0.5f);

					if (!visual_rotator)
					{
						if (StaticPointToStaticCircle(&InputManager->GetMouse(), &(pos + rotate_visual), 15 * ratio))
							visual_rotator = true;
					}
					if (!visual_scalator)
					{
						if (StaticPointToOrientedRect(&InputManager->GetMouse(), &(pos + obj_top), 16 * ratio, 4 * ratio, Editor::selected_object->mRotation))
						{
							visual_scalator = true;
							scale_mode = ScaleMode::Vertical;
							scalator_origin = InputManager->GetMouse();
							original_scale = Editor::selected_object->mScale;
						}
						else if (StaticPointToOrientedRect(&InputManager->GetMouse(), &(pos + obj_right), 4 * ratio, 16 * ratio, Editor::selected_object->mRotation))
						{
							visual_scalator = true;
							scale_mode = ScaleMode::Horizontal;
							scalator_origin = InputManager->GetMouse();
							original_scale = Editor::selected_object->mScale;
						}
						else if (StaticPointToOrientedRect(&InputManager->GetMouse(), &(pos + obj_corner), 12 * ratio, 12 * ratio, Editor::selected_object->mRotation))
						{
							visual_scalator = true;
							scale_mode = ScaleMode::Diagonal;
							scalator_origin = InputManager->GetMouse();
							original_scale = Editor::selected_object->mScale;
						}
					}
				}
				if (!visual_rotator && !visual_scalator)
				{
					GameObject * OldSelection = Editor::selected_object;
					Editor::selected_object = FindFirstObject(InputManager->GetMouse(), Editor::goVect);

					if (OldSelection && Editor::selected_object) {
						if (OldSelection->GetUID() != Editor::selected_object->GetUID()) {
							delete Editor::game_object_for_undo;
							Editor::game_object_for_undo = nullptr;
						}
					}

					if (Editor::selected_object)
					{
						selection_point = InputManager->GetMouse();
						original_position = Editor::selected_object->mPosition;
					}
					else if (!EditableCollider::editing_collider && !Editor::not_moveable_object)
					{
						Editor::slected = false;
						//EditableCollider::editing_collider = false;
					}

				}
			}
			if (Editor::selected_object && MouseDown(MouseInput::LEFT) && !EditableCollider::editing_collider && !Editor::not_moveable_object)
			{
				dragging = true;
				if (!visual_rotator && !visual_scalator)
					Editor::selected_object->mPosition = original_position + Vector2(InputManager->GetMouse().x - selection_point.x,
						InputManager->GetMouse().y - selection_point.y);

				/* Some components do things when objects are selected on the editor */
				auto comps = Editor::selected_object->GetComps();
				for (auto it = comps.begin(); it != comps.end(); ++it)
					(*it)->Dragging();
			}

			if (previous_visual_rotator == false && visual_rotator == true ||
				previous_visual_scalator == false && visual_scalator == true ||
				previous_dragging == true && dragging == false ||
				previous_scale_mode != ScaleMode::None && scale_mode == ScaleMode::None) {
				if (Editor::slected) {
					Editor::SaveToUndoStack(0);
				}
			}

			previous_visual_rotator = visual_rotator;
			previous_visual_scalator = visual_scalator;
			previous_scale_mode = scale_mode;
			previous_dragging = dragging;
		}
	}

	void EditSelectedObject()
	{
		if (visual_rotator)
		{
			Vector2 pos(Editor::selected_object->mPosition.x, Editor::selected_object->mPosition.y);

			if (MouseDown(MouseInput::LEFT))
				Editor::selected_object->mRotation = -(InputManager->GetMouse() - pos).GetAngle();

			else
				visual_rotator = false;
		}
		if (visual_scalator)
		{
			if (MouseDown(MouseInput::LEFT))
			{
				if (scale_mode == ScaleMode::Horizontal)
				{
					Editor::selected_object->mScale.x = original_scale.x + 2 * (InputManager->GetMouse().x - scalator_origin.x);
				}
				else if (scale_mode == ScaleMode::Diagonal)
				{
					auto diff = (InputManager->GetMouse() - scalator_origin);

					float imp_diff = max(diff.x, diff.y);
					if (diff.x < 0 && diff.y < 0) imp_diff = min(diff.x, diff.y);

					float rate = max((original_scale.x + 2 * imp_diff) / original_scale.x, (original_scale.y + 2 * imp_diff) / original_scale.y);

					Editor::selected_object->mScale = original_scale * rate;

					if (Editor::selected_object->mScale.x <= 0) Editor::selected_object->mScale.x = 10;
					if (Editor::selected_object->mScale.y <= 0) Editor::selected_object->mScale.y = 10;
				}
				else if (scale_mode == ScaleMode::Vertical)
				{
					Editor::selected_object->mScale.y = original_scale.y + 2 * (InputManager->GetMouse().y - scalator_origin.y);
				}
			}
			else
			{
				visual_scalator = false;
				scale_mode = ScaleMode::None;
			}
		}

		if (dragging && MouseUp(MouseInput::LEFT))
			dragging = false;
	}

	void DisplayAllColliders()
	{
		std::for_each(Editor::goVect.begin(), Editor::goVect.end(), 
		[](GameObject* object)
		{
			BoxCollider* mBox = object->get_component_type<BoxCollider>();
			if (!mBox)	return;

			RenderManager::Instance()->DrawOrientedRectangleAt(object->mPosition + mBox->mOffset, mBox->mCollSize.x, mBox->mCollSize.y, object->mRotation, Engine::Color::red);
		});
	}
} // namespace ObjSelection
