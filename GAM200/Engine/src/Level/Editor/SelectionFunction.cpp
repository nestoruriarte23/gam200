#include "../../../Engine.h"
#include "../../Components/RTTI/RTTI.h"
#include "../../Serializer/SerializeFactory.h"
#include "ImGUI_Basics.h"
#include "ImGui/imgui.h"
#include "../../Input/Input.h"
#include "..//..//GameStateManager/GameStateManager.h"
#include "SelectionFunction.h"
/******      Object Selection: Variables     ******/

#include "../../Physics/Collisions.h"

static bool				slected = false;
static GameObject*		selected_object = nullptr;
static bool				visual_rotator = false;
static bool				visual_scalator = false;
static bool				horizontal_scale = false;
static Vector2			scalator_origin;
static Vector2			original_scale;
static Vector2			selection_point;
static Vector3			original_position;
static std::vector<GameObject *> goVect;

/**************************************************/
static GameObject* Selection::FindFirstObject(const Vector2& _pos,
	const std::vector<GameObject *> & _goVect)
{
	for (auto it = _goVect.begin(); it != _goVect.end(); ++it)
	{
		Vector2 pos((*it)->mPosition.x, (*it)->mPosition.y);
		if (StaticPointToOrientedRect(&_pos, &pos, (*it)->mScale.x,
			(*it)->mScale.y, (*it)->mRotation))
		{
			selected_object = *it;
			slected = true;
			return *it;
		}
	}

	return NULL;
}

static void Selection::DrawSelection(GameObject* _obj)
{
	/* Safety check if an object is selected */
	if (!_obj) return;

	/* Some components do things when objects are selected on the editor */
	auto comps = _obj->GetComps();
	for (auto it = comps.begin(); it != comps.end(); ++it)
		(*it)->Selected();

	Vector2 pos(_obj->mPosition.x, _obj->mPosition.y);

	/* Draw basic frame */
	RenderManager::Instance()->DrawOrientedRectangleAt(
		pos, _obj->mScale.x,
		_obj->mScale.y, _obj->mRotation, Color::blue);

	Matrix33 rotate = Matrix33::RotRad(_obj->mRotation);

	/* Useful points */
	Vector2 rotate_visual = rotate * Vector2(0, _obj->mScale.y * 0.5f + 20);
	Vector2 obj_top = rotate * Vector2(0, _obj->mScale.y * 0.5f);
	Vector2 obj_right = rotate * Vector2(_obj->mScale.x * 0.5f, 0);

	/* Draw Rotator */
	RenderManager::Instance()->DrawCircleAt(rotate_visual + pos,
		20, 10, Color::red);

	/* Draw Scalators */
	RenderManager::Instance()->DrawOrientedRectangleAt(obj_top + pos,
		16, 4, _obj->mRotation, Color::red);
	RenderManager::Instance()->DrawOrientedRectangleAt(obj_right + pos,
		4, 16, _obj->mRotation, Color::red);
}

static void Selection::SelectObject()
{
	ImGuiIO& io = ImGui::GetIO();
	if (!io.WantCaptureMouse)
	{
		if (MouseTriggered(MouseInput::LEFT))
		{
			if (selected_object)
			{
				Vector2 pos(selected_object->mPosition.x, selected_object->mPosition.y);

				Matrix33 rotate = Matrix33::RotRad(selected_object->mRotation);
				Vector2 rotate_visual = rotate * Vector2(0, selected_object->mScale.y * 0.5f + 20);
				Vector2 obj_top = rotate * Vector2(0, selected_object->mScale.y * 0.5f);
				Vector2 obj_right = rotate * Vector2(selected_object->mScale.x * 0.5f, 0);

				if (!visual_rotator)
				{
					if (StaticPointToStaticCircle(&InputManager->GetMouse(), &(pos + rotate_visual), 15))
						visual_rotator = true;
				}
				if (!visual_scalator)
				{
					if (StaticPointToOrientedRect(&InputManager->GetMouse(), &(pos + obj_top), 20, 5, selected_object->mRotation))
					{
						visual_scalator = true;
						horizontal_scale = false;
						scalator_origin = InputManager->GetMouse();
						original_scale = selected_object->mScale;
					}
					else if (StaticPointToOrientedRect(&InputManager->GetMouse(), &(pos + obj_right), 5, 20, selected_object->mRotation))
					{
						visual_scalator = true;
						horizontal_scale = true;
						scalator_origin = InputManager->GetMouse();
						original_scale = selected_object->mScale;
					}
				}
			}
			if (!visual_rotator && !visual_scalator)
			{
				selected_object = FindFirstObject(InputManager->GetMouse(), goVect);
				if (selected_object)
				{
					selection_point = InputManager->GetMouse();
					original_position = selected_object->mPosition;
				}
				else
					slected = false;
			}
		}
		if (selected_object && MouseDown(MouseInput::LEFT))
		{
			if (!visual_rotator && !visual_scalator)
				selected_object->mPosition = original_position + Vector3(InputManager->GetMouse().x - selection_point.x,
					InputManager->GetMouse().y - selection_point.y, 0);

			/* Some components do things when objects are selected on the editor */
			auto comps = selected_object->GetComps();
			for (auto it = comps.begin(); it != comps.end(); ++it)
				(*it)->Dragging();
		}
	}
}

static void Selection::EditSelectedObject()
{
	if (visual_rotator)
	{
		Vector2 pos(selected_object->mPosition.x, selected_object->mPosition.y);

		if (MouseDown(MouseInput::LEFT))
			selected_object->mRotation = -(InputManager->GetMouse() - pos).GetAngle();

		else
			visual_rotator = false;
	}
	if (visual_scalator)
	{
		if (MouseDown(MouseInput::LEFT))
		{
			if (horizontal_scale)
				selected_object->mScale.x = original_scale.x + 2 * (InputManager->GetMouse().x - scalator_origin.x);
			else
				selected_object->mScale.y = original_scale.y + 2 * (InputManager->GetMouse().y - scalator_origin.y);
		}
		else
			visual_scalator = false;
	}
}

