#include "../../../Engine.h"
#include "ObjectSelection.h"
#include "Grid.h"

namespace Grid
{
	bool is_grid_on = false;

	FloorGrid::FloorGrid()
	{
		mTexture = nullptr;

		mRows = mColumns = 5;
		mTileSize = Vector2(200, 200);
	}
	FloorGrid::FloorGrid(const FloorGrid & _rhs)
	{
		mTexture = nullptr;

		mRows = _rhs.mRows;
		mColumns = _rhs.mColumns;

		mTileSize = _rhs.mTileSize;
	}
	FloorGrid::~FloorGrid()
	{
		Shutdown();
	}
	void FloorGrid::Initialize()
	{
	}
	void FloorGrid::Shutdown()
	{
		//for (auto i : mTiles)
		//{
		//	for (auto j : i.second)
		//	{
		//		if (j.second != nullptr)
		//		{
		//			Game->DestroyObject(mTiles[i.first][j.first]);
		//		}
		//	}
		//}
		//Game->GetCurrScene()->FreeDestroyedObjects();
		mTiles.clear();
	}

	static std::array<char, 30> texture_filter{ 0 };

	bool FloorGrid::Edit()
	{
		bool changed = false;

		UpdatePosition();
		UpdateScale();

		ImGui::PushID("FloorGrid");

		if (ImGui::TreeNode("FloorGrid"))
		{
			if (ImGui::Button("EDIT"))
			{
				is_grid_on = !is_grid_on;
			}


			static unsigned selected_item = -2;
			auto& all_textures = gActualResourceMgr.GetAllFilenamesOfType(FILETYPE::TextureType);

			if (selected_item == -2 && mTexture)
			{
				auto position = std::find(all_textures.begin(), all_textures.end(), mTexture->GetFilename());
				selected_item = std::distance(all_textures.begin(), position);
			}


			ImGui::PushID(332);
			ImGui::InputText(" Filter", texture_filter.data(), 30);
			ImGui::PopID();

			if (ImGui::BeginCombo(" Texture", mTexture ? all_textures[selected_item].data() : " - Default - "))
			{
				bool is_selected = (mTexture == nullptr);

				if (ImGui::Selectable(" - Default - ", is_selected))
				{
					selected_item = -1;
					mTexture = nullptr;
					changed = true;
				}

				if (is_selected)
				{
					selected_item = -1;
					ImGui::SetItemDefaultFocus();
				}

				for (unsigned n = 0; n < all_textures.size(); n++)
				{
					if (all_textures[n].find(texture_filter.data()) == std::string::npos) continue;

					bool is_selected = (selected_item == n);
					if (ImGui::Selectable(all_textures[n].data(), is_selected))
					{
						selected_item = n;
						mTexture = gActualResourceMgr.GetResource<Texture>(all_textures[n]);
						changed = true;
					}
					if (ImGui::IsItemHovered())
					{
						ImGui::BeginTooltip();
						ImGui::Image((void*)(intptr_t)(gActualResourceMgr.GetResource<Texture>
							(all_textures[n])->Get()->mHandle), ImVec2(80, 80), ImVec2(0, 0),
							ImVec2(1, -1), ImVec4(1.0f, 1.0f, 1.0f, 1.0f), ImVec4(1.0f, 1.0f, 1.0f, 0.5f));
						ImGui::EndTooltip();
					}

					if (is_selected)
					{
						selected_item = n;
						ImGui::SetItemDefaultFocus();
					}
				}
				ImGui::EndCombo();
			}

			ImGui::TreePop();
		}
		else
		{
			ImGui::SameLine(ImGui::GetWindowWidth() - 35);
			if (ImGui::Button("", ImVec2(14, 14))) {
				mOwner->RemoveComp(this);
				ImGui::PopID();
				is_grid_on = false;
				return true;
			}
		}
		ImGui::PopID();

		if (is_grid_on)
		{
			changed = EditGrid() | changed;
		}

		return changed;
	}
	bool FloorGrid::equal_to(IComp const & other) const
	{
		if (FloorGrid const* p = dynamic_cast<FloorGrid const*>(&other)) {
			return  mTileSize == p->mTileSize && mRows == p->mRows;
		}
		else {
			return false;
		}
	}
	bool FloorGrid::are_the_same_type(IComp const & lhs)
	{
		if (dynamic_cast<const FloorGrid*>(&lhs)) {
			return true;
		}
		return false;
	}
	FloorGrid * FloorGrid::Clone()
	{
		FloorGrid* _new = DBG_NEW FloorGrid(*this);

		_new->mTiles.clear();

		for (auto i : mTiles)
		{
			for (auto j : i.second)
			{
				if (j.second != nullptr)
				{
					_new->mTiles[i.first][j.first] = j.second->CloneNoInitialize();
				}
			}
		}

		return _new;
	}
	void FloorGrid::FromJson(nlohmann::json & _j)
	{
		auto dsadsa = RenderMgr->GetEditorRenderables();
		if (_j.find("mCenter") != _j.end())
		{
			mCenter.FromJson(_j["mCenter"]);

			mOwner->mPosition = mCenter;
		}

		if (_j.find("mTexture") != _j.end())
		{
			mTexture = gActualResourceMgr.GetResource<Texture>(_j["mTexture"]);
		}
		else
			mTexture = nullptr;

		if (_j.find("mRows") != _j.end())
		{
			mRows = _j["mRows"];
			mColumns = _j["mColumns"];

			mTileSize.FromJson(_j["mTileSize"]);

			mOwner->mScale = Vector2(mTileSize.x * mColumns, mTileSize.y * mRows);

			UpdateScale();
			UpdatePosition();
		}

		if (_j.find("mTileSize") != _j.end())
		{
			for (auto k = _j["Tiles"].begin(); k != _j["Tiles"].end(); k++) 
			{
				int x = (*k)["x"];
				int y = (*k)["y"];

				auto new_tile = mTiles[x][y] = DBG_NEW GameObject;
				new_tile->mSceneSpace = mOwner->mSceneSpace;
				new_tile->mPosition = mOwner->mPosition - Vector2(mTileSize.x * x,
					mTileSize.y * y);
				new_tile->mScale = mTileSize;

				auto sprite = DBG_NEW Sprite;
				new_tile->AddCompNoInitialize(sprite);
				sprite->SetVisibility(true);
				sprite->ChangeTexture((*k)["Texture"]);

				new_tile->mbSerializable = false;
				new_tile->GetSpace()->AddObject(*new_tile);
			}
		}
		dsadsa = RenderMgr->GetEditorRenderables();
	}
	void FloorGrid::ToJson(nlohmann::json & _j)
	{
		_j["_type"] = "Grid::FloorGrid";

		mTileSize.ToJson(_j["mTileSize"]);

		_j["mRows"] = mRows;
		_j["mColumns"] = mColumns;

		if (mTexture)
			_j["mTexture"] = mTexture->GetFilename();

		mCenter.ToJson(_j["mCenter"]);

		for (auto i : mTiles)
		{
			for (auto j : i.second)
			{
				if (j.second != nullptr)
				{
					nlohmann::json k;

					k["x"] = i.first;
					k["y"] = j.first;

					k["Texture"] = j.second->get_component_type<Sprite>()->GetTextureName();

					_j["Tiles"].push_back(k);
				}
			}
		}
	}
	void FloorGrid::operator=(IComp & _comp)
	{
		FloorGrid & grid = *dynamic_cast<FloorGrid*>(&_comp);

		mTileSize = grid.mTileSize;
		mRows =		grid.mRows;
		mColumns =	grid.mColumns;

		mCenter = grid.mCenter;

		Shutdown();

		for (auto i : grid.mTiles)
		{
			for (auto j : i.second)
			{
				auto go = mTiles[i.first][j.first] = j.second->Clone();

				go->mScale = mTileSize;
				go->mPosition = mOwner->mPosition - Vector2(mTileSize.x * i.first,
						mTileSize.y * j.first);
			}
		}
	}
	void FloorGrid::DrawGrid()
	{
		RenderMgr->DrawRectangleAt(mOwner->mPosition, mOwner->mScale, Engine::Color::red);

		for (unsigned i = 1; i < mRows; ++i)
			RenderMgr->DrawLineAt(
				Vector2(mOwner->mPosition.x - mOwner->mScale.x / 2,
					mOwner->mPosition.y + mOwner->mScale.y / 2 - mTileSize.y * i),
				Vector2(mOwner->mPosition.x + mOwner->mScale.x / 2,
					mOwner->mPosition.y + mOwner->mScale.y / 2 - mTileSize.y * i),
				Engine::Color::blue);

		for (unsigned i = 1; i < mColumns; ++i)
			RenderMgr->DrawLineAt(
				Vector2(mOwner->mPosition.x - mOwner->mScale.x / 2 + mTileSize.x * i,
					mOwner->mPosition.y + mOwner->mScale.y / 2),
				Vector2(mOwner->mPosition.x - mOwner->mScale.x / 2 + mTileSize.x * i,
					mOwner->mPosition.y - mOwner->mScale.y / 2),
				Engine::Color::blue);
	}
	bool FloorGrid::EditGrid()
	{
		DrawGrid();

		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureMouse) return false;


		if (MouseTriggered(MouseInput::LEFT) || MouseTriggered(MouseInput::RIGHT))
		{
			Vector2 mouse = InputManager->GetMouse();

			bool mouse_left = MouseTriggered(MouseInput::LEFT);

			if (StaticPointToOrientedRect(&mouse, &mOwner->mPosition, mOwner->mScale.x,
				mOwner->mScale.y, mOwner->mRotation))
			{
				bool changed = false;

				auto difference = mOwner->mPosition - mouse;
				Vector2 normalized{ difference.x / mOwner->mScale.x + 0.5f,
					difference.y / mOwner->mScale.y + 0.5f };

				auto x_index = static_cast<int>(normalized.x * mColumns) - (int)(mColumns / 2);
				auto y_index = static_cast<int>(normalized.y * mRows) - (int)(mRows / 2);

				if (mColumns == 2 * abs(x_index) + 1)
				{
					Grow(false);
					changed = true;
				}
				if (mRows == 2 * abs(y_index) + 1)
				{
					Grow(true);
					changed = true;
				}

				changed = HandleInput(x_index, y_index, mouse_left) | changed;

				return changed;
			}
			else
			{
				is_grid_on = false;
				ObjSelection::SelectObject();
				return false;
			}
		}

		return false;
	}
	bool FloorGrid::HandleInput(int _x, int _y, bool action)
	{

		if (action) // Input == left_click
		{
			/* If there is already a value */
			if (mTiles.find(_x) != mTiles.end() && mTiles[_x].find(_y) != mTiles[_x].end()
				&& mTiles[_x][_y] != nullptr)
			{
				if (mTexture && mTiles[_x][_y]->get_component_type<Sprite>()->GetTextureName()
					== mTexture->GetFilename())
				{
					return false;
				}
				else
				{
					delete mTiles[_x][_y];
					mTiles[_x][_y] = nullptr;
				}
			}

			auto new_tile = mTiles[_x][_y] = DBG_NEW GameObject;
			new_tile->mSceneSpace = mOwner->mSceneSpace;
			new_tile->mPosition = mOwner->mPosition - Vector2(mTileSize.x * _x,
				mTileSize.y * _y);
			new_tile->mScale = mTileSize;

			auto sprite = DBG_NEW Sprite;
			new_tile->AddComp(sprite);
			sprite->SetVisibility(true);
			if (mTexture)
				sprite->ChangeTexture(mTexture->GetFilename());

			new_tile->mbSerializable = false;
			new_tile->GetSpace()->AddObject(*new_tile);

			return true;
		}
		else
		{
			if (mTiles.find(_x) != mTiles.end() && mTiles[_x].find(_y) != mTiles[_x].end())
			{
				if (mTiles[_x][_y]) 
					Game->DestroyObject( mTiles[_x][_y]);
				mTiles[_x][_y] = nullptr;
				return true;
			}

			return false;
		}
	}
	void FloorGrid::UpdatePosition()
	{
		if (mOwner->mPosition != mCenter)
		{
			for (auto i : mTiles)
			{
				for (auto j : i.second)
				{
					if (j.second)
						j.second->mPosition += mOwner->mPosition - mCenter;
				}
			}

			mCenter = mOwner->mPosition;
		}
	}
	void FloorGrid::UpdateScale()
	{
		if (Vector2(mOwner->mScale.x / mColumns, mOwner->mScale.y / mRows) != mTileSize)
		{
			mTileSize = Vector2(mOwner->mScale.x / mColumns, mOwner->mScale.y / mRows);

			for (auto i : mTiles)
			{
				for (auto j : i.second)
				{
					if (j.second)
					{
						j.second->mPosition.x = mCenter.x - i.first * mTileSize.x;
						j.second->mPosition.y = mCenter.y - j.first * mTileSize.y;
						j.second->mScale = mTileSize;
					}
				}
			}
		}
	}
	void FloorGrid::Grow(bool _row)
	{
		if (_row)
			mRows += 2;
		else
			mColumns += 2;

		mOwner->mScale = Vector2(mTileSize.x * mColumns, mTileSize.y * mRows);

		UpdateScale();
		UpdatePosition();
	}
}