#pragma once

#include "../../Engine/Engine.h"
#include "../../Engine/src/Level/Editor/Editor.h"

namespace RegionSelector {
	void SelectRegion();
	void Edit();
	void ViewRegions();
}
