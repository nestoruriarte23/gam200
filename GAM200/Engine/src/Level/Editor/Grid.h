#pragma once

class Texture;

template <typename T>
class TResource;

namespace Grid
{
	extern bool is_grid_on;

	class FloorGrid : public IComp
	{
		RTTI_DECLARATION_INHERITED(FloorGrid, IComp);

	public:
		FloorGrid();
		FloorGrid(const FloorGrid& _rhs);
		~FloorGrid();

		void Initialize() override;
		void Shutdown() override;
		bool Edit() override;

		bool equal_to(IComp const& other) const override;
		bool are_the_same_type(IComp const& lhs) override;
		FloorGrid * Clone() override;

		void FromJson(nlohmann::json & _j) override;
		void ToJson(nlohmann::json & _j) override;
		void operator=(IComp &) override;

	private:
		void DrawGrid();
		bool EditGrid();
		bool HandleInput(int _x, int _y, bool action);	// true = left
		void UpdatePosition();
		void UpdateScale();
		void Grow(bool _row);	// true = increase rows

		unsigned mRows;
		unsigned mColumns;
		Vector2 mTileSize;
		Vector2 mCenter;

		/* Probably want to hold multiple ones at a time 3-4 */
		TResource<Texture>*		mTexture;

		std::map<int, std::map<int, GameObject*>> mTiles;
	};
}