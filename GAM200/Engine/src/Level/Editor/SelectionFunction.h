#pragma once

namespace Selection {
	void SelectObject();
	GameObject* FindFirstObject(const Vector2& _pos, const std::vector<GameObject *> & _goVect);
	void DrawSelection(GameObject* _obj);
	void EditSelectedObject();
}