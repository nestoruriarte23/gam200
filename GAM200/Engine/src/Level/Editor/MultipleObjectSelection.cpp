#include <ios>
#include <fstream>
#include "..//..//Engine/Engine.h"
#include "Editor.h"
#include "MultipleObjectSelection.h"

Vector2* FirstPos = nullptr;

Vector2* StartingDragPos = nullptr;

bool MouseIsDraggin = false;
bool IsDraggable = false;
Vector2 PrevDisplacement;

void DrawSelectionRectangle(Vector2& _first, Vector2& _last);
bool MauseInSelectionRectangle(Vector2& _first, Vector2& _last, Vector2& mouse);

std::vector<GameObject*> MultiObjSelection::SelectObjects(Vector2& _startPos, Vector2& _endPos, std::vector<GameObject*>& _objects) {

	float width = _startPos.x - _endPos.x;
	float height = _startPos.y - _endPos.y;

	Vector2 pCenter{ _startPos.x - (width / 2), _startPos.y - (height / 2) };

	float absWidth = abs(width);
	float absHeight = abs(height);

	std::vector<GameObject*> selectedObjt;

	for (GameObject* obj : _objects) {
		if (StaticPointToStaticRect(&(obj->mPosition), &pCenter, absWidth, absHeight)) {
			selectedObjt.push_back(obj);
		}
	}
	return selectedObjt;
}

void MultiObjSelection::SelectMultipleObjects(std::vector<GameObject*>& _selected) {
	ImGuiIO& io = ImGui::GetIO();
	if (!io.WantCaptureMouse && !MouseIsDraggin) {
		if (MouseDown(MouseInput::LEFT) && !KeyDown(Key::Control)) {
			if (!FirstPos) {
				FirstPos = DBG_NEW Vector2(InputManager->GetMouse());
			}
			IsDraggable = false;
			Vector2* secondPos = &InputManager->GetMouse();

			Editor::is_in_multi_edit = true;

			DrawSlectionRectangle(*FirstPos, *secondPos);
			Editor::multipleSelectionGameObjectVector = SelectObjects(*FirstPos, *secondPos, Editor::goVect);
		}
		if (MouseReleased(MouseInput::LEFT) && FirstPos) {
			delete FirstPos;
			FirstPos = nullptr;
			IsDraggable = true;
		}
		if (MouseReleased(MouseInput::LEFT) && Editor::multipleSelectionGameObjectVector.size() == 0) {
			Editor::is_in_multi_edit = false;
		}
	}
}

void MultiObjSelection::EditObjects() {
	if (Editor::multipleSelectionGameObjectVector.size() && IsDraggable) {

		//get min and max x,y to create the selection rectangle

		float minx, miny, maxx, maxy;
		minx = Editor::multipleSelectionGameObjectVector.front()->mPosition.x;
		miny = Editor::multipleSelectionGameObjectVector.front()->mPosition.y;
		maxx = Editor::multipleSelectionGameObjectVector.front()->mPosition.x;
		maxy = Editor::multipleSelectionGameObjectVector.front()->mPosition.y;

		for (GameObject* obj : Editor::multipleSelectionGameObjectVector) {
			minx = min(minx, obj->mPosition.x);
			miny = min(miny, obj->mPosition.y);
			maxx = max(maxx, obj->mPosition.x);
			maxy = max(maxy, obj->mPosition.y);
		}


		Vector2 first(maxx, maxy);
		Vector2 last(minx, miny);

		DrawSelectionRectangle(first, last);

		ImGuiIO& io = ImGui::GetIO();
		if (!io.WantCaptureMouse) {
			if (MouseDown(MouseInput::LEFT) && !KeyDown(Key::Control)) {
				if (!StartingDragPos) {
					StartingDragPos = DBG_NEW Vector2(InputManager->GetMouse());
					SaveMultiplePositions();
				}

				Vector2* secondPos = &InputManager->GetMouse();

				Vector2 displacement = *secondPos - *StartingDragPos;

				Vector2 currentDisplacement = displacement - PrevDisplacement;

				MouseIsDraggin = MauseInSelectionRectangle(first, last, *secondPos);

				PrevDisplacement = displacement;

				for (GameObject * go : Editor::multipleSelectionGameObjectVector) {
					go->mPosition += currentDisplacement;
				}
			}

			if ((MouseReleased(MouseInput::LEFT) && StartingDragPos)) {
				//TODO drag has finished now save the old ones
				Editor::SaveToUndoStack(5);
				delete StartingDragPos;
				StartingDragPos = nullptr;
				PrevDisplacement = Vector2();
			}else if (!MouseIsDraggin) {
				delete StartingDragPos;
				StartingDragPos = nullptr;
				PrevDisplacement = Vector2();
			}
		}

		if (MouseDown(MouseInput::LEFT)) {
			Vector2* secondPos = &InputManager->GetMouse();
			if (!MauseInSelectionRectangle(first, last, *secondPos)) {
				Editor::is_in_multi_edit = false;
				Editor::multipleSelectionGameObjectVector.clear();
			}
		}
	}
}

void MultiObjSelection::SaveMultiplePositions() {
	for (GameObject* obj : Editor::multipleSelectionGameObjectVector) {
		Editor::multipleOldPositions.push_back(std::pair<int, Vector2>(obj->GetUID(), obj->mPosition));
	}
}

void DrawSelectionRectangle(Vector2& _first, Vector2& _last) {

	float width = _first.x - _last.x;
	float height = _first.y - _last.y;

	Vector2 pCenter{ _first.x - (width / 2), _first.y - (height / 2) };

	RenderManager::Instance()->DrawOrientedRectangleAt(pCenter, width, height, 0, Engine::Color::violet);
}

bool MauseInSelectionRectangle(Vector2& _first, Vector2& _last, Vector2& mouse ) {

	float width = _first.x - _last.x;
	float height = _first.y - _last.y;

	Vector2 pCenter{ _first.x - (width / 2), _first.y - (height / 2) };

	if (StaticPointToStaticRect(&(mouse), &pCenter, abs(width), abs(height))) {
		return true;
	}

	return false;
}

void MultiObjSelection::DrawSlectionRectangle(Vector2& _startPos, Vector2& _endPos) {

	float width = _startPos.x - _endPos.x;
	float height = _startPos.y - _endPos.y;

	Vector2 pCenter{ _startPos.x - (width / 2), _startPos.y - (height / 2) };

	RenderManager::Instance()->DrawOrientedRectangleAt(pCenter, width, height, 0, Engine::Color::lima);
}

void MultiObjSelection::DrawSelectedObjects() {
	for(GameObject* obj : Editor::multipleSelectionGameObjectVector){
		Engine::Color c = Engine::Color::cyan;
		c.a = 0.5f;
		RenderManager::Instance()->DrawOrientedRectangleAt(obj->mPosition, obj->mScale.x, obj->mScale.y, obj->mRotation, c);
	}
}