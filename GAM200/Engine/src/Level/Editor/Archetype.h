#pragma once

#include <map>
#include <string>
#include <vector>
#include <algorithm>
#include "..//..//Engine/Engine.h"
#include "Editor.h"
#include "../../Engine/src/UndoRedo/UndoRedo.h"

namespace Archetype {
	
	void AddArchetype(std::string _name, GameObject * _archetypeObject);
	void CreateArchetype(std::string _name);
	void RemoveFromArchetype(GameObject& _go, std::string& _name);
	void AddToArchetype(GameObject* _go, std::string& _name);
	void UpdateArchetypes(std::string _name);
	void ReloadAll();
}


struct OnEditArchetype : public OnGui {
	public:
		OnEditArchetype(std::string _name) :name(_name) {};
		bool opened = false;
		std::string GetName() const {
			return name;
		};

		bool EditObject(GameObject& _go);
		std::string name;
		void Edit(bool *);
};
