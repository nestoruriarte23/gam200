#include <filesystem> 
#include <algorithm>

#include "../../Utilities/MyDebug/LeakDetection.h"
#include "../../UndoRedo/UndoRedo.h"
#include "../../../Engine.h"
#include "ResourceImporter.h"
#include "ImGUI_Basics.h"
#include "ImGui/imgui.h"
#include "Editor.h"


bool			Editor::open_game_objects_menu = true;
bool			Editor::new_level = false;
bool			Editor::new_space = false;
bool			Editor::previous_frame_changed = false;
GameObject *	Editor::game_object_for_undo = nullptr;

float			Editor::TimeToSave = 100.0f;
float			Editor::SaveTime = 0.0f;
GameObject*		Editor::ObjectCopied = nullptr;



void Editor::LoadLevels() {
	levels.clear();
	for (auto i : std::experimental::filesystem::directory_iterator("..//Resources//Levels//")) {
		std::string level = (i.path()).generic_string();
		level.erase(0, 23);
		level.erase(level.size() - 5, level.size());
		levels.push_back(level);
	}
}

void Editor::LoadArchetypes() {
	archetypes.clear();
	for (auto i : std::experimental::filesystem::directory_iterator("..//Resources//Archetypes//")) {
		std::string archetype = (i.path()).generic_string();;
		archetype.erase(0, 27);
		archetype.erase(archetype.size() - 5, archetype.size());
		archetypes.push_back(archetype);
	}

}

void Editor::Play() {
	EditableCollider::editing_collider = false;
	serializer->SaveLevel(Editor::LevelName.c_str(), Editor::demo_scene);

	Editor::goVect.clear();
	selected_object = NULL;
	slected = false;
	GSM->nextLevel = ENGINE_PROOF_LEVEL;
}

void Editor::ControlCamera()
{
	if (float wheel_scroll = InputManager->GetMouseWheel())
	{
		if (wheel_scroll > 0)
			Camera::Instance()->mViewScale *= 0.96f;
		else
			Camera::Instance()->mViewScale *= 1.04f;
	}
	static Vector2 start_mouse_pos;
	static Vector2 start_cam_pos;
	static bool wheel_down = false;
	if (!wheel_down && (InputManager->MouseIsDown(MouseInput::MID) || InputManager->MouseIsDown(MouseInput::RIGHT)))
	{
		wheel_down = true;
		start_mouse_pos = InputManager->GetMouseInWindow();
		start_cam_pos = Camera::Instance()->mPos;
	}
	else if (InputManager->MouseIsUp(MouseInput::MID) && InputManager->MouseIsUp(MouseInput::RIGHT))
	{
		wheel_down = false;
	}
	if (wheel_down)
	{
		Vector2 diff(InputManager->GetMouseInWindow() - start_mouse_pos);
		Camera::Instance()->mPos = start_cam_pos - diff * (Camera::Instance()->mViewScale.x / Game->GetWindowSize().x);
	}
}

void Editor::ChangeLevel(std::string _name) {
	serializer->SaveLevel(LevelName.c_str(), Editor::demo_scene);
	delete Editor::demo_scene;
	Editor::goVect.clear();
	Editor::demo_scene = DBG_NEW Scene();
	Game->SetScene(demo_scene);
	std::vector<GameObject *> goTmp = serializer->LoadLevel(_name.c_str(), Editor::demo_scene);
	goVect.insert(Editor::goVect.end(), goTmp.begin(), goTmp.end());
	LevelName = _name;
	Editor::slected = false;
	selected_object = nullptr;
	clearAllSacks();
}


void Editor::DrawPreview()
{
	/* Render everything to a texture (stored in the scene) */
	RenderMgr->RenderScene(Game->GetCurrScene());

	/* Save the position and size of the preview area */
	Vector2 pos(-Game->GetWindowSize() * 3 / 8);
	Vector2 size(Game->GetWindowSize() / 4);

	/* Change the render target to be the default/final texture/frame buffer */
	FrameBuffer::SetDefaultRenderTarget();

	/* Set the viewport to the preview area */
	Viewport new_view(pos, size);
	new_view.SetOpenGLViewport();

	/* Clear the buffer */
	RenderMgr->CleanViewport(Engine::Color::empty);

	/* Draw the scene's buffer onto the selected viewport */
	RenderManager::Instance()->DrawScreenQuad(Game->GetCurrScene()->mFrameBuffer.GetResultingTexture());
}

void Editor::AddOnGuiEdit(std::string _menu, OnGui * _gui) {
	guiFunctions[_menu].push_back(_gui);
}

namespace EditorFunctions
{
	Space* GetFirstActiveSpace()
	{
		auto curr_scene = Game->GetCurrScene();

		for (auto _space : curr_scene->scene_spaces)
		{
			if (_space->mbEditable)
				return _space;
		}

		return curr_scene->scene_spaces[0];
	}
}	// namespace EditorFunctions

//void Editor::AutoSave()
//{
//	SaveTime += (f32)TimeManager::Instance()->GetDt();
//	if (SaveTime > TimeToSave)
//	{
//		SaveTime = 0.0f;
//		serializer->SaveLevel(Editor::LevelName.c_str(), Editor::demo_scene);
//	}
//}
//
//void Editor::CopyObj()
//{
//	if (selected_object)
//		ObjectCopied = selected_object;
//	else
//		ObjectCopied = nullptr;
//}
//
//void Editor::PasteObj()
//{
//	if (ObjectCopied)
//	{
//		GameObject* newGo = ObjectCopied->Clone();
//		newGo->mPosition = Vector2(InputManager->GetMouse().x, InputManager->GetMouse().y);
//		Editor::demo_scene->FindSpace(newGo->mSceneSpace.c_str())->AddObject(*newGo);
//		goVect.push_back(newGo);
//	}
//}
//
//void Editor::SaveToUndoStack() {
//	undo->SaveOldObject(game_object_for_undo, selected_object->GetUID());
//	delete game_object_for_undo;
//	game_object_for_undo = nullptr;
//}

