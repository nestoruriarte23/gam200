#pragma once

void CreateFontTexture();
void CreateGUIDeviceObject();
void InitializeImGui();
void UpdateGui();
void DrawGui();
void MyEditor();