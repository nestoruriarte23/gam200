#include <filesystem> 
#include <algorithm>

#include "../../Utilities/MyDebug/LeakDetection.h"
#include "../../UndoRedo/UndoRedo.h"
#include "../../../Engine.h"
#include "ResourceImporter.h"
#include "ImGUI_Basics.h"
#include "ImGui/imgui.h"
#include "Archetype.h"
#include "Editor.h"





//////////////////////////CreateObject functions//////////////////////////
void Editor::CreateObject() {

	GameObject * goT = DBG_NEW GameObject();
	//Editor::demo_scene->scene_spaces[0]->AddObject(*goT);

	goT->SetName("GameObject");
	goT->mScale = Vector2(200, 200);
	goT->AddComp(factory->Create("Sprite"));

	Editor::goVect.push_back(goT);
	selected_object = Editor::goVect.back();
	Editor::slected = true;

	auto default_space = demo_scene->GetDefault();
	if (default_space) default_space->AddObject(*goT);
	else std::cout << "You must select first which Space you want as the default one!" << std::endl;

	goT->Initialize();
	//Game->CreateObject(goT);
	undo->SaveOldObject(goT, goT->GetUID(), (int)Action::ADDOBJECT);
}


void Editor::CreateObject(std::string _name) {
	Archetype::CreateArchetype(_name);
}


//////////////////////////DeleteObject Functions//////////////////////////
void Editor::DeleteObject(GameObject * _delGo, Space * _fromDelSpace, bool FromUndo) {
	if (!FromUndo) {
		undo->SaveOldObject(_delGo, _delGo->GetUID(), (int)Action::DELETEOBJECT);
	}
	_fromDelSpace->DeleteObject(_delGo->GetUID());
	if (_delGo == selected_object) {
		Editor::selected_object = nullptr;
		Editor::slected = false;
		delete game_object_for_undo;
		game_object_for_undo = nullptr;
	}
	goVect.erase(std::remove(goVect.begin(), goVect.end(), _delGo), goVect.end());
}

void Editor::DeleteMultipleObjects() {
	undo->UndoSaveMultiStackObj(multipleSelectionGameObjectVector, (int)Action::MULTIPLEDELETE);
	for (GameObject * obj : multipleSelectionGameObjectVector) {
		demo_scene->FindSpace(obj->mSceneSpace.data())->DeleteObject(obj->GetUID());
		goVect.erase(std::remove(goVect.begin(), goVect.end(), obj), goVect.end());
	}
	is_in_multi_edit = false;
	multipleSelectionGameObjectVector.clear();
}

//////////////////////////AddComponent Functions//////////////////////////
void Editor::AddComponent(const char * s) {
	if (std::find(compNames.begin(), compNames.end(), s) == compNames.end()) {
		compNames.push_back(s);
	}
}
