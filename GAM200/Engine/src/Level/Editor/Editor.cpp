#include <filesystem>
#include <algorithm> 

#include "../../../Engine.h"
#include "../../UndoRedo/UndoRedo.h"
#include "../../Engine/src/Utilities/MyDebug/LeakDetection.h"
#include "ImGUI_Basics.h"
#include "ImGui/imgui.h"
#include "ObjectSelection.h"
#include "MultipleObjectSelection.h"
#include "Archetype.h"
#include "RegionSelector.h"
#include "Editor.h"
#include "Grid.h"

///////////////////////* Components *////////////////////////////

#include "../../GamePlay/src/LogicComponents/Spawner/Spawner.h"
#include "../../GamePlay/src/LogicComponents/CameraSync.h"
#include "../../GamePlay/src/LogicComponents/ChangeAnimationsComponent/AnimationChangeComponent.h"
#include "../../GamePlay/src/LogicComponents/InteractionComponent/InteractionComponent.h"
#include "../../GamePlay/src/PlayerLogic/PlayerLogic.h"
#include "../../GamePlay/src/PlayerLogic/PlayerAnimation.h"
#include "../../GamePlay/src/CompanionLogic/CompanionLogic.h"
#include "../../GamePlay/EnemyLogic.h"
#include "../../GamePlay/src/LogicComponents/HealthComponent/HealthComponent.h"
#include "..//..//Graphics/Text/Text.h"
#include "..//..//Graphics/Text/Glyph.h"
#include "../../GamePlay/src/LamiaLogic/LamiaProjectile.h"
#include "../../GamePlay/src/TitleScreenLogic/GameInitialization.h"
#include "../../GamePlay/src/GaltxaLogic/GaltxagorriLogic.h"
#include "../../GamePlay/src/SorginaLogic/SorginaLogic.h"
#include "../../GamePlay/src/SorginaLogic/SorginaMagic.h"
#include "../../GamePlay/src/ClosedAreas/ClosedAreas.h"

////////////////////* Text Rendering *////////////////////////////

GameObject*					gMainText;

////////////////////* Editor Statics *////////////////////////////

using namespace ObjSelection;

std::vector<GameObject *>	Editor::goVect;
std::map<std::string, std::vector<OnGui*>> Editor::guiFunctions;
std::string					Editor::LevelName =	 "";
bool						Editor::slected = false;
bool						Editor::is_in_multi_edit = false;
bool						Editor::not_moveable_object = false;
bool						Editor::show_all_colliders = false;
bool						Editor::show_all_regions = false;
Text*						gTextInfo;
std::vector<GameObject *>	Editor::multipleSelectionGameObjectVector;
std::vector<GameObject *>	Editor::multipleSelectionGameObjectVectorCopy;
std::vector<std::pair<int, Vector2>> Editor::multipleOldPositions;
std::map<std::string, std::vector<GameObject *>> Editor::archetypeObjects;
std::vector<std::string>	Editor::levels;
std::vector<std::string>	Editor::archetypes;
std::vector<const char *>	Editor::compNames;
//////////////////////////////////////////////////////////////////

void Editor::Load()
{
	Game->onEditor = true;
	
	LoadLevels();
	LoadArchetypes();

	Editor::demo_scene = DBG_NEW Scene();
	Game->SetScene(demo_scene);
	if (LevelName == "") {
		LevelName = levels[0];
	}
}

void Editor::Initialize() 
{
	TimeManager::Instance()->Initialize(true, 60.0);
	Camera::Instance()->Initialize();

	/* TODO: This should happen only once per session */
	//factory->Register<renderable>();
	AddComponent("CameraComp");
	AddComponent("BoxCollider");
	AddComponent("RigidBody");
	AddComponent("Sprite");
	AddComponent("Shadow");
	AddComponent("ShadowEmitter");
	AddComponent("GetOutlined");
	AddComponent("SpineAnimation");
	AddComponent("GaltzagorriSpawner");
	AddComponent("BackgroundCamSync");
	AddComponent("SoundEmitter");
	AddComponent("SoundListener");
	AddComponent("Actor");
	AddComponent("AnimationChange");
	AddComponent("PlayerMovement");
	AddComponent("ParticleSystem");
	AddComponent("EnemyLogic");
	AddComponent("PlayerInfo");
	AddComponent("Grid::FloorGrid");
	AddComponent("Health");
	AddComponent("DeathParticles");
	AddComponent("ExplosionLogic");
	AddComponent("GaltzagorriLogic");
	AddComponent("SorginaLogic");
	AddComponent("LamiaProjectileLogic");
	AddComponent("MagicChaseLogic");
	AddComponent("PlayerAnimationChange");
	AddComponent("HealthBar");
	AddComponent("CompanionBar");
	AddComponent("TackleLogic");
	AddComponent("TitleScreenLogic");
	AddComponent("TextComp");
	AddComponent("FaceSprite");
	AddComponent("InteractionComponent");
	AddComponent("DestroyableObject");
	AddComponent("CameraJoint");
	AddComponent("TriggerLogic");
	AddComponent("DestroyObjectTime");
	AddComponent("TornadoLogic");
	AddComponent("ClosedAreas");

	InitializeImGui();

	RenderMgr->Reset();
	goVect = serializer->LoadLevel(LevelName.c_str(), Editor::demo_scene);
	AddOnGuiEdit("Edition", DBG_NEW OnEditArchetype("Archetype"));
	AddOnGuiEdit("Graphics", DBG_NEW DrawOrder("Draw Order"));
}

static void ResizeImGUI(int x, int y)
{
	ImGuiIO& io = ImGui::GetIO();

	io.DisplaySize = ImVec2((float)x, (float)y);

	// TODO: DEBUG THIS
	Game->ChangeResolution(x, y);

	ImGui::GetStyle().ScaleAllSizes(y / io.DisplaySize.y);
	ImGui::GetIO().FontGlobalScale = y / io.DisplaySize.y;

	Game->ChangeResolution(x, y);
	Game->GetCurrScene()->mFrameBuffer.RegenerateBuffer();
}

void Editor::Update()
{
	TimeManager::Instance()->StartFrame();

	ImGuiIO& io = ImGui::GetIO();
	if (!io.WantCaptureKeyboard)
	{
		if (KeyTriggered(Key::Y) && KeyUp(Key::Control)) ResizeImGUI(1280, 720);
		if (KeyTriggered(Key::U)) ResizeImGUI(1600, 900);
		if (KeyTriggered(Key::I)) ResizeImGUI(1920, 1080);
		if (KeyTriggered(Key::O)) ResizeImGUI(3840, 2160);
		if (KeyTriggered(Key::X)) Game->ToogleFullscreen();

		//if (KeyTriggered(Key::Right)) RenderMgr->ToggleWireframeDraw();

		if (KeyTriggered(Key::R)) 
			gActualResourceMgr.Update();

		if (ButtonTriggered(SDL_CONTROLLER_BUTTON_START))
		{
			Play();
		}
	}

	if (!io.WantCaptureMouse)
	{
		ControlCamera();
	}

	KeyboardShortcuts();

	UpdateGui();

	/* Rotate/Scale the current selected object if any */
	EditSelectedObject();

	// get main window dimensions
	auto winWH = Game->GetWindowSize();

	if (!slected) {
		delete game_object_for_undo;
		game_object_for_undo = nullptr;
	}

	if (!is_in_multi_edit && !EditableCollider::editing_collider
		&& !Editor::not_moveable_object && !Grid::is_grid_on) {
		/* Select object */
		SelectObject();
	}

	if (!slected) {
		MultiObjSelection::EditObjects();
		MultiObjSelection::SelectMultipleObjects(multipleSelectionGameObjectVector);
		MultiObjSelection::DrawSelectedObjects();
	}
	RegionSelector::SelectRegion();
	RegionSelector::Edit();

	Editor::ShowOnGuiEdit();

	if (Editor::show_all_colliders)
		DisplayAllColliders();

	/* Draw selected scalator_origin;object, visual feedback */
	if (EditableCollider::editing_collider == false)
		DrawSelection(Editor::selected_object);

	f64 fps = TimeManager::Instance()->GetFPS();

	std::string wintitle = "Engine Proof - FPS: "; 
	wintitle += std::to_string(fps);
	wintitle += "  Alive Particles: ";
	wintitle += std::to_string(ParticleMgr->GetActiveParticles());

	SDL_SetWindowTitle(Game->GetSDLWindow(), wintitle.c_str());
	if (!Editor::new_level) {
		AutoSave();
	}
}

void Editor::Render()
{
	/* Clear what is already on the buffer */
	Camera::Instance()->GetViewport()->SetOpenGLViewport();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* Call draw (The editor itself, the general view) */
	RenderManager::Instance()->DrawAllRenderables();
	ParticleMgr->Render();

	RenderManager::Instance()->DrawDebugLines();

	Game->onEditor = false;
	DrawPreview();
	Game->onEditor = true;

	MyEditor(goVect);

	Camera::Instance()->GetViewport()->SetOpenGLViewport();

	RenderMgr->WireframeSetFillMode();
	DrawGui();
	if (RenderMgr->GetWireframeActive()) RenderMgr->WireframeSetLineMode();

	if (Editor::selected_object)
	{
		/* Some components do things when objects are selected on the editor */
		auto comps = Editor::selected_object->GetComps();
		for (auto it = comps.begin(); it != comps.end(); ++it)
			(*it)->Selected();
	}

	/* Swap buffers */
	SDL_GL_SwapWindow(Game->GetSDLWindow());

	/* Frame time */
	TimeManager::Instance()->EndFrame();
}

void Editor::Free()
{
	RenderManager::Instance()->RemoveAllRenderables();
	RenderManager::Instance()->RemoveAllCameras();
	
	LogicManager::Instance()->Shutdown();

	clearAllSacks();

	if (game_object_for_undo != nullptr)
	{
		delete game_object_for_undo;
		game_object_for_undo = nullptr;
	}

	std::for_each(guiFunctions.begin(), guiFunctions.end(),
	[](auto pair)
	{
		std::vector<OnGui*>& vec = pair.second;
		while (!vec.empty())
		{
			delete vec.back();
			vec.pop_back();
		}
	});

	guiFunctions.clear();
}

void Editor::Unload()
{
	delete demo_scene;
	Game->SetScene(NULL);

	Game->onEditor = false;
}



/////////////////// BASURA DE TEXT ////////////////
