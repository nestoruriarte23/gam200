#pragma once
#include <utility>
#include <map>

#include <string>
#include <vector>
#include "../../Utilities/Math/MyMath.h"

#include "..//Level.h"

#include "../../Components/Components.h"

#include "../../../src/System/GameObjectManager/GameObject.h"



//TEMPORAL CLASS COMP TESTING/////////////////////////////////////

enum ColType { cOBB, cAABB, cCIRCLE };



using json = nlohmann::json;



struct OnGui {

	public:

		bool opened = false;

		std::string name;

		virtual std::string GetName() const = 0;

		virtual void Edit(bool *) {};

};



struct DrawOrder : public OnGui {

	public:

		DrawOrder(std::string _name) : name(_name) {};

		bool opened = false;

		std::string name;

		std::string GetName() const {

			return name;

		};

		void Edit(bool *);

};





class Editor : public Level
{

	public:



	//Editor basic functions

	void Initialize();

	void Load();

	void Update();

	void Render();

	void Free();

	void Unload();



	//EditFunctions

	void MyEditor(std::vector<GameObject *> & _goVect);



	void MainMenuBar();

	void Menus();

	void ShowOnGuiEdit();



	void LodeableResources();

	void Levels();

	void Archetypes();



	void NewLevel();

	void NewSpace();



	void ConcreteGO(GameObject * _go);





	//ObjectModifFunctions

	void CreateObject(std::string _name);

	void CreateObject();



	void DeleteObject(GameObject * delGo, Space * fromDelSpace, bool FromUndo = false);

	void DeleteMultipleObjects();



	void AddComponent(const char *);





	//EditorStandardAbilities

	void KeyboardShortcuts();



	void Undo();

	void Redo();

	static void SaveToUndoStack(int _action);



	void AutoSave();



	void CopyObj();

	void PasteObj();





	//EditorFunctions

	void LoadLevels();

	void LoadArchetypes();

	void Play();

	void ChangeLevel(std::string _name);

	void ControlCamera();

	void DrawPreview();

	void AddOnGuiEdit(std::string _menu, OnGui * _gui);



	static std::string	GetLevel() { return LevelName; }



	static std::vector<std::string>	levels;

	static std::vector<std::string>	archetypes;

	static std::string			LevelName;

	static bool					slected;

	static bool					not_moveable_object;

	static bool					show_all_colliders;

	static bool					show_all_regions;

	static GameObject*			selected_object;

	static GameObject *			game_object_for_undo;

	static bool					new_level;

	static bool					open_game_objects_menu;

	static bool					new_space;

	static bool					previous_frame_changed;

	static bool					is_in_multi_edit;

	static float				TimeToSave;

	static float				SaveTime;

	static GameObject*			ObjectCopied;

	static std::map<std::string, std::vector<OnGui*>> guiFunctions;

	static std::vector<std::pair<int, Vector2>> multipleOldPositions;

	static std::vector<GameObject *> multipleSelectionGameObjectVector;

	static std::vector<GameObject *> multipleSelectionGameObjectVectorCopy;



	//very temporal

	static std::vector<GameObject *> goVect;

	static std::map<std::string, std::vector<GameObject *>> archetypeObjects;


	static std::vector<const char *> compNames;

};



/* Utilities in general */

namespace EditorFunctions

{

	Space* GetFirstActiveSpace();





}	// namespace EditorFunctions