#ifndef MULTIPLE_OBJECT_SELECTION_H
#define MULTIPLE_OBJECT_SELECTION_H

#include <vector>
#include <utility>
#include "../../Utilities/Math/MyMath.h"

class GameObject;
struct Vector2;

namespace MultiObjSelection {
	std::vector<GameObject*> SelectObjects(Vector2& _startPos, Vector2& _endPos, std::vector<GameObject*>& _objects);

	void SelectMultipleObjects(std::vector<GameObject*>& _selected);

	void SaveMultiplePositions();

	void EditObjects();

	void DrawSlectionRectangle(Vector2& _startPos, Vector2& _endPos);

	void DrawSelectedObjects();
}

#endif // !MULTIPLE_OBJECT_SELECTION_H

