#include <filesystem> 
#include <algorithm>
#include "../../Utilities/MyDebug/LeakDetection.h"
#include "../../UndoRedo/UndoRedo.h"
#include "../../../Engine.h"
#include "ResourceImporter.h"
#include "ImGUI_Basics.h"
#include "ImGui/imgui.h"
#include "Archetype.h"
#include "Editor.h"



void Editor::MyEditor(std::vector<GameObject *> & _goVect) {

	LodeableResources();
	MainMenuBar();
	//OtherWindows();

	ImGui::ShowTestWindow();
	ImGui::Begin("Game Objects", &open_game_objects_menu, ImGuiWindowFlags_MenuBar);

	if (ImGui::Button("Play")) {
		Play();
	}

	ImGui::SameLine();
	if (ImGui::Button("New level")) {
		serializer->SaveLevel(Editor::LevelName.c_str(), demo_scene);
		selected_object = NULL;
		slected = false;
		_goVect.clear();

		new_level = true;
	}

	/*ImGui::SameLine();
	if (ImGui::Button("ConfigureDraw"))
	{
	DrawOrderOpen = !DrawOrderOpen;
	}*/

	if (ImGui::Button("Add Space")) {
		new_space = true;
	}

	ImGui::SameLine();
	if (ImGui::Button("Create Game Objet")) {
		CreateObject();
	}

	GameObject * delGo = nullptr;
	Space * fromDelSpace = nullptr;
	bool del = false;

	for (Space * s : Editor::demo_scene->scene_spaces) {
		// Visible / Not-Visible Space
		{
			ImGui::PushID(s->GetUID());
			ImGui::Checkbox("", &s->mbEditable);
			ImGui::SameLine();
			ImGui::PopID();

			ImGui::PushID(s->GetUID());
			ImGui::SameLine();
			if (ImGui::Button("D")) {
				Editor::demo_scene->SetDefault(s);
			}
			ImGui::SameLine();
			ImGui::PopID();
		}
		if (ImGui::TreeNode(s->space_name_.c_str())) {

			ImGui::PushID(s->GetUID());
			ImGui::Checkbox(" Visible", &s->mbVisible);

			ImGui::PopID();
			for (GameObject * go : s->space_objects) {
				ImGui::PushID(go->GetUID());
				if (ImGui::Button(go->GetName(), ImVec2(90.0f, 0.0f))) {
					selected_object = go;
					slected = true;
				}
				ImGui::SameLine();
				if (ImGui::Button("Delete")) {
					fromDelSpace = s;
					delGo = go;
					del = true;
				}
				ImGui::PopID();

			}
			ImGui::TreePop();
		}

	}

	if (del) {
		DeleteObject(delGo, fromDelSpace);
	}



	if (ImGui::Button("Save")) {
		serializer->SaveLevel(Editor::LevelName.c_str(), Editor::demo_scene);
	}

	ImGui::End();

	if (Editor::slected) {
		ConcreteGO(selected_object);
	}

	if (!Editor::slected) {
		selected_object = nullptr;
	}

	if (new_level) {
		NewLevel();
	}

	if (new_space) {
		NewSpace();
	}
}




void Editor::MainMenuBar() {

	if (ImGui::BeginMainMenuBar()) {
		if (ImGui::BeginMenu("Edit"))
		{
			bool hasUnodStack = undo->GetUndoStackSize() ? true : false;
			if (ImGui::MenuItem("Undo", "ctrl+Z", false, hasUnodStack)) {
				Undo();
			}
			bool hasRedoStack = redo->GetRedoStackSize() ? true : false;
			if (ImGui::MenuItem("Redo", "ctrl+Y", false, hasRedoStack)) {
				Redo();
			}
			ImGui::EndMenu();
		}

		Menus();

		SelectResourcesToLoad();

		ImGui::EndMainMenuBar();
	}
}

void Editor::Menus() {
	for (auto guiType : guiFunctions) {
		if (ImGui::BeginMenu(guiType.first.data())){
			for (auto gui : guiType.second) {
				if (ImGui::MenuItem(gui->GetName().data(), "", false, true)) {
					gui->opened = true;
				}
			}
			ImGui::EndMenu();
		}
	}
}

void Editor::ShowOnGuiEdit() {
	for (auto guiType : guiFunctions) {
		for (auto gui : guiType.second) {
			if (gui->opened) {
				gui->Edit(&gui->opened);
			}
		}
	}
}



//////////////////////////Loadeable resource window (Levels/Archetypes)//////////////////////////
void Editor::LodeableResources() {
	ImGui::Begin("Resources", &open_game_objects_menu, ImGuiWindowFlags_MenuBar);
	ImGui::BeginTabBar("Settings#left_tabs_bar");

	if (ImGui::BeginTabItem("Levels")) {
		Levels();
		ImGui::EndTabItem();
	}

	if (ImGui::BeginTabItem("Archetypes")) {
		Archetypes();
		ImGui::EndTabItem();
	}

	ImGui::EndTabBar();
	ImGui::End();
}

void Editor::Levels() {

	std::vector<const char *> names;
	std::vector<std::string>::iterator it;
	for (it = levels.begin(); it != levels.end(); ++it) {
		names.push_back(it->c_str());
	}

	ImGui::PushID("Levels");
	int level = levels.size() + 1;
	if (ImGui::ListBox("Levels", &level, &names[0], names.size(), 7)) {
		ChangeLevel(levels[level]);
	}
	ImGui::PopID();
}

void Editor::Archetypes() {

	std::vector<const char *> names;
	std::vector<std::string>::iterator it;
	for (it = archetypes.begin(); it != archetypes.end(); ++it) {
		names.push_back(it->c_str());
	}

	ImGui::PushID("Archetypes");
	int archetype = archetypes.size();

	if (archetype != 0)
	{
		if (ImGui::ListBox("Archetypes", &archetype, &names[0], names.size(), 7)) {
			Archetype::CreateArchetype(names[archetype]);
			//CreateObject(names[archetype]);
		}
	}

	ImGui::PopID();
}




//////////////////////////Create windows//////////////////////////
void Editor::NewLevel() {
	ImGui::Begin("New level", &new_level, ImGuiWindowFlags_MenuBar);

	static char str0[128] = "";

	ImGui::InputText("Level Name", str0, IM_ARRAYSIZE(str0));

	if (str0 != "" && str0[0] != '\n') {
		Editor::LevelName = str0;
	}

	if (ImGui::Button("Create")) {
		new_level = false;
		LoadLevels();
	}

	ImGui::End();

}

void Editor::NewSpace() {

	ImGui::Begin("New Space", &new_space, ImGuiWindowFlags_MenuBar);

	static char str0[128] = "";

	ImGui::InputText("Space Name", str0, IM_ARRAYSIZE(str0));

	if (ImGui::Button("Add")) {
		new_space = false;
		demo_scene->AddSpace(str0);
	}

	ImGui::End();

}



//////////////////////////GameObject Edit//////////////////////////
void Editor::ConcreteGO(GameObject * _go) {
	bool changed = false;
	bool positionChanged = false;
	if (game_object_for_undo == nullptr) {
		game_object_for_undo = _go->CloneNoInitialize();
		game_object_for_undo->SetEnabled(false);
	}

	ImGui::PushID(_go->GetUID());

	ImGui::SetNextWindowPos(ImVec2(0, 20), ImGuiCond_Once);
	ImGui::SetNextWindowSize(ImVec2(320, 450), ImGuiCond_Once);

	ImGui::Begin("Game Object", &Editor::slected/*, ImGuiWindowFlags_MenuBar*/);

	char * tempName[20] = { const_cast<char *>(_go->GetName()) };

	ImGui::PushItemWidth(160);
	ImGui::InputText("", *tempName, IM_ARRAYSIZE(tempName));

	ImGui::SameLine();
	if (ImGui::Button("Clone")) {
		GameObject * newGo = _go->Clone();
		Editor::demo_scene->FindSpace(_go->mSceneSpace.data())->AddObject(*newGo);
		goVect.push_back(newGo);
	}

	ImGui::SameLine();
	if (ImGui::Button("Archetype")) {
		Archetype::AddArchetype(_go->GetName(),_go);
	}

	ImGui::Separator();
	if (ImGui::TreeNode("Transform"))
	{
		changed = ImGui::InputFloat2(" Position", selected_object->mPosition.v) || changed;
		positionChanged = changed;
		changed = ImGui::InputFloat2(" Scale", selected_object->mScale.v) || changed;
		changed = ImGui::SliderFloat(" Rotation", &selected_object->mRotation, 0.0f, 2 * PI) || changed;
		changed = ImGui::DragFloat2(" mSortingPos", selected_object->mSortingPos.v) || changed;
		RenderManager::Instance()->DrawCircleAt(selected_object->mPosition + selected_object->mSortingPos, 100, 10, Engine::Color::red);

		ImGui::TreePop();
	}


	ImGui::Separator();
	int tag = selected_object->mTag;
	const char* dyn[9] = { "Enemy", "Player", "Miscellaneous", "Hazard", "GameObject", "Pickable", "Breakable", "Projectile", "TextObject" };

	if (ImGui::Combo(" ", &tag, dyn, 9, 10)) {
		switch (tag)
		{
		case 0:
			selected_object->mTag = Enemies;
			break;
		case 1:
			selected_object->mTag = Player;
			break;
		case 2:
			selected_object->mTag = Miscellaneous;
			break;
		case 3:
			selected_object->mTag = Hazard;
			break;
		case 4:
			selected_object->mTag = GameObjects;
			break;
		case 5:
			selected_object->mTag = Pickable;
			break;
		case 6:
			selected_object->mTag = Breakable;
			break;
		case 7:
			selected_object->mTag = Projectile;
			break;
		case 8:
			selected_object->mTag = TextObject;
			break;
		}
	}

	ImGui::Separator();
	for (unsigned i = 0; i < _go->GetCompCount(); i++) {
		changed = _go->GetComp(i)->Edit() || changed;
		ImGui::Separator();
	}

	ImGui::NewLine();
	if (ImGui::TreeNode("Spaces"))
	{
		auto selected = Editor::demo_scene->FindSpace(_go->mSceneSpace.data());
		for (auto _space : Editor::demo_scene->scene_spaces)
		{
			ImGui::Bullet();
			if (ImGui::Selectable(_space->space_name_.data(), _space == selected))
			{
				selected->RemoveObjFromAlive(_go->GetUID());
				_go->Shutdown();	// Update the spaces of the components
				_space->AddObject(*_go);
				_go->Initialize();	// Update the spaces of the components
				changed = true;
			}
		}
		ImGui::TreePop();
	}

	if (ImGui::TreeNode("AddComponent"))
	{
		//ImGuiTextFilter filter;
		for (unsigned i = 0; i < compNames.size(); ++i)
		{
			ImGui::Bullet();
			ImGui::Text(compNames[i]);
			if (ImGui::IsItemClicked())
			{
				_go->AddComp(factory->Create(compNames[i]));//->Initialize();
				changed = true;
			}
		}
		ImGui::TreePop();
	}
	/*char search[128] = "";
	if (ImGui::InputText("Search Component", search, IM_ARRAYSIZE(search))) {
	SearchComponents(search);
	}*/
	ImGui::End();
	ImGui::PopID();

	if (previous_frame_changed == false && changed == true) {
		SaveToUndoStack((int)Action::NONE);
	}

	if (_go->mbArchetype && changed && !positionChanged) {
		Archetype::RemoveFromArchetype(*_go, _go->mArchetypeName);
	}

	previous_frame_changed = changed;

}


////////////////////////// OnGui Struct Functions //////////////////////////

static void DrawSpacesAndProperties()
{
	const char * blending_modes[] = { "Normal", "Add", "Mult" };

	unsigned current_space = 0;
	static bool changed = false;

	ImGui::Separator();
	auto& spaces = Game->GetCurrScene()->scene_spaces;
	for (auto space : spaces)
	{
		ImGui::PushID(space->GetUID() + 100);
		if (ImGui::BeginCombo("", spaces[current_space]->space_name_.data()))
		{
			for (auto space_2 : spaces)
			{
				bool is_selected = (space_2->mRenderOrder == current_space);

				if (current_space > space_2->mRenderOrder)
					continue;

				if (ImGui::Selectable(space_2->space_name_.data(), is_selected))
				{
					spaces[current_space]->mRenderOrder = -1;
					space_2->mRenderOrder = current_space;
					changed = true;
				}

				if (is_selected)
				{
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}
		ImGui::PopID();

		ImGui::Separator();

		ImGui::PushID(space->GetUID() + 1000);
		if (ImGui::BeginCombo("", blending_modes[spaces[current_space]->mBlendMode]))
		{
			for (unsigned mode = 0; mode < CustomBlendMode::CustomBlends_Total; ++mode)
			{
				bool is_selected = (spaces[current_space]->mBlendMode == mode);
				if (ImGui::Selectable(blending_modes[mode], is_selected))
				{
					spaces[current_space]->mBlendMode = mode;
				}
				if (is_selected)
				{
					ImGui::SetItemDefaultFocus();
				}
			}

			ImGui::EndCombo();
		}
		ImGui::PopID();

		ImGui::PushID(space->GetUID() + 10000);
		ImGui::Checkbox(" Draw Shadows", &space->mbDrawShadows);
		ImGui::Checkbox(" Draw Outlines", &space->mbDrawOutlines);
		ImGui::Checkbox(" Use FrameBuffer", &space->mbUseFrameBuffer);
		ImGui::PopID();

		auto image_width = ImGui::GetWindowContentRegionWidth() * 0.5f - 10;
		if (space->mbUseFrameBuffer)
		{
			ImGui::Image((void*)(intptr_t)(space->mFrameBuffer.GetResultingTexture()),
				ImVec2(image_width, image_width * 1080 / 1920), ImVec2(0, 0), ImVec2(1, -1));
		}
		ImGui::Separator();

		current_space++;
	}

	if (changed)
	{
		std::sort(spaces.begin(), spaces.end(), [](const Space* _lhs, const Space* _rhs)
		{
			return _lhs->mRenderOrder < _rhs->mRenderOrder;
		});
		changed = false;
	}
}

void DrawOrder::Edit(bool * _opened) 
{
	if (!_opened) return;

	ImGui::SetNextWindowSize(ImVec2(Game->GetWindowSize().x - 100, Game->GetWindowSize().y - 80), ImGuiCond_Once);
	ImGui::SetNextWindowSizeConstraints(ImVec2(-1, -1), ImVec2(-1, -1));
	ImGui::SetNextWindowBgAlpha(1.0f);
	
	ImGui::Begin("Draw Order", _opened);

	ImGui::Columns(2);

	{
		auto image_width = ImGui::GetWindowContentRegionWidth() * 0.5f - 10;
		ImGui::Image((void*)(intptr_t)(Game->GetCurrScene()->mFrameBuffer.GetResultingTexture()),
			ImVec2(image_width, image_width * 1080 / 1920), ImVec2(0, 0), ImVec2(1, -1));

		ImGui::NewLine();
		ImGui::ColorEdit4(" ModColor", RenderMgr->GetShadowColor().v, RenderMgr->GetColorFlags());
	}

	ImGui::NextColumn();

	{
		ImGui::BeginChild("ChildWindow", ImVec2(0, 0));

		DrawSpacesAndProperties();
			   
		ImGui::EndChild();
	}

	ImGui::End();
}



