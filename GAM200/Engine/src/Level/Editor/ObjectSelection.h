#pragma once

#include <vector>

class GameObject;
struct Vector2;

namespace ObjSelection
{
	GameObject* FindFirstObject(const Vector2& _pos,
		std::vector<GameObject *> & _goVect);
	void DrawSelection(GameObject* _obj);
	void SelectObject();
	void EditSelectedObject();
	void DisplayAllColliders();
}