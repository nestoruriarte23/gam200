#include "..//..//Engine/Engine.h"

static std::string _to_load_file = "..//Resources//to_load.TXT";

static bool file_open = false;
static std::fstream open_file;
static const std::string starting_path = "..//Resources//";
static std::vector<std::string> deleted_files;
static std::vector<std::string> new_resources;

static char path_and_file_buffer[200] = { "" };

static void RemoveLine(std::string& _filename, std::fstream& _open_file, std::string& _line)
{
	ofstream temp;
	temp.open("temp.txt");
	if (!temp.is_open())
		std::cout << "Could not generate temp file" << std::endl;

	std::string other_line;
	bool first = true;

	_open_file.clear();
	_open_file.seekg(0, ios::beg);
	while (getline(_open_file, other_line))
	{
		if (other_line != _line)
		{
			if (first)
			{
				temp << other_line; first = false;
			}
			else
				temp << endl << other_line;
		}
	}

	temp.close();
	_open_file.close();
	remove(_filename);
	rename("temp.txt", _filename);
}

static int autocompleteCallback(ImGuiTextEditCallbackData *data)
{
	std::string output;
	if (data->EventFlag == ImGuiInputTextFlags_CallbackCompletion)
	{
		std::string path_and_file = path_and_file_buffer;

		/* Get the path */
		std::string _path;
		std::string _filter = path_and_file;
		size_t last_slash = path_and_file.rfind("/");
		if (last_slash != std::string::npos)
		{
			_path = path_and_file.substr(0, last_slash + 1);
			_filter = path_and_file.substr(last_slash + 1);
		}

		/* Display all elements in folder */
		unsigned count = 0, pos = 0;
		for (auto i : directory_iterator(starting_path + _path))
		{
			std::string temp = (i.path()).generic_string().substr((i.path()).generic_string().rfind("/") + 1);

			if (temp.find(_filter) != std::string::npos)
			{
				output = _path + temp;
				break;
			}
		}
	}

	if (output.find(".") == std::string::npos)
		output += "//";
	strcpy_s(data->Buf, data->BufSize, output.c_str());
	strcpy_s(path_and_file_buffer, output.c_str());
	data->BufTextLen = strlen(data->Buf);
	data->CursorPos = data->BufTextLen;
	data->BufDirty = true;

	return 0;
}

static void AppendToOpenFile(std::fstream& _file, std::string& _line)
{
	if (!_file.is_open())
		std::cout << "File not open" << std::endl;

	_file.clear();
	_file.seekg(0, ios::beg);

	if (_file.peek() == std::ifstream::traits_type::eof())
	{
		_file.clear();
		_file.seekg(0, ios::beg);
		_file << _line;
	}
	else
	{
		_file.clear();
		_file.seekg(0, ios::beg);
		_file << std::endl << _line;
	}
}

static void LoadingWindow(bool* _open)
{
	ImGui::SetNextWindowSize(ImVec2(430, 450), ImGuiCond_FirstUseEver);
	if (!ImGui::Begin("Resource Loading", _open))
	{
		ImGui::End();
		return;
	}

	/* Print starting path */
	ImGui::Text(starting_path.c_str());
	ImGui::SameLine();

	/* Recieve input */
	ImGui::InputText("", path_and_file_buffer, IM_ARRAYSIZE(path_and_file_buffer), ImGuiInputTextFlags_CallbackCompletion, autocompleteCallback);
	ImGui::Separator();
	std::string path_and_file = path_and_file_buffer;

	/* Get the path */
	std::string _path = starting_path;
	std::string _temp_path;
	std::string _filter = path_and_file;
	size_t last_slash = path_and_file.rfind("/");
	if (last_slash != std::string::npos)
	{
		_temp_path = path_and_file.substr(0, last_slash + 1);
		_path += _temp_path;
		_filter = path_and_file.substr(last_slash + 1);
	}

	/* Display all elements in folder */
	unsigned count = 0, pos = 0;
	for (auto i : directory_iterator(_path))
	{
		std::string temp = (i.path()).generic_string().substr((i.path()).generic_string().rfind("/") + 1);

		if (temp.find(_filter) != std::string::npos)
		{
			ImGui::BulletText(temp.c_str());
			if (ImGui::IsItemClicked())
			{
				std::string chosen_path = _temp_path + temp;
				if (temp.find(".") == std::string::npos)
				{
					chosen_path += "//";
					strcpy_s(path_and_file_buffer, chosen_path.c_str());
				}
				else
				{
					AppendToOpenFile(open_file, chosen_path);
					new_resources.push_back(chosen_path);
					*path_and_file_buffer = 0;
				}
				break;
			}
		}
	}

	if (KeyTriggered(Key::Enter))
	{
		if (path_and_file.find(".") != std::string::npos || path_and_file.rfind("/") == path_and_file.length() - 1)
		{
			AppendToOpenFile(open_file, path_and_file);
			new_resources.push_back(path_and_file);
			*path_and_file_buffer = 0;
		}
	}

	ImGui::NewLine();
	ImGui::Separator();
	ImGui::Columns(2, ""); // 4-ways, with border
	ImGui::Text("Saved Resources (To be loaded): ");
	ImGui::NextColumn();
	ImGui::Text("Deleted this session: ");
	ImGui::NextColumn();
	ImGui::Separator();

	/* Reset file (since we are not closing it, we have to return the
	internal iterator back to the start of the file) */
	open_file.clear();
	open_file.seekg(0, ios::beg);

	std::string line;
	if (open_file.peek() == std::ifstream::traits_type::eof())
		ImGui::Text("-empty-");
	while (getline(open_file, line))
	{
		ImGui::Text(line.c_str());
		if (ImGui::IsItemClicked())
		{
			auto position = std::find(new_resources.begin(), new_resources.end(), line);
			if (position == new_resources.end())
				deleted_files.push_back(line);
			else
				new_resources.erase(position);

			RemoveLine(_to_load_file, open_file, line);

			open_file.open(_to_load_file, std::fstream::in |
				std::fstream::out | std::fstream::app);
		}
	}

	ImGui::NextColumn();

	for (auto _str = deleted_files.begin(); _str != deleted_files.end(); ++_str)
	{
		ImGui::Text(_str->c_str());
		if (ImGui::IsItemClicked())
		{
			AppendToOpenFile(open_file, *_str);

			deleted_files.erase(_str);
			break;
		}
	}

	ImGui::Columns(1);
	ImGui::Separator();
	ImGui::NewLine();
	ImGui::Separator();
	ImGui::Text("Loaded Resources: ");
	ImGui::Separator();

	if (ImGui::TreeNode("Textures"))
	{
		auto& vec = gActualResourceMgr.GetAllFilenamesOfType(FILETYPE::TextureType);
		for (auto& i : vec)
		{
			ImGui::BulletText(i.data());
			if (ImGui::IsItemHovered())
			{
				ImGui::BeginTooltip();
				ImGui::Image((void*)(intptr_t)(gActualResourceMgr.GetResource<Texture>(i)->Get()->mHandle), ImVec2(80, 80), ImVec2(0, 0),
					ImVec2(1, -1), ImVec4(1.0f, 1.0f, 1.0f, 1.0f), ImVec4(1.0f, 1.0f, 1.0f, 0.5f));
				ImGui::EndTooltip();
			}
		}
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Animations"))
	{
		auto& animations = gActualResourceMgr.GetAllFilenamesOfType(FILETYPE::AnimationType);
		for (auto& i : animations)
		{
			ImGui::BulletText(i.data());
		}
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Sounds"))
	{
		auto& vec = gActualResourceMgr.GetAllFilenamesOfType(FILETYPE::SoundType);
		for (auto& i : vec)
		{
			ImGui::BulletText(i.data());
		}
		ImGui::TreePop();
	}

	ImGui::End();
}

void SelectResourcesToLoad() {

	static bool loading_data = false;

	if (ImGui::MenuItem("Resources", NULL, &loading_data)) {

		if (!file_open)
		{
			open_file.open(_to_load_file, std::fstream::in | std::fstream::out | std::fstream::app);
			if (open_file.is_open())
				file_open = true;
			else
				std::cout << "Could not open file" << std::endl;

			new_resources.clear();
		}
		else
		{
			open_file.close();
			file_open = false;
			if (!new_resources.empty())
			{
				for (auto filename : new_resources)
					gActualResourceMgr.LoadResource("..//Resources//" + filename);
				new_resources.clear();
			}
		}
	}

	if (loading_data && file_open) LoadingWindow(&loading_data);
	if (!loading_data && file_open)
	{
		open_file.close();
		file_open = false;
		std::cout << "File Closed" << std::endl;
		if (!new_resources.empty())
		{
			for (auto filename : new_resources)
				gActualResourceMgr.LoadResource("..//Resources//" + filename);
			new_resources.clear();
		}
	}
}