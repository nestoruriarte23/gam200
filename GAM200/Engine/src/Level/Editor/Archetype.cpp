#include "Archetype.h"

std::string editArchetype;

void Archetype::AddArchetype(std::string _name, GameObject* _archetypeObject) {

	if (!_archetypeObject->mbArchetype) {
		_archetypeObject->mbArchetype = true;
		_archetypeObject->mArchetypeName = _name;
	}

	serializer->SaveArchetype(_name.data(), _archetypeObject);

	//UpdateArchetypes(_name);
}


void Archetype::CreateArchetype(std::string _name) {
	GameObject * go = Game->CreateArchetype(_name);//DBG_NEW GameObject();

	//serializer->LoadArchetype(_name.c_str(), go);

	go->mPosition = Camera::Instance()->mPos;

	Editor::goVect.push_back(go);
	go->mbArchetype = true;
	go->mArchetypeName = _name;

	AddToArchetype(go, _name);

	//Game->CreateObject(go);
	undo->SaveOldObject(go, go->GetUID(), (int)Action::ADDOBJECT);
}


void Archetype::RemoveFromArchetype(GameObject& _go, std::string& _name) {
	std::vector<GameObject*>::iterator pos =  std::find_if(Editor::archetypeObjects[_name].begin(), Editor::archetypeObjects[_name].end(), [&](GameObject*_searchGo) {
		return _searchGo->GetUID() == _go.GetUID();
	});
	Editor::archetypeObjects[_name].erase(pos, pos+1);
	_go.mArchetypeName = "";
	_go.mbArchetype = false;
}

void Archetype::AddToArchetype(GameObject* _go, std::string& _name) {
	
	Editor::archetypeObjects[_name].push_back(_go);

}

void Archetype::UpdateArchetypes(std::string _name) {
	//TODO get all object with the archetype name
	//update all the components except positions and uncommont componets
	GameObject go;
	serializer->LoadArchetype(_name.data(), &go);
	for (GameObject * updateGo : Editor::archetypeObjects[_name]) {
		updateGo->UpdateArchetype(go);
	}
}

void Archetype::ReloadAll() {
	
	for (auto i : std::experimental::filesystem::directory_iterator("..//Resources//Archetypes//")) {
		GameObject go;

		
		std::string archetype = (i.path()).generic_string();
		archetype.erase(0, 27);
		archetype.erase(archetype.size() - 5, archetype.size());
		

		serializer->LoadArchetype(archetype.data(), &go);

		serializer->SaveArchetype(archetype.data(), &go);
	}



}

////////////Archetype edit structure/////////////////

void OnEditArchetype::Edit(bool * _opened) {
	if (!_opened) return;

	ImGui::SetNextWindowSize(ImVec2(700, 550), ImGuiCond_Once);
	ImGui::SetNextWindowSizeConstraints(ImVec2(-1, -1), ImVec2(-1, -1));
	ImGui::SetNextWindowBgAlpha(1.0f);

	ImGui::Begin("Archetype edition", _opened);


	ImGui::Columns(2);
	{
		if (editArchetype != "") {
			GameObject go;

			serializer->LoadArchetype(editArchetype.data(), &go);

			bool changed = EditObject(go);


			if (changed) {
				serializer->SaveArchetype(editArchetype.data(), &go);
				Archetype::UpdateArchetypes(editArchetype);
			}
		}
	}

	ImGui::NextColumn();
	{
		std::vector<const char *> names;
		
		for (auto it = Editor::archetypes.begin(); it != Editor::archetypes.end(); ++it) {
			names.push_back(it->c_str());
		}

		ImGui::PushID("Archetypes");
		int archetype = Editor::archetypes.size();

		if (archetype != 0){
			ImGui::PushItemWidth(-1);
			if (ImGui::ListBox("", &archetype, &names[0], names.size(), 25)) {
				editArchetype = names[archetype];
			}
		}
	}
	ImGui::PopID();
	ImGui::End();

}

bool OnEditArchetype::EditObject(GameObject& _go) {

	bool changed = false;
	if (ImGui::TreeNode("Transform"))
	{
		changed = ImGui::InputFloat2(" Position", _go.mPosition.v) || changed;
		changed = ImGui::InputFloat2(" Scale", _go.mScale.v) || changed;
		changed = ImGui::SliderFloat(" Rotation", &_go.mRotation, 0.0f, 2 * PI) || changed;
		changed = ImGui::DragFloat2(" mSortingPos", _go.mSortingPos.v) || changed;

		ImGui::TreePop();
	}

	//ImGui::Separator();
	int tag = _go.mTag;
	const char* dyn[6] = { "Enemy", "Player", "Miscellaneous", "Hazard", "GameObject", "Pickable" };

	if (ImGui::Combo("Object Tag", &tag, dyn, 6, 7)) {
		changed = true;
		switch (tag)
		{
		case 0:
			_go.mTag = Enemies;
			break;
		case 1:
			_go.mTag = Player;
			break;
		case 2:
			_go.mTag = Miscellaneous;
			break;
		case 3:
			_go.mTag = Hazard;
			break;
		case 4:
			_go.mTag = GameObjects;
			break;
		case 5:
			_go.mTag = Pickable;
			break;
		}
	}

	//ImGui::Separator();
	for (unsigned i = 0; i < _go.GetCompCount(); i++) {
		changed = _go.GetComp(i)->Edit() || changed;
		//ImGui::Separator();
	}


	if (ImGui::TreeNode("AddComponent"))
	{
		//ImGuiTextFilter filter;
		for (unsigned i = 0; i < Editor::compNames.size(); ++i)
		{
			ImGui::Bullet();
			ImGui::Text(Editor::compNames[i]);
			if (ImGui::IsItemClicked())
			{
				_go.AddComp(factory->Create(Editor::compNames[i]))->Initialize();
				changed = true;
			}
		}
		ImGui::TreePop();
	}


	return changed;

}