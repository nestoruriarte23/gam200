#include "RegionSelector.h"
Vector2* RegionFirstPos = nullptr;
Vector2 RegionSecondPos;
Area * area;
bool newText;
bool edit = false;

void RegionSelector::SelectRegion() {
	ImGuiIO& io = ImGui::GetIO();
	if (!io.WantCaptureMouse) {
		if (MouseDown(MouseInput::LEFT) && KeyDown(Key::Control) && !edit) {
			if (!RegionFirstPos) {
				RegionFirstPos = DBG_NEW Vector2(InputManager->GetMouse());
			}

			RegionSecondPos = InputManager->GetMouse();
			float width = RegionFirstPos->x - RegionSecondPos.x;
			float height = RegionFirstPos->y - RegionSecondPos.y;

			Vector2 pCenter{ RegionFirstPos->x - (width / 2), RegionFirstPos->y - (height / 2) };

			RenderManager::Instance()->DrawOrientedRectangleAt(pCenter, width, height, 0, Engine::Color::white);
		}
		if (MouseReleased(MouseInput::LEFT) && RegionFirstPos && !edit) {
			area = new Area("", *RegionFirstPos, RegionSecondPos);
			delete RegionFirstPos;
			RegionFirstPos = nullptr;
			edit = true;
		}
	}
}

char tempName[200] = "";

void RegionSelector::Edit() {
	if (!edit) return;

	Vector2 vectorPos = InputManager->GetMouse();

	ImGui::SetNextWindowSize(ImVec2(700, 550), ImGuiCond_Once);
	ImGui::SetNextWindowSizeConstraints(ImVec2(-1, -1), ImVec2(-1, -1));
	ImGui::SetNextWindowPos(ImVec2(vectorPos.x, vectorPos.y), ImGuiCond_Once);
	ImGui::SetNextWindowBgAlpha(1.0f);

	ImGui::Begin("Region selector", &edit);

	//Region::regions;


	if (ImGui::Button("Add new text")){
		newText = true;
	}

	if (newText) {
		ImGui::PushID("newText");
		
		ImGui::InputText("", tempName, IM_ARRAYSIZE(tempName));

		ImGui::SameLine();
		
		if (ImGui::Button("Add")) {
			newText = false;
			area->name = tempName;
			RegionInst->AddRegion(area);
			area = nullptr;
			memset(tempName, 0, 200);
			edit = false;
		}
		ImGui::PopID();
	}

	std::map<std::string, Area*>::iterator it;

	ImGui::PushID("interaction text");
	unsigned i = 0;
	for (it = RegionInst->regions.begin(); it != RegionInst->regions.end();  it++, i++) {

		ImGui::PushID(i);

		if(ImGui::Button(it->first.data())) {
			area->name = tempName;
			RegionInst->AddRegion(area);
			area = nullptr;
			edit = false;
		}

		ImGui::PopID();
	}
	ImGui::PopID();
	ImGui::End();
}


void RegionSelector::ViewRegions() {

	std::map<std::string, Area*>::iterator it;
	for (it = RegionInst->regions.begin(); it != RegionInst->regions.end(); it++) {

		Vector2 first = it->second->first;
		Vector2 last = it->second->last;
		float width = first.x - last.x;
		float height = first.y - last.y;

		Vector2 pCenter{ first.x - (width / 2), first.y - (height / 2) };

		RenderManager::Instance()->DrawOrientedRectangleAt(pCenter, width, height, 0, Engine::Color::white);
	}


}