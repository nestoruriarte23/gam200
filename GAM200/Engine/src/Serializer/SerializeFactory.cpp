#include <map>
#include <direct.h>
#include <iostream>
#include <fstream>

#include  "SerializeFactory.h"
#include "..//System/GameObjectManager/GameObject.h"
#include ".//..//Level//Editor/Grid.h"
#include "..//Level/Scene/Scene.h"
#include "..//Level/Space/Space.h"
#include "..//Utilities/MyDebug/MyDebug.h"
#include "..//Physics/RigidBody.h"
#include "..//Graphics/RenderableComp.h"
#include "..//Graphics/Outline/GetOutlined.h"
#include "..//Graphics/Particles/ParticleProperties.h"
#include "..//Graphics/SpineComp.h"
#include "..//Graphics/Text/Text.h"
#include "..//Sound/Audio.h"
#include "..//StateMachines/State.h"
#include "..//StateMachines/StateMachine.h"
#include "..//..//GamePlay/src/LogicComponents/CameraSync.h"
#include "..//..//GamePlay/src/PlayerLogic/PlayerLogic.h"
#include "..//..//GamePlay/src/PlayerLogic/PlayerAnimation.h"
#include "..//..//GamePlay/src/LogicComponents/Spawner/Spawner.h"
#include "..//..//GamePlay/src/LogicComponents/ChangeAnimationsComponent/AnimationChangeComponent.h"
#include "..//..//GamePlay/src/LogicComponents/InteractionComponent/InteractionComponent.h"
#include "..//..//GamePlay/EnemyLogic.h"
#include "..//..//GamePlay/src/PlayerLogic/PlayerLogic.h"
#include "..//..//GamePlay/src/LogicComponents/HealthComponent/HealthComponent.h"
#include <direct.h>
#include <iostream>
#include <fstream>

#include "../../GamePlay/EnemyLogic.h"
#include "../../GamePlay/src/CompanionLogic/CompanionLogic.h"
#include "../../GamePlay/src/LogicComponents/HealthComponent/HealthComponent.h"
#include "../../GamePlay/src/LamiaLogic/LamiaProjectile.h"
#include "../../GamePlay/src/GaltxaLogic/GaltxagorriLogic.h"
#include "../../GamePlay/src/SorginaLogic/SorginaLogic.h"
#include "../../GamePlay/src/SorginaLogic/SorginaMagic.h"
#include "../../GamePlay/src/ClosedAreas/ClosedAreas.h"

#include "../../GamePlay/src/HUDLogic/HUDLogic.h"
#include "../../GamePlay/src/CameraLogic/CameraLogic.h"
#include "../../GamePlay/src/TitleScreenLogic/GameInitialization.h"

using json = nlohmann::json;

static std::string levelPath = "../Resources/Levels/";
static std::string archetypePath = "../Resources/Archetypes/";
static std::string metaPath = "../Resources/Metadata/";
static std::string stackPath = "../Resources/temp/";

Factory::Factory() {
}

Factory::~Factory()
{
	for (auto creator : creators)
	{
		delete creator.second;
	}
}

void Factory::Initialize()
{
	Register<CameraComp>();
	Register<BoxCollider>();
	Register<RigidBody>();
	Register<Sprite>();
	Register<Shadow>();
	Register<ShadowEmitter>();
	Register<GetOutlined>();
	Register<PlayerMovement>();
	Register<SpineAnimation>();
	Register<GaltzagorriSpawner>();
	Register<BackgroundCamSync>();
	Register<SoundEmitter>();
	Register<SoundListener>();
	Register<Actor>();
	Register<AnimationChange>();
	Register<ParticleSystem>();
	Register<EnemyLogic>();
	Register<PlayerInfo>();
	Register<Grid::FloorGrid>();
	Register<Health>();
	Register<DeathParticles>();
	Register<ExplosionLogic>();
	Register<GaltzagorriLogic>();
	Register<SorginaLogic>();
	Register<LamiaProjectileLogic>();
	Register<MagicChaseLogic>();
	Register<PlayerAnimationChange>();
	Register<HealthBar>();
	Register<CompanionBar>();
	Register<TackleLogic>();
	Register<TitleScreenLogic>();
	Register<TextComp>();
	Register<FaceSprite>();
	Register<InteractionComponent>();
	Register<DestroyableObject>();
	Register<CameraJoint>();
	Register<TriggerLogic>(); 
	Register<DestroyObjectTime>();
	Register<TornadoLogic>();
	Register<ClosedAreas>();
}

void Factory::AddComponent(const char * _name, ICreator* _creator) {
	if (creators.find(_name) == creators.end()){
		creators.insert(std::pair<std::string, ICreator*>(_name, _creator));
	}
}

IComp* Factory::CreateComponent(const char * _typename) {
	// call _new somewhere here... 
	// from the typename, get the actual type so you can call new T
	auto creatorIt = creators.find(_typename);
	if (creatorIt != creators.end())
		return creatorIt->second->Create();
	return NULL;
}

void Factory::LoadFromJson(std::vector<GameObject*> & _go, json & _j) {
	


	//json & comps = *val.find("comps");
	for (auto it = _j.begin(); it != _j.end(); ++it)
	{
		json & compVal = *it;
		GameObject * go = DBG_NEW GameObject();
 		go->FromJson(compVal["GameObject"]);
		_go.push_back(go);
	}
	
	//Base * g = CreateComponent
}

void Factory::SaveToJson(std::vector<GameObject*> & _go, json & _j) {
	for (GameObject* go : _go)
	{
		json goJson;
		go->ToJson(goJson);
		_j.push_back(goJson);
	}
	std::cout << "";
}

void Factory::LoadFromJson(GameObject & _go, json & _j) {

	//for (auto it = _j.begin(); it != _j.end(); ++it){
		//json & compVal = *it;
		//_go.FromJson(compVal["GameObject"]);
	if (_j.find("GameObject") != _j.end()) {
		_go.FromJson(_j["GameObject"]);
	} else {
		_go.FromJson(_j);
	}
	//}	
}

//TODO(thomas): This is redundant. 
void Factory::SaveToJson(GameObject & _go, json & _j) {
	_go.ToJson(_j);
}

void Factory::LoadFromJson(Scene & _s, nlohmann::json & j) {
	_s.FromJson(j);
}

Serializer::Serializer() {
}

void Serializer::LoadLevel(const char * _levelName, std::vector<GameObject*> & _goVect) {
	json j;

	std::string path;
	path += levelPath;
	path += _levelName;
	path += ".json";

	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();
		factory->LoadFromJson(_goVect, j);
	}
}

void Serializer::LoadArchetype(const char * _archetypeName, GameObject* _go) {

	json j;

	std::string path;
	path += archetypePath;
	path += _archetypeName;
	path += ".json";
	   
	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();
		factory->LoadFromJson(*_go, j);
	}
}

void Serializer::LoadMeta(const char * _metaName, GameObject* _go) {


}

int Serializer::LoadStackUndo(const char * _objectName, GameObject* _go, int & _action) {

	json j;

	std::string path;
	path += stackPath;
	path += _objectName;
	path += ".json";

	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();
		factory->LoadFromJson(*_go, j);
		remove(path.c_str());
		_action = j["action"];
		_go->mSceneSpace = j["space"].get<std::string>();
		return j["ID"];
	}
	return NULL;
}

int Serializer::LoadStackUndo(const char * _objectName, std::vector<std::pair<int, Vector2>>& _objects) {

	json j;

	std::string path;
	path += stackPath;
	path += _objectName;
	path += ".json";

	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();
		for (auto k = j["Objs"].begin(); k != j["Objs"].end(); k++) {
			Vector2 v;
			v.FromJson((*k)["mPosition"]);
			_objects.push_back(std::pair<int, Vector2>((*k)["id"], v));
		}
		remove(path.c_str());
	}
	return NULL;
}

int Serializer::LoadStackUndo(const char * _objectName, std::vector<GameObject*>& _objects) {

	json j;

	std::string path;
	path += stackPath;
	path += _objectName;
	path += ".json";

	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();
		for (auto k = j["GameObjects"].begin(); k != j["GameObjects"].end(); k++) {
			GameObject* go = DBG_NEW GameObject();
			go->FromJson((*k)["GameObject"]);
			_objects.push_back(go);
		}
		remove(path.c_str());
	}
	return NULL;
}

std::vector<GameObject*> Serializer::LoadLevel(const char * _levelName, Scene * _s) {

	json j;

	std::string path;
	path += levelPath;
	path += _levelName;
	path += ".json";

	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();
		return (_s->FromJson(j));
	}
	std::vector<GameObject*> dummy;
	return dummy;
}

void LoadLevel(const char * _levelName, Scene * _s) {
	//json js

	json j;

	std::string path;
	path += levelPath;
	path += _levelName;
	path += ".json";

	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();
		factory->LoadFromJson(*_s, j);
	}

}


void Serializer::SaveLevel(const char * _levelName, std::vector<GameObject*> & _goVect) {
	json j;
	factory->SaveToJson(_goVect, j);

	std::string path;
	path += levelPath;
	path += _levelName;
	path += ".json";

	std::ofstream outFile(path.c_str());
	if (outFile.good() && outFile.is_open()) {
		outFile << std::setw(4) << j;
		outFile.close();
	}
}

void Serializer::SaveArchetype(const char * _archetypeName, GameObject* _go) {
	json j;
	//factory->SaveToJson(*_go, j);
	_go->ToJson(j);

	std::string path;
	path += archetypePath;
	path += _archetypeName;
	path += ".json";

	std::ofstream outFile(path.c_str());
	if (outFile.good() && outFile.is_open()) {
		outFile << std::setw(4) << j;
		outFile.close();
	}
}

void Serializer::SaveMeta(const char * _metaName, GameObject* _go){

}

void Serializer::SaveLevel(const char * _levelName, Scene * _s) {
	json j;
	//factory->SaveToJson(*_go, j);
	_s->ToJson(j);

	std::string path;
	path += levelPath;
	path += _levelName;
	path += ".json";

	std::ofstream outFile(path.c_str());
	if (outFile.good() && outFile.is_open()) {
		outFile << std::setw(4) << j;
		outFile.close();
	}
}

void Serializer::SaveStackUndo(const char * _objectName, GameObject* _go, int _id, int _action) {

	json j;
	//factory->SaveToJson(*_go, j);
	_go->ToJson(j);

	j["ID"] = _id;
	j["action"] = _action;
	j["space"] = _go->mSceneSpace;

	_mkdir("../Resources/temp");

	std::string path;
	path += stackPath;
	path += _objectName;
	path += ".json";

	std::ofstream outFile(path.c_str());
	if (outFile.good() && outFile.is_open()) {
		outFile << std::setw(4) << j;
		outFile.close();
	}

}

void Serializer::SaveStackUndo(const char * _objectName, std::vector<std::pair<int, Vector2>>& _objects, int _action) {
	json j;

	j["action"] = _action;

	for (std::pair<int, Vector2> _pair : _objects) {
		json k;
		k["id"] = _pair.first;
		_pair.second.ToJson(k["mPosition"]);
		j["Objs"].push_back(k);
	}

	_mkdir("../Resources/temp");

	std::string path;
	path += stackPath;
	path += _objectName;
	path += ".json";

	std::ofstream outFile(path.c_str());
	if (outFile.good() && outFile.is_open()) {
		outFile << std::setw(4) << j;
		outFile.close();
	}

}

void Serializer::SaveStackUndo(const char * _objectName, std::vector<GameObject*> & _goVect, int _action) {
	json j;

	j["action"] = _action;

	std::string path;
	path += stackPath;
	path += _objectName;
	path += ".json";

	std::ofstream outFile(path.c_str());

	if (outFile.good() && outFile.is_open()) {
		factory->SaveToJson(_goVect, j["GameObjects"]);
		outFile << std::setw(4) << j;
		outFile.close();
	}

}

//LEAKS HERE
void PropertyMap::FromJson(nlohmann::json & j, PropertyMap& map)
{
	nlohmann::json & properties = *j.find("Properties");

	for (auto it = properties.begin(); it != properties.end() && properties.size() != 0; ++it)
	{
		std::string key = (it.value()).begin().key();

		value = (*it)[key]["code"];
		if (value == Type_Int)
		{
			Property<int>* temp_int = DBG_NEW Property<int>();

			temp_int->FromJson((*it)[key]["value"]);

			map.properties[key] = temp_int;
		}

		if (value == Type_Float)
		{
			Property<float>* temp_float = DBG_NEW Property<float>();

			temp_float->FromJson((*it)[key]["value"]);

			map.properties[key] = temp_float;
		}

		if (value == Type_Double)
		{
			Property<double>* temp_double = DBG_NEW Property<double>();

			temp_double->FromJson((*it)[key]["value"]);

			map.properties[key] = temp_double;
		}

		if (value == Type_Bool)
		{
			Property<bool>* temp_bool = DBG_NEW Property<bool>();

			temp_bool->FromJson((*it)[key]["value"]);

			map.properties[key] = temp_bool;
		}

		if (value == Type_String)
		{
			Property<std::string>* temp_string = DBG_NEW Property<std::string>();

			temp_string->FromJson((*it)[key]["value"]);

			map.properties[key] = temp_string;
		}

		if (value == Type_Unknown)
		{
			ASSERT(value == Type_Unknown);
		}
	}

}

void PropertyMap::ToJson(nlohmann::json & j)
{
	for (auto prop : properties)
	{
		nlohmann::json j_;
		prop.second->ToJson(j_[prop.first]);
		j.push_back(j_);
	}
}