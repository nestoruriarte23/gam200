#pragma once
//#ifndef 0x0F00
#include <utility>
#include <vector>
#include <unordered_map>
#include "json.hpp"
#include "../Components/System.h"
#include "../Utilities/MyDebug/LeakDetection.h"
#include "../Utilities/Math/MyMath.h"

enum Tags
{
	Enemies,
	Player,
	Miscellaneous,
	Hazard,
	GameObjects,
	Pickable,
	Breakable,
	Projectile,
	TextObject
};

enum Types
{
	Type_Int,
	Type_Float,
	Type_Double,
	Type_Bool,
	Type_String,
	Type_Unknown
};


class IBase;
class IComp;
class GameObject;
struct Scene;
struct Space;

class ICreator
{
public:
	virtual IComp * Create() = 0;
};

template <typename T>
class TCreator : public ICreator
{
public:
	virtual IComp * Create() {
		return DBG_NEW T();
	}
};

class Factory {
	MAKE_SINGLETON(Factory);
private:
	std::map<std::string, ICreator*> creators;
public:

	~Factory();

	void Initialize();

	void Register(const char * typeName, ICreator * creator)
	{
		if (creators.find(typeName) == creators.end())
			creators[typeName] = creator;
	}

	IComp * Create(const char * typeName)
	{
		// IMPORTANT: FIND THE CREATOR HERE
		if (creators.find(typeName) != creators.end())
			return creators[typeName]->Create();
		// NO CREATOR REGISTERED
		return NULL;
	}

	template <typename T> void Register() {
		Register(T::TYPE().GetName(), DBG_NEW TCreator<T>());
		// TODO: handle duplicate creator (avoid memory leaks)
	}


	template <typename T> T* Create() {
		return dynamic_cast<T*>(Create(T::TYPE().GetName()));
	}

	void AddComponent(const char * typeName, ICreator* c);

	IComp* CreateComponent(const char * typeName);

	void LoadFromJson(std::vector<GameObject*> & go,  nlohmann::json & j);

	void SaveToJson(std::vector<GameObject*> & go, nlohmann::json & j);

	void LoadFromJson(GameObject & go, nlohmann::json & j);

	void LoadFromJson(Scene & _s, nlohmann::json & j);

	void SaveToJson(GameObject & go, nlohmann::json & j);

	// basic types - write
	void ToJson(nlohmann::json &j, const int & val)         { j["value"] = val;  j["code"] = Type_Int;   }
	void ToJson(nlohmann::json &j, const float & val)       { j["value"] = val;  j["code"] = Type_Float; }
	void ToJson(nlohmann::json &j, const double & val)      { j["value"] = val;  j["code"] = Type_Double;}
	void ToJson(nlohmann::json &j, const bool & val)        { j["value"] = val;  j["code"] = Type_Bool;  }
	void ToJson(nlohmann::json &j, const std::string & val) { j["value"] = val;  j["code"] = Type_String;}


	// basic types - read
	void FromJson(const nlohmann::json &j, int & val)         { val = j; }
	void FromJson(const nlohmann::json &j, float & val)       { val = j; }
	void FromJson(const nlohmann::json &j, double & val)      { val = j; }
	void FromJson(const nlohmann::json &j, bool & val)        { val = j; }
	void FromJson(const nlohmann::json &j, std::string & val) { val = j.get<std::string>(); }

};

class Serializer {
	MAKE_SINGLETON(Serializer);
	public:

		std::vector<GameObject*> LoadLevel(const char * _levelName, Scene * _s);
		void LoadLevel(const char * _levelName, std::vector<GameObject*> & _goVect);
		void LoadArchetype(const char * _archetypeName, GameObject* _go);
		void LoadMeta(const char * _metaName, GameObject* _go);
		int LoadStackUndo(const char * _objectName, GameObject* _go, int & _action);
		int LoadStackUndo(const char * _objectName, std::vector<std::pair<int, Vector2>>& _objects);
		int LoadStackUndo(const char * _levelName, std::vector<GameObject*> & _goVect);

		void SaveLevel(const char * _levelName, std::vector<GameObject*> & _goVect);
		void SaveLevel(const char * _levelName, Scene * _s);
		void SaveArchetype(const char * _archetypeName, GameObject* _go);
		void SaveMeta(const char * _metaName, GameObject* _go);
		void SaveStackUndo(const char * _objectName, GameObject* _go, int _id, int _action);
		void SaveStackUndo(const char * _objectName, std::vector<std::pair<int, Vector2>>& _objects, int _action);
		void SaveStackUndo(const char * _levelName, std::vector<GameObject*> & _goVect, int _action);
};

struct ISerializable {

	virtual void FromJson(nlohmann::json & j)  = 0;
	virtual void ToJson (nlohmann::json & j) const = 0;
};

struct PropertyMap
{
	std::map<std::string, ISerializable*> properties;

	void FromJson(nlohmann::json & j, PropertyMap& map);

	void ToJson(nlohmann::json & j);

	Types value = Type_Unknown;

};

template<typename T>
struct Property : public ISerializable
{
	Property() {}
	~Property() {}
	Property(const T& value) : variable(value) {}
	Property(const char * name, PropertyMap& prop_map);
	Property(const char * name, const T &value, PropertyMap& prop_map) : Property(name, prop_map) { variable = value; }

	T variable;

	operator T() const { return variable; }
	T& operator=(const T& rhs) { variable = rhs; return variable; }
	T* operator&() { return &variable; }

	template<typename U>
	bool operator==(const U& _rhs) const { return _rhs == variable; }

	T* Get() { return static_cast<T*>(&variable); }

	void FromJson(nlohmann::json & j)  { variable = j; }
	void ToJson(nlohmann::json & j)const { Factory::Instance()->ToJson(j, variable); }

};

template<typename T>
inline Property<T>::Property(const char* name, PropertyMap& prop_map)
{
	// dont' allow duplicates
	if (prop_map.properties.find(name) == prop_map.properties.end())
		prop_map.properties[name] = this;
}

#define PROP_Explicit(Type_, Name_, Properties_)     Property<Type_> Name_ {#Name_, Properties_}
#define PROP_Val_Explicit(Type_, Name_, Val_, Properties_) Property<Type_> Name_ {#Name_,Val_, Properties_ }

#define PROP(Type_, Name_) PROP_Explicit(Type_, Name_, properties)
#define PROP_Val(Type_, Name_, Val_) PROP_Val_Explicit(Type_, Name_, Val_, properties)

#define factory (Factory::Instance())
#define serializer (Serializer::Instance())
//#endif // !0x0F00
