#include "GameStateManager.h"
#include "../Graphics/Particles/Particles.h"
#include "../Input/Input.h"
//#include "..//Level/Level.h"
#include "..//Level//Editor/Editor.h"

GameStateManager::GameStateManager() : previousLevel(0), currentLevel(0), nextLevel(0), isRunning(false), level(nullptr), pause(false) {}

GameStateManager::~GameStateManager() 
{ 
	for (unsigned i = 0; i < all_Levels.size(); i++)
		delete all_Levels[i];
}

bool GameStateManager::Initialize(unsigned initialLevel)
{
	int count = 0;
	for (std::vector<unsigned>::iterator it = level_ID.begin(); it != level_ID.end(); ++it, ++count)
	{
		if (*it == initialLevel)
		{
			previousLevel = count;
			currentLevel = count;
			nextLevel = count;
			isRunning = true;
			level = all_Levels[count];

			return true;
		}
	}

	return false;
}

bool GameStateManager::AddLevel(unsigned levelID, Level * level)
{
	if (std::find(level_ID.begin(), level_ID.end(), levelID) != level_ID.end())
		return false;

	level_ID.push_back(levelID);
	all_Levels.push_back(level);

	return true;
}

void GameStateManager::Load() { level->Load(); }

void GameStateManager::Init() 
{
	InputManager->Initialize();
	level->Initialize(); 
	level->demo_scene->Initialize();
}

void GameStateManager::Update() { level->Update(); }

void GameStateManager::Render() { level->Render(); }

void GameStateManager::Free() 
{ 
	level->Free();
}

void GameStateManager::Unload() { level->Unload(); }

void GameStateManager::GoToLevel(std::string _level)
{
	Editor::LevelName = _level;
	nextLevel = RESTART;
}

void GameStateManager::SetLevel(unsigned _level)
{
	if (_level == QUIT_GAME)
		nextLevel = QUIT_GAME;
	else
	{
		int count = 0;
		for (std::vector<unsigned>::iterator it = level_ID.begin(); it != level_ID.end(); ++it, ++count)
		{
			if (*it == _level)
			{
				level = all_Levels[count];
			}
		}
	}
}

void GameStateManager::HandleEvents()
{
	// Create the event variable (holds any event)
	SDL_Event event;
	// For each current event
	while (SDL_PollEvent(&event))
	{
		// Sort by event type
		switch (event.type)			// Determine its type
		{

		#pragma region // QUIT_GAME_WITH_CROSS
		case SDL_QUIT:
			nextLevel = QUIT_GAME;	// Force gamestate change
			isRunning = false;		// Quit the game (bool for active game loop)
			break;
		#pragma endregion

		#pragma region // MOUSE INPUT
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			InputManager->HandleMouseEvent(event);
			break;
		case SDL_MOUSEWHEEL:
			InputManager->HandleMouseWheel((float)event.wheel.y);
			break;
		#pragma endregion

		#pragma region // KEYBOARD INPUT
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			InputManager->HandleKeyEvent(event);
			break;
		#pragma endregion

		#pragma region // GAMEPAD INPUT
		case SDL_CONTROLLERBUTTONDOWN:
		case SDL_CONTROLLERBUTTONUP:
			InputManager->HandleEvent(event);
			break;

		case SDL_CONTROLLERAXISMOTION:
			InputManager->HandleAxisEvent(event);
			break;
		#pragma endregion

		default:
			break;
		}
	}
}