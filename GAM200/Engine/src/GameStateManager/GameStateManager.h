#pragma once
#include <vector>
#include "SDL.h"
#include "../Components/System.h"

struct Level;

class GameStateManager
{
	MAKE_SINGLETON(GameStateManager);

public:
	~GameStateManager();
	bool Initialize(unsigned initialLevel);
	bool AddLevel(unsigned levelID, Level* level);

	void Load();
	void Init();
	void Update();
	void Render();
	void Free();
	void Unload();

	void GoToLevel(std::string _level);

	void SetLevel(unsigned _level);

	void HandleEvents();		// Wrapper around SDL handeling

	//Pause gettor and settor
	bool isPaused() { return pause; }
	void setPause(bool state) { pause = state; }

	unsigned currentLevel;
	unsigned previousLevel;
	unsigned nextLevel;
	bool isRunning;
	Level* level;

private:
		
	std::vector<Level*> all_Levels;
	std::vector<unsigned> level_ID;
	bool pause;
};


#define GSM (GameStateManager::Instance())

#define RESTART 69
#define QUIT_GAME 420
#define DEMO_LEVEL 0
#define AUDIO_LEVEL 1
#define EDITOR_LEVEL 2
#define ENGINE_PROOF_LEVEL 3
