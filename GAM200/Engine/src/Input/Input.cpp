#include "Input.h"
#include "../Game/Game.h"
#include "../Graphics/Camera.h"
#include "../Utilities/Time/FrameTime.h"
#include "InputEvent.h"
#include "..//EventSystem/EventDispatcher.h"

#pragma region // General Input Set Up //

InputHandler::InputHandler() {}

void InputHandler::Initialize()
{
	// Set both Current and previous arrays of booleans to false
	for (unsigned i = 0; i < MOUSE_KEY_AMOUNT; ++i)
	{
		mMouseCurrent[i] = 0;
		mMousePrevious[i] = 0;
	}

	for (unsigned i = 0; i < KEYBOARD_KEY_AMOUNT; ++i)
	{
		mKeyCurrent[i] = 0;
		mKeyPrevious[i] = 0;
	}

	mPreviusButton[SDL_CONTROLLER_BUTTON_A] = 0;			mCurrentButton[SDL_CONTROLLER_BUTTON_A] = 0;
	mPreviusButton[SDL_CONTROLLER_BUTTON_B] = 0;			mCurrentButton[SDL_CONTROLLER_BUTTON_B] = 0;
	mPreviusButton[SDL_CONTROLLER_BUTTON_X] = 0;			mCurrentButton[SDL_CONTROLLER_BUTTON_X] = 0;
	mPreviusButton[SDL_CONTROLLER_BUTTON_Y] = 0;			mCurrentButton[SDL_CONTROLLER_BUTTON_Y] = 0;
	mPreviusButton[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = 0;	mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = 0;
	mPreviusButton[SDL_CONTROLLER_BUTTON_DPAD_UP] = 0;		mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_UP] = 0;
	mPreviusButton[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = 0;	mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = 0;
	mPreviusButton[SDL_CONTROLLER_BUTTON_START] = 0;		mCurrentButton[SDL_CONTROLLER_BUTTON_START] = 0;
	mPreviusButton[SDL_CONTROLLER_BUTTON_LEFTSHOULDER] = 0;		mCurrentButton[SDL_CONTROLLER_BUTTON_LEFTSHOULDER] = 0;
	mPreviusButton[SDL_CONTROLLER_BUTTON_RIGHTSHOULDER] = 0;		mCurrentButton[SDL_CONTROLLER_BUTTON_RIGHTSHOULDER] = 0;

	mMouseWheel = 0.0f;
	mbCapsLocked = false;
}
void InputHandler::StartFrame()
{
	lapse += TimeManager::Instance()->GetDt();
	/* Get the raw inout data from the mouse pos in window (0,0) -> Top - Left */
	SDL_GetMouseState(&RawMouse[0], &RawMouse[1]);

	/* Transform mouse input from Window Space to World space (TODO) */
	Vector2 WinSize = Game->GetWindowSize();

	gMouse.x = RawMouse[0] - WinSize.x / 2;		
	gMouse.y = - RawMouse[1] + WinSize.y / 2;

	/* Reset the Current and Previous arrays */
	for (unsigned i = 0; i < MOUSE_KEY_AMOUNT; ++i)
		mMousePrevious[i] = mMouseCurrent[i];

	for (unsigned i = 0; i < KEYBOARD_KEY_AMOUNT; ++i) 
		mKeyPrevious[i] = mKeyCurrent[i];

	mPreviusButton[SDL_CONTROLLER_BUTTON_A] = mCurrentButton[SDL_CONTROLLER_BUTTON_A];
	mPreviusButton[SDL_CONTROLLER_BUTTON_B] = mCurrentButton[SDL_CONTROLLER_BUTTON_B];
	mPreviusButton[SDL_CONTROLLER_BUTTON_X] = mCurrentButton[SDL_CONTROLLER_BUTTON_X];
	mPreviusButton[SDL_CONTROLLER_BUTTON_Y] = mCurrentButton[SDL_CONTROLLER_BUTTON_Y];
	mPreviusButton[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_DOWN];
	mPreviusButton[SDL_CONTROLLER_BUTTON_DPAD_UP] = mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_UP];
	mPreviusButton[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_RIGHT];
	mPreviusButton[SDL_CONTROLLER_BUTTON_START] = mCurrentButton[SDL_CONTROLLER_BUTTON_START];
	mPreviusButton[SDL_CONTROLLER_BUTTON_RIGHTSHOULDER] = mCurrentButton[SDL_CONTROLLER_BUTTON_LEFTSHOULDER];
	mPreviusButton[SDL_CONTROLLER_BUTTON_LEFTSHOULDER] = mCurrentButton[SDL_CONTROLLER_BUTTON_LEFTSHOULDER];

	/* Reset Mouse Wheel */
	mMouseWheel = 0.0f;
}

#pragma endregion

#pragma region // MOUSE //

void InputHandler::HandleMouseEvent(SDL_Event event)
{
	// Access the index with -1 beacuse they go:
	// LEFT = 1, MIDDLE = 2, RIGHT = 3
	mMouseCurrent[event.button.button - 1] = event.button.state ? true : false;
}
bool InputHandler::MouseIsDown(unsigned index)
{
	return mMouseCurrent[index];
}
bool InputHandler::MouseIsUp(unsigned index)
{
	return !mMouseCurrent[index];
}
bool InputHandler::MouseIsTriggered(unsigned index)
{
	if (mMouseCurrent[index] == true)
	{
		if (mMouseCurrent[index] != mMousePrevious[index])
			return true;
	}
	return false;
}
bool InputHandler::MouseIsReleased(unsigned index)
{
	if (mMouseCurrent[index] == false)
	{
		if (mMouseCurrent[index] != mMousePrevious[index])
			return true;
	}
	return false;
}

#pragma endregion

#pragma region // KEYBOARD //

void InputHandler::HandleKeyEvent(SDL_Event event)
{
	SDL_Keycode ScanCode = event.key.keysym.sym;

	int masked_key = ScanCode & ~SDLK_SCANCODE_MASK;

	/* Numeric Pad */
	if (ScanCode >= SDLK_KP_1 && ScanCode <= SDLK_KP_9)
	{
		mKeyCurrent[ScanCode - SDLK_KP_1 + '1'] = event.key.state ? true : false;
		return;
	}
	else if (ScanCode == SDLK_KP_0)
	{
		mKeyCurrent['0'] = event.key.state ? true : false;
		return;
	}
	/* TAB */
	else if (ScanCode == SDLK_TAB)
	{
		mKeyCurrent[SDLK_TAB] = event.key.state ? true : false;
		return;
	}
	/* Special keys */
	else if (ScanCode == 13) // Enter
	{
		mKeyCurrent[13] = event.key.state ? true : false;
		return;
	}
	else if (ScanCode == SDLK_CAPSLOCK) // CapsLock
	{
		mKeyCurrent[2] = event.key.state ? true : false;
		return;
	}

	mKeyCurrent[masked_key] = event.key.state ? true : false;
}
void InputHandler::HandleAxisEvent(SDL_Event event)
{
	switch (event.caxis.axis)
	{
		case 0://X axis
			mCurrentJoyStickValue.x = event.caxis.value >= 0 ? event.caxis.value / 32767.0F : event.caxis.value / 32768.0F;
			
			mCurrentJoyStickValue.x = abs(mCurrentJoyStickValue.x) <= 0.2F ? 0.00F : mCurrentJoyStickValue.x;

			
			break;

		case 1://Y axis 
			mCurrentJoyStickValue.y = event.caxis.value >= 0 ? -event.caxis.value / 32767.0F : -event.caxis.value / 32768.0F;

			mCurrentJoyStickValue.y = abs(mCurrentJoyStickValue.y) < 0.2F ? 0.00F : mCurrentJoyStickValue.y;

			break;

	}
}
void InputHandler::HandleMouseWheel(float scroll)
{
	mMouseWheel = scroll;
}
bool InputHandler::KeyIsDown(Key index)
{
	return mKeyCurrent[index];
}
bool InputHandler::KeyIsUp(Key index)
{
	return !KeyIsDown(index);
}
bool InputHandler::KeyIsTriggered(Key index)
{
	if (mKeyCurrent[index] == true)
	{
		if (mKeyCurrent[index] != mKeyPrevious[index])
			return true;
	}
	return false;
}
bool InputHandler::KeyIsReleased(Key index)
{
	if (mKeyCurrent[index] == false)
	{
		if (mKeyCurrent[index] != mKeyPrevious[index])
			return true;
	}
	return false;
}

void InputHandler::HandleEvent(SDL_Event gamepad_event)
{
	//mCurrentButton[SDL_CONTROLLER]
	switch (gamepad_event.cbutton.button)
	{
	case SDL_CONTROLLER_BUTTON_A:
		mCurrentButton[SDL_CONTROLLER_BUTTON_A] = gamepad_event.cbutton.state ? true : false;
		break;

	case SDL_CONTROLLER_BUTTON_B:
		mCurrentButton[SDL_CONTROLLER_BUTTON_B] = gamepad_event.cbutton.state ? true : false;
		break;

	case SDL_CONTROLLER_BUTTON_X:
		mCurrentButton[SDL_CONTROLLER_BUTTON_X] = gamepad_event.cbutton.state ? true : false;
		break;

	case SDL_CONTROLLER_BUTTON_Y:
		mCurrentButton[SDL_CONTROLLER_BUTTON_Y] = gamepad_event.cbutton.state ? true : false;
		break;

	case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
		mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = gamepad_event.cbutton.state ? true : false;
		break;

	case SDL_CONTROLLER_BUTTON_DPAD_UP:
		mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_UP] = gamepad_event.cbutton.state ? true : false;
		break;

	case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
		mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_LEFT] = gamepad_event.cbutton.state ? true : false;
		break;

	case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
		mCurrentButton[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = gamepad_event.cbutton.state ? true : false;
		break;

	case SDL_CONTROLLER_BUTTON_START:
		mCurrentButton[SDL_CONTROLLER_BUTTON_START] = gamepad_event.cbutton.state ? true : false;
		break;
	case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
		mCurrentButton[SDL_CONTROLLER_BUTTON_RIGHTSHOULDER] = gamepad_event.cbutton.state ? true : false;
		break;
	case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
		mCurrentButton[SDL_CONTROLLER_BUTTON_LEFTSHOULDER] = gamepad_event.cbutton.state ? true : false;
		break;
	default:
		break;
	}
}

void InputHandler::ResetPad(SDL_Event gamepad_event)
{
	/*switch (gamepad_event.cbutton.button)
	{
	case SDL_CONTROLLER_BUTTON_A:
		buttonUp[SDL_CONTROLLER_BUTTON_A] = gamepad_event.cbutton.timestamp;
		ButtonIsTriggered(SDL_CONTROLLER_BUTTON_A);
		break;

	case SDL_CONTROLLER_BUTTON_B:
		buttonUp[SDL_CONTROLLER_BUTTON_B] = gamepad_event.cbutton.timestamp;
		ButtonIsTriggered(SDL_CONTROLLER_BUTTON_B);
		break;

	case SDL_CONTROLLER_BUTTON_X:
		buttonUp[SDL_CONTROLLER_BUTTON_X] = gamepad_event.cbutton.timestamp;
		ButtonIsTriggered(SDL_CONTROLLER_BUTTON_X);
		break;

	case SDL_CONTROLLER_BUTTON_Y:
		buttonUp[SDL_CONTROLLER_BUTTON_Y] = gamepad_event.cbutton.timestamp;
		ButtonIsTriggered(SDL_CONTROLLER_BUTTON_Y);
		break;

	case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
		buttonUp[SDL_CONTROLLER_BUTTON_DPAD_DOWN] = gamepad_event.cbutton.timestamp;
		ButtonIsTriggered(SDL_CONTROLLER_BUTTON_DPAD_DOWN);
		break;

	case SDL_CONTROLLER_BUTTON_DPAD_UP:
		buttonUp[SDL_CONTROLLER_BUTTON_DPAD_UP] = gamepad_event.cbutton.timestamp;
		ButtonIsTriggered(SDL_CONTROLLER_BUTTON_DPAD_UP);
		break;

	case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
		buttonUp[SDL_CONTROLLER_BUTTON_DPAD_LEFT] = gamepad_event.cbutton.timestamp;
		ButtonIsTriggered(SDL_CONTROLLER_BUTTON_DPAD_LEFT);
		break;

	case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
		buttonUp[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = gamepad_event.cbutton.timestamp;
		ButtonIsTriggered(SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
		break;

	case SDL_CONTROLLER_BUTTON_START:
		buttonUp[SDL_CONTROLLER_BUTTON_START] = gamepad_event.cbutton.timestamp;
		ButtonIsTriggered(SDL_CONTROLLER_BUTTON_START);
		break;
	}*/
}

bool InputHandler::ButtonIsDown(unsigned index)
{
	/*if (buttonDown.size() != 0)
	{
		if (buttonDown.find(index) != buttonDown.end())
		{
			return true;
		}
	}
	return false;*/
	return mCurrentButton[index];
}

bool InputHandler::ButtonIsUp(unsigned index)
{
	/*if (buttonUp.size() != 0)
	{
		if (buttonUp.find(index) != buttonUp.end())
		{
			return true;
		}
	}
	return false;*/
	return !ButtonIsDown(index);
}
bool InputHandler::ButtonIsTriggered(unsigned index)
{
	if (mCurrentButton[index] == true)
	{
		if (mCurrentButton[index] != mPreviusButton[index])
			return true;
	}
	return false;
	/*if (ButtonIsDown(index) && ButtonIsUp(index))
	{
		unsigned time = buttonUp[index] - buttonDown[index];

		EventDispatcher::get_instance().trigger_event(GamepadEvent(index, time));

		buttonDown.erase(index);
		buttonUp.erase(index);

		return true;
	}

	return false;*/
}

bool InputHandler::CheckOther(unsigned button)
{
	if (button == SDL_CONTROLLER_BUTTON_A)
	{
		if (!ButtonIsUp(SDL_CONTROLLER_BUTTON_X))
		{
			if (lapse > offset && ButtonIsDown(SDL_CONTROLLER_BUTTON_X))
			{
				GamepadEvent double_press(SDL_CONTROLLER_BUTTON_A, 0);
				double_press.double_press = true;

				EventDispatcher::get_instance().trigger_event(double_press);

				buttonDown.erase(button);
				buttonDown.erase(SDL_CONTROLLER_BUTTON_X);

				return true;
			}
		}
	}

	if (button == SDL_CONTROLLER_BUTTON_X)
	{
		if (!ButtonIsUp(SDL_CONTROLLER_BUTTON_A))
		{
			if (lapse > offset && ButtonIsDown(SDL_CONTROLLER_BUTTON_A))
			{
				GamepadEvent double_press(SDL_CONTROLLER_BUTTON_X, 0);
				double_press.double_press = true;

				EventDispatcher::get_instance().trigger_event(double_press);

				buttonDown.erase(button);
				buttonDown.erase(SDL_CONTROLLER_BUTTON_A);

				return true;
			}
		}
	}

	return false;
}

Vector2 InputHandler::GetMouse()
{
	return  Camera::Instance()->GetNDCToWorld()
		* GetNormalizedMouseInWindow();
}

Vector2 InputHandler::GetRawMouse()
{ 
	return Vector2((f32)RawMouse[0], (f32)RawMouse[1]);
}

Vector2 InputHandler::GetNormalizedMouseInWindow()
{
	Vector2 MouseInBottomLeftWindow = GetMouseInWindow();
	Vector2 ScreenSize = Game->GetWindowSize() / 2;

	return Vector2(	MouseInBottomLeftWindow.x / ScreenSize.x,
					MouseInBottomLeftWindow.y / ScreenSize.y);
}

Vector2 InputHandler::GetMouseInViewport()
{
	return  Camera::Instance()->GetNDCToWorld() * Camera::Instance()->GetViewport()
		->GetWindowToNDC()	* GetMouseInWindow();
}

bool InputHandler::KeyByUnsignedIsTriggered(unsigned index)
{
	if (mKeyCurrent[index] == true)
	{
		if (mKeyCurrent[index] != mKeyPrevious[index])
			return true;
	}
	return false;
}

bool InputHandler::KeyByUnsignedIsPressed(unsigned index)
{
	return mKeyCurrent[index];
}

#pragma endregion
