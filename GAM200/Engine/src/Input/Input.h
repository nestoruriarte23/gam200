#pragma once

#include "SDL.h"
#include "../Utilities/Math/Vector2.h"
#include "../Components/System.h"

#define KEYBOARD_KEY_AMOUNT 256
#define	MOUSE_KEY_AMOUNT 3

enum MouseInput {
	LEFT, MID, RIGHT
};

enum Key
{
	Enter		= 13,
	Tab			= SDLK_TAB,
	Control		= SDL_SCANCODE_LCTRL,
	LShift		= SDL_SCANCODE_LSHIFT,
	BloqMay		= 2,
	Delete		= 8,
	Supr		= 127,
	Left		= SDL_SCANCODE_LEFT,
	Right		= SDL_SCANCODE_RIGHT,
	Up			= SDL_SCANCODE_UP,
	Down		= SDL_SCANCODE_DOWN,
	Esc			= 27,
	KeySpace	= 32,
	F1			= SDL_SCANCODE_F1,
	F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
	A = 'a', B, C, D, E, F, G, H, I, J, K, L, M, 
	N, O, P, Q, R, S, T, U, V, W, X, Y, Z
};

class InputHandler
{
	MAKE_SINGLETON(InputHandler);

public:
	void Initialize();
	void StartFrame();
	void HandleMouseEvent(SDL_Event event);
	void HandleKeyEvent(SDL_Event event);
	void HandleAxisEvent(SDL_Event event);
	void HandleMouseWheel(float);

	void HandleEvent(SDL_Event gamepad_event);
	void ResetPad(SDL_Event gamepad_event);

	bool MouseIsDown(unsigned index);
	bool MouseIsUp(unsigned index);
	bool MouseIsTriggered(unsigned index);
	bool MouseIsReleased(unsigned index);

	bool KeyIsDown(Key index);
	bool KeyIsUp(Key index);
	bool KeyIsTriggered(Key index);
	bool KeyIsReleased(Key index);

	bool ButtonIsDown(unsigned index);
	bool ButtonIsUp(unsigned index);
	bool ButtonIsTriggered(unsigned index);
	bool CheckOther(unsigned button);

	Vector2 GetMouse();
	Vector2 GetRawMouse();
	float	GetMouseWheel() { return mMouseWheel; }
	Vector2 GetNormalizedMouseInWindow();
	Vector2 GetMouseInViewport();
	Vector2 GetMouseInWindow() { return gMouse; }

	bool*	GetKeyboardState() { return mKeyCurrent; }
	Vector2	GetJoystickState() { return mCurrentJoyStickValue; }

	bool KeyByUnsignedIsTriggered(unsigned);
	bool KeyByUnsignedIsPressed(unsigned);
	bool GetCapsLocked() { return mbCapsLocked; }
	void ToogleCapsLock() { mbCapsLocked = !mbCapsLocked; }


	std::map<unsigned, unsigned> buttonDown;
	std::map<unsigned, unsigned> buttonUp;

	std::map<unsigned, bool> mCurrentButton;
	std::map<unsigned, bool> mPreviusButton;
private:
	/* Mouse Buttons (3) */
	bool mMouseCurrent[MOUSE_KEY_AMOUNT];
	bool mMousePrevious[MOUSE_KEY_AMOUNT];

	/* KeyboardKeys (256) */
	bool mKeyCurrent[KEYBOARD_KEY_AMOUNT];
	bool mKeyPrevious[KEYBOARD_KEY_AMOUNT];


	/* JoyStick */
	Vector2 mCurrentJoyStickValue;


	/* Mouse position */
	Vector2		gMouse;
	int			RawMouse[2];
	float		mMouseWheel;

	f64			offset = 0.2;
	f64			lapse = 0.0;
	bool		mbCapsLocked;
	bool		double_button;
};

#define InputManager (InputHandler::Instance())

#define MouseDown(i) InputManager->MouseIsDown(i)
#define MouseUp(i) InputManager->MouseIsUp(i)
#define MouseTriggered(i) InputManager->MouseIsTriggered(i)
#define MouseReleased(i) InputManager->MouseIsReleased(i)

#define KeyDown(i) InputManager->KeyIsDown(i)
#define KeyUp(i) InputManager->KeyIsUp(i)
#define KeyTriggered(i) InputManager->KeyIsTriggered(i)
#define KeyReleased(i) InputManager->KeyIsReleased(i)

#define ButtonDown(i) InputManager->ButtonIsDown(i)
#define ButtonUp(i) InputManager->ButtonIsUp(i)
#define ButtonTriggered(i) InputManager->ButtonIsTriggered(i)
#define ButtonReleased(i) InputManager->ButtonIsReleased(i)
#define CheckButtons(i, j) InputManager->CheckButton(i, j)
