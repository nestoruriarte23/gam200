#pragma once
#include <string.h>
#include <iostream>
#include <filesystem>

using namespace std;
using namespace std::experimental::filesystem;

void ShowConsole(bool show);

void PrintTime(const file_time_type& time);

bool ErrorMessage(bool expression, const char* message);

#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define ASSERT( expression ) 				        \
	if( !(expression)) {	\
  		cout << " Expression : " << #expression;       \
  		cout << ", Failed in file: " << __FILENAME__ ;       \
  		cout << " at line: " << __LINE__ << endl;	\
		std::string message; \
		std::string file = __FILENAME__; \
		char line[4];\
		_itoa_s(__LINE__, line, 4, 10); \
\
		message.append("Expression :"); \
		message.append(#expression); \
		message.append(", Failed in file: "); \
		message.append(file); \
		message.append(" In line : "); \
		message.append(line); \
  		ErrorMessage(expression, message.data());	\
	}							\
	//else cout << "Assertion Passed with expression : "<<#expression  << endl ; 	




