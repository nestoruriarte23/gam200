#include "MyDebug.h"
#include <Windows.h>
#include <winuser.h>

void ShowConsole(bool show)
{
	if (show)
	{
		int success = AllocConsole();
		ASSERT(success == 0);
	}
	else
	{
		FreeConsole();
	}
}

bool ErrorMessage(bool expression, const char* message)
{
	int result = MessageBox(NULL, message, "ASSERTION FAILED", MB_ABORTRETRYIGNORE | MB_ICONSTOP);

	switch (result)
	{
	case IDABORT:
		exit(-1);
	case IDRETRY:
		__debugbreak();
	case IDIGNORE:
		return !expression;
	};

	return expression;
}
