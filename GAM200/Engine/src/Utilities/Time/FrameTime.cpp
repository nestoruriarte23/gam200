#include <Windows.h>
#include "FrameTime.h"

f64 GetCPUTime()
{
	s64 f, t;
	f64 r, r0, r1;

	QueryPerformanceFrequency((LARGE_INTEGER*)(&f));
	QueryPerformanceCounter((LARGE_INTEGER*)(&t));

	r0 = f64(t / f);
	r1 = (t - ((t / f) * f)) / (f64)(f);
	r = r0 + r1;

	return r;
}

TimeManager::TimeManager() {}

void TimeManager::Initialize(bool lockstate, f64 fps)
{
	mLockedFrameRate = lockstate;
	mFramesPerSecond = fps;

	mDt = 0.1;
}

void TimeManager::StartFrame()
{
	mFrameStartTime = GetCPUTime();
}

void TimeManager::EndFrame()
{
	mDt = GetCPUTime() - mFrameStartTime;
	if (mLockedFrameRate && (mDt < 1.0 / mFramesPerSecond))
	{
		Sleep((DWORD)((1.0 / mFramesPerSecond - mDt) * 1000));
	}
	mDt = GetCPUTime() - mFrameStartTime;
}

void TimeManager::InvertLockState()
{
	mLockedFrameRate = !mLockedFrameRate;
}
