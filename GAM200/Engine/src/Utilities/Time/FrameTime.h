#pragma once

#include "../../Types.h"
#include "../../Components/System.h"

f64 GetCPUTime();

// TODO: Un sistema de Time de verdad, con un manager y funciones 
// para conseguir dt con facilidad o lock/unlock frame rate

class TimeManager
{
	MAKE_SINGLETON(TimeManager);

public:
	void Initialize(bool lockstate, f64 fps);

	void StartFrame();
	void EndFrame();
	f64  GetDt() { return mDt; }
	f64  GetFPS() { return 1.0 / mDt; }

	bool GetLockState() { return mLockedFrameRate; }
	void SetFrameLock(bool newState) { mLockedFrameRate = newState; }
	void InvertLockState();

	f64  GetFrameRate() { return mFramesPerSecond; }
	void SetFrameRate(f64 fps) { mFramesPerSecond = fps; }

private:
	bool			mLockedFrameRate;
	f64				mFramesPerSecond;

	f64				mFrameStartTime;
	f64				mDt;
};
