#include "Vector2.h"
#include "Vector3.h"
#include "Matrix33.h"
#include "Matrix44.h"

Matrix44::Matrix44() : m11(1), m12(0), m13(0), m14(0),
					   m21(0), m22(1), m23(0), m24(0),
					   m31(0), m32(0), m33(1), m34(0),
					   m41(0), m42(0), m43(0), m44(1) {}

Matrix44::Matrix44(float m11, float m12, float m13, float m14, 
				   float m21, float m22, float m23, float m24, 
				   float m31, float m32, float m33, float m34, 
				   float m41, float m42, float m43, float m44)
{
	m[0][0] = m11;	m[0][1] = m12;	m[0][2] = m13;	m[0][3] = m14;
	m[1][0] = m21;	m[1][1] = m22;	m[1][2] = m23;	m[1][3] = m24;
	m[2][0] = m31;	m[2][1] = m32;	m[2][2] = m33;	m[2][3] = m34;
	m[3][0] = m41;	m[3][1] = m42;	m[3][2] = m43;	m[3][3] = m44;
}

Matrix44::Matrix44(Matrix33 original) : Matrix44()
{
	m11 = original.m11;		m12 = original.m12;		m13 = original.m13;
	m21 = original.m21;		m22 = original.m22;		m23 = original.m23;
	m31 = original.m31;		m32 = original.m32;		m33 = original.m33;
}

Matrix44 & Matrix44::AddTranslation(const Vector3 & pos)
{
	m14 = pos.x;			m24 = pos.y;			m34 = pos.z;

	return *this;
}

Matrix44 Matrix44::Translate(const Vector3 & pos)
{
	return Matrix44(1, 0, 0, pos.x,
					0, 1, 0, pos.y,
					0, 0, 1, pos.z,
					0, 0, 0, 1);
}

Matrix44 Matrix44::Scale(const Vector3 & sca)
{
	return Matrix44(sca.x, 0, 0, 0,
					0, sca.y, 0, 0,
					0, 0, sca.z, 0,
					0, 0, 0, 1);
}

Matrix44 Matrix44::Rotate(float angle)
{
	return Matrix44(cos(angle), -sin(angle), 0, 0,
					sin(angle), cos(angle), 0, 0,
					0, 0, 1, 0,
					0, 0, 0, 1);
}

Matrix44 Matrix44::Concat(const Matrix44 & rhs) const
{
	Matrix44 nM;

	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			nM.m[i][j] = m[i][0] * rhs.m[0][j] +
						 m[i][1] * rhs.m[1][j] +
						 m[i][2] * rhs.m[2][j] +
						 m[i][3] * rhs.m[3][j];

	return nM;
}

Matrix44 Matrix44::operator*(const Matrix44 & rhs) const
{
	// Use the Concat function
	return Concat(rhs);
}

Vector3 Matrix44::operator*(const Vector3 & rhs) const
{
	return Vector3(m11 * rhs.x + m12 * rhs.y + m13 * rhs.z + m14,
		m21 * rhs.x + m22 * rhs.y + m23 * rhs.z + m24,
		m31 * rhs.x + m32 * rhs.y + m33 * rhs.z + m34);
}

Vector2 Matrix44::operator*(const Vector2 & rhs) const
{
	Vector3 result = operator*(Vector3(rhs.x, rhs.y, 0));

	return Vector2(result.x, result.y);
}
