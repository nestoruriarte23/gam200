#pragma once

struct Matrix44
{
	union
	{
		float v[16];
		float m[4][4];
		struct
		{
			float m11, m12, m13, m14,
				m21, m22, m23, m24,
				m31, m32, m33, m34,
				m41, m42, m43, m44;
		};
	};

	Matrix44();

	Matrix44(float m11, float m12, float m13, float m14,
		float m21, float m22, float m23, float m24,
		float m31, float m32, float m33, float m34,
		float m41, float m42, float m43, float m44);

	Matrix44(Matrix33 original);

	Matrix44 & AddTranslation(const Vector3 & pos);

	static Matrix44 Translate(const Vector3 & pos);

	static Matrix44 Scale(const Vector3 & sca);

	static Matrix44 Rotate(float angle);

	Matrix44 Concat(const Matrix44 & rhs) const;

	Matrix44 operator*(const Matrix44 & rhs)const;

	Vector3 operator*(const Vector3 & rhs)const;

	Vector2 operator*(const Vector2 & rhs)const;
};