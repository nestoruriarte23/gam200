/*!
*	\file		Matrix33.cpp
*	\brief		Implementation of 3x3 matrixes' functions.
*	\details	It manages all the basic operations done with those.
*	\author		David Miranda Pazos - m.david@digipen.edu
*	\date		02/11/2019
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/
#include "MyMath.h"
/*!
*	\fn		Matrix33
*	\brief	Initializes the matrix
*/
Matrix33::Matrix33()
{
	m11 = 1.0f; m12 = 0.0f; m13 = 0.0f; // set the identity matrix
	m21 = 0.0f; m22 = 1.0f; m23 = 0.0f;	// set the identity matrix
	m31 = 0.0f; m32 = 0.0f; m33 = 1.0f;	// set the identity matrix
}
/*!
*	\fn		Matrix33
*	\brief	Initializes the matrix
*	\param	float a11: The value of that location.
*	\param	float a12: The value of that location.
*	\param	float a13: The value of that location.
*	\param	float a21: The value of that location.
*	\param	float a22: The value of that location.
*	\param	float a23: The value of that location.
*	\param	float a31: The value of that location.
*	\param	float a32: The value of that location.
*	\param	float a33: The value of that location.
*/
Matrix33::Matrix33( float a11, float a12, float a13,
					float a21, float a22, float a23,
					float a31, float a32, float a33)
{
	m11 = a11; m12 = a12; m13 = a13; //set the matrix using the passed values
	m21 = a21; m22 = a22; m23 = a23; //set the matrix using the passed values
	m31 = a31; m32 = a32; m33 = a33; //set the matrix using the passed values
}
/*!
*	\fn		SetIdentity
*	\brief	Sets the matrix to identity.
*/
void Matrix33::SetIdentity()
{
	Matrix33(); // call the constructor
}
/*!
*	\fn		Transpose
*	\brief	Calculates the transposed matrix.
*	\return The transposed matrix
*/
Matrix33 Matrix33::Transpose()const
{
	Matrix33 temp; // holder matrix
	temp.m11 = m11; temp.m12 = m21; temp.m13 = m31; // transpose the row
	temp.m21 = m12; temp.m22 = m22; temp.m23 = m32;	// transpose the row
	temp.m31 = m13; temp.m32 = m23; temp.m33 = m33;	// transpose the row
	return temp; // return the holder matrix
}
/*!
*	\fn		Transpose
*	\brief	Calculates the transposed matrix.
*	\return The transposed matrix
*/
Matrix33 & Matrix33::TransposeThis()
{
	return *this = Transpose(); // call the transpose function
}
/*!
*	\fn		Concat
*	\brief	Calculates the concatenated matrix.
*	\param	const Matrix33 & rhs: The matrix to concatenate with.
*	\return The concatenated matrix
*/
Matrix33 Matrix33::Concat(const Matrix33 & rhs)const
{
	Matrix33 temp; // holder matrix

	temp.m11 = m11*rhs.m11 + m12*rhs.m21 + m13*rhs.m31; // first row calculations
	temp.m12 = m11*rhs.m12 + m12*rhs.m22 + m13*rhs.m32;	// first row calculations
	temp.m13 = m11*rhs.m13 + m12*rhs.m23 + m13*rhs.m33;	// first row calculations

	temp.m21 = m21*rhs.m11 + m22*rhs.m21 + m23*rhs.m31;	// second row calculations
	temp.m22 = m21*rhs.m12 + m22*rhs.m22 + m23*rhs.m32;	// second row calculations
	temp.m23 = m21*rhs.m13 + m22*rhs.m23 + m23*rhs.m33;	// second row calculations

	temp.m31 = m31*rhs.m11 + m32*rhs.m21 + m33*rhs.m31;	// third row calculations
	temp.m32 = m31*rhs.m12 + m32*rhs.m22 + m33*rhs.m32;	// third row calculations
	temp.m33 = m31*rhs.m13 + m32*rhs.m23 + m33*rhs.m33;	// third row calculations

	return temp; // return the holder matrix
}
/*!
*	\fn		operator*
*	\brief	Calculates the concatenated matrix.
*	\param	const Matrix33 & rhs: The matrix to concatenate with.
*	\return The concatenated matrix
*/
Matrix33 Matrix33::operator *(const Matrix33 & rhs)const
{
	Matrix33 temp; // holder matrix

	temp = Concat(rhs); // concatenate the matrix

	return temp; // return the holder matrix
}
/*!
*	\fn		*=
*	\brief	Calculates the concatenated matrix.
*	\param	const Matrix33 & rhs: The matrix to concatenate with.
*	\return The concatenated matrix
*/
Matrix33& Matrix33::operator *=(const Matrix33 &rhs)
{
	Matrix33 temp; // holder matrix

	temp = Concat(rhs); // concatenate the matrix

	*this = temp; // set the matrix

	return *this; // return the matrix
}
/*!
*	\fn		MultPoint
*	\brief	Calculates the multiplied matrix by a point.
*	\param	const Vector2 & vec: The point to multiply with.
*	\return The multiplied point
*/
Vector2 Matrix33::MultPoint(const Vector2 & vec)const
{
	Vector2 temp; // holder point
	temp.x = m11*vec.x + m12*vec.y + m13; // x coordinate calculation
	temp.y = m21*vec.x + m22*vec.y + m23; // y coordinate calculation

	return temp; // return the holder point
}
/*!
*	\fn		operator*
*	\brief	Calculates the multiplied matrix by a point.
*	\param	const Vector2 & vec: The point to multiply with.
*	\return The multiplied point
*/
Vector2 Matrix33::operator*(const Vector2 & vec)const
{
	Vector2 temp; // holder point
	temp = MultPoint(vec); // call MultPoint
	return temp; // return the holder point
}
/*!
*	\fn		MultVector
*	\brief	Calculates the multiplied matrix by a vector.
*	\param	const Vector2 & vec: The vector to multiply with.
*	\return The multiplied vector
*/
Vector2 Matrix33::MultVec(const Vector2 &vec)const
{
	Vector2 temp; // Holder vector
	temp.x = m11*vec.x + m12*vec.y;	// Calculate the x coordinate
	temp.y = m21*vec.x + m22*vec.y;	// Calculate the y coordinate

	return temp; // return the holder vector
}
/*!
*	\fn		MultVector
*	\brief	Get the identity matrix.
*	\return The matrix
*/
Matrix33 Matrix33::Identity()
{
	Matrix33 temp; // holder matrix
	return temp; // return the holder matrix
}
/*!
*	\fn		MultVector
*	\brief	Get the identity matrix.
*	\param  float x: the translation on x
*	\param	float y: the translation on y
*	\return The transformation matrix
*/
Matrix33 Matrix33::Translate(float x, float y)
{
	Matrix33 temp; // holder matrix
	temp.m13 = x; // set the last column
	temp.m23 = y; // set the last column
	return temp; // return the holder matrix
}
/*!
*	\fn		Scale
*	\brief	Scale a vector
*	\param  float sx: the scale on x
*	\param	float sy: the scale on y
*	\return The transformation matrix
*/
Matrix33 Matrix33::Scale(float sx, float sy)
{
	Matrix33 temp; // holder matrix
	temp.m11 = sx; // set the first row first column
	temp.m22 = sy; // set the the second row second colunm
	return temp; // return the holder matrix
}
/*!
*	\fn		RotDeg
*	\brief	Rotate with degrees
*	\param  float anflge_deg: the rotation angle
*	\return The transformation matrix
*/
Matrix33 Matrix33::RotDeg(float angle_deg)
{
	Matrix33 temp; // holder matrix
	angle_deg *= (PI/180); // change to radians
	temp.m11 = static_cast<f32>(cos(angle_deg)); temp.m12 = static_cast<f32>(-sin(angle_deg)); // first row
	temp.m21 = static_cast<f32>(sin(angle_deg)); temp.m22 = static_cast<f32>(cos(angle_deg)); // second row
	return temp; // return the holder matrix
}
/*!
*	\fn		RotRad
*	\brief	Rotate with radians
*	\param  float anflge_rad: the rotation angle
*	\return The transformation matrix
*/
Matrix33 Matrix33::RotRad(float angle_rad)
{
	Matrix33 temp; // holder matrix
	temp.m11 = static_cast<f32>(cos(angle_rad)); temp.m12 = static_cast<f32>(-sin(angle_rad)); // first row
	temp.m21 = static_cast<f32>(sin(angle_rad)); temp.m22 = static_cast<f32>(cos(angle_rad)); // second row
	return temp; // return the holder matrix
}