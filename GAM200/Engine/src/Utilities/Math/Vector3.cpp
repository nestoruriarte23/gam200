#include "Vector3.h"
#include "MyMath.h"


Vector3::Vector3()
{
	x = y = z = 0.0f; // Set both coordenates to 0
}
/*!
*	\fn		Vector3
*	\brief	Initializes the values of the vector to 0.
*	\param float xx: The value x will get
*	\param float yy: The value y will get
*/
Vector3::Vector3(float xx, float yy, float zz)
{
	x = xx; // Set the coordinate to its square
	y = yy;	// Set the coordinate to its square
	z = zz;
}
Vector3&	Vector3::operator-=	(const Vector2& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	return *this;
}
Vector3&	Vector3::operator+=	(const Vector2& rhs)
{
	x += rhs.x;
	y += rhs.y;
	return *this;
}

Vector2	Vector3::operator-	(const Vector2& rhs)
{
	Vector2 temp;
	temp.x = x - rhs.x;
	temp.y = y - rhs.y;
	return temp;
}
Vector2	Vector3::operator+	(const Vector2& rhs)
{
	Vector2 temp;
	temp.x = x + rhs.x;
	temp.y = y + rhs.y;
	return temp;
}
/*!
*	\fn		operator+
*	\brief	Adds two vectors.
*	\param const Vector3 & egs: A reference to the vector to set.
*	\return the vector addition result
*/
Vector3		Vector3::operator+	(const Vector3& rhs) const
{
	Vector3 temp;	// holder vector
	temp.x = x + rhs.x; // set the coordinates to the addition
	temp.y = y + rhs.y;	// set the coordinates to the addition
	temp.z = z + rhs.z;
	return temp;	// return the holder vector
}
/*!
*	\fn		operator+=
*	\brief	Adds two vectors and sets the value to one of them.
*	\param const Vector3 & egs: A reference to the vector to set.
*	\return the vector addition result
*/
Vector3&	Vector3::operator+=	(const Vector3& rhs)
{
	x += rhs.x;	// set the coordinate to the addition
	y += rhs.y;	// set the coordinate to the addition
	z += rhs.z;
	return *this; // return the vector
}
/*!
*	\fn		operator-
*	\brief	Subtracts two vectors and sets the value to one of them.
*	\param const Vector3 & egs: A reference to the vector to set.
*	\return the vector substraction result
*/
Vector3		Vector3::operator-	(const Vector3& rhs) const
{
	Vector3 temp; // holder vector
	temp.x = x - rhs.x;	// set the coordinate to the substraction	
	temp.y = y - rhs.y;	// set the coordinate to the substraction
	temp.z = z - rhs.z;
	return temp; // return the holder vector
}
/*!
*	\fn		operator-=
*	\brief	Subtracts two vectors and sets the value to one of them.
*	\param const Vector3 & egs: A reference to the vector to set.
*	\return the vector substraction result
*/
Vector3&	Vector3::operator-=	(const Vector3& rhs)
{
	x -= rhs.x;	// set the coordinate to the substraction
	y -= rhs.y;	// set the coordinate to the substraction
	z -= rhs.z;
	return *this; // return the vector
}
/*!
*	\fn		operator*
*	\brief	Multiplies two vectors.
*	\param	float s: The scalar to multiply with
*	\return the vector multiplication result
*/
Vector3		Vector3::operator*	(float s) const
{
	Vector3 temp;	// holder vector
	temp.x = x * s;	// set the coordinate to the multiplication
	temp.y = y * s;	// set the coordinate to the multiplication
	temp.z = z*s;
	return temp; // return the holder vector
}
/*!
*	\fn		operator*=
*	\brief	Multiplies two vectors and sets the value to one of them.
*	\param	float s: the scalar to multiply with
*	\return the vector multiplication result
*/
Vector3&	Vector3::operator*=	(float s)
{
	x *= s;	// set the coordinate to the multiplication
	y *= s;	// set the coordinate to the multiplication
	z *= z;
	return *this;	// return the vector
}
/*!
*	\fn		operator/
*	\brief	Divides two vectors and sets the value to one of them.
*	\param	float s: the scalar to divide with
*	\return the vector division result
*/
Vector3		Vector3::operator/	(float s) const
{
	Vector3 temp;	// holder vector
	temp.x = x / s;	// set the coordinate to the division
	temp.y = y / s;	// set the coordinate to the division
	temp.z = z / s;
	return temp;	// return the holder vector
}
/*!
*	\fn		operator/=
*	\brief	Divides two vectors and sets the value to one of them.
*	\param  float s: the scalar to divide with
*	\return the vector division result
*/
Vector3&	Vector3::operator/=	(float s)
{
	x /= s;	// set the coordinate to the division
	y /= s;	// set the coordinate to the division
	z /= z;
	return *this;	// return the vector
}
/*!
*	\fn		operator-
*	\brief	Changes the sign of the values in the vector.
*	\return the vector changed
*/
Vector3		Vector3::operator-() const
{
	Vector3 temp;	// holder vector
	temp.x = -x;	// set the coordinate to its negative
	temp.y = -y;	// set the coordinate to its negative
	temp.z = -z;
	return temp;	// return the holder vector
}
/*!
*	\fn		Length
*	\brief	Calculates the value of the vector's length
*	\return The length
*/
float Vector3::Length() const
{
	float length = static_cast<f32>(sqrt(sqrt(x*x + y*y)+z*z));	// calculate the lentght (x^2 + y^2)^(1/2)
	return length; // return the length
}
/*!
*	\fn		LengthSq
*	\brief	Calculates the value of the vector's length squared
*	\return The length squared
*/
float Vector3::LengthSq() const
{
	float length = x*x + y*y + z*z; //calculate the squared length x^2 + y^2
	return length; // return the squared length
}
/*!
*	\fn		Distance
*	\brief	Calculates the distance from one vector to another.
*	\param const Vector3 &: A reference to the vector to calculate the distance with.
*	\return The distance
*/
float	Vector3::Distance(const Vector3 &rhs)
{
	Vector3 temp; // holder vector
	temp.x = x - rhs.x; // calculate the distance and set it to the proper vector coordinate
	temp.y = y - rhs.y;	// calculate the distance and set it to the proper vector coordinate
	temp.z = z - rhs.z;
	return temp.Length(); // return the length of the holder vector
}
/*!
*	\fn		DistanceSq
*	\brief	Calculates the distance squared from one vector to another.
*	\param const Vector3 &: A reference to the vector to calculate the distance with.
*	\return The distance squared
*/
float	Vector3::DistanceSq(const Vector3 &rhs)
{
	Vector3 temp; // holder vector
	temp.x = x - rhs.x;	// calculate the disyance and set it to the proper vector coordinate
	temp.y = y - rhs.y;	// calculate the disyance and set it to the proper vector coordinate
	temp.z = z - rhs.z;
	return temp.LengthSq(); // return the squared length of the holder vector
}
/*!
*	\fn		Normalize
*	\brief	Normalizes one vector
*	\return The normalized vector
*/
Vector3	Vector3::Normalize() const
{
	Vector3 temp; // holder vector
	temp.x = x / Length(); // normalize the coordinate
	temp.y = y / Length(); // normalize the coordinate
	temp.z = z / Length();
	return temp; // return the normalized vector
}
/*!
*	\fn		Normalize
*	\brief	Normalizes one specific vector
*	\return The normalized vector
*/
Vector3 & Vector3::NormalizeThis()
{
	float Length_ = Length(); // store the original length
	x /= Length_;	// normalize each coordinate
	y /= Length_;	// normalize each coordinate
	z /= Length_;
	return *this; // return the vector
}
/*!
*	\fn		Dot
*	\brief	Calculates the dot product of two vectors
*	\param	The vector to calculate the product with.
*	\return The scalar result.
*/
float Vector3::Dot(const Vector3& rhs) const
{
	float dot = x*rhs.x + y*rhs.y+ z*rhs.z; // calculate the dot product
	return dot; // return the result
}
/*!
*	\fn		operator*
*	\brief	Calculates the dot product of two vectors
*	\param	The vector to calculate the product with.
*	\return The scalar result.
*/
float Vector3::operator*(const Vector3& rhs) const
{
	float dot = x*rhs.x + y*rhs.y + z*rhs.z; // calculate the dot product
	return dot;	// return the result
}
/*!
*	\fn		Perp
*	\brief	Calculates the perpendicular vector
*	\return The perpendicular vector
*/
Vector3		Vector3::Perp()
{
	Vector3 temp = { -y,x , z}; // calculate the perpendicular
	return temp; // return the perpendicular vector
}
/*!
*	\fn		Project
*	\brief	Calculates the projection of one vector onto another
*	\param	const Vector3&: A reference to the vector to project onto
*	\return The projection
*/
Vector3		Vector3::Project(const Vector3& rhs)
{
	Vector3 temp; // holder vector
	temp = rhs * ((*this *rhs) / (rhs*rhs)); // calculate the projection
	return temp; // return the projection
}
/*!
*	\fn		ProjectPerp
*	\brief	Calculates the perpendicular projection of one vector onto another
*	\param	const Vector3&: A reference to the vector to project onto
*	\return The projection
*/
Vector3		Vector3::ProjectPerp(const Vector3& rhs)
{
	Vector3 temp; // holder vector
	temp = *this - Project(rhs); // calculate the perpendicular projection vector
	return temp; // return the perpendicular projection vector
}
/*!
*	\fn		CrossMag
*	\brief	Calculates cross magnitude of two vectors
*	\param	const Vector3&: A reference to the vector to calculate with
*	\return The scalar result
*/
float  Vector3::CrossMag(const Vector3& rhs)
{
	f32 mag = x*rhs.y - rhs.x*y; // calculate the cross magnitude
	return mag; // return the result
}
/*!
*	\fn		GetAngle
*	\brief	Gets the angle of the vector respect to the origin
*	\return The scalar result
*/
float  Vector3::GetAngle()
{
	f32 angle = static_cast<f32>(atan2(x, y)); // get the angle
	return angle; // return the angle
}
/*!
*	\fn		FromAngle
*	\brief	Gets the vector based on an angle
*	\param float rad_angle: The angle to start with
*/
void  Vector3::FromAngle(float rad_angle)
{
	x = static_cast<f32>(cos(rad_angle)); // get the coordinate using trigonometry
	y = static_cast<f32>(sin(rad_angle));	// get the coordinate using trigonometry
}
/*!
*	\fn		Random
*	\brief	Calculates a random vector in a range
*	\param float min_x: The minimun x value.
*	\param float min_x: The maximum x value.
*	\param float min_x: The minimun y value.
*	\param float min_x: The maximum y value.
*	\return the randomized vector
*/
Vector3 Vector3::Random(float min_x, float max_x, float min_y, float max_y, float min_z, float max_z)
{
	Vector3 temp; // holder vector
	srand(static_cast <int>(time(0))); // randomize the seed
	temp.x = min_x + static_cast <f32> (rand()) / (RAND_MAX / (max_x - min_x)); // set a random value in the range
	temp.y = min_y + static_cast <f32> (rand()) / (RAND_MAX / (max_y - min_y)); // set a random value in the range
	temp.z = min_z + static_cast <f32> (rand()) / (RAND_MAX / (max_z - min_z));
	return temp; // return the holder vector
}
/*!
*	\fn		Random01
*	\brief	Calculates a random vector
*	\return the randomized vector
*/
Vector3 Vector3::Random01()
{
	Vector3 temp; // holder vector
	srand(static_cast <int>(time(0))); // randomize the seed
	temp.x = static_cast<f32> (rand()) / static_cast <f32> (RAND_MAX); // set a random value
	temp.y = static_cast<f32> (rand()) / static_cast <f32> (RAND_MAX);	// set a random value
	temp.z = static_cast<f32> (rand()) / static_cast <f32> (RAND_MAX);	// set a random value
	return temp; // return the holder vector
}
/*!
*	\fn		Lerp
*	\brief	Calculates the lerp of two vectors
*	\param	Vector 2 start: The vector to start with
*	\param  Vector3 end: The vector to end in
*	\param  float tn: the range
*	\return The vector lerped.
*/
Vector3 Vector3::Lerp(Vector3 start, Vector3 end, float tn)
{
	return  start *(1 - tn) + end*tn; // calculate and return the lerp
}
