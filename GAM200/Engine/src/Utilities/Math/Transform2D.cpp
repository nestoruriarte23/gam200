/*!
*	\file		Transform2D.cpp
*	\brief		Implementation of 2D transformation functions.
*	\details	It manages all the basic operations done with those.
*	\author		David Miranda Pazos - m.david@digipen.edu
*	\date		02/11/2019
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/
#include"Transform2D.h"

/*!
*	\fn		Transform2D
*	\brief	Initializes the transform2D
*/
Transform2D::Transform2D()
{
	mPosition = { 0.0f,0.0f };	// set all the values to the basic ones
	mScale = { 1.0f,1.0f };		// set all the values to the basic ones
	mOrientation = 0.0f;		// set all the values to the basic ones
}
/*!
*	\fn		Transform2D
*	\brief	Initializes the transform2D
*	\param const Vector2&: The position
*	\param const Vector2&: The scale
*	\param const float &:  The angle
*/
Transform2D::Transform2D(const Vector2 & pos, const Vector2 & scale, const float & rot)
{
	mPosition = pos;	// set the value to the passed one
	mScale = scale;		// set the value to the passed one
	mOrientation = rot;	// set the value to the passed one
}
/*!
*	\fn		Transform2D
*	\brief	Initializes the transform2D
*	\param	float tx: The position x
*	\param	float ty: The position y
*	\param	float sx: The scale x
*	\param	float sy: The scale y
*	\param  float rot:  The angle
*/
Transform2D::Transform2D(float tx, float ty, float sx, float sy, float rot)
{
	mPosition = { tx,ty };	// set the value to the passed one
	mScale = { sx,sy };		// set the value to the passed one
	mOrientation = rot;		// set the value to the passed one
}
/*!
*	\fn		SetIdentity
*	\brief	Sets the transform 2D to the most basic values.
*/
void Transform2D::SetIdentity()
{
	mPosition = { 0.0f,0.0f }; // set all the values to the basic ones
	mScale = { 1.0f,1.0f };	   // set all the values to the basic ones
	mOrientation = 0.0f;	   // set all the values to the basic ones
}
/*!
*	\fn		GetMatrix
*	\brief	Gets the matrix of the transform
*	\return The matrix
*/
Matrix33 Transform2D::GetMatrix() const
{
	Matrix33 translation = translation.Translate(mPosition.x,mPosition.y); // Translation matrix
	Matrix33 rotation = rotation.RotRad(mOrientation); // rotation matrix
	Matrix33 scale = scale.Scale(mScale.x, mScale.y); // scale matrix

	
	return translation*rotation*scale;	// return the multiplied matrix using the other three
}
/*!
*	\fn		GetInvMatrix
*	\brief	Gets the inverse matrix of the transform
*	\return The inverse matrix
*/
Matrix33 Transform2D::GetInvMatrix() const
{
	Matrix33 translation = translation.Translate(-mPosition.x, -mPosition.y); // inverse of the translation matrix
	Matrix33 rotation = rotation.RotRad(-mOrientation); // inverse of the rotation matrix
	Matrix33 scale; // scale matrix
	if (mScale.x == 0 || mScale.y == 0) // check if the scale is 0
	{
		if (mScale.x == 0 && mScale.y != 0) // x=0 y!= 0
			scale = scale.Scale(0, 1 / mScale.y); // sx= 0 sy = mScale.y

		if (mScale.y == 0 && mScale.x != 0) // y= 0 x!=0
			scale = scale.Scale(1 / mScale.x, 0); // sy= 0 sx = mScale.x

		if(mScale.y == 0 && mScale.x == 0) // both 0
			scale = scale.Scale(0, 0); // sx = sy = 0
	}
	else
		scale = Matrix33::Scale(1/mScale.x, 1/mScale.y); // scale matrix using original values


	return scale*rotation*translation; // return the multiplied inverse matrix
}
/*!
*	\fn		Concat
*	\brief	Concatenates two matrixes
*	\param	The matrix to concatenate with
*	\return The concatenated matrix
*/
Transform2D Transform2D::Concat(const Transform2D & rhs) const
{
	Transform2D temp; // holder transform

	temp.mPosition.x = mPosition.x + static_cast<f32>(cos(mOrientation)) *  mScale.x * rhs.mPosition.x - static_cast<f32>(sin(mOrientation))*mScale.y* rhs.mPosition.y; // position x
	temp.mPosition.y = mPosition.y + static_cast<f32>(sin(mOrientation)) *  mScale.x * rhs.mPosition.x + static_cast<f32>(cos(mOrientation))*mScale.y*rhs.mPosition.y; // position y

	temp.mScale.x = (mScale.x*rhs.mScale.x); // scale x
	temp.mScale.y = (mScale.y*rhs.mScale.y);	// scale y

	temp.mOrientation = (mOrientation + rhs.mOrientation); // rotation addition
	return temp; // return holder transform
}
/*!
*	\fn		operator*
*	\brief	Concatenates two matrixes
*	\param	The matrix to concatenate with
*	\return The concatenated matrix
*/
Transform2D Transform2D::operator*(const Transform2D & rhs) const
{
	Transform2D temp = Concat(rhs); // holder transform concatenated with the passed one
	
	return temp; // return the holder transform
}
/*!
*	\fn		operator*=
*	\brief	Concatenates two matrixes
*	\param	The matrix to concatenate with
*	\return The concatenated matrix
*/
Transform2D & Transform2D::operator*=(const Transform2D & rhs)
{
	*this = Concat(rhs); // concatenate the original transform with the passed one
	
	return *this; // return the transform
}
/*!
*	\fn		MultPoint
*	\brief	Transforms one point
*	\param	const Vector2 & rhs: The vector to transform
*	\return The transformed vector
*/
Vector2 Transform2D::MultPoint(const Vector2 & rhs) const
{
	Vector2 temp =rhs; // holder vector
	temp = GetMatrix()*temp; // multiply the point with the transformation matrix
	return temp; // return the holder vector
}
/*!
*	\fn		operator*
*	\brief	Transforms one point
*	\param	const Vector2 & rhs: The vector to transform
*	\return The transformed vector
*/
Vector2 Transform2D::operator*(const Vector2 & rhs) const
{
	Vector2 temp = rhs; // holder vector
	temp = GetMatrix()*temp; // multiply the point with the transformation matrix
	return temp; // return the holder vector
}
/*!
*	\fn		MultPointArray
*	\brief	Transforms an array of points
*	\param	const Vector2 * vecArray: The vector to transform
*	\param	int size: The size of the array
*/
void Transform2D::MultPointArray(Vector2 * vecArray, int size) const
{
	for(int i = 0 ; i < size ; i++) // until there are not more points
	vecArray[i] = GetMatrix()*vecArray[i]; // multiply each point by the transformation matrix
}

bool Transform2D::operator==(const Transform2D &rhs)
{
	if (mPosition == rhs.mPosition && mScale == rhs.mScale && mOrientation == rhs.mOrientation)
		return true;
	return false;
}