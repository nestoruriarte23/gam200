/*!
*	\file		Vector2.cpp
*	\brief		Implementation of 2D vector's functions.
*	\details	It manages all the basic operations done with those vectors.
*	\author		David Miranda Pazos - m.david@digipen.edu
*	\date		02/11/2019
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/
#include "MyMath.h"
/*!
*	\fn		Vector2
*	\brief	Initializes the values of the vector to 0.
*/
Vector2::Vector2()
{
	x = y = 0.0f; // Set both coordenates to 0
}
/*!
*	\fn		Vector2
*	\brief	Initializes the values of the vector to 0.
*	\param float xx: The value x will get
*	\param float yy: The value y will get
*/
Vector2::Vector2(float xx, float yy)
{
	x = xx; // Set the coordinate to its square
	y = yy;	// Set the coordinate to its square
}
Vector2::Vector2(const Vector3 & _rhs)
{	
	x = _rhs.x;
	y = _rhs.y;
}

Vector2 & Vector2::operator=(const Vector3 & _rhs)
{
	x = _rhs.x;
	y = _rhs.y;

	return *this;
}

/*!
*	\fn		operator+
*	\brief	Adds two vectors.
*	\param const Vector2 & egs: A reference to the vector to set.
*	\return the vector addition result
*/
Vector2		Vector2::operator+	(const Vector2& rhs) const
{
	Vector2 temp;	// holder vector
	temp.x = x + rhs.x; // set the coordinates to the addition
	temp.y = y + rhs.y;	// set the coordinates to the addition
	return temp;	// return the holder vector
}
/*!
*	\fn		operator+=
*	\brief	Adds two vectors and sets the value to one of them.
*	\param const Vector2 & egs: A reference to the vector to set.
*	\return the vector addition result
*/
Vector2&	Vector2::operator+=	(const Vector2& rhs)
{
	x += rhs.x;	// set the coordinate to the addition
	y += rhs.y;	// set the coordinate to the addition
	return *this; // return the vector
}
/*!
*	\fn		operator-
*	\brief	Subtracts two vectors and sets the value to one of them.
*	\param const Vector2 & egs: A reference to the vector to set.
*	\return the vector substraction result
*/
Vector2		Vector2::operator-	(const Vector2& rhs) const
{
	Vector2 temp; // holder vector
	temp.x = x - rhs.x;	// set the coordinate to the substraction	
	temp.y = y - rhs.y;	// set the coordinate to the substraction
	return temp; // return the holder vector
}
/*!
*	\fn		operator-=
*	\brief	Subtracts two vectors and sets the value to one of them.
*	\param const Vector2 & egs: A reference to the vector to set.
*	\return the vector substraction result
*/
Vector2&	Vector2::operator-=	(const Vector2& rhs)
{
	x -= rhs.x;	// set the coordinate to the substraction
	y -= rhs.y;	// set the coordinate to the substraction
	return *this; // return the vector
}
/*!
*	\fn		operator*
*	\brief	Multiplies two vectors.
*	\param	float s: The scalar to multiply with
*	\return the vector multiplication result
*/
Vector2		Vector2::operator*	(float s) const
{
	Vector2 temp;	// holder vector
	temp.x = x * s;	// set the coordinate to the multiplication
	temp.y = y * s;	// set the coordinate to the multiplication
	return temp; // return the holder vector
}
/*!
*	\fn		operator*=
*	\brief	Multiplies two vectors and sets the value to one of them.
*	\param	float s: the scalar to multiply with
*	\return the vector multiplication result
*/
Vector2&	Vector2::operator*=	(float s)
{
	x *= s;	// set the coordinate to the multiplication
	y *= s;	// set the coordinate to the multiplication
	return *this;	// return the vector
}
/*!
*	\fn		operator/
*	\brief	Divides two vectors and sets the value to one of them.
*	\param	float s: the scalar to divide with
*	\return the vector division result
*/
Vector2		Vector2::operator/	(float s) const
{
	Vector2 temp;	// holder vector
	temp.x = x / s;	// set the coordinate to the division
	temp.y = y / s;	// set the coordinate to the division
	return temp;	// return the holder vector
}
/*!
*	\fn		operator/=
*	\brief	Divides two vectors and sets the value to one of them.
*	\param  float s: the scalar to divide with
*	\return the vector division result
*/
Vector2&	Vector2::operator/=	(float s)
{
	x /= s;	// set the coordinate to the division
	y /= s;	// set the coordinate to the division
	return *this;	// return the vector
}
/*!
*	\fn		operator-
*	\brief	Changes the sign of the values in the vector.
*	\return the vector changed
*/
Vector2		Vector2::operator-() const
{
	Vector2 temp;	// holder vector
	temp.x = -x;	// set the coordinate to its negative
	temp.y = -y;	// set the coordinate to its negative
	return temp;	// return the holder vector
}
/*!
*	\fn		operator==
*	\brief	Compares if two vector are equal.
*	\return if the vectors are equal or not
*/
bool		Vector2::operator==(const Vector2& rhs) const
{
	return rhs.x == x && rhs.y == y;
}

bool		Vector2::operator>  (const Vector2& rhs) const
{
	if (x > rhs.x&& y > rhs.y) return true;
	return false;
}
/*!
*	\fn		operator!=
*	\brief	Compares if two vector are different.
*	\return if the vectors are equal or not
*/
bool		Vector2::operator!=(const Vector2& rhs) const
{
	return rhs.x != x || rhs.y != y;
}
bool Vector2::operator<(const Vector2 & rhs) const
{
	if (y <= rhs.y && x < rhs.x)
		return true;

	return false;
}
/*!
*	\fn		Length
*	\brief	Calculates the value of the vector's length
*	\return The length
*/
float Vector2::Length() const
{
	float length = static_cast<f32>(sqrt(x*x + y*y));	// calculate the lentght (x^2 + y^2)^(1/2)
	return length; // return the length
}
/*!
*	\fn		LengthSq
*	\brief	Calculates the value of the vector's length squared
*	\return The length squared
*/
float Vector2::LengthSq() const
{
	float length = x*x + y*y; //calculate the squared length x^2 + y^2
	return length; // return the squared length
}
/*!
*	\fn		Distance
*	\brief	Calculates the distance from one vector to another.
*	\param const Vector2 &: A reference to the vector to calculate the distance with.
*	\return The distance
*/
float	Vector2::Distance(const Vector2 &rhs)
{
	Vector2 temp; // holder vector
	temp.x = x - rhs.x; // calculate the distance and set it to the proper vector coordinate
	temp.y = y - rhs.y;	// calculate the distance and set it to the proper vector coordinate
	return temp.Length(); // return the length of the holder vector
}
/*!
*	\fn		DistanceSq
*	\brief	Calculates the distance squared from one vector to another.
*	\param const Vector2 &: A reference to the vector to calculate the distance with.
*	\return The distance squared
*/
float	Vector2::DistanceSq(const Vector2 &rhs)
{
	Vector2 temp; // holder vector
	temp.x = x - rhs.x;	// calculate the disyance and set it to the proper vector coordinate
	temp.y = y - rhs.y;	// calculate the disyance and set it to the proper vector coordinate
	return temp.LengthSq(); // return the squared length of the holder vector
}
/*!
*	\fn		Normalize
*	\brief	Normalizes one vector
*	\return The normalized vector
*/
Vector2	Vector2::Normalize() const
{
	Vector2 temp; // holder vector
	temp.x = x / Length(); // normalize the coordinate
	temp.y = y / Length(); // normalize the coordinate
	return temp; // return the normalized vector
}
/*!
*	\fn		Normalize
*	\brief	Normalizes one specific vector
*	\return The normalized vector
*/
Vector2 & Vector2::NormalizeThis()
{
	float Length_ = Length(); // store the original length
	x /= Length_;	// normalize each coordinate
	y /=  Length_;	// normalize each coordinate
	return *this; // return the vector
}
/*!
*	\fn		Dot
*	\brief	Calculates the dot product of two vectors
*	\param	The vector to calculate the product with.
*	\return The scalar result.
*/
float Vector2::Dot(const Vector2& rhs) const
{
	float dot = x*rhs.x + y*rhs.y; // calculate the dot product
	return dot; // return the result
}
/*!
*	\fn		operator*
*	\brief	Calculates the dot product of two vectors
*	\param	The vector to calculate the product with.
*	\return The scalar result.
*/
float Vector2::operator*(const Vector2& rhs) const
{
	float dot = x*rhs.x + y*rhs.y; // calculate the dot product
	return dot;	// return the result
}
/*!
*	\fn		Perp
*	\brief	Calculates the perpendicular vector
*	\return The perpendicular vector
*/
Vector2		Vector2::Perp()
{
	Vector2 temp = { -y,x }; // calculate the perpendicular
	return temp; // return the perpendicular vector
}
/*!
*	\fn		Project
*	\brief	Calculates the projection of one vector onto another
*	\param	const Vector2&: A reference to the vector to project onto
*	\return The projection
*/
Vector2		Vector2::Project(const Vector2& rhs)
{
	Vector2 temp; // holder vector
	temp = rhs * ((*this *rhs) / (rhs*rhs)); // calculate the projection
	return temp; // return the projection
}
/*!
*	\fn		ProjectPerp
*	\brief	Calculates the perpendicular projection of one vector onto another
*	\param	const Vector2&: A reference to the vector to project onto
*	\return The projection
*/
Vector2		Vector2::ProjectPerp(const Vector2& rhs)
{
	Vector2 temp; // holder vector
	temp = *this - Project(rhs); // calculate the perpendicular projection vector
	return temp; // return the perpendicular projection vector
}
/*!
*	\fn		CrossMag
*	\brief	Calculates cross magnitude of two vectors
*	\param	const Vector2&: A reference to the vector to calculate with
*	\return The scalar result
*/
float  Vector2::CrossMag(const Vector2& rhs)
{
	f32 mag = x*rhs.y - rhs.x*y; // calculate the cross magnitude
	return mag; // return the result
}
/*!
*	\fn		GetAngle
*	\brief	Gets the angle of the vector respect to the origin
*	\return The scalar result
*/
float  Vector2::GetAngle()
{
	f32 angle = static_cast<f32>(atan2(x, y)); // get the angle
	return angle; // return the angle
}
/*!
*	\fn		FromAngle
*	\brief	Gets the vector based on an angle
*	\param float rad_angle: The angle to start with
*/
Vector2&  Vector2::FromAngle(float rad_angle)
{
	x = static_cast<f32>(cos(rad_angle)); // get the coordinate using trigonometry
	y = static_cast<f32>(sin(rad_angle));	// get the coordinate using trigonometry
	return *this;
}
/*!
*	\fn		Random
*	\brief	Calculates a random vector in a range
*	\param float min_x: The minimun x value.
*	\param float min_x: The maximum x value.
*	\param float min_x: The minimun y value.
*	\param float min_x: The maximum y value.
*	\return the randomized vector
*/
Vector2 Vector2::Random(float min_x, float max_x, float min_y, float max_y)
{
	Vector2 temp; // holder vector
	srand(static_cast <int>(time(0))); // randomize the seed
	temp.x = min_x + static_cast <f32> (rand()) / (RAND_MAX / (max_x - min_x)); // set a random value in the range
	temp.y = min_y + static_cast <f32> (rand()) / (RAND_MAX / (max_y - min_y)); // set a random value in the range
	return temp; // return the holder vector
}
/*!
*	\fn		Random01
*	\brief	Calculates a random vector
*	\return the randomized vector
*/
Vector2 Vector2::Random01()
{
	Vector2 temp; // holder vector
	srand(static_cast <int>(time(0))); // randomize the seed
	temp.x = static_cast<f32> (rand()) / static_cast <f32> (RAND_MAX); // set a random value
	temp.y= static_cast<f32> (rand()) / static_cast <f32> (RAND_MAX);	// set a random value
	return temp; // return the holder vector
}
/*!
*	\fn		Lerp
*	\brief	Calculates the lerp of two vectors
*	\param	Vector 2 start: The vector to start with
*	\param  Vector2 end: The vector to end in
*	\param  float tn: the range
*	\return The vector lerped. 
*/
Vector2 Vector2::Lerp(Vector2 start, Vector2 end, float tn)
{
	return  start *(1 - tn) +  end*tn; // calculate and return the lerp
}

void Vector2::FromJson(nlohmann::json& _j)
{
	x = _j["x"];
	y = _j["y"];
}

void Vector2::ToJson(nlohmann::json& _j)
{
	_j["x"] = x;
	_j["y"] = y;
}

Vector2& Vector2::Clamp(Vector2 start, Vector2 end)
{
	float maxX = start.x > end.x ? start.x : end.x;
	float maxY = start.y > end.y ? start.y : end.y;
	float minX = start.x < end.x ? start.x : end.x;
	float minY = start.y < end.y ? start.y : end.y;

	x = std::min(maxX, std::max(minX, x));
	y = std::min(maxY, std::max(minY, y));
	//x = std::min(start.x, std::max(end.x, x));
	//y = std::min(start.y, std::max(end.y, y));
	return *this;
}

std::ostream & operator<<(std::ostream & os, const Vector2 & vec)
{
	os << "{" << vec.x << ", " << vec.y << "}";
	return os;
}

double ToRadians(double og)
{
	return og *= (PI / 180);
}

double ToDegrees(double og)
{
	return og /= (PI / 180);
}