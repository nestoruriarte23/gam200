// ----------------------------------------------------------------------------
//	Copyright (C)DigiPen Institute of Technology.
//	Reproduction or disclosure of this file or its contents without the prior
//	written consent of DigiPen Institute of Technology is prohibited.
//	
//	File Name:		Vector2.h
//	Purpose:		Declaration of Vector2 class and operations.
//	Project:		GAM200
//	Author:			David Miranda Pazos
// ----------------------------------------------------------------------------
#ifndef VECTOR2_H
#define VECTOR2_H

#include <iostream>
#include "../../Serializer/json.hpp"


struct Vector3;

struct Vector2
{
	union
	{
		struct
		{
			float x, y;
		};
		float v[2];
	};

	Vector2();
	Vector2(float xx, float yy);

	explicit Vector2(const Vector3& _rhs);
	Vector2& operator=(const Vector3& _rhs);

	Vector2		operator+	(const Vector2& rhs) const;
	Vector2&	operator+=	(const Vector2& rhs);
	Vector2		operator-	(const Vector2& rhs) const;
	Vector2&	operator-=	(const Vector2& rhs);

	Vector2		operator*	(float s) const;
	Vector2&	operator*=	(float s);
	Vector2		operator/	(float s) const;
	Vector2&	operator/=	(float s);

	bool		operator==  (const Vector2& rhs) const;
	bool		operator>  (const Vector2& rhs) const;
	bool		operator!=  (const Vector2& rhs) const;
	bool		operator<  (const Vector2& rhs) const;

	Vector2		operator-() const;

	float			Length() const;
	float			LengthSq() const;
	float			Distance(const Vector2 &rhs);
	float			DistanceSq(const Vector2 &rhs);

	Vector2		Normalize() const;
	Vector2&	NormalizeThis();

	float			Dot(const Vector2& rhs) const;
	float			operator*		(const Vector2& rhs) const;

	Vector2		Perp();
	Vector2		Project(const Vector2& rhs);
	Vector2		ProjectPerp(const Vector2& rhs);

	float			CrossMag(const Vector2& rhs);
	float			GetAngle();
	Vector2&		FromAngle(float rad_angle);

	static Vector2	Random(float min_x, float max_x, float min_y, float max_y);
	static Vector2	Random01();

	static Vector2 Lerp(Vector2 start, Vector2 end, float tn);
	Vector2& Clamp(Vector2 start, Vector2 end);

	void FromJson(nlohmann::json& _j);
	void ToJson(nlohmann::json& _j);

	friend std::ostream & operator<<(std::ostream & os, const Vector2 & vec);
};

double ToRadians(double og);
double ToDegrees(double og);

#endif