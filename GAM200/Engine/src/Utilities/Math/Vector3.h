#pragma once

#include "MyMath.h"

struct Vector3
{
	union
	{
		struct
		{
			float x, y, z;
		};
		float v[3];
	};
		// ------------------------------------------------------------------------
		// Default Constructor - Sets the vector to (0,0).
		// ------------------------------------------------------------------------
		Vector3();

		Vector3(const Vector2& vec2, float zz) : x(vec2.x), y(vec2.y), z(zz) {}

		// ------------------------------------------------------------------------
		// Custom Constructor - Sets to the specified values.
		// ------------------------------------------------------------------------
		Vector3(float xx, float yy, float zz);

		// ------------------------------------------------------------------------
		// add or substract along with the assignment versions.
		// ------------------------------------------------------------------------
		Vector3		operator+	(const Vector3& rhs) const;
		Vector3&	operator+=	(const Vector3& rhs);
		Vector3		operator-	(const Vector3& rhs) const;
		Vector3&	operator-=	(const Vector3& rhs);

		Vector3&	operator-=	(const Vector2& rhs);
		Vector3&	operator+=	(const Vector2& rhs);
		Vector2	operator-	(const Vector2& rhs);
		Vector2	operator+	(const Vector2& rhs);
		// ------------------------------------------------------------------------
		// multiply or divide by a scalar along with the assignment versions.
		// ------------------------------------------------------------------------
		Vector3		operator*	(float s) const;
		Vector3&	operator*=	(float s);
		Vector3		operator/	(float s) const;
		Vector3&	operator/=	(float s);

		// ------------------------------------------------------------------------
		// unitary negation
		// ------------------------------------------------------------------------
		Vector3		operator-() const;

		// ------------------------------------------------------------------------
		// Length - Returns the length of this vector.
		// ------------------------------------------------------------------------
		float			Length() const;

		// ------------------------------------------------------------------------
		// LengthSq - Returns the squared length of this vector.
		// ------------------------------------------------------------------------
		float			LengthSq() const;

		// ------------------------------------------------------------------------
		// Distance - Returns the distance from this vector to 'rhs'
		// ------------------------------------------------------------------------
		float			Distance(const Vector3 &rhs);

		// ------------------------------------------------------------------------
		// DistanceSq - Returns the squared distance from this vector to 'rhs'
		// ------------------------------------------------------------------------
		float			DistanceSq(const Vector3 &rhs);

		// ------------------------------------------------------------------------
		// Normalize - Returns the normalized version of this vector
		// ------------------------------------------------------------------------
		Vector3		Normalize() const;

		// ------------------------------------------------------------------------
		// NormalizeThis - Computes the normalized version of this vector and set
		// this vector to it. 
		// ------------------------------------------------------------------------
		Vector3 & NormalizeThis();

		// ------------------------------------------------------------------------
		// Dot product
		// ------------------------------------------------------------------------
		float			Dot(const Vector3& rhs) const;
		float			operator*		(const Vector3& rhs) const;

		// ------------------------------------------------------------------------
		// perpendicular, projection & perpendiculat projection
		// ------------------------------------------------------------------------
		Vector3		Perp();
		Vector3		Project(const Vector3& rhs);
		Vector3		ProjectPerp(const Vector3& rhs);

		// ------------------------------------------------------------------------
		// CrossMag - Returns the cross product magnitude.
		// ------------------------------------------------------------------------
		float			CrossMag(const Vector3& rhs);

		// ------------------------------------------------------------------------
		// GetAngle - Returns and angle (in radians) that represents the CCW positive
		// angle of this vector.
		// ------------------------------------------------------------------------
		float		GetAngle();

		// ------------------------------------------------------------------------
		// FromAngle - Sets this vector as the result of getting a unit vector 
		// from the specified angle in radians
		// ------------------------------------------------------------------------
		void		FromAngle(float rad_angle);

		// ------------------------------------------------------------------------
		// Static interface
		// ------------------------------------------------------------------------

		// ------------------------------------------------------------------------
		// Random - Returns a random vector such that min_x < x < max_x  and 
		// min_y < y < max_y
		// ------------------------------------------------------------------------
		static Vector3 Random(float min_x, float max_x, float min_y, float max_y, float min_z, float max_z);

		// ------------------------------------------------------------------------
		// Random01 - Returns a random vector such that x and y are both in the 
		// range [0,1]
		// ------------------------------------------------------------------------
		static Vector3 Random01();

		// ------------------------------------------------------------------------
		// Lerp - Returns the result vector of linearly interpolating from 
		// start to end using a normalized parameter tn (i.e. tn is in the range
		// [0,1].
		// ------------------------------------------------------------------------
		static Vector3 Lerp(Vector3 start, Vector3 end, float tn);

		//friend std::ostream & operator<<(std::ostream & os, const Vector3 & vec);
};