#pragma once

#include "SDL.h"
#include "../Utilities/Math/Vector2.h"
#include "../GL/glew.h"
#include "../GL/wglew.h"
#include "../GL/GL.h"

class Window
{
	public:
		Window();
		~Window();

		int Init(const char* name, int xsize, int ysize, bool fullscreen = false, int xpos = SDL_WINDOWPOS_CENTERED, int ypos = SDL_WINDOWPOS_CENTERED);
		void Free();
		HWND GetWindowHandle() { return mHWND; }

		Vector2 GetWindowSize() { return WindowSize; }
		Vector2 GetStartingWindowSize() { return StartingWindowSize; }
		
		SDL_Window* GetSDLWindow() { return window; }
		SDL_Renderer* GetRenderer() { return renderer; }
		SDL_GLContext GetRenderContext() { return OpenGLContext; }

		void ResizeWindow(int w, int h);
		void ChangeResolution(int w, int h);
		bool GetFullscreen() { return mbFullscreen; }
		void ToogleFullscreen();

	private:
		SDL_Window *window;
		SDL_Renderer *renderer;
		SDL_GLContext OpenGLContext;

		HWND	mHWND;			// Window identifier within windows, necessary for ImGUI
		bool	mbFullscreen;	// true = fullscreen // false = windowed //

		Vector2 StartingWindowSize;
		Vector2 WindowSize;	
};

//extern Window* game;
typedef SDL_GameController Controller;
