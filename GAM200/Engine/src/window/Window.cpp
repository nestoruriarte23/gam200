#include <glew.h>
#include <SDL_syswm.h>
#include <SDL_ttf.h>
#include "..\Graphics\Camera.h"
#include "Window.h"

Window::Window() : window(nullptr), renderer(nullptr) {}

Window::~Window()
{
	Free();
}

int Window::Init(const char* name, int xsize, int ysize, bool fullscreen, int xpos, int ypos)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		/// Create the window with SDL

		// Set the fullscreen boolean
		mbFullscreen = fullscreen;

		// Create the window accordingly
		if (fullscreen)
			window = SDL_CreateWindow(name, xpos, ypos, xsize, ysize, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN);
		else
			window = SDL_CreateWindow(name, xpos, ypos, xsize, ysize, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

		// Error checking
		if (window == nullptr)
			return -1;

		SDL_SysWMinfo wmInfo;
		SDL_VERSION(&wmInfo.version);
		SDL_GetWindowWMInfo(window, &wmInfo);
		HWND mHWND = wmInfo.info.win.window;

		/// Store the width and height variables 

		WindowSize = Vector2((float)xsize, (float)ysize); 
		StartingWindowSize = WindowSize;

		//SDL_RenderSetLogicalSize(renderer, xsize, ysize);

		/// Open GL things, set the atributes etc

		// Deprecated code disabled
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);	// OpenGL 3+
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);	// OpenGL 3.3

		SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 1);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);			// Use double buffer

		OpenGLContext = SDL_GL_CreateContext(window);
		if (OpenGLContext == NULL)				// Rendering context for GL
			return -1;

		/* Sync with refresh rate (0 == FALSE) */
		SDL_GL_SetSwapInterval(0);								// Match buffer Swap with monitor refresh

		glewInit();

		return 0;

	}
		
	return -1;

}

void Window::Free()
{
	SDL_DestroyWindow(window);
	window = nullptr;

	SDL_DestroyRenderer(renderer);
	renderer = nullptr;
}

void Window::ToogleFullscreen()
{
	SDL_SetWindowFullscreen(GetSDLWindow(),
		mbFullscreen ? SDL_WINDOW_SHOWN : SDL_WINDOW_FULLSCREEN);
	mbFullscreen = !mbFullscreen;
}

void Window::ChangeResolution(int w, int h)
{
	Camera::Instance()->GetViewport()->SetProperties(Vector2(0, 0), Vector2((float)w, (float)h));
	ResizeWindow(w, h);
}

void Window::ResizeWindow(int w, int h)
{
	SDL_SetWindowSize(this->GetSDLWindow(), w, h);
	WindowSize = Vector2((float)w, (float)h);
}
