#pragma once

typedef unsigned int GLuint;

class FrameBuffer
{
public:
	FrameBuffer();
	~FrameBuffer();

	void	SetAsRenderTarget() const;
	GLuint	GetHandle() const;
	GLuint	GetResultingTexture();

	void CreateBuffer();
	void DeleteBuffer();
	void RegenerateBuffer();

	static void SetDefaultRenderTarget();

private:
	GLuint		mHandle;
	GLuint		mResultTexture;
	GLuint		mDepth;
};