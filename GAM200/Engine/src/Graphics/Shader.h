#pragma once

#include <list>
#include <iostream>
#include <fstream>
#include <sstream>

typedef unsigned int GLuint;

class Shader
{
public:
	unsigned int ID;
	Shader(std::string filename);
	GLuint GetHandle() { return mHandle; }

private:
	void CreateOpenGLShader(std::string filename);
	void LoadFileData(std::string filename);
	void CompileShader();

	GLuint				mHandle;
	std::string			mSource;
	std::string			mFilename;
};


class ShaderProgram 
{
public:
	ShaderProgram(std::string directory);
	GLuint GetHandle() const { return mHandle; }
	void SetMatrix44(const char* name, float* value_array);
	void Destroy();

private:
	void CreateOpenGLShaderProgram();
	void GetShadersFromDirectory(std::string directory);
	void LinkAllShaders();
		
	GLuint					mHandle;
	std::list<GLuint>		mShaderHandles;
};