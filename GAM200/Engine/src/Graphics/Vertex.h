#pragma once

#include "../Utilities/Math/Vector2.h"
#include "../Utilities/Math/Vector3.h"
#include "Color.h"

class Vertex
{
public:
	Vertex() : mPos(), mUV(), mColor() {}
	Vertex(const Vector3& _pos, const Vector2& _uv, const Engine::Color& _color)
		: mPos(_pos), mUV(_uv), mColor(_color) {}
	Vertex(const Vector3& _pos, const Engine::Color& _color)
		: mPos(_pos), mUV(), mColor(_color) {}
	Vertex(const Vector3& _pos)
		: mPos(_pos), mUV(), mColor() {}
	Vertex(float x, float y, float z, float u, float v,
		float r, float g, float b, float a) :
		mPos(x, y, z), mUV(u, v), mColor(r, g, b, a) {}

	void SetVertexColor(const Engine::Color& _color) { mColor = _color; }
	void SetPosition(const Vector2& _pos);
	void SetPosition(const Vector3& _pos);
	void SetTexCoord(const Vector2& _coords);

private:
	Vector3				mPos;
	Vector2				mUV;
	Engine::Color		mColor;
};

class ParticleVertex
{
public:
	ParticleVertex(float x, float y, float u, float v)
		: mPos(x, y), mUV(u, v) {}

private:
	Vector2			mPos;
	Vector2			mUV;
};

class Line
{
public:
	Line() : origin(), end() {}
	Line(const Vector3& _origin, const Vector3& _end, const Engine::Color& _color)
		: origin(_origin, _color), end(_end, _color) {}

private:
	Vertex origin;
	Vertex end;
};