#include <iostream>

#include "RenderManager.h"
#include "..//Utilities/MyDebug/MyDebug.h"
#include "..//Game/Game.h"
#include "FrameBuffer.h"

FrameBuffer::FrameBuffer()
{
	CreateBuffer();
}

FrameBuffer::~FrameBuffer()
{
	DeleteBuffer();
}

void FrameBuffer::SetAsRenderTarget() const
{
	check_gl_error();
	glBindFramebuffer(GL_FRAMEBUFFER, mHandle);
	check_gl_error();
}

GLuint FrameBuffer::GetHandle() const
{
	return mHandle;
}

GLuint FrameBuffer::GetResultingTexture()
{
	return mResultTexture;
}

void FrameBuffer::CreateBuffer()
{
	// Create the frame buffer itself
	glGenFramebuffers(1, &mHandle);
	glBindFramebuffer(GL_FRAMEBUFFER, mHandle);

	// Window size
	Vector2 size = Game->GetWindowSize();

	// Generate the color (texture) part of the frame buffer
	glGenTextures(1, &mResultTexture);
	glBindTexture(GL_TEXTURE_2D, mResultTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int)size.x, (int)size.y, 0, GL_RGBA, 
		GL_UNSIGNED_BYTE, nullptr); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Generate the depth buffer
	glGenTextures(1, &mDepth);
	glBindTexture(GL_TEXTURE_2D, mDepth);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, (int)size.x, (int)size.y, 0,
		GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Attach it to currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
		GL_TEXTURE_2D, mResultTexture, 0); 
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, mDepth, 0);

	// IF DEPTH / STENCIL TESTING NEEDED, GO TO THIS TUTORIAL ON RENDER BUFFERS
	// (https://learnopengl.com/Advanced-OpenGL/Framebuffers)	// TOOD

	// ERROR CHECKING
	ASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::DeleteBuffer()
{
	/* Unbind the buffer */
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	/* Destroy / Delete this buffer */
	glDeleteFramebuffers(1, &mHandle);
}

void FrameBuffer::RegenerateBuffer()
{
	DeleteBuffer();
	CreateBuffer();
}

void FrameBuffer::SetDefaultRenderTarget()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
