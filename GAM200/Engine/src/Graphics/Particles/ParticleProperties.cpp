#include <cstdlib>
#include "Particles.h"
#include "ParticleProperties.h"
#include "../RenderManager.h"
#include "../../Game/Game.h"
#include "../../Level/Editor/ImGUI_Basics.h"
#include "../../Level/Editor/ImGui/imgui.h"
#include "../../System/GameObjectManager/GameObject.h"
#include "../../ResourceManager/ActualResourceManager.h"

static std::string ShapeNames[] = { "Point", "Line", "Circle", "Rectangle" };

/* Random float between 0 and 1 with as many decimals as requested */
static float RandFloat(int decimals = 4)
{
	int precision = 1;
	while (decimals-- > 0) precision *= 10;
	return (rand() % precision) / (float)precision;
}

static float LerpFloat(float _min, float _max, float _factor)
{
	return _min + (_max - _min) * _factor;
}

static float RandFloat(float _min, float _max)
{
	return LerpFloat(_min, _max, RandFloat());
}

template <typename T>
static T Lerp(T _class[2], float _factor)
{
	return T::Lerp(_class[0], _class[1], _factor);
}

ParticleSystem::~ParticleSystem()
{
}

void ParticleSystem::Initialize()
{
	SetEnabled(true);

	renderable::Initialize();
	ParticleMgr->AddSystem(this);

	mParticleTexture = gActualResourceMgr.GetResource<Texture>
		("..//Resources//Textures//Particle.png");

	mShaderProgram = ParticleMgr->GetShaderProgram();
	mModel = ParticleMgr->GetParticleModel();

	mNewSize = mSize;

	mParticles = nullptr;
	mParticleProperties = nullptr;

	if (mNext)
	{
		mNext->mOwner = this->mOwner;
		mNext->Initialize();
	}

	GenerateParticlePool();
}

void ParticleSystem::Shutdown()
{
	DeleteParticlePool();

	if (mNext)
	{
		mNext->Shutdown();
		delete mNext;
	}

	if (!IsEnabled()) return;

	renderable::Shutdown();
	ParticleMgr->RemoveSystem(this);
}

bool yeep = false;
void ParticleSystem::Update(double _dt)
{
	//if (Game->onEditor && !mbIsActive) return;

	/* Get the amount of particles to emit */
	unsigned amount = !mbPause ? mEmitRate : 0;
	if (!mbIsActive && Game->onEditor)
	{
		amount = 0;
	}

	/* Store only the position in the array */
	std::list<unsigned> deadParticles;

	for (int i = 0; i < mActiveParticles; ++i)
	{
		/* Modifes the particle based on the properties, return true if dead */
		if (mParticleProperties[i].Update(_dt))
		{
			deadParticles.push_back(mParticleProperties[i].mParticle - mParticles);
			mParticleProperties[i].mParticleSystem->mParticleCount--;
		}
	}

	/* Sort */
	deadParticles.sort();

	/* Fill the empty spaces with new particles if required */
	while (amount > 0 && !deadParticles.empty())
	{
		/* Initalize the particle with the right properties and update the count */
		mParticleProperties[deadParticles.front()].Initialize(this);
		deadParticles.pop_front();
		mParticleCount++;
		amount--;
	}

	/* Add "new" particles */
	while (amount > 0)
	{
		if (mActiveParticles >= mSize) return;

		mParticleProperties[mActiveParticles++].Initialize(this);
		mParticleCount++;
		amount--;
	}

	/* Shift particles from the back, to fill empty spaces */
	while (!deadParticles.empty())
	{
		CopyParticle(mActiveParticles - 1, deadParticles.front());

		mActiveParticles--;
		deadParticles.pop_front();
	}
	
}

void ParticleSystem::Render()
{
	ParticleMgr->AddToRenderQueue(this);
}

void ParticleSystem::RenderParticles()
{
	if (mActiveParticles == 0)
		return;


	if (!RenderMgr->GetDrawingOutline())		// First iteration, draw everything normally 
	{
		if (!mbEmitOutline)
			glStencilMask(0x00);					// Do not write to Stencil Buffer 
		else
		{
			glStencilFunc(GL_ALWAYS, 1, 0xFF);		// set the stencil value 0000 0001 
			glStencilMask(0xFF);					// Write to Stencil Buffer
		}
	}

	check_gl_error();

	RenderMgr->BindModel(&ParticleMgr->mParticleModel);


	check_gl_error();

	// Bind buffer que contiene el array con los "offsets"
	glBindBuffer(GL_ARRAY_BUFFER, ParticleMgr->mParticleModel.mOffsetArray);
	glBufferSubData(GL_ARRAY_BUFFER, 0, mActiveParticles * sizeof(Particle), mParticles);

	RenderMgr->BindModel(&ParticleMgr->mParticleModel);

	glBindTexture(GL_TEXTURE_2D, mParticleTexture->Get()->mHandle);

	glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, mActiveParticles);

	check_gl_error();

	glBindVertexArray(0);

	check_gl_error();
}

void ParticleSystem::CopyParticle(unsigned _origin, unsigned _target)
{
	/* Copy both the controller/properties and the particle data */
	mParticleProperties[_target] = mParticleProperties[_origin];
	mParticles[_target] = mParticles[_origin];

	/* Rebind the conexions */
	mParticleProperties[_target].mParticle = &mParticles[_target];
	mParticleProperties[_origin].mParticle = &mParticles[_origin];
}

void ParticleSystem::GenerateParticlePool()
{
	DeleteParticlePool();

	mParticles = DBG_NEW Particle[mSize];
	mParticleProperties = DBG_NEW ParticleProperties[mSize];

	for (int i = 0; i < mSize; ++i)
	{
		mParticleProperties[i].mParticle = &mParticles[i];
	}
}

void ParticleSystem::DeleteParticlePool()
{
	if (mParticles)				delete[] mParticles;
	if (mParticleProperties)	delete[] mParticleProperties;

	mActiveParticles = 0;
}

bool ParticleSystem::Edit()
{
	bool changed = false;
	ImGui::PushID("ParticleSystem");

	if (ImGui::TreeNode("ParticleSystem"))
	{
		/* IF there are multiple systems */
		if (mNext)
		{
			if (ImGui::Button("Pause/Unpause"))
			{
				mbPause = !mbPause;

				auto temp_ = mNext;
				while (temp_)
				{
					temp_->mbPause = this->mbPause;
					temp_ = temp_->mNext;
				}

				changed = true;
			} ImGui::SameLine();
			if (ImGui::Button("RunAll"))
			{
				auto temp_ = this;
				while (temp_)
				{
					temp_->mbIsActive = true;
					temp_ = temp_->mNext;
				}
			} ImGui::SameLine();
			if (ImGui::Button("ResetAll"))
			{
				auto temp_ = this;
				while (temp_)
				{
					temp_->Reset();
					temp_ = temp_->mNext;
				}
			}
		}

		auto next = this;
		int counter = 0;
		while (next)
		{
			ImGui::PushID(counter++);

			if (ImGui::TreeNode(next->mName.data()))
			{
				ImGui::PushID(300 + counter);
				ImGui::InputText("", next->new_name.data(), 30);
				if (ImGui::IsItemDeactivatedAfterChange())
				{
					next->mName = next->new_name.data();
				}
				ImGui::PopID();
				if (next->mPrev)
				{
					ImGui::SameLine();
					if (ImGui::Button("Up"))
					{
						ParticleSystem* og_prev_next = next->mPrev->mNext;
						ParticleSystem* og_next = next->mNext;
						ParticleSystem* og_prev = next->mPrev;
						ParticleSystem* og_prev_prev = next->mPrev->mPrev;

						/* Copy the next system to a temporal objects, but first set it's "next"
						to null so only the contents of this system are copied */
						next->mPrev->mNext = nullptr;
						ParticleSystem temp_sys = *og_prev;

						/* Same logic, copy contents of "next" (current system) to it's next */
						next->mNext = nullptr;
						*og_prev = *next;

						/* Final copy */
						*next = temp_sys;

						/* Restore the pointer values */
						next->mPrev = og_prev;
						next->mPrev->mNext = og_prev_next;
						next->mPrev->mPrev = og_prev_prev;
						next->mNext = og_next;

						std::swap(next->mName, next->mPrev->mName);

						changed = true;
					}
				}
				if (next->mNext)
				{
					ImGui::SameLine();

					/* Swap contents of the current system with the next one */
					if (ImGui::Button("Down"))
					{
						ParticleSystem* og_next_next = next->mNext->mNext;
						ParticleSystem* og_next = next->mNext;
						ParticleSystem* og_prev = next->mPrev;
						ParticleSystem* og_next_prev = next->mNext->mPrev;

						/* Copy the next system to a temporal objects, but first set it's "next"
						to null so only the contents of this system are copied */
						next->mNext->mNext = nullptr;
						ParticleSystem temp_sys = *next->mNext;

						/* Same logic, copy contents of "next" (current system) to it's next */
						next->mNext = nullptr;
						*og_next = *next;

						/* Final copy */
						*next = temp_sys;

						/* Restore the pointer values */
						next->mNext = og_next;
						next->mNext->mNext = og_next_next;
						next->mPrev = og_prev;
						next->mNext->mPrev = og_next_prev;
						
						std::swap(next->mName, next->mNext->mName);

						changed = true;
					}
				}

				changed = ImGui::Checkbox(" Pause", &next->mbPause) || changed;
				ImGui::SameLine();
				if (ImGui::Checkbox(" EditorActive", &next->mbIsActive))
				{
					next->Reset();
				}
				ImGui::SameLine();
				if (ImGui::Button("Reset"))
				{
					next->Reset();
				}

				changed = ImGui::Checkbox(" EmitOutline", &next->mbEmitOutline) || changed;

				ImGui::Separator();

				changed = ImGui::DragInt(" Max Particles", &next->mNewSize, 5.0f, 0, MAX_PARTICLES_PER_SYS) || changed;
				changed = ImGui::DragInt(" EmitRate", &next->mEmitRate, 1.0f, 0, 100) || changed;
				changed = ImGui::DragFloat2(" LifeTime", next->mLifeTimeRange, 0.1f) || changed;

				ImGui::Separator();

				if (ImGui::BeginCombo(" Shape", ShapeNames[next->mShape].data()))
				{
					for (unsigned n = 0; n < ParticleShapes::TotalShapes; n++)
					{
						bool is_selected = (next->mShape == n);
						if (ImGui::Selectable(ShapeNames[n].data(), is_selected))
						{
							next->mShape = n;
							changed = true;
						}
						if (is_selected)
						{
							next->mShape = n;
							ImGui::SetItemDefaultFocus();
						}
					}
					ImGui::EndCombo();
				}
				changed = ImGui::DragFloat2(" CenterOffset", next->mCenterOffset.v, 0.5f) || changed;

				if (next->mShape == ParticleShapes::Line_Shape)
				{
					changed = ImGui::DragFloat2(" LineStart", next->mLineStart.v, 3.0f) || changed;
					changed = ImGui::DragFloat2(" LineEnd", next->mLineEnd.v, 3.0f) || changed;
				}
				else if (next->mShape == ParticleShapes::Circle_Shape)
				{
					changed = ImGui::Checkbox(" Fill", &next->mFillShape) || changed;
					changed = ImGui::DragFloat(" Radius", &next->mRadius, 1.0f) || changed;
				}
				else if (next->mShape == ParticleShapes::Rectangle_Shape)
				{
					changed = ImGui::Checkbox(" Fill", &next->mFillShape) || changed;
					changed = ImGui::DragFloat2(" Dimensions", next->mFrame.v, 3.0f) || changed;
				}

				ImGui::Separator();

				changed = ImGui::Checkbox(" GoToDestination", &next->mbGoToDestination) || changed;
				if (next->mbGoToDestination)
				{
					changed = ImGui::DragFloat2(" TargetPoint", next->mDestination.v, 3.0f) || changed;
					changed = ImGui::DragFloat(" Difusion Rate", &next->mDistorsionRate, 0.01f) || changed;
				}
				else
				{
					changed = ImGui::DragFloat2(" StartVelocityX", next->mVelRangeX[0].v, 0.1f) || changed;
					changed = ImGui::DragFloat2(" EndVelocityX", next->mVelRangeX[1].v, 0.1f) || changed;
					changed = ImGui::DragFloat2(" StartVelocityY", next->mVelRangeY[0].v, 0.1f) || changed;
					changed = ImGui::DragFloat2(" EndVelocityY", next->mVelRangeY[1].v, 0.1f) || changed;
					changed = ImGui::Checkbox(" FollowEmitter", &next->mbFollowParent) || changed;
				}

				ImGui::Separator();
				changed = ImGui::DragFloat2(" StartSize", next->mScaleStart.v, 0.1f) || changed;
				changed = ImGui::DragFloat2(" EndSize", next->mScaleEnd.v, 0.1f) || changed;
				ImGui::Separator();

				changed = ImGui::ColorEdit4(" StartColor", (float*)&next->mColorStart,
					RenderMgr->GetColorFlags()) || changed;
				changed = ImGui::ColorEdit4(" EndColor", (float*)&next->mColorEnd,
					RenderMgr->GetColorFlags()) || changed;

				#pragma region // Change Texture //
				unsigned selected_item = -2;
				auto& all_textures = gActualResourceMgr.GetAllFilenamesOfType(FILETYPE::TextureType);

				if (selected_item == -2 && next->mParticleTexture)
				{
					auto position = std::find(all_textures.begin(), all_textures.end(), next->mParticleTexture->GetFilename());
					selected_item = std::distance(all_textures.begin(), position);
				}

				if (ImGui::BeginCombo(" Texture", next->mParticleTexture ? all_textures[selected_item].data() : " - Default - "))
				{
					bool is_selected = (next->mParticleTexture == nullptr);

					if (ImGui::Selectable(" - Default - ", is_selected))
					{
						selected_item = -1;
						next->mParticleTexture = nullptr;
						changed = true;
					}

					if (is_selected)
					{
						selected_item = -1;
						ImGui::SetItemDefaultFocus();
					}

					for (unsigned n = 0; n < all_textures.size(); n++)
					{
						bool is_selected = (selected_item == n);
						if (ImGui::Selectable(all_textures[n].data(), is_selected))
						{
							selected_item = n;
							next->mParticleTexture = gActualResourceMgr.GetResource<Texture>(all_textures[n]);
							changed = true;
						}
						if (ImGui::IsItemHovered())
						{
							ImGui::BeginTooltip();
							ImGui::Image((void*)(intptr_t)(gActualResourceMgr.GetResource<Texture>
								(all_textures[n])->Get()->mHandle), ImVec2(80, 80), ImVec2(0, 0),
								ImVec2(1, -1), ImVec4(1.0f, 1.0f, 1.0f, 1.0f), ImVec4(1.0f, 1.0f, 1.0f, 0.5f));
							ImGui::EndTooltip();
						}

						if (is_selected)
						{
							selected_item = n;
							ImGui::SetItemDefaultFocus();
						}
					}
					ImGui::EndCombo();
				}
				#pragma endregion

				ImGui::TreePop();
			}
			else
			{
				if (next->mPrev || next->mNext)
				{
					ImGui::SameLine(ImGui::GetWindowWidth() - 35);

					ImGui::PushID("Delete");
					if (ImGui::Button("", ImVec2(14, 14))) 
					{
						if (next->mPrev)
						{
							if (next->mNext) next->mNext->mPrev = next->mPrev;
							next->mPrev->mNext = next->mNext;
							next->mNext = nullptr;
							next->Shutdown();
							delete next;
						}
						else
						{
							auto temp_next = next->mNext;
							temp_next->mPrev = nullptr;
							next->mNext = nullptr;
							mOwner->RemoveComp(next);
							temp_next->mOwner->AddComp(temp_next);
						}

						changed = true;

						ImGui::PopID();
						ImGui::PopID();
						break;
					}
					ImGui::PopID();
				}
			}

			ImGui::PopID();

			if (next->mNext)
				next = next->mNext;
			else
				break;	// This way next is the last part sys of the 
						// list, and we could add another one
		}

		if (ImGui::Button("AddSystem"))
		{
			auto _last = next;
			auto _created = _last->mNext = DBG_NEW ParticleSystem();
			_created->mPrev = _last;
			_created->mOwner = this->mOwner;
			_created->Initialize();
			_created->mName = "New System";
			memcpy(_created->new_name.data(), mName.data(), mName.length() + 1);
			changed = true;
		}

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}

	ImGui::PopID();
	return changed;
}

void ParticleSystem::SetPause(bool _pause)
{
	mbPause = _pause;
}

void ParticleSystem::SetDestination(const Vector2 & _target)
{
	mDestination = _target;
}

void ParticleSystem::SetDestination(GameObject * _target)
{
	mTarget = &_target->mPosition;
}

void ParticleSystem::SetPosition(const Vector2 & _location)
{
	mPosition = _location;
}

void ParticleSystem::SetTargetPoint(Vector2& _location)
{
	mTarget = &_location;
}

void ParticleSystem::SetRadius(float radius)
{
	mRadius = radius;
}

float ParticleSystem::GetMaxParticleLife()
{
	return (mLifeTimeRange[0] > mLifeTimeRange[1] ? mLifeTimeRange[0] : mLifeTimeRange[1]);
}

unsigned ParticleSystem::GetSystemCount()
{
	unsigned count = 0;
	auto temp_next = this;
	while (temp_next)
	{
		temp_next = temp_next->mNext;
		count++;
	}
	return count;
}

ParticleSystem * ParticleSystem::GetSubSystem(unsigned index)
{
	if (index > GetSystemCount()) return nullptr;

	auto temp_next = mNext;
	while (index-- > 0)
	{
		if (!temp_next) return nullptr;
		temp_next = temp_next->mNext;
	}

	return temp_next;
}

ParticleSystem * ParticleSystem::GetSubSystem(std::string _name)
{
	auto temp_next = this;
	while (temp_next)
	{
		if (temp_next->mName == _name) return temp_next;
	}
	return nullptr;
}

ParticleSystem * ParticleSystem::Clone()
{
	ParticleSystem* _new = DBG_NEW ParticleSystem(*this);

	_new->mParticles = nullptr;
	_new->mParticleProperties = nullptr;

	_new->mNext = mNext ? mNext->Clone() : nullptr;

	return _new;
}

void ParticleSystem::FromJson(nlohmann::json & _j)
{
	if (_j.find("mbPause") != _j.end())
		mbPause = _j["mbPause"];

	mEmitRate = _j["mEmitRate"];

	mLifeTimeRange[0] = _j["minLife"];
	mLifeTimeRange[1] = _j["maxLife"];

	if (_j.find("XStartVel") != _j.end())
	{
		mVelRangeX[0].FromJson(_j["XStartVel"]);
		mVelRangeX[1].FromJson(_j["XEndtVel"]);

		mVelRangeY[0].FromJson(_j["YStartVel"]);
		mVelRangeY[1].FromJson(_j["YEndtVel"]);
	}

	if (_j.find("mParticleColor") != _j.end())
		mParticleColor.FromJson(_j["mParticleColor"]);


	if (_j.find("mColorStart") != _j.end())
		mColorStart.FromJson(_j["mColorStart"]);

	if (_j.find("mColorEnd") != _j.end())
		mColorEnd.FromJson(_j["mColorEnd"]);

	if (_j.find("mScaleStart") != _j.end()) {
		mScaleStart.x = _j["mScaleStart"]["x"];
		mScaleStart.y = _j["mScaleStart"]["y"];
	}

	if (_j.find("mScaleEnd") != _j.end()) {
		mScaleEnd.x = _j["mScaleEnd"]["x"];
		mScaleEnd.y = _j["mScaleEnd"]["y"];
	}

	if (_j.find("mbFollowParent") != _j.end())
		mbFollowParent = _j["mbFollowParent"];

	if (_j.find("mPosition") != _j.end()) {
		mPosition.x = _j["mPosition"]["x"];
		mPosition.y = _j["mPosition"]["y"];
	}

	if (_j.find("maxParticles") != _j.end()) {
		mSize = _j["maxParticles"];
	}

	if (_j.find("mDestination") != _j.end()) {
		mDestination.ToJson(_j["mDestination"]);
		mbGoToDestination = _j["mbGoToDestination"];
	}

	if (_j.find("mDistorsionRate") != _j.end()) {
		mDistorsionRate = _j["mDistorsionRate"];
	}

	if (_j.find("NextPartSystem") != _j.end()) {
		mNext = DBG_NEW ParticleSystem();
		mNext->mOwner = mOwner;
		mNext->FromJson(_j["NextPartSystem"]);
		mNext->mPrev = this;
	}

	if (_j.find("name") != _j.end()) {
		new_name = _j["name"];
		mName = new_name.data();
	}

	if (_j.find("CenterOffset") != _j.end())
		mCenterOffset.FromJson(_j["CenterOffset"]);

	if (_j.find("mShape") != _j.end())
		mShape = _j["mShape"];

	if (_j.find("mRadius") != _j.end())
	{
		mRadius = _j["mRadius"];
		mFillShape = _j["mFillShape"];
		mFrame.FromJson(_j["mFrame"]);
		mLineStart.FromJson(_j["mLineStart"]);
		mLineEnd.FromJson(_j["mLineEnd"]);
	}
}

void ParticleSystem::ToJson(nlohmann::json & _j)
{
	_j["_type"] = "ParticleSystem";

	_j["name"] = new_name;

	_j["maxParticles"] = mSize;

	_j["mbPause"] = mbPause;
	_j["mEmitRate"] = mEmitRate;

	_j["minLife"] = mLifeTimeRange[0];
	_j["maxLife"] = mLifeTimeRange[1];

	_j["mShape"] = mShape;
	_j["mRadius"] = mRadius;
	_j["mFillShape"] = mFillShape;
	mCenterOffset.ToJson(_j["CenterOffset"]);
	mFrame.ToJson(_j["mFrame"]);
	mLineStart.ToJson(_j["mLineStart"]);
	mLineEnd.ToJson(_j["mLineEnd"]);

	mVelRangeX[0].ToJson(_j["XStartVel"]);
	mVelRangeX[1].ToJson(_j["XEndtVel"]);

	mVelRangeY[0].ToJson(_j["YStartVel"]);
	mVelRangeY[1].ToJson(_j["YEndtVel"]);

	mParticleColor.ToJson(_j["mParticleColor"]);

	mColorStart.ToJson(_j["mColorStart"]);
	mColorEnd.ToJson(_j["mColorEnd"]);

	mDestination.ToJson(_j["mDestination"]);
	_j["mbGoToDestination"] = mbGoToDestination;
	_j["mDistorsionRate"] = mDistorsionRate;

	_j["mScaleStart"];
	_j["mScaleStart"]["x"] = mScaleStart.x;
	_j["mScaleStart"]["y"] = mScaleStart.y;

	_j["mScaleEnd"];
	_j["mScaleEnd"]["x"] = mScaleEnd.x;
	_j["mScaleEnd"]["y"] = mScaleEnd.y;

	_j["mbFollowParent"] = mbFollowParent;

	_j["mPosition"];
	_j["mPosition"]["x"] = mPosition.x;
	_j["mPosition"]["y"] = mPosition.y;

	if (mNext) mNext->ToJson(_j["NextPartSystem"]);
}

bool ParticleSystem::are_the_same_type(IComp const & lhs)
{
	if (dynamic_cast<const ParticleSystem*>(&lhs)) {
		return true;
	}
	return false;
}

void ParticleSystem::operator=(IComp & _go)
{
	ParticleSystem * particle = dynamic_cast<ParticleSystem*>(&_go);

	mbPause = particle->mbPause;
	mEmitRate = particle->mEmitRate;

	new_name = particle->new_name;
	mName = particle->mName;

	mShape = particle->mShape;
	mCenterOffset = particle->mCenterOffset;
	mRadius = particle->mRadius;
	mFrame = particle->mFrame;
	mLineStart = particle->mLineStart;
	mLineEnd = particle->mLineEnd;
	mFillShape = particle->mFillShape;

	mLifeTimeRange[0] = particle->mLifeTimeRange[0];
	mLifeTimeRange[1] = particle->mLifeTimeRange[1];

	mVelRangeX[0] = particle->mVelRangeX[0];
	mVelRangeX[1] = particle->mVelRangeX[1];

	mVelRangeY[0] = particle->mVelRangeY[0];
	mVelRangeY[1] = particle->mVelRangeY[1];

	mParticleColor = particle->mParticleColor;

	mDestination = particle->mDestination;
	mbGoToDestination = particle->mbGoToDestination;
	mDistorsionRate = particle->mDistorsionRate;

	mColorStart = particle->mColorStart;
	mColorEnd = particle->mColorEnd;

	mScaleStart = particle->mScaleStart;
	mScaleEnd = particle->mScaleEnd;

	mbFollowParent = particle->mbFollowParent;

	mPosition = particle->mPosition;
	if (particle->mNext)
	{
		auto part_next = particle->mNext;
		auto this_next = &mNext;
		auto temp_prev = this;

		if (mNext)
		{
			mNext->Shutdown();
			delete mNext;
		}

		while (part_next)
		{
			*this_next = DBG_NEW ParticleSystem(*part_next);
			(*this_next)->mNext = nullptr;
			(*this_next)->mPrev = temp_prev;
			(*this_next)->mOwner = mOwner;
			(*this_next)->Initialize();

			part_next = part_next->mNext;
			this_next = &(*this_next)->mNext;
			temp_prev = temp_prev->mNext;
		}

		if (temp_prev) temp_prev->mNext = nullptr;
	}
	else
		mNext = particle->mNext;

	// Set all
}

bool ParticleSystem::equal_to(IComp const & other) const
{
	if (ParticleSystem const* p = dynamic_cast<ParticleSystem const*>(&other)) {
		return mEmitRate == p->mEmitRate && mLifeTimeRange == p->mLifeTimeRange;
	}
	else {
		return false;
	}
}

void ParticleProperties::Initialize(ParticleSystem * _sys)
{
	mParticleSystem = _sys;

	mParticle->mScale = Vector2(0, 0);
	mParticle->mColor = _sys->mParticleColor;

	mColorStart = _sys->mColorStart;
	mColorEnd = _sys->mColorEnd;

	mScaleStart = _sys->mScaleStart;
	mScaleEnd = _sys->mScaleEnd;

	if (_sys->mShape == ParticleShapes::Point_Shape)	// Point
	{
		mStartingOffset = _sys->mCenterOffset;
	}
	else if (_sys->mShape == ParticleShapes::Line_Shape)	// Line
	{
		mStartingOffset = (_sys->mLineStart + ((_sys->mLineEnd - _sys->mLineStart) * RandFloat()));;
	}
	else if (_sys->mShape == ParticleShapes::Circle_Shape)	// Circle
	{
		// Set the angle in the range
		float angle = _sys->mTotalAngle += 0.006f; //RandFloat(-PI, PI);
		if (_sys->mTotalAngle > 2 * PI) _sys->mTotalAngle = 0;

		// The rad normalized (if it is not filled, it just takes the
		// max value = 1 so it is the outer border)
		float Rad = _sys->mRadius * (rand() % 2 == 0 ? -1 : 1);

		// Check if the ellipse/circle must be filled
		if (_sys->mFillShape)
			// If so, get a random normalized value
			Rad = RandFloat(0, Rad);

		// Return a random point in that range
		mStartingOffset = Vector2(_sys->mCenterOffset.x + cos(angle) * Rad,
			_sys->mCenterOffset.y + sin(angle) * Rad);
	}
	else if (_sys->mShape == ParticleShapes::Rectangle_Shape)	// Rectangle
	{
		// If the square has to be filled
		if (_sys->mFillShape)
		{
			// Return a completely random point inside the square
			mStartingOffset = Vector2(_sys->mCenterOffset.x + RandFloat(-_sys->mFrame.x / 2, _sys->mFrame.x / 2),
				_sys->mCenterOffset.y + RandFloat(-_sys->mFrame.y / 2, _sys->mFrame.y / 2));
		}

		// Otherwise
		else
		{
			// Decide, with a random float, in which of the 4 sides of the square the particle 
			// should be. After, just create a random particle in said line
			float randomF = RandFloat();

			// Vertical left
			if (randomF > 0.75f)
			{
				mStartingOffset = Vector2(_sys->mCenterOffset.x - _sys->mFrame.x / 2,
					_sys->mCenterOffset.y + RandFloat(-_sys->mFrame.y / 2, _sys->mFrame.y / 2));
			}
			// Vertical right
			else if (randomF > 0.5f)
			{
				mStartingOffset = Vector2(_sys->mCenterOffset.x + _sys->mFrame.x / 2,
					_sys->mCenterOffset.y + RandFloat(-_sys->mFrame.y / 2, _sys->mFrame.y / 2));
			}
			// Top
			else if (randomF > 0.25f)
			{
				mStartingOffset = Vector2(_sys->mCenterOffset.x + RandFloat(-_sys->mFrame.x / 2, 
					_sys->mFrame.x / 2), _sys->mCenterOffset.y + _sys->mFrame.y / 2);
			}
			// Bottom
			else
			{
				mStartingOffset = Vector2(_sys->mCenterOffset.x + RandFloat(-_sys->mFrame.x / 2,
					_sys->mFrame.x / 2), _sys->mCenterOffset.y - _sys->mFrame.y / 2);
			}
		}
	}
	else 	// Default
	{
		mStartingOffset = Vector2();
	}

	mOffset = Vector2(0, 0);
	mStartingPos = _sys->mOwner->mPosition + mStartingOffset;
	mFollowParent = _sys->mbFollowParent;

	mTarget = (_sys->mTarget != nullptr) ? _sys->mTarget : &_sys->mDestination;
	mbGoToDestination = _sys->mbGoToDestination;
	if (mbGoToDestination)
	{
		mDistorsion = (*mTarget - mStartingPos).Perp();
		mDistorsionRate = RandFloat(-_sys->mDistorsionRate, _sys->mDistorsionRate);
	}

	mCurrLife = 0;
	mMaxLife = RandFloat(_sys->mLifeTimeRange[0], _sys->mLifeTimeRange[1]);

	mVelocityXStart =	RandFloat(_sys->mVelRangeX[0].x, _sys->mVelRangeX[0].y);
	mVelocityXEnd =		RandFloat(_sys->mVelRangeX[1].x, _sys->mVelRangeX[1].y);
	mVelocityYStart =	RandFloat(_sys->mVelRangeY[0].x, _sys->mVelRangeY[0].y);
	mVelocityYEnd =		RandFloat(_sys->mVelRangeY[1].x, _sys->mVelRangeY[1].y);
}

bool ParticleProperties::Update(double dt)
{
	mCurrLife += (float)dt;

	if (mCurrLife >= mMaxLife) return true;

	float normalizedTime = mCurrLife / mMaxLife;

	/* Color and Scale */
	mParticle->mColor = (mColorStart + ((mColorEnd - mColorStart) * normalizedTime));
	mParticle->mScale = (mScaleStart + ((mScaleEnd - mScaleStart) * normalizedTime));

	/* Position */
	if (mbGoToDestination)
	{
		if (!mTarget) return true;

		float x = normalizedTime * 2;
		mParticle->mPos = (mStartingPos + ((*mTarget - mStartingPos) * normalizedTime));
		if (normalizedTime < 0.5f)
			mParticle->mPos += mDistorsion * (1 - (1 - x) * (1 - x)) * mDistorsionRate;
		else
			mParticle->mPos += mDistorsion * (1 - (1 - x) * (1 - x)) * mDistorsionRate;
	}
	else
	{
		mOffset.x += LerpFloat(mVelocityXStart, mVelocityXEnd, normalizedTime);
		mOffset.y += LerpFloat(mVelocityYStart, mVelocityYEnd, normalizedTime);;

		mParticle->mPos = mOffset + (mFollowParent ? 
			mParticleSystem->mOwner->mPosition + mStartingOffset : mStartingPos);
	}

	return false;
}


void ParticleSystem::Reset()
{
	mActiveParticles = 0;
	mParticleCount = 0;

	if (mSize != mNewSize)
	{
		if (mNewSize > MAX_PARTICLES_PER_SYS) mNewSize = MAX_PARTICLES_PER_SYS;

		mSize = mNewSize;
		GenerateParticlePool();
	}
}

void ParticleSystem::SetPauseAll(bool _state)
{
	auto temp_next = this;
	while (temp_next)
	{
		temp_next->SetPause(_state);
	}
}
