#include <glew.h>
#include "Particles.h"
#include "ParticleProperties.h"
#include "../Model.h"
#include "../RenderManager.h"
#include "../../System/GameObjectManager/GameObject.h"
#include "../../Level/Scene/Scene.h"
#include "../../Game/Game.h"

ParticleManager::ParticleManager() {}

bool ParticleManager::Initialize()
{
	if (!RenderMgr->CreateShaderProgram(&mParticleShader, "..//Resources//Shaders//Particle//"))
		return false;

	mParticleModel.CreateParticleQuad();
	mParticleModel.UploadParticleModelToGPU();

	return true;
}

void ParticleManager::Update(double _dt)
{
	for (auto system : mPaticleSystems)
		system->Update(_dt);
}

#include "../../Input/Input.h"

void ParticleManager::Render()
{
	/*if (!Game->onEditor)
	{
		RenderMgr->SetFrameBuffer(Game->GetCurrScene()->mParticleRealm);
		RenderMgr->SetViewport(Viewport::WholeScreen());
		RenderMgr->CleanViewport(Engine::Color::empty);
	}*/

	check_gl_error();

	RenderMgr->UseShaderProgram(ParticleMgr->mParticleShader);

	GLuint loc = glGetUniformLocation(mParticleShader->GetHandle(), "highest");
	glUniform1f(loc, RenderMgr->mHighestRenderable);

	check_gl_error();
	
	unsigned func, source, dest;
	RenderMgr->GetBlendingEquation(&func, &source, &dest);

	RenderMgr->SetBlendEquation(BlendParameter::Src_Alpha, BlendFunction::Add, BlendParameter::One);
	
	for (auto system : mRenderQueue)
		system->RenderParticles();

	mRenderQueue.clear();

	RenderMgr->SetBlendEquation(source, func, dest);

	check_gl_error();
}

unsigned ParticleManager::GetActiveParticles()
{
	unsigned mActiveParticles{0};

	std::for_each(mPaticleSystems.begin(), mPaticleSystems.end(),
	[&mActiveParticles](ParticleSystem* _sys)
	{
		mActiveParticles += _sys->mActiveParticles;
	});

	return mActiveParticles;
}

unsigned ParticleManager::GetSystemCount()
{
	return mPaticleSystems.size();
}

void ParticleManager::AddToRenderQueue(ParticleSystem * _sys)
{
	mRenderQueue.push_back(_sys);
}

void ParticleManager::Shutdown()
{
	mPaticleSystems.clear();

	/* Detach the current shader program */
	glUseProgram(0);

	/* Delete the Shader programs */
	mParticleShader->Destroy();
	delete mParticleShader;
}

void ParticleManager::AddSystem(ParticleSystem * _system)
{
	mPaticleSystems.push_back(_system);
}

void ParticleManager::RemoveSystem(ParticleSystem * _system)
{
	auto iterator = std::find(mPaticleSystems.begin(), mPaticleSystems.end(), _system);
	if (iterator == mPaticleSystems.end()) return;
	mPaticleSystems.erase(iterator);
}
