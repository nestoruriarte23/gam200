#pragma once

#include "Particles.h"

#define	MAX_PARTICLES_PER_SYS 1<<15 // ~16k
#define	DEFAULT_PARTICLES_PER_SYS 1<<10 // ~16k

enum ParticleShapes
{
	Point_Shape,
	Line_Shape,
	Circle_Shape,
	Rectangle_Shape,
	TotalShapes
};

class ParticleSystem;

class ParticleProperties
{
public:
	void Initialize(ParticleSystem* _system_prop);
	bool Update(double dt);

	// To update the transform and color of the particle overtime
	Particle*			mParticle;
	float				mMaxLife;
	float				mCurrLife;

	float				mVelocityXStart;
	float				mVelocityXEnd;
	float				mVelocityYStart;
	float				mVelocityYEnd;

	Engine::Color		mColorStart;
	Engine::Color		mColorEnd;

	Vector2				mScaleStart;
	Vector2				mScaleEnd;

	// Allow to update the particle relative to the parent or not
	Vector2				mOffset;
	Vector2				mStartingOffset;
	Vector2				mStartingPos;
	bool				mFollowParent{ false };
	bool				mbGoToDestination{ false };

	// Launch particles to a target
	Vector2				mDestination;
	Vector2*			mTarget{ nullptr };
	float				mTimeToArrive;	// Seconds
	Vector2				mDistorsion;
	float				mDistorsionRate;

	// Pointer to the owner to keep some stats
	ParticleSystem*		mParticleSystem;
};

class ParticleSystem : public renderable
{
	RTTI_DECLARATION_INHERITED(ParticleSystem, renderable)

public:
	~ParticleSystem();

	void Initialize() override;
	void Shutdown() override;
	void Update(double _dt);
	void Render();
	void Reset();
	void RenderParticles();
	bool Edit();

	void SetPause(bool _pause);
	void SetDestination(const Vector2 & _target);
	void SetDestination(GameObject* _target);
	void SetPosition(const Vector2 & _location);
	void SetTargetPoint(Vector2& _location);
	void SetRadius(float radius);
	float GetMaxParticleLife();

	unsigned GetSystemCount();
	ParticleSystem* GetSubSystem(unsigned index);

	/*!	Return a pointer to a particle system that is called the same as "name"
		If none is found, return nullptr. Is up to the user to check if the returned pointer
		is null or not. (Otherwise, buzon de sugerencias) */
	ParticleSystem* GetSubSystem(std::string _name);

	ParticleSystem*	Clone();

	void FromJson(nlohmann::json & _j);
	void ToJson(nlohmann::json & _j);

	bool are_the_same_type(IComp const& lhs);
	void operator=(IComp& _go) override;
		 
	void SetPauseAll(bool _state);

private:
	virtual bool	equal_to(IComp const& other) const;

	unsigned		mParticleCount{ 0 };
	std::array<char, 30> new_name{ 0 };

	/* Serializable data */
	bool				mbPause{ false };
	bool				mbIsActive{ false };	// Only for editor
	bool				mbFollowParent{ false };
	bool				mbGoToDestination{ false };

	int					mEmitRate;			// Emissions per update
	float				mLifeTimeRange[2];	// Life Range
	Vector2				mVelRangeX[2];		// Velocity Range. (start (max, min), end (max, min))
	Vector2				mVelRangeY[2];		// Velocity Range. (start (max, min), end (max, min))
	Engine::Color		mColorStart;	//ColorStart Value
	Engine::Color		mColorEnd;		//ColorEnd Value
	Vector2				mScaleStart = Vector2(0, 0); //Scale start value
	Vector2				mScaleEnd = Vector2(30, 30); //scale end value
	Engine::Color		mParticleColor;

	unsigned			mShape{ 0 };
	Vector2				mCenterOffset;
	float				mRadius{0};
	Vector2				mFrame;
	Vector2				mLineStart;
	Vector2				mLineEnd;
	bool				mFillShape{ false };

	Vector2				mPosition;
	Vector2				mDestination;		  // If we have a target (useful when it is a moving object,
	Vector2*			mTarget{nullptr};	  // use its position not the Destination Vector)
	float				mTimeToArrive{1};		// Seconds
	float				mDistorsionRate{0.2f};

	float				mTotalAngle{ 0 };

	TResource<Texture>*		mParticleTexture;
	ParticleSystem*			mNext{ nullptr };
	ParticleSystem*			mPrev{ nullptr };

	friend class ParticleManager;
	friend class ParticleProperties;

private:
	/* Raw Storage and const data */
	int					mSize{ DEFAULT_PARTICLES_PER_SYS };
	Particle*			mParticles{nullptr};						// Array of the amount of particles
	ParticleProperties*	mParticleProperties{nullptr};				// Array holding the data of particles
	int					mActiveParticles{ 0 };						// Total count of particles

	int					mNewSize{ DEFAULT_PARTICLES_PER_SYS };

	unsigned			mEmitQueue;

	/* Particle Management and Emitting */
	void CopyParticle(unsigned _origin, unsigned _target);

	void GenerateParticlePool();
	void DeleteParticlePool();
};
