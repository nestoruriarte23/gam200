#pragma once

#include "../Color.h"
#include "../RenderableComp.h"
#include "../../Components/Components.h"
#include "../../Utilities/Math/MyMath.h"
#include "../../Serializer/SerializeFactory.h"

class ParticleSystem;
class ParticleProperties;

struct Particle
{
	Vector2						mPos;
	Vector2						mScale;
	Engine::Color				mColor;
};

class Model;
class ShaderProgram;

class ParticleManager : public ISystem
{
	RTTI_DECLARATION_INHERITED(ParticleManager, ISystem)
	MAKE_SINGLETON(ParticleManager)

public:
	bool Initialize() override;
	void Shutdown();

	void AddSystem(ParticleSystem* _system);
	void RemoveSystem(ParticleSystem* _system);

	ShaderProgram* GetShaderProgram() { return mParticleShader; }
	Model* GetParticleModel() { return &mParticleModel; }
	void Update(double _dt);
	void Render();

	unsigned GetActiveParticles();
	unsigned GetSystemCount();

private:

	void AddToRenderQueue(ParticleSystem* _sys);

	std::vector<ParticleSystem*>	mPaticleSystems;
	std::vector<ParticleSystem*>	mRenderQueue;

	/* OpenGL and rendering Stuff */
	Model					mParticleModel;
	ShaderProgram*			mParticleShader;

	friend class RenderManager;
	friend class ParticleSystem;
};

#define ParticleMgr ParticleManager::Instance()
