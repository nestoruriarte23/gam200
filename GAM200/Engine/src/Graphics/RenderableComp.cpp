#include <string.h>
#include "Shadow.h"
#include "RenderManager.h"
#include "RenderableComp.h"
#include "../Utilities/Math/MyMath.h"
#include "../ResourceManager/ActualResourceManager.h"
#include "../System/GameObjectManager/GameObject.h"
#include "../Level/Editor/ImGUI_Basics.h"
#include "../Level/Editor/ImGui/imgui.h"
#include "../Serializer/SerializeFactory.h"
#include "../Physics/BoxCollider.h"
#include "../Game/Game.h"

#include "../Utilities/MyDebug/LeakDetection.h"

Sprite::Sprite():flipX(false),flipY(false)
{
	mbVisible = true;

	mShaderProgram = RenderManager::Instance()->GetQuadShaderProgram();
	mModel = RenderManager::Instance()->GetBasicQuad();

	mTexture = gActualResourceMgr.GetResource<Texture>
		("..//Resources//Textures//Bidooof.png");
}

void Sprite::Initialize()
{
	renderable::Initialize();

	mShadow = mOwner->get_component_type<Shadow>();
}

void Sprite::Shutdown()
{
	renderable::Shutdown();
}

renderable::~renderable() 
{
	//Shutdown();
}

Sprite * Sprite::Clone()
{
	Sprite* _new = DBG_NEW Sprite(*this);

	return _new;
}

void renderable::Initialize()
{
	if (abs(RenderMgr->mHighestRenderable) < mOwner->mPosition.y) RenderMgr->mHighestRenderable = mOwner->mPosition.y;

	RenderMgr->AddEditorRenderable(this, mOwner->GetSpace());
}

void renderable::Shutdown()
{
	RenderManager::Instance()->RemoveEditorRenderable(this);
}

bool renderable::EditRenderable()
{
	bool changed = false;

	changed = (ImGui::ColorEdit4(" ModColor", mModColor.v, RenderMgr->GetColorFlags())) || changed;
	changed = ImGui::Checkbox(" Visible", &mbVisible) || changed;

	/* Outlining */
	changed = ImGui::Checkbox(" EmitOutline", &mbEmitOutline) || changed;

	return changed;
}

void renderable::ToJsonRenderable(nlohmann::json & _j)
{
	_j["mbVisible"]			= mbVisible;
	_j["mbEmitOutline"]		= mbEmitOutline;

	mModColor.ToJson(_j["mModColor"]);
}

void renderable::FromJsonRenderable(nlohmann::json & _j)
{
	if (_j.find("mbVisible") != _j.end())
		mbVisible = _j["mbVisible"];

	if (_j.find("mbEmitOutline") != _j.end())
		mbEmitOutline = _j["mbEmitOutline"];
	else mbEmitOutline = false;

	if (_j.find("mModColor") != _j.end())
		mModColor.FromJson(_j["mModColor"]);
	else
		mModColor = Engine::Color(1, 1, 1, 1);
}

void Sprite::Render()
{
	/* Sanity Checks */
	if (!mbVisible) return;

	GameObject* owner = this->GetOwner();
	if (owner == NULL) return;

	check_gl_error();

	/* Object outlining */
	if (!RenderMgr->GetDrawingOutline())	// First iteration, draw everything normally
	{
		glStencilMask(0x00);					// Do not write to Stencil Buffer

		if (mbEmitOutline)
		{
			glStencilMask(0xFF);					// Write to Stencil Buffer
			glStencilFunc(GL_ALWAYS, 1, 0xFF);	// set the stencil value 0000 0001
		}
	}

	check_gl_error();

	ShaderProgram* myShader = RenderMgr->GetDrawingOutline() ?
		RenderMgr->GetOutlineShader() : mShaderProgram;

	check_gl_error();
	/* Set the Transform of the object */
	Transform2D mObjTransf(0, 0,
		flipX ? -owner->mScale.x : owner->mScale.x, 
		flipY ? -owner->mScale.y : owner->mScale.y,
		owner->mRotation);

	/* Get the 44 matrix from the Transform */
	Matrix44 DrawMatrix = mObjTransf.GetMatrix();
	DrawMatrix.AddTranslation(Vector3(owner->mPosition.x,
		owner->mPosition.y, (owner->mPosition.y + mOwner->mSortingPos.y)/(RenderMgr->mHighestRenderable + 10)));

	/* TODO: Check the different texture units */
	glActiveTexture(GL_TEXTURE0);

	/* Compute the modulation color based on the shadows */
	if (mShadow && !(Game->onEditor)) RenderMgr->ComputeAlphaBasedOnShadow(mOwner, mShadow, this);

	RenderManager::Instance()->BindModel(mModel);
		
	/* Set the right texture */
	if (mTexture && mTexture->Get())
		glBindTexture(GL_TEXTURE_2D, mTexture->Get()->mHandle);
	else
		glBindTexture(GL_TEXTURE_2D, 0);


	check_gl_error();


	/* Get the memory (GPU?) location of the variable for the mtx */
	GLint loc = glGetUniformLocation(myShader->GetHandle(), "mtxModel");
	glUniformMatrix4fv(loc, 1, GL_TRUE, DrawMatrix.v);

	loc = glGetUniformLocation(myShader->GetHandle(), "ourTexture");
	glUniform1i(loc, 0);

	loc = glGetUniformLocation(myShader->GetHandle(), "modColor");
	glUniform4f(loc, mModColor.r, mModColor.g, mModColor.b, mModColor.a);


	check_gl_error();

	/* Draw the actual triangles */
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	check_gl_error();

	RenderManager::Instance()->UnbindModel();

	check_gl_error();
}

static std::array<char, 30> texture_filter{ 0 };

bool Sprite::Edit()
{
	bool changed = false;
	ImGui::PushID("Sprite");

	if (ImGui::TreeNode("Sprite"))
	{
		static unsigned selected_item = -2;
		auto& all_textures = gActualResourceMgr.GetAllFilenamesOfType(FILETYPE::TextureType);

		if (selected_item == -2 && mTexture)
		{
			auto position = std::find(all_textures.begin(), all_textures.end(), mTexture->GetFilename());
			selected_item = std::distance(all_textures.begin(), position);
		}

		changed = ImGui::Checkbox("FlipX", &flipX) || changed;
		changed = ImGui::Checkbox("FlipY", &flipY) || changed;



		ImGui::PushID(333);
		ImGui::InputText(" Filter", texture_filter.data(), 30);
		ImGui::PopID();

		if (ImGui::BeginCombo(" Texture", mTexture ? all_textures[selected_item].data() : " - Default - "))
		{
			bool is_selected = (mTexture == nullptr);

			if (ImGui::Selectable(" - Default - ", is_selected))
			{
				selected_item = -1;
				mTexture = nullptr;
				changed = true;
			}

			if (is_selected)
			{
				selected_item = -1;
				ImGui::SetItemDefaultFocus();
			}

			for (unsigned n = 0; n < all_textures.size(); n++)
			{
				if (all_textures[n].find(texture_filter.data()) == std::string::npos) continue;

				bool is_selected = (selected_item == n);
				if (ImGui::Selectable(all_textures[n].data(), is_selected))
				{
					selected_item = n;
					mTexture = gActualResourceMgr.GetResource<Texture>(all_textures[n]);

					mOwner->mScale = Vector2(static_cast<float>(mTexture->Get()->mWidth), 
						static_cast<float>(mTexture->Get()->mHeight));

					changed = true;
				}
				if (ImGui::IsItemHovered())
				{
					ImGui::BeginTooltip();
					ImGui::Image((void*)(intptr_t)(gActualResourceMgr.GetResource<Texture>
						(all_textures[n])->Get()->mHandle), ImVec2(80, 80), ImVec2(0, 0),
						ImVec2(1, -1), ImVec4(1.0f, 1.0f, 1.0f, 1.0f), ImVec4(1.0f, 1.0f, 1.0f, 0.5f));
					ImGui::EndTooltip();
				}

				if (is_selected)
				{
					selected_item = n;
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}

		changed = EditRenderable() || changed;

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}

	ImGui::PopID();
	return changed;
}

CameraComp::~CameraComp()
{
	RenderManager::Instance()->RemoveCamera(this);
}

void CameraComp::Initialize()
{
	RenderManager::Instance()->AddCamera(this, mOwner->GetSpace());
}

void CameraComp::Shutdown()
{
	RenderManager::Instance()->RemoveCamera(this);
}

void CameraComp::Selected()
{
	RenderManager::Instance()->DrawOrientedRectangleAt(Vector2(mOwner->mPosition.x,
		mOwner->mPosition.y), mViewSize.x, mViewSize.y, mOwner->mRotation, Engine::Color::white);
}

bool CameraComp::Edit()
{
	bool changed = false;
	ImGui::PushID("Camera");

	if (ImGui::TreeNode("Camera"))
	{
		changed = ImGui::InputFloat2("ViewScale", mViewSize.v) || changed;
		changed = ImGui::InputFloat2("VP Center", mViewport.GetPos().v) || changed;
		changed = ImGui::InputFloat2("VP Size", mViewport.GetSize().v) || changed;

		changed = ImGui::Checkbox("Clear Viewport", &mbClean) || changed;
		if (mbClean)
			changed = ImGui::ColorEdit4("CleanColor", (float*)&mCleanColor, RenderMgr->GetColorFlags()) || changed;

		ImGui::Checkbox("DrawDebugLines", &mbDrawDebug);

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}
	ImGui::PopID();
	return changed;
}

void CameraComp::SetOpenGLViewport() const
{
	mViewport.SetOpenGLViewport();
}

IComp * CameraComp::Clone()
{
	CameraComp* _new = DBG_NEW CameraComp(*this);
	return _new;
}

void CameraComp::FromJson(nlohmann::json & _j)
{
	mViewSize.x = _j["mViewSize"]["x"];
	mViewSize.y = _j["mViewSize"]["y"];

	mbClean = _j["mbClean"];
	mViewport.FromJson(_j["mViewport"]);
	mCleanColor.FromJson(_j["mCleanColor"]);
	mbDrawDebug = _j["mbDrawDebug"];
}

void CameraComp::ToJson(nlohmann::json & _j)
{
	_j["mViewSize"];
	_j["mViewSize"]["x"] = mViewSize.x;
	_j["mViewSize"]["y"] = mViewSize.y;

	_j["mbDrawDebug"] = mbDrawDebug;
	_j["mbClean"] = mbClean;

	mCleanColor.ToJson(_j["mCleanColor"]);
	mViewport.ToJson(_j["mViewport"]);

	_j["_type"] = "CameraComp";
}

void CameraComp::operator=(IComp & _comp) {
	CameraComp & camera = *dynamic_cast<CameraComp*>(&_comp);
	mViewSize.x = camera.mViewSize.x;
	mViewSize.y = camera.mViewSize.y;

	mbDrawDebug = camera.mbDrawDebug;
	mbClean = camera.mbClean;

	mCleanColor = camera.mCleanColor;
	mViewport = camera.mViewport;
}

bool CameraComp::are_the_same_type(IComp const& lhs) {
	if (dynamic_cast<const CameraComp*>(&lhs)) {
		return true;
	}
	return false;
}

bool CameraComp::equal_to(IComp const& other) const {
	if (CameraComp const* p = dynamic_cast<CameraComp const*>(&other)) {
		return mViewSize == p->mViewSize && mViewport == p->mViewport;
	}
	else {
		return false;
	}
}

Matrix44 GetWorldToNDC(const CameraComp& _cam_comp)
{
	GameObject* obj = _cam_comp.GetOwner();

	Matrix44 InvTranslate = Matrix44::Translate(Vector3(-obj->mPosition.x, -obj->mPosition.y, 0));
	Matrix44 InvRotate = Matrix44::Rotate(-obj->mRotation);
	Matrix44 InvScale = Matrix44::Scale(Vector3(2 / _cam_comp.GetViewSize().x,
		2 / _cam_comp.GetViewSize().y, 1));

	return InvScale * InvRotate * InvTranslate;
}

void Sprite::FromJson(nlohmann::json & _j) {

	mShaderProgram = RenderManager::Instance()->GetQuadShaderProgram();

	if (_j.find("mTextureName") != _j.end() && _j["mTextureName"] != "")
		mTexture = gActualResourceMgr.GetResource<Texture>(_j["mTextureName"]);
	else
		mTexture = nullptr;

	if (_j.find("FlipX") != _j.end())
	{
		flipX = _j["FlipX"];
		flipY = _j["FlipY"];
	}

	FromJsonRenderable(_j);
}

void Sprite::ToJson(nlohmann::json & _j) 
{
	_j["_type"] = "Sprite";

	ToJsonRenderable(_j);

	_j["FlipX"] = flipX;
	_j["FlipY"] = flipY;
	_j["mTextureName"] = mTexture ? mTexture->GetFilename() : "";
}

bool Sprite::are_the_same_type(IComp const& lhs) {
	if (dynamic_cast<const Sprite*>(&lhs)) {
		return true;
	}
	return false;
}

bool Sprite::equal_to(IComp const& other) const {
	if (Sprite const* p = dynamic_cast<Sprite const*>(&other)) {
		return mTexture == p->mTexture && flipX == p->flipX && flipY == p->flipY;
	}
	else {
		return false;
	}
}

void Sprite::operator=(IComp& _comp) {
	Sprite & sprite = *dynamic_cast<Sprite*>(&_comp);
	mTexture = gActualResourceMgr.GetResource<Texture>(sprite.mTexture->GetFilename());

	flipX = sprite.flipX;
	flipY = sprite.flipY;

	renderable::operator=(static_cast<renderable&>(_comp));
}
