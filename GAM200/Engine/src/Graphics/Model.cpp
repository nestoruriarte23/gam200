#include "Model.h"
#include "../Graphics/RenderManager.h"
#include "../Graphics/Particles/Particles.h"
#include "../Graphics/Particles/ParticleProperties.h"

Model::Model()
{
	/* Allocate and assign a Vertex Array Object to our handle */
	glGenVertexArrays(1, &mHandle);

	/* Bind our Vertex Array Object as the current used object */
	glBindVertexArray(mHandle);

	/* Allocate and assign two Vertex Buffer Objects to our handle */
	glGenBuffers(1, &mVertexBuffer);
	glGenBuffers(1, &mIndexBuffer);
}

Model::~Model()
{
	ClearModel();
	glDeleteBuffers(1, &mVertexBuffer);
	glDeleteBuffers(1, &mIndexBuffer);
	glDeleteVertexArrays(1, &mHandle);

	if (mOffsetArray != 0)	glDeleteBuffers(1, &mOffsetArray);
}

void Model::AddIndices(const unsigned * indices, unsigned size)
{
	for (unsigned i = 0; i < size; ++i)
		AddIndices(indices[i]);
}

void Model::AddIndex()
{
	mIndices.push_back(mIndices.size());
}

void Model::ClearModel()
{
	if (mVertices.size() > 0)
	{
		mVertices.clear();
		mIndices.clear();

	}
}

void Model::UploadToGPU()
{
	check_gl_error();

	/* Bind our first VAO as being the active buffer and storing vertex attributes (coordinates) */
	glBindVertexArray(mHandle);

	check_gl_error();

	/* Upload vertex data to GPU as is */
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * mVertices.size(),
		reinterpret_cast<void*>(mVertices.data()), GL_STATIC_DRAW);

	check_gl_error();

	/* Also, pass the index data */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * mIndices.size(),
		reinterpret_cast<void*>(mIndices.data()), GL_STATIC_DRAW);

	check_gl_error();

	/* Specify how the data for the vertices is layed out */
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);												// position
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(sizeof(float) * 3));	// texture coord
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(sizeof(float) * 5));	// color

	check_gl_error();

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	/* Unbid */
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Model::UploadParticleModelToGPU()
{
	/* Bind our first VAO as being the active buffer and storing vertex attributes (coordinates) */
	glBindVertexArray(mHandle);

	/* Upload vertex data to GPU as is */
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * mParticleVertices.size(),
		reinterpret_cast<void*>(mParticleVertices.data()), GL_STATIC_DRAW);

	check_gl_error();

	/* Also, pass the index data */
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * mIndices.size(),
		reinterpret_cast<void*>(mIndices.data()), GL_STATIC_DRAW);

	check_gl_error();

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	/* Specify how the data for the vertices is layed out */
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 
		sizeof(ParticleVertex), 0);						// position
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(ParticleVertex), 
		reinterpret_cast<void*>(sizeof(float) * 2));	// texture coord

	check_gl_error();

	/* INSTANCE DATA */

	glGenBuffers(1, &mOffsetArray);
	glBindBuffer(GL_ARRAY_BUFFER, mOffsetArray);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Particle) * MAX_PARTICLES_PER_SYS, NULL, GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
		sizeof(Particle), 0);							// instance position
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Particle), 
		reinterpret_cast<void*>(sizeof(Vector2)));		// instance scale
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(Particle),
		reinterpret_cast<void*>(sizeof(Vector2) * 2));	// instance color

	// Mark this paramters as to change per instance 
	glVertexAttribDivisor(2, 1);
	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);

	/* Unbid */
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

int Model::GetSize()
{
	return mIndices.size();
}

void Model::SetColor(Engine::Color _color)
{
	std::for_each(mVertices.begin(), mVertices.end(), 
	[&_color](Vertex& _vertex)
	{
		_vertex.SetVertexColor(_color);
	});
}

void Model::SaveHandle(GLuint * destination)
{
	destination[0] = mHandle;
	destination[1] = mVertexBuffer;
	destination[2] = mIndexBuffer;
}

GLuint Model::GetHandle()
{
	return mHandle;
}

void Model::CreateBasicQuad(float size)
{
	float h = size;			
					 // XYZ			// UV		// RGBA 
	AddVertex(Vertex(-h,  h, 0,		0, 1,		1, 1, 1, 1	));
	AddVertex(Vertex(-h, -h, 0,		0, 0,		1, 1, 1, 1	));
	AddVertex(Vertex( h, -h, 0,		1, 0,		1, 1, 1, 1	));
	AddVertex(Vertex( h,  h, 0,		1, 1,		1, 1, 1, 1	));
	
	unsigned indices[] = { 0, 1, 2, 0, 2, 3 };
	AddIndices(indices, sizeof(indices) / sizeof(indices[0]));
}

void Model::CreateParticleQuad()
{
	float h = 0.5f;
									 // XY		// UV	
	AddParticleVertex(ParticleVertex(-h,  h,	0, 1));		// TOP-Left
	AddParticleVertex(ParticleVertex(-h, -h,	0, 0));		// BOT-Left
	AddParticleVertex(ParticleVertex( h, -h,	1, 0));		// BOT-Right
	AddParticleVertex(ParticleVertex( h,  h,	1, 1));		// TOP-Right

	unsigned indices[] = { 0, 1, 2, 0, 2, 3 };
	AddIndices(indices, sizeof(indices) / sizeof(indices[0]));
}

void Model::CreateLineMesh()
{
	for (unsigned i = 0; i < MAX_DEBUG_LINES; ++i)
	{
		AddVertex(Vertex());
		AddIndices(i);
	}
}
