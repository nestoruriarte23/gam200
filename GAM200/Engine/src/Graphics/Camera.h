#pragma once

#include "../Serializer/SerializeFactory.h"
#include "../Utilities/Math/MyMath.h"
#include "../Components/System.h"

class Viewport
{
public:
	Viewport(const Vector2 & pos, const Vector2 & dimensions);

	Matrix44 GetNDCToViewport();
	Matrix44 GetViewportToNDC();
	Matrix44 GetWindowToNDC();

	void		SetProperties(Vector2 pos, Vector2 size);
	void		SetOpenGLViewport() const;
	Vector2&	GetPos() { return mPos; }
	Vector2&	GetSize() { return mDimensions; }

	bool operator==(const Viewport& v) const;
	bool operator!=(const Viewport& v) const;

	void FromJson(nlohmann::json & _j);
	void ToJson(nlohmann::json & _j);

	static Viewport WholeScreen();

private:
	Vector2 mPos;
	Vector2 mDimensions;
};

class Camera : public ISystem
{
	MAKE_SINGLETON(Camera);	/* TODO: Rethink the camera for the Editor, check if it 
							is a good idea to make it a singleton */

public:
	Camera(const Vector2 & pos, const Vector2 & scale, float rotation,
		const Vector2 & viewportPos, const Vector2 & viewportScale);

	bool Initialize();
	void AlignWithViewport(Viewport* vp);

	Matrix44	GetWorldToViewport();
	Matrix44	GetViewportToWorld();
	Viewport*	GetViewport() { return &mViewport; }

	void RotateByDeg(f32 angle);
	void RotateByRad(f32 angle);

	Matrix44 GetWorldToNDC();
	Matrix44 GetNDCToWorld();

	void ScaleBy(f32 ratio);

	Vector2 mPos;
	Vector2 mViewScale;
	f32		mOrientation;

	// DEBUG
	void PrintInfo();

private:

	Viewport mViewport;
};

Matrix44 GetWorldToNDC(const Camera& _cam);