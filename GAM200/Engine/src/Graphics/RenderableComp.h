#pragma once

#include "..//..//OpenGL/include/GL/glew.h"
#include "..//Graphics/Camera.h"
#include "..//Graphics/Shader.h"
#include "..//Graphics/Model.h"
#include "..//Graphics/Color.h"
#include "..//Serializer/SerializeFactory.h"
#include "..//Components/Components.h"
#include "../ResourceManager/ActualResourceManager.h"

class Texture
{
public:
	GLuint		mHandle;		// Texture Handle

	int			mHeight;
	int			mWidth;
};

template <typename T>
class TResource;

class Text;

class renderable : public IComp
{
	friend class RenderManager;
	RTTI_DECLARATION_INHERITED(renderable, IComp);

public:
	virtual ~renderable();

	void Initialize() override;
	void Shutdown() override;

	void SetModColor(Engine::Color _color) { mModColor = _color; }

	virtual void Render() = 0;
	virtual bool Edit() = 0;

protected:

	bool EditRenderable();
	void ToJsonRenderable(nlohmann::json & _j);
	void FromJsonRenderable(nlohmann::json & _j);

	ShaderProgram*		mShaderProgram;
	Model*				mModel;
	Engine::Color		mModColor;
	bool				mbVisible;

	bool				mbEmitOutline{false};

	friend class RenderManager;
};

class Shadow;

class Sprite : public renderable
{
	RTTI_DECLARATION_INHERITED(Sprite, renderable);

public:
	Sprite();

	void Initialize() override;
	void Shutdown() override;

	virtual void Render() override;
	virtual bool Edit() override;

	Sprite * Clone();

	void FromJson(nlohmann::json & _j);
	void ToJson(nlohmann::json & _j);

	bool are_the_same_type(IComp const& lhs);

	void SetVisibility(bool _b) { mbVisible = _b; }
	std::string GetTextureName() { return mTexture ? mTexture->GetFilename() : ""; }
	void ChangeTexture(std::string name) { mTexture = gActualResourceMgr.GetResource<Texture>(name); }
	void operator=(IComp& _go);

protected:
	virtual bool		equal_to(IComp const& other) const;

	TResource<Texture>*		mTexture;
	Shadow*					mShadow{nullptr};
	bool					flipX;
	bool					flipY;
};

class CameraComp : public IComp
{
	RTTI_DECLARATION_INHERITED(CameraComp, IComp);

public:
	CameraComp() : mViewSize(960, 540), mbDrawDebug(false), mViewport(Vector2(0, 0), Vector2(1, 1)) {}
	virtual ~CameraComp();
	void Initialize() override;
	void Shutdown() override;

	/* Editor behaviour */
	void Selected();
	bool Edit();

	/* Functionalities */
	void SetOpenGLViewport() const;
	const Vector2& GetViewSize() const { return mViewSize; }

	/* Variables */
	Vector2			mViewSize;
	Viewport		mViewport;
	Engine::Color	mCleanColor;
	bool			mbClean;
	bool			mbDrawDebug;

	/* Component management */
	void FromJson(nlohmann::json & _j);
	void ToJson(nlohmann::json & _j);
	virtual void operator=(IComp &);
	IComp * Clone();

	bool are_the_same_type(IComp const& lhs);
	virtual bool equal_to(IComp const& other) const;
};

Matrix44 GetWorldToNDC(const CameraComp& _cam_comp);