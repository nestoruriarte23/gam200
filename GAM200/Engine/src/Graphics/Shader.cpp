#include "Shader.h"
#include <glew.h>
#include <dirent.h>			// Directory navigation

#include "../Graphics/RenderManager.h"

Shader::Shader(std::string filename)
{
	/* There are 3 steps involved in the creation of the shader
	for it to be used */

	CreateOpenGLShader(filename);
	LoadFileData(filename);
	CompileShader();
}

void Shader::CreateOpenGLShader(std::string filename)
{
	/* Check if the name contains the vert extension */
	std::size_t Position = filename.find(".vert");
	if (Position != std::string::npos)		// If actually found
	{
		mHandle = glCreateShader(GL_VERTEX_SHADER);
		return;
	}

	/* Same for fragment shader */
	Position = filename.find(".frag");
	if (Position != std::string::npos)		// If actually found
	{
		mHandle = glCreateShader(GL_FRAGMENT_SHADER);
		return;
	}
}

void Shader::LoadFileData(std::string filename)
{
	/* Open the shader file & load its content to a string stream */
	mFilename = filename;

	/* Create the file */
	std::fstream FileStream(mFilename.c_str(), std::ios::in);

	/* Also the intermediate buffer */
	std::stringstream source;
	source << FileStream.rdbuf();

	/* Pass the source code (in string format) to the shader */
	mSource = source.str();
}

void Shader::CompileShader()
{
	/* Pass the code to OpenGL */
	const char * SourceInChars = mSource.c_str();
	if (*SourceInChars == 0)
	{
		mHandle = -1;
		return;
	}

	glShaderSource(mHandle, 1, &SourceInChars, NULL);

	/* Compile it */
	glCompileShader(mHandle);

	/* Sanity check */
	GLint Diagnose;
	glGetShaderiv(mHandle, GL_COMPILE_STATUS, &Diagnose);
	if (Diagnose == GL_FALSE)
	{
		std::cout << "Error loading Shader: " << mFilename << std::endl;
		mHandle = -1;
	}
}

ShaderProgram::ShaderProgram(std::string directory)
{
	CreateOpenGLShaderProgram();
	GetShadersFromDirectory(directory);
	LinkAllShaders();
}

void ShaderProgram::SetMatrix44(const char * name, float * value)
{
	/* Get the memory (GPU?) location of the variable for the mtx */
	GLint loc = glGetUniformLocation(mHandle, name);

	/* Get the memory location of the starting Mtx44 value */
	glUniformMatrix4fv(loc, 1, GL_TRUE, value);
}

void ShaderProgram::Destroy()
{
	/* Free the shaders */
	while (mShaderHandles.empty() == false)
	{
		glDeleteShader(mShaderHandles.back());
		mShaderHandles.pop_back();
	}

	/* Delete the program itself */
	glDeleteProgram(mHandle);
}

void ShaderProgram::CreateOpenGLShaderProgram()
{
	mHandle = glCreateProgram();
}

void ShaderProgram::GetShadersFromDirectory(std::string directory)
{
	/* For each file in the directory (expected that all are
	shaders that work together) compile them individually and
	create the shader program */

	DIR*		mDir = opendir(directory.c_str());	// Directory
	dirent*		ent;								// Each file

													/* Sanity check */
	if (mDir != NULL)
	{
		/* For each file within the directory, create a shader*/
		while ((ent = readdir(mDir)) != NULL)
		{
			/* We just need the name to compile each shader */
			std::cout << directory + ent->d_name << std::endl;
			Shader s(directory + ent->d_name);

			/* Check if everything went okay */
			if (s.GetHandle() != (GLuint)-1)
				mShaderHandles.push_back(s.GetHandle());
		}

		closedir(mDir);		// Delete/Close the directory
	}
	else
	{
		std::cout << "Unable to locate directory. No shader" <<
			" has been loaded nor shader program created" << std::endl;
	}
}

void ShaderProgram::LinkAllShaders()
{
	/* Attach all the shaders to the program */
	for (auto it = mShaderHandles.begin(); it !=
		mShaderHandles.end(); ++it)
	{
		glAttachShader(this->mHandle, *it);
	}

	/* Link the shader program */
	glLinkProgram(this->mHandle);

	/* Error checking */
	GLint status;
	glGetProgramiv(this->mHandle, GL_LINK_STATUS, &status);
	if (GL_FALSE == status)
	{
		std::cout << "Error linking Shader Program" << std::endl;
		mHandle = -1;
	}

	/* We can now detach each individual shader */
	for (auto it = mShaderHandles.begin(); it !=
		mShaderHandles.end(); ++it)
	{
		glDetachShader(this->mHandle, *it);
	}
}
