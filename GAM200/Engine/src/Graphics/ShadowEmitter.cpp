#include "ShadowEmitter.h"

#include "Shadow.h"
#include "RenderManager.h"
#include "../Level/Editor/ImGUI_Basics.h"
#include "../Level/Editor/ImGui/imgui.h"
#include "../System/GameObjectManager/GameObject.h"

void ShadowEmitter::Initialize()
{
	mShadow = mOwner->get_component_type<Shadow>();

	RenderMgr->AddShadowEmitter(this, mOwner->GetSpace());
}

void ShadowEmitter::Shutdown()
{
	RenderMgr->RemoveShadowEmitter(this);
}

bool ShadowEmitter::Edit()
{
	bool changed = false;
	ImGui::PushID("ShadowEmitter");

	if (ImGui::TreeNode("ShadowEmitter"))
	{
		ImGui::TreePop();
		//ImGui::PopID();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
		}
	}

	ImGui::PopID();
	return changed;
}

Shadow * ShadowEmitter::GetShadow()
{
	return mShadow;
}

ShadowEmitter * ShadowEmitter::Clone()
{
	ShadowEmitter* _new = DBG_NEW ShadowEmitter(*this);

	return _new;
}

void ShadowEmitter::FromJson(nlohmann::json & _j)
{
}

void ShadowEmitter::ToJson(nlohmann::json & _j)
{
	_j["_type"] = "ShadowEmitter";
}

bool ShadowEmitter::are_the_same_type(IComp const & lhs)
{
	if (dynamic_cast<const ShadowEmitter*>(&lhs)) {
		return true;
	}
	return false;
}

void ShadowEmitter::operator=(IComp & _comp)
{
}

bool ShadowEmitter::equal_to(IComp const & other) const
{
	if (ShadowEmitter const* p = dynamic_cast<ShadowEmitter const*>(&other)) {
		return mShadow == p->mShadow;
	}
	else {
		return false;
	}
}
