#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "RenderManager.h"
#include "Camera.h"
#include "Model.h"
#include "Shadow.h"
#include "ShadowEmitter.h"
#include "SpineComp.h"
#include "RenderableComp.h"
#include "Text/Text.h"
#include "Particles/Particles.h"
#include "Particles/ParticleProperties.h"
#include "Outline/GetOutlined.h"
#include "../Game/Game.h"
#include "../Level/Space/Space.h"
#include "../Level/Scene/Scene.h"
#include "../Level/Editor/ImGui/imgui.h"
#include "../Utilities/MyDebug/MyDebug.h"
#include "../Serializer/SerializeFactory.h"
#include "../GameStateManager/GameStateManager.h"
#include "../System/GameObjectManager/GameObject.h"
#include "../ResourceManager/ActualResourceManager.h"
#include "../../Settings.h"
#include "../Physics/Collisions.h"

RenderManager::RenderManager() : mLineCount(0) {}

bool RenderManager::Initialize()
{
	/* Create our models */
	mQuad.CreateBasicQuad(0.5f);
	mScreenQuad.CreateBasicQuad(1.0f);
	mLineMesh.CreateLineMesh();

	/* Upload models to GPU and copy the handle to the render manager */
	mQuad.UploadToGPU();
	mScreenQuad.UploadToGPU();
	mLineMesh.UploadToGPU();

	/* Create Shader Programs */
	if (!CreateShaderProgram(&mQuadShader, "..//Resources//Shaders//Quad//"))
		return false;
	if (!CreateShaderProgram(&mLineShader, "..//Resources//Shaders//Line//"))
		return false;
	if (!CreateShaderProgram(&mScreenShader, "..//Resources//Shaders//ScreenQuad//"))
		return false;
	if (!CreateShaderProgram(&mTextShader, "..//Resources//Shaders//Text//"))
		return false;
	if (!CreateShaderProgram(&mOutlineShader, "..//Resources//Shaders//Outline//"))
		return false;

	/* Set up Blend */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/* Wireframe toogle / polygon mode */
	mbWireframeActive = false;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	/* Enable Scissor */
	glEnable(GL_SCISSOR_TEST);
	
	/* Set up depth test*/
	glClearDepth(2.0f);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	check_gl_error();

	/* Stencil test */
	glEnable(GL_STENCIL_TEST); 
	glStencilFunc(GL_ALWAYS, 1, 0xFF);				// Always draw the fragments to the color buffer (and write a 1 to the stencil buffer)
	glStencilMask(0x00);							// Mask the second parameter from glStencilFunc (So, by multipying by 0, we are disabling the write to the stencil buffer)
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);		// What to do when: 1 - Stencil fails     2 - Depth fails		3 - Both pass or Stencil passes and Depth deactivated

	glClear(GL_STENCIL_BUFFER_BIT);					// Clear the stencil buffer, by default the clear values is 0

	SetCustomBlendMode(CustomBlendMode::Normal);

	check_gl_error();

	/* Set better lines */
	glLineWidth(2);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	ColorEdit_Flags = ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_NoDragDrop |
		ImGuiColorEditFlags_AlphaPreview | ImGuiColorEditFlags_NoOptions;

	check_gl_error();
	
	/* If everything has gone right */
	return true;
}

void RenderManager::Reset()
{
	mHighestRenderable = 0;
}

void RenderManager::Update()
{
	//Space* mainArea = Game->GetCurrScene()->FindSpace("MainArea");
	//
	//CameraComp* cam = GetCamera(mainArea);
	//Vector2 camPos = cam->mOwner->mPosition;
	//Vector2 viewSize = cam->mViewSize;
	//
	//Vector2 centerClamp(static_cast<int>(camPos.x / GRID_SIZE), static_cast<int>(camPos.y / GRID_SIZE));
	//
	//Vector2 camCenter = Vector2(centerClamp.x * GRID_SIZE, centerClamp.y * GRID_SIZE);
	//
	//bool same_coord = StaticPointToStaticRect(&camCenter, &mLastCenter, GRID_SIZE, GRID_SIZE);
	//
	//if (same_coord && viewSize == mLastView) 
	//	get_comps = false;
	//else {
	//	get_comps = true;
	//
	//	mLastCenter = camCenter;
	//	mLastView = viewSize;
	//}

	//if (get_comps == true)
	//{
		mOutlineds.clear();
		mShadows.clear();
		mShadowEmitters.clear();
		mRenderables.clear();


		mOutlineds = Game->GetCurrScene()->GetComponentsInScreen<GetOutlined>();
		mShadows = Game->GetCurrScene()->GetComponentsInScreen<Shadow>();
		mShadowEmitters = Game->GetCurrScene()->GetComponentsInScreen<ShadowEmitter>();

		auto sprite = Game->GetCurrScene()->GetComponentsInScreen<Sprite>();
		auto spine = Game->GetCurrScene()->GetComponentsInScreen<SpineAnimation>();
		auto particles = Game->GetCurrScene()->GetComponentsInScreen<ParticleSystem>();
		auto text = Game->GetCurrScene()->GetComponentsInScreen<TextComp>();

		for (auto _space_pair : Game->GetCurrScene()->scene_spaces)
		{
			for (auto _vector : sprite[_space_pair])
				mRenderables[_space_pair].push_back(_vector);

			for (auto _vector : spine[_space_pair])
				mRenderables[_space_pair].push_back(_vector);

			for (auto _vector : particles[_space_pair])
				mRenderables[_space_pair].push_back(_vector);

			for (auto _vector : text[_space_pair])
				mRenderables[_space_pair].push_back(_vector);
		}
	//}
}

void RenderManager::Free()
{
	/* Detach the current shader program */
	glUseProgram(0);

	/* Delete the Shader programs */
	mQuadShader->Destroy();
	delete mQuadShader;
	mLineShader->Destroy();
	delete mLineShader;
	mScreenShader->Destroy();
	delete mScreenShader;
	mTextShader->Destroy();
	delete mTextShader;
	mOutlineShader->Destroy();
	delete mOutlineShader;

	//check_gl_error();
}

bool RenderManager::CreateShaderProgram(ShaderProgram** sp, std::string directory)
{
	/* Create the shader program */
	*sp = DBG_NEW ShaderProgram(directory);

	/* If there has been any issue, the handle will be (-1) */
	if ((*sp)->GetHandle() == (GLuint)-1)
		return false;
	else
		return true;

	check_gl_error();
}

void RenderManager::UseShaderProgram(const ShaderProgram * sp)
{
	glUseProgram(sp->GetHandle());

	check_gl_error();
}

/*static bool compute = true;
static std::map<Space*, std::vector<std::vector<IComp*>*>>	wanted_renderable;*/

/* Do not care about the spaces (or cameras in the space) */
void RenderManager::DrawAllRenderables()
{
	/* Set the VertexArray (Model) to use. If this was 3D we wolud set it for each
	object but in our case all objects use a simple 2D texture */
	BindModel(&mQuad);

	Camera::Instance()->GetViewport()->SetOpenGLViewport();
	glClearColor(0.2f, 0.3f, 0.3f, 0.0f);	// Clear color
	glClear(GL_COLOR_BUFFER_BIT);

	//Matrix44 worldtoview = Camera::Instance()->GetWorldToNDC();

	mbDrawingOutline = false;

	/* Get the matrix to get the teture onto NDC space (set that transformation to all shaders) */
	SetWorldToViewInAllShaderPrograms(Camera::Instance()->GetWorldToNDC().v);

	/* Get the components */
	/*if (compute)
	{
		compute = false;

		Vector2 centre = Camera::Instance()->mPos; //mera->GetOwner()->mPosition;
		Vector2 size = Camera::Instance()->mViewScale;

		Vector2 TL_Cam = centre + Vector2(-(size.x / 2), size.y / 2);
		Vector2 BR_Cam = centre + Vector2(size.x / 2, -(size.y / 2));

		Vector2 topLeftClamp(static_cast<int>(TL_Cam.x / GRID_SIZE), static_cast<int>(TL_Cam.y / GRID_SIZE));
		Vector2 TL = Vector2(topLeftClamp.x * GRID_SIZE, topLeftClamp.y * GRID_SIZE);

		Vector2 botRightClamp(static_cast<int>(BR_Cam.x / GRID_SIZE), static_cast<int>(BR_Cam.y / GRID_SIZE));
		Vector2 BR = Vector2(botRightClamp.x * GRID_SIZE, botRightClamp.y * GRID_SIZE);

		wanted_renderable = Game->GetCurrScene()->GetComponentsInScreen<Sprite>(TL, BR);
		auto spine = Game->GetCurrScene()->GetComponentsInScreen<SpineAnimation>(TL, BR);
		auto particles = Game->GetCurrScene()->GetComponentsInScreen<ParticleSystem>(TL, BR);
		auto text = Game->GetCurrScene()->GetComponentsInScreen<TextComp>(TL, BR);

		for (auto _space_pair : Game->GetCurrScene()->scene_spaces)
		{
			for (auto _vector : spine[_space_pair])
				wanted_renderable[_space_pair].push_back(_vector);

			for (auto _vector : particles[_space_pair])
				wanted_renderable[_space_pair].push_back(_vector);

			for (auto _vector : text[_space_pair])
				wanted_renderable[_space_pair].push_back(_vector);
		}
	}*/

	// The images are then automagically drawn onto the active Viewport, 
	// previously set in the initialize
	/*for (auto space : mRenderables)
	{
		if (space.first->mbEditable)
			if (mOutlineds.find(space.first) != mOutlineds.end())
			{

				for (auto _renderable_vector : mOutlineds[space.first])
				{
					for (auto _renderable : *_renderable_vector)
					{
						static_cast<GetOutlined*>(_renderable)->
							GetRenderable()->Render();
					}

					check_gl_error();
				}
			}

		if (space.first->mbEditable)
			for (auto renderable_vec : space.second)
			{
				for (auto rend : *renderable_vec)
				{
					auto _rendera = static_cast<renderable*>(rend);

					this->UseShaderProgram(_rendera->mShaderProgram);
					_rendera->Render();

					check_gl_error();
				}
			}
	}*/

	for (auto space : mEditorRenderables)
	{
		/*if (space.first->mbEditable)
			if (mOutlineds.find(space.first) != mOutlineds.end())
			{

				for (auto _renderable_vector : mOutlineds[space.first])
				{
					for (auto _renderable : *_renderable_vector)
					{
						static_cast<GetOutlined*>(_renderable)->
							GetRenderable()->Render();
					}

					check_gl_error();
				}
			}*/

		if (space.first->mbEditable)
			for (auto renderable_it : space.second)
			{
					this->UseShaderProgram(renderable_it->mShaderProgram);
					renderable_it->Render();

					check_gl_error();
			}
	}
}

/* Shadows need to be rendered using the GL_LESS option from the depth test while 
Spine animations need GL_LEQUAL so we just render Shadows in a separate loop. Additionally, 
there is an ugly messy code so we just not draw Shadows twice in the Editor */
void RenderManager::RenderShadows(Space * _space)
{
	if (!Game->onEditor)
	{
		glDepthFunc(GL_LESS);
		Game->onEditor = true;

		if (_space->mbVisible)
		{
			DrawShadows(mShadows[_space], mCameras[_space]);
		}

		Game->onEditor = false;
		glDepthFunc(GL_LEQUAL);

		check_gl_error();
	}
}

void RenderManager::RenderScene(Scene * _scene)
{
	check_gl_error();

	glEnable(GL_DEPTH_TEST);

	check_gl_error();

	/* Draw each space to its own frame buffer */
	std::for_each(_scene->scene_spaces.begin(), _scene->scene_spaces.end(), 
	[this, &_scene](Space* _space) 
	{
		check_gl_error();
		if (_space->mbVisible)
		{
			/* Render Target */
			if (_space->mbUseFrameBuffer)
			{
				SetFrameBuffer(_space->mFrameBuffer);

				SetViewport(Viewport::WholeScreen());
				CleanViewport(Engine::Color::empty);
			}

			/* Blend mode */
			SetCustomBlendMode(_space->mBlendMode);

			glClear(GL_DEPTH_BUFFER_BIT);

			/* Shadows */
			if (_space->mbDrawShadows) RenderShadows(_space);

			glClear(GL_DEPTH_BUFFER_BIT);

			/* Draw and update the outlined objects before any other */
			if (mOutlineds.find(_space) != mOutlineds.end())
			{

				for (auto _renderable_vector : mOutlineds[_space])
				{
					for (auto _renderable : *_renderable_vector)
					{
						static_cast<GetOutlined*>(_renderable)->
							GetRenderable()->Render();
					}

				}
			}

			/* Actual Objects */
			DrawSpace(_space);

			/* Outline */
			if (_space->mbDrawOutlines)	DrawOutlines(_space);
		}
	});

	glDisable(GL_DEPTH_TEST);

	/* Set the render target and clean it */
	SetFrameBuffer(_scene->mFrameBuffer);

	bool first = true;

	/* Blend all spaces together */
	std::for_each(_scene->scene_spaces.begin(), _scene->scene_spaces.end(),
	[this, &first](Space* _space)
	{
		if (_space->mbUseFrameBuffer)
		{
			/* If we are using space frame buffers at all, clean the scenes framebuffer
			and then blend, otherwise the resulting texture is already in the scene's 
			texture so we should not touch it*/
			if (first)
			{
				SetViewport(Viewport::WholeScreen());
				CleanViewport(Engine::Color::empty);

				first = false;
			}

			DrawScreenQuad(_space->mFrameBuffer.GetResultingTexture());
		}
	});
}

void RenderManager::AddShadow(Shadow * _shadow, Space * _space)
{
	// Need to check if the space already exists
	//if (_space != nullptr) {
	//	mShadows[_space].push_back(_shadow);
	//}
}

void RenderManager::RemoveShadow(Shadow * _shadow)
{
	//auto space = _shadow->mOwner->GetSpace();
	//auto it = mShadows.find(space);
	//if (it != mShadows.end())
	//{
	//	// For every component
	//	auto objective = std::find(it->second.begin(), it->second.end(), _shadow);
	//	if (objective != it->second.end())
	//		it->second.erase(objective);
	//}
}

void RenderManager::AddShadowEmitter(ShadowEmitter * _shadow, Space * _space)
{
	//if (_space != nullptr) {
	//	mShadowEmitters[_space].push_back(_shadow);
	//}
}

void RenderManager::RemoveShadowEmitter(ShadowEmitter * _shadow)
{
	//auto space = _shadow->mOwner->GetSpace();
	//auto it = mShadowEmitters.find(space);
	//if (it != mShadowEmitters.end())
	//{
	//	// For every component
	//	auto objective = std::find(it->second.begin(), it->second.end(), _shadow);
	//	if (objective != it->second.end())
	//		it->second.erase(objective);
	//}
}

//const std::vector<ShadowEmitter*>& RenderManager::GetShadowEmitters(Space* _space)
//{
//	//ASSERT(_space != nullptr)
//	//return mShadowEmitters[_space];
//}

void RenderManager::DrawSpace(Space* _space)
{
	/* Basically where the stencil data gets cleaned */
	glStencilMask(0xFF);
	glClear(GL_STENCIL_BUFFER_BIT);

	auto renderables = mRenderables.find(_space);
	auto cameras = mCameras.find(_space);

	/* IF there are any cameras, and any renderables */
	if (cameras != mCameras.end() && renderables != mRenderables.end())
		DrawComponents(renderables->second, cameras->second);
}

void RenderManager::DrawComponents(const std::vector<std::vector<IComp*>*>& _comps,
	const std::vector<CameraComp*>& _cameras)
{
	for (auto it : _cameras)
	{
		/* For each camera */
			DrawByCamera(_comps, it);
	}
}

void RenderManager::SetWorldToViewInAllShaderPrograms(float* _matrix_data)
{
	check_gl_error();

	UseShaderProgram(mTextShader);
	mTextShader->SetMatrix44("mtxWorldToView", _matrix_data);

	UseShaderProgram(mQuadShader);
	mQuadShader->SetMatrix44("mtxWorldToView", _matrix_data);

	UseShaderProgram(ParticleMgr->mParticleShader);
	ParticleMgr->mParticleShader->SetMatrix44("mtxWorldToView", _matrix_data);

	UseShaderProgram(mOutlineShader);
	mOutlineShader->SetMatrix44("mtxWorldToView", _matrix_data);

	UseShaderProgram(mQuadShader);
	mQuadShader->SetMatrix44("mtxWorldToView", _matrix_data);

	check_gl_error();
}

void RenderManager::DrawByCamera(const std::vector<std::vector<IComp*>*>& _comps, CameraComp * _cam)
{
	check_gl_error();

	/* Set the viewport based on the camera's normalized values and the size of the screen */
	Viewport temp_viewport(
		Vector2(_cam->mViewport.GetPos().x * Game->GetWindowSize().x,
			_cam->mViewport.GetPos().y * Game->GetWindowSize().y),
		Vector2(_cam->mViewport.GetSize().x * Game->GetWindowSize().x,
			_cam->mViewport.GetSize().y * Game->GetWindowSize().y)
	);
	temp_viewport.SetOpenGLViewport();

	/* Clean the viewport of the camera if asked for */
	if (_cam->mbClean)
	{
		glClearColor(_cam->mCleanColor.r, _cam->mCleanColor.g,
			_cam->mCleanColor.b, _cam->mCleanColor.a);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	/* Set the World to Viewport Matrix in all Shader Programs */
	SetWorldToViewInAllShaderPrograms(GetWorldToNDC(*_cam).v);

	/* Draw the actual objects */
	for (auto it = _comps.begin(); it != _comps.end(); ++it)
	{
		for (auto it2 : *(*it))
		{
			auto _render = static_cast<renderable*>(it2);

			if (GSM->currentLevel != EDITOR_LEVEL ||
				(GSM->currentLevel == EDITOR_LEVEL &&
					_render->GetOwner()->mSceneSpace == _cam->GetOwner()->mSceneSpace))
			{
				UseShaderProgram(_render->mShaderProgram);	// Set the shader program
				_render->Render();	// Call the respective Render function, it could be a Sprite, a Shadow, an Animation, text...

				check_gl_error();	// Important check_gl_error, but is called a lot
			}
		}
	}

	/*auto prev_buffer = RenderMgr->GetCurrentFrameBuffer();*/

	/* Particles should be drawn on top of anything else, to avoid semi transparent 
	alpha issues with Depth Buffering, however, depth should be correctly applied 
	since the height of the particle system is also taken into account */
	ParticleMgr->Render();

	/*if (!Game->onEditor)
	{
		RenderMgr->SetFrameBuffer(prev_buffer);
		DrawScreenQuad(Game->GetCurrScene()->mParticleRealm.GetResultingTexture());
	}*/
}

void RenderManager::DrawShadows(const std::vector<std::vector<IComp*>*>& _comps,
	const std::vector<CameraComp*>& _cameras)
{
	/* For each camera */
	for (auto it = _cameras.begin();
		it != _cameras.end(); ++it)
	{
			DrawShadowsByCamera(_comps, *it);
	}
}

void RenderManager::DrawShadowsByCamera(const std::vector<std::vector<IComp*>*>& _comps,
		CameraComp* _cam)
{
	/* Set the right model and Shader Program */
	BindModel(&mQuad);
	UseShaderProgram(mQuadShader);

	Viewport temp_viewport(
		Vector2(_cam->mViewport.GetPos().x * Game->GetWindowSize().x,
			_cam->mViewport.GetPos().y * Game->GetWindowSize().y),
		Vector2(_cam->mViewport.GetSize().x * Game->GetWindowSize().x,
			_cam->mViewport.GetSize().y * Game->GetWindowSize().y)
	);
	temp_viewport.SetOpenGLViewport();

	if (_cam->mbClean)
	{
		glClearColor(_cam->mCleanColor.r, _cam->mCleanColor.g,
			_cam->mCleanColor.b, _cam->mCleanColor.a);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	/* Set the World to Viewport Matrix */
	Matrix44 worldtoview = GetWorldToNDC(*_cam);
	mQuadShader->SetMatrix44("mtxWorldToView", worldtoview.v);

	/* Draw the actual objects */
	for (auto it = _comps.begin(); it != _comps.end(); ++it)
	{
		for (auto it2 : (*(*it)))
		{
			Shadow* temp = static_cast<Shadow*>(it2);

			if (GSM->currentLevel != EDITOR_LEVEL ||
				(GSM->currentLevel == EDITOR_LEVEL &&
					temp->GetOwner()->mSceneSpace == _cam->GetOwner()->mSceneSpace))
			{
				temp->Render();
			}
		}
	}
}

#include "../Physics/Collisions.h"

void RenderManager::ComputeAlphaBasedOnShadow(GameObject * _owner, 
	Shadow * _shadow, renderable* _renderable)
{
	//return;

	if (!_shadow->mbReceiveShadow) return;

	auto iter = mShadowEmitters.find(_owner->GetSpace());
	if (iter == mShadowEmitters.end()) return;

	auto _emitter_vec = iter->second;

	// Computations to get the mod color (by default the factor will be 0)
	float max_shadow = 0.0f;
	
	for (auto it : _emitter_vec)
	{
		for (auto it2 : *it)
		{
			ShadowEmitter* _emitter = static_cast<ShadowEmitter*>(it2);

			if (_shadow->mOwner == _emitter->mOwner) continue;	// Go next iteration

			float temp_shadow;

			GameObject* caster = _emitter->mOwner;

			StaticPointToStaticEllpise(
				Vector2(_owner->mPosition.x, _owner->mPosition.y) + _shadow->mOffset,
				Vector2(caster->mPosition.x, caster->mPosition.y) + _emitter->GetShadow()->mOffset,
				(-_owner->mScale.x * _shadow->mRelativeSize.x + caster->mScale.x * _emitter->GetShadow()->mRelativeSize.x) * 0.5f,
				(-_owner->mScale.y * _shadow->mRelativeSize.y + caster->mScale.y * _emitter->GetShadow()->mRelativeSize.y) * 0.5f,
				&temp_shadow);

			temp_shadow = 2.0f - max(min(2.0f, temp_shadow), 1.0f);

			if (temp_shadow > max_shadow) max_shadow = temp_shadow;
		}
	}

	/* TODO: Work with colors (RGB), not only alpha values */
	const Engine::Color& _color = RenderMgr->GetShadowColor();

	_renderable->SetModColor(
		Engine::Color(
			1 - _color.a * max_shadow * 2,
			1 - _color.a * max_shadow * 2,
			1 - _color.a * max_shadow * 2,
			_renderable->mModColor.a
		));
}

void RenderManager::AddOutlined(GetOutlined * _outlined, Space * _space)
{
	//if (_space != nullptr) {
	//	mOutlineds[_space].push_back(_outlined);
	//}
}

void RenderManager::RemoveOutlined(GetOutlined * _outlined)
{
	//auto space = _outlined->mOwner->GetSpace();
	//auto it = mOutlineds.find(space);
	//if (it != mOutlineds.end())
	//{
	//	// For every component
	//	auto objective = std::find(it->second.begin(), it->second.end(), _outlined);
	//	if (objective != it->second.end())
	//		it->second.erase(objective);
	//}
}

void RenderManager::DrawOutlines(Space* _space)
{
	check_gl_error();

	/*GLboolean depth_active;
	glGetBooleanv(GL_DEPTH_TEST, &depth_active);
	std::cout << "Depth Active: " << (depth_active ? "true" : "false") << std::endl;
	check_gl_error();

	GLint mask_value;
	glGetIntegerv(GL_STENCIL_WRITEMASK, &mask_value);
	std::cout << "Mask Value 1: " << mask_value << std::endl;
	check_gl_error();

	GLint stencil_function;
	glGetIntegerv(GL_STENCIL_FUNC, &stencil_function);
	std::cout << "Stencil Function: " << stencil_function << std::endl;
	check_gl_error();*/


	mbDrawingOutline = true;

	check_gl_error();

	glStencilFunc(GL_EQUAL, 1, 0xFF);
	glStencilMask(0x00);
	glDisable(GL_DEPTH_TEST);

	UseShaderProgram(mOutlineShader);
	BindModel(&mQuad);

	check_gl_error();

	if (mOutlineds.find(_space) != mOutlineds.end())
	{
		for (auto _renderable_vector : mOutlineds[_space])
		{
			for (auto _renderable : *_renderable_vector)
			{
				static_cast<GetOutlined*>(_renderable)->
					GetRenderable()->Render();
			}
		}
	}

	glStencilFunc(GL_ALWAYS, 1, 0xFF);
	glStencilMask(0xFF);
	glEnable(GL_DEPTH_TEST);

	mbDrawingOutline = false;

	check_gl_error();
}

void RenderManager::DrawModel(GameObject* _go, Model* _model, Texture* _texture, 
	ShaderProgram* _sp, const Vector2& _rel_size)
{
	/* Allow Spine animations to emit shadows */
	auto renderable = _go->get_component_type<SpineAnimation>();

	/* Object outlining */
	if (!mbDrawingOutline)		// First iteration, draw everything normally 
	{
		if (!renderable->mbEmitOutline)
			glStencilMask(0x00);					// Do not write to Stencil Buffer 
		else
		{
			glStencilFunc(GL_ALWAYS, 1, 0xFF);		// set the stencil value 0000 0001 
			glStencilMask(0xFF);					// Write to Stencil Buffer
		}
	}

	/* Set the Transform of the object */
	Transform2D mObjTransf(0, 0, _go->mScale.x / 200 * _rel_size.x, 
		_go->mScale.y / 200 * _rel_size.y, _go->mRotation);

	/* Get the 44 matrix from the Transform */
	Matrix44 DrawMatrix = mObjTransf.GetMatrix();
	DrawMatrix.AddTranslation(Vector3(_go->mPosition.x,
		_go->mPosition.y, _go->mPosition.y / (RenderMgr->mHighestRenderable + 10)));

	/* TODO: Check the different texture units */
	glActiveTexture(GL_TEXTURE0);

	RenderManager::Instance()->BindModel(_model);

	/* Set the right texture */
	glBindTexture(GL_TEXTURE_2D, _texture ? _texture->mHandle : 0);

	check_gl_error();

	this->UseShaderProgram(_sp);

	/* Get the memory (GPU?) location of the variable for the mtx */
	GLint loc = glGetUniformLocation(_sp->GetHandle(), "mtxModel");

	/* Get the memory location of the starting Mtx44 value */
	glUniformMatrix4fv(loc, 1, GL_TRUE, DrawMatrix.v);

	loc = glGetUniformLocation(_sp->GetHandle(), "ourTexture");

	glUniform1i(loc, 0);

	/* Draw the actual triangles */
	unsigned indx_count = _model->GetSize();
	glDrawElements(GL_TRIANGLES, indx_count, GL_UNSIGNED_INT, 0);

	RenderManager::Instance()->UnbindModel();
	
	check_gl_error();
}

void RenderManager::AddEditorRenderable(renderable * _comp, Space * _space)
{
	// No need to check if the space already exists (but check if the 
	// space is NUll because that should not happen)
	if (_space != nullptr) {
		mEditorRenderables[_space].push_back(_comp);
	}


}

/* Only removes the pointer from the vector, does not delete the memory is pointing to */
void RenderManager::RemoveEditorRenderable(renderable * _comp)
{
	auto space = _comp->mOwner->GetSpace();
	auto it = mEditorRenderables.find(space);
	if (it != mEditorRenderables.end())
	{
		// For every component
		auto objective = std::find(it->second.begin(), it->second.end(), _comp);
		if (objective != it->second.end())
			it->second.erase(objective);
	}
}

void RenderManager::RemoveAllRenderables()
{
	mEditorRenderables.clear();
}

void RenderManager::UpdateRenderables()
{
	for (auto space : mRenderables)
	{
		for (auto renderable_vec : space.second)
		{
			for (auto _render : *renderable_vec)
				static_cast<renderable*>(_render)->Update();
		}
	}
}

std::map<Space*, std::vector<renderable*>>& RenderManager::GetEditorRenderables()
{
	return mEditorRenderables;
}

void RenderManager::AddCamera(CameraComp * _cam, Space * _space)
{
	mCameras[_space].push_back(_cam);
}

void RenderManager::RemoveCamera(CameraComp * _cam)
{
	auto it = mCameras.find(_cam->mOwner->GetSpace());
	if (it != mCameras.end())
	{
		// For every component
		auto objective = std::find(it->second.begin(), it->second.end(), _cam);
		if (objective != it->second.end())
			it->second.erase(objective);
	}
}

void RenderManager::RemoveAllCameras()
{
	mCameras.clear();
}

void RenderManager::DrawScreenQuad(GLuint _texture)
{
	UseShaderProgram(mScreenShader);
	BindModel(&mScreenQuad);

	glBindTexture(GL_TEXTURE_2D, _texture);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void RenderManager::DrawDebugLines(CameraComp* _cam)
{
	glClear(GL_DEPTH_BUFFER_BIT);

	/* Set the shader */
	UseShaderProgram(mLineShader);

	/* Bind Model */
	BindModel(&mLineMesh);

	/* Set the World to Viewport Matrix */
	Matrix44 worldtoview;
	if (_cam == nullptr)
	{
		worldtoview = Camera::Instance()->GetWorldToNDC();
		//std::cout << "Camera not found, so im drawing lines with the Editor Camera. Oki?" << std::endl;
	}
	else
		worldtoview = GetWorldToNDC(*_cam);

	mLineShader->SetMatrix44("mtxWorldToView", worldtoview.v);

	/* Update the mesh already at GPU with _new values */
	glBufferSubData(GL_ARRAY_BUFFER, 0, mLineCount * 2 * sizeof(Vertex),
	     reinterpret_cast<void*>(mLines.data()));

	/* Draw a subset of the line mesh */
	glDrawElements(GL_LINES, mLineCount * 2,
		GL_UNSIGNED_INT, 0);

	/* TODO: Do this on another point so multiple cameras render the lines */
	mLines.clear();
	mLineCount = 0;
}

void RenderManager::SetCustomBlendMode(unsigned _mode)
{
	switch (_mode)
	{
	case Normal:
		SetBlendEquation(BlendParameter::Src_Alpha, BlendFunction::Add, BlendParameter::One_Minus_Src_Alpha);
		break;
	case Additive:
		SetBlendEquation(BlendParameter::One, BlendFunction::Add, BlendParameter::One);
		break;
	case Multiplicative:
		SetBlendEquation(BlendParameter::Dst_Color, BlendFunction::Add, BlendParameter::Zero);
		break;
	default:
		break;
	}
}

void RenderManager::SetBlendFunction(unsigned _function)
{
	mBlendingFunction = _function;
	glBlendEquation(_function);
}

void RenderManager::SetBlendingParameters(unsigned _source_factor, unsigned _dest_factor)
{
	mBlendingSource = _source_factor;
	mBlendingDestination = _dest_factor;
	glBlendFunc(_source_factor, _dest_factor);
}

void RenderManager::SetBlendEquation(unsigned _source_factor, unsigned _function, unsigned _dest_factor)
{
	SetBlendFunction(_function);
	SetBlendingParameters(_source_factor, _dest_factor);
}

unsigned RenderManager::GetBlendingFunc()
{
	return mBlendingFunction;
}

unsigned RenderManager::GetBlendingSourceParam()
{
	return mBlendingSource;
}

unsigned RenderManager::GetBlendingDestParam()
{
	return mBlendingDestination;
}

void RenderManager::GetBlendingEquation(unsigned * _func, unsigned * _source, unsigned * _dest)
{
	*_func = mBlendingFunction;
	*_source = mBlendingSource;
	*_dest = mBlendingDestination;
}

void RenderManager::FromJson(nlohmann::json & _j)
{
	if (_j.find("mGeneralShadowColor") != _j.end())
		mGeneralShadowColor.FromJson(_j["mGeneralShadowColor"]);
	else
		mGeneralShadowColor = Engine::Color(0.157f, 0.132f, 0.132f, 0.176f);

	if (_j.find("CameraPosition") != _j.end())
		Camera::Instance()->mPos.FromJson(_j["CameraPosition"]);

	if (_j.find("CameraScale") != _j.end())
		Camera::Instance()->mViewScale.FromJson(_j["CameraScale"]);
}

void RenderManager::ToJson(nlohmann::json & _j)
{
	mGeneralShadowColor.ToJson(_j["mGeneralShadowColor"]);

	Camera::Instance()->mPos.ToJson(_j["CameraPosition"]);
	Camera::Instance()->mViewScale.ToJson(_j["CameraScale"]);
}

bool RenderManager::GetDrawingOutLine()
{
	return mbDrawingOutline;
}

int RenderManager::GetColorFlags()
{
	return ColorEdit_Flags;
}

Engine::Color& RenderManager::GetShadowColor()
{
	return mGeneralShadowColor;
}

void RenderManager::BindModel(Model * _model)
{
	glBindVertexArray(_model->GetHandle());
	glBindBuffer(GL_ARRAY_BUFFER, _model->mVertexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _model->mIndexBuffer);
}

void RenderManager::UnbindModel()
{
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

GLuint RenderManager::GetBasicQuadHandle()
{
	return mQuad.GetHandle();
}

//std::vector<CameraComp*>& RenderManager::GetCamerasOfSpace(Space * wanted)
//{
//	//return mCameras[wanted];
//}

void RenderManager::SetViewport(const Viewport & _vp)
{
	_vp.SetOpenGLViewport();
}

void RenderManager::CleanViewport(Engine::Color _color, float depth)
{
	glClearColor(_color.r, _color.g, _color.b, _color.a);
	//glClearDepth(depth);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT /*  | GL_STENCIL_BUFFER_BIT */);
}

GLuint RenderManager::GetCurrentFrameBuffer()
{
	return mCurrentFrameBuffer;
}

void RenderManager::SetFrameBuffer(const FrameBuffer & _fb)
{
	_fb.SetAsRenderTarget();
	mCurrentFrameBuffer = _fb.GetHandle();
}

void RenderManager::SetFrameBuffer(GLuint _fb_handle)
{
	glBindFramebuffer(GL_FRAMEBUFFER, _fb_handle);
	check_gl_error();
}

Model* RenderManager::GetBasicQuad()
{
	return &mQuad; 
}

void RenderManager::DrawRunTimeDebugLines()
{
	for (auto space : mCameras)
	{
		for (auto cam : space.second)
		{
			CameraComp* temp = static_cast<CameraComp*>(cam);
			if (temp->mbDrawDebug)
				DrawDebugLines(temp);
		}
	}
}

void  _check_gl_error(const char * file, int line)
{
#ifndef CHECK_GL_ERROR

	return;

#endif // CHECK_GL_ERROR


	GLenum err = glGetError();

	while (err != GL_NO_ERROR) {
		std::string error;

		switch (err) {
		case GL_INVALID_OPERATION:      error = "INVALID_OPERATION";      break;
		case GL_INVALID_ENUM:           error = "INVALID_ENUM";           break;
		case GL_INVALID_VALUE:          error = "INVALID_VALUE";          break;
		case GL_OUT_OF_MEMORY:          error = "OUT_OF_MEMORY";          break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:  error = "INVALID_FRAMEBUFFER_OPERATION";  break;
		}

		// format error message
		std::stringstream errorMsg;
		errorMsg << "GL_" << error.c_str() << " - " << file << ":" << line << std::endl;

		// send to VS outpput
		std::cout << (errorMsg.str().c_str());

		// repeat
		err = glGetError();
	}
}

float get_gl_depth(int x, int y)
{
	float depth_z = -168.0f;

	check_gl_error();

	//glReadBuffer(GL_FRONT);

	check_gl_error();

	glReadPixels(x, (int)(Game->GetWindowSize().y) - y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth_z);

	check_gl_error();

	return depth_z;
}

float get_gl_depth(Vector2 _pos)
{
	return get_gl_depth(static_cast<int>(_pos.x), static_cast<int>(_pos.y));
}

/* Not working */
unsigned get_gl_stencil(int x, int y)
{
	unsigned depth_z = 666;

	check_gl_error();

	//glReadBuffer(GL_FRONT);

	//check_gl_error();

	glReadPixels(x, (int)(Game->GetWindowSize().y) - y, 1, 1,
		GL_STENCIL_INDEX, GL_FLOAT, &depth_z);

	check_gl_error();

	return depth_z;
}

/* Not working */
unsigned get_gl_stencil(Vector2 _pos)
{
	return get_gl_stencil(static_cast<int>(_pos.x), static_cast<int>(_pos.y));
}

struct
{
	bool operator()(renderable * a, renderable* b) const
	{
		GameObject* a_obj = a->mOwner;
		GameObject* b_obj = b->mOwner;

		float a_pos = a_obj->mPosition.y;
		float b_pos = b_obj->mPosition.y;

		float a_offset = a_obj->mSortingPos.y;
		float b_offset = b_obj->mSortingPos.y;


		return (a_pos + a_offset) > (b_pos + b_offset);
	}

}CustomSort;

void RenderManager::SortObjects()
{
	/*for (auto i = mRenderables.begin(); i != mRenderables.end(); i++)
	{
		if (i->first != NULL) {
			if (i->first->mbVisible)
				std::sort(i->second.begin(), i->second.end(), CustomSort);
		}
	}*/
}

CameraComp* RenderManager::GetCamera(Space* _space)
{
	CameraComp* cam = mCameras[_space].front();
	return cam;
}
