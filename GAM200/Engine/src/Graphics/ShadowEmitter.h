#pragma once

#include "RenderableComp.h"

class Shadow;

class ShadowEmitter : public IComp
{
	RTTI_DECLARATION_INHERITED(ShadowEmitter, IComp);

public:
	void Initialize() override;
	void Shutdown() override;

	bool Edit() override;

	Shadow* GetShadow();

	ShadowEmitter* Clone() override;

	void FromJson(nlohmann::json & _j) override;
	void ToJson(nlohmann::json & _j) override;

	bool are_the_same_type(IComp const& lhs) override;
	void operator=(IComp& _comp);

protected:
	bool equal_to(IComp const& other) const override;

	Shadow*					mShadow;

	friend class RenderManager;
};