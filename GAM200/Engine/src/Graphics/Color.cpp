#include "Color.h"

using namespace Engine;

Color Color::white =	Color(1, 1, 1, 1);
Color Color::red =		Color(1, 0, 0, 1);
Color Color::green =	Color(0.489f, 1, 0, 1);
Color Color::blue =		Color(0, 0, 1, 1);
Color Color::cyan =		Color(0, 1, 1, 1);
Color Color::black =	Color(0, 0, 0, 1);
Color Color::empty =	Color(0, 0, 0, 0);
Color Color::yellow =	Color(1, 1, 0, 1);
Color Color::rosa =		Color(1, 0, 1, 1);
Color Color::violet =	Color(0.541f, 0.168f, 0.886f, 1);
Color Color::turquesa = Color(0, 1, 0.498f, 1);
Color Color::lima =		Color(0, 1, 0, 1);

Color Engine::Color::LerpColor(float _ratio)
{
	return Engine::Color(r*_ratio, g*_ratio, b*_ratio, a);
}

Engine::Color Engine::Color::operator-(const Engine::Color & _other) {
	Engine::Color c;
	c.r = r - _other.r;
	c.g = g - _other.g;
	c.b = b - _other.b;
	c.a = a - _other.b;
	return c;
}

Engine::Color Engine::Color::operator+(const Engine::Color & _other) {
	Engine::Color c;
	c.r = r + _other.r;
	c.g = g + _other.g;
	c.b = b + _other.b;
	c.a = a + _other.b;
	return c;
}

Engine::Color Engine::Color::operator*(float _f) {
	Color c;
	c.r = _f * r;
	c.g = _f * g;
	c.b = _f * b;
	c.a = _f * a;
	return c;
}

void Engine::Color::FromJson(nlohmann::json & _j)
{
	r = _j["r"];
	g = _j["g"];
	b = _j["b"];
	a = _j["a"];
}

void Engine::Color::ToJson(nlohmann::json & _j)
{
	_j["r"] = r;
	_j["g"] = g;
	_j["b"] = b;
	_j["a"] = a;
}