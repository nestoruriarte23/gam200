#pragma once

#include "../Serializer/SerializeFactory.h"

namespace Engine
{
	struct  Color
	{
		union
		{
			struct
			{
				float r, g, b, a;
			};
			float v[4];
		};

		Color(float rr = 1.0f, float gg = 1.0f, float bb = 1.0f, float aa = 1.0f)
			: r(rr), g(gg), b(bb), a(aa) {}

		Color LerpColor(float _ratio);

		Color operator-(const Color & _other);

		Color operator+(const Color & _other);

		friend Color operator*(float _f, Color & _color) {
			Color c;
			c.r = _f * _color.r;
			c.g = _f * _color.g;
			c.b = _f * _color.b;
			c.a = _f * _color.a;
			return c;
		}


		Color operator*(float _f);


		void FromJson(nlohmann::json & _j);
		void ToJson(nlohmann::json & _j);

		static Color white;
		static Color red;
		static Color green;
		static Color lima;
		static Color blue;
		static Color black;
		static Color empty;
		static Color yellow;
		static Color cyan;
		static Color violet;
		static Color rosa;
		static Color turquesa;
	};
}