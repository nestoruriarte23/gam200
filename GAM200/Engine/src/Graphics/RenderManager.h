#pragma once

#include <glew.h>

#include "../Utilities/Math/Vector2.h"
#include "../Components/System.h"
#include "Shader.h"
#include "FrameBuffer.h"  
#include "Model.h"
#include "Vertex.h"
#include "Camera.h"
#include "ShadowEmitter.h"

#define MAX_DEBUG_LINES 200000

class renderable;
class CameraComp;
class Texture;
class Shadow;
class GetOutlined;

enum CustomBlendMode
{
	Normal,
	Additive,
	Multiplicative,
	CustomBlends_Total
};

enum BlendFunction
{
	Add = GL_FUNC_ADD,
	Substract = GL_FUNC_SUBTRACT,
	ReverseSubstract = GL_FUNC_REVERSE_SUBTRACT,
	Min = GL_MIN,
	Max = GL_MAX,
	Function_Total = 5
};

enum BlendParameter
{
	Zero = GL_ZERO,
	One = GL_ONE,
	Src_Color = GL_SRC_COLOR,
	One_Minus_Src_Color = GL_ONE_MINUS_SRC_COLOR,
	Dst_Color = GL_DST_COLOR,
	One_Minus_Dst_Color = GL_ONE_MINUS_DST_COLOR,
	Src_Alpha = GL_SRC_ALPHA,
	One_Minus_Src_Alpha = GL_ONE_MINUS_SRC_ALPHA,
	Dst_Alpha = GL_DST_ALPHA,
	One_Minus_Dst_Alpha = GL_ONE_MINUS_DST_ALPHA,
	Parameter_Total = 10
};

class RenderManager : public ISystem
{
	MAKE_SINGLETON(RenderManager)

public:

	/* Initialize program (Create the basic quad and shader program) */
	bool Initialize(); 
	void Reset();
	void Update();
	void Free();

	/* Shader Creation / Setting */
	bool			CreateShaderProgram(ShaderProgram** sp, std::string directory);
	void			UseShaderProgram(const ShaderProgram* sp);
	ShaderProgram*	GetQuadShaderProgram() { return mQuadShader; }
	ShaderProgram*	GetTextShaderProgram() { return mTextShader; }

	/* Model management */
	void		BindModel(Model* _model);
	void		UnbindModel();
	Model*		GetBasicQuad();
	GLuint		GetBasicQuadHandle();
	//std::vector<CameraComp*>& GetCamerasOfSpace(Space* wanted);

	/* Viewport */
	void SetViewport(const Viewport& _vp);
	void CleanViewport(Engine::Color _color, float depth = 0.0f);

	/* Frame Buffer */
	GLuint GetCurrentFrameBuffer();
	void SetFrameBuffer(const FrameBuffer& _fb);
	void SetFrameBuffer(GLuint _fb_handle);

	/* Do not care about the spaces (or cameras in the space) (Generally, just for the editor) */
	void DrawAllRenderables();

	/* Render an Scene */
	void RenderScene(Scene* _scene);

	/* Shadows */
	void AddShadow(Shadow* _shadow, Space* _space);
	void RemoveShadow(Shadow* _shadow);
	void AddShadowEmitter(ShadowEmitter* _shadow, Space* _space);
	void RemoveShadowEmitter(ShadowEmitter* _shadow);
	//const std::vector<ShadowEmitter*>& GetShadowEmitters(Space*);

	void RenderShadows(Space * _space);
	void DrawShadows(const std::vector<std::vector<IComp*>*>& _comps,
		const std::vector<CameraComp*>& _cameras);
	void DrawShadowsByCamera(const std::vector<std::vector<IComp*>*>& _comps,
		CameraComp* _cameras);
	void ComputeAlphaBasedOnShadow(GameObject* _owner, Shadow* _shadow
		, renderable* _renderable);

	/* Outlining objects behind other objects */
	void AddOutlined(GetOutlined* _outlined, Space* _space);
	void RemoveOutlined(GetOutlined* _outlined);
	void DrawOutlines(Space* _space);
	bool GetDrawingOutline() { return mbDrawingOutline; }
	ShaderProgram* GetOutlineShader() { return mOutlineShader; }

	/* Using the space concepts */
	void DrawSpace(Space* _space);
	void DrawComponents(const std::vector<std::vector<IComp*>*>& _comps,
		const  std::vector<CameraComp*>& _cameras);
	void DrawByCamera(const std::vector<std::vector<IComp*>*>& _comps,
		CameraComp* _cameras);
	void DrawModel(GameObject*, Model*, Texture*, ShaderProgram*, const Vector2& _rel_size = Vector2(1,1));
	void SetWorldToViewInAllShaderPrograms(float* _matrix_data);

	void AddEditorRenderable(renderable* _comp, Space* _space);
	void RemoveEditorRenderable(renderable* _comp);
	void RemoveAllRenderables();	
	void UpdateRenderables();


	std::map<Space*, std::vector<renderable*>>&	GetEditorRenderables();

	void AddCamera(CameraComp* _cam, Space* _space);
	CameraComp* GetCamera(Space* _space);
	void RemoveCamera(CameraComp* _cam);
	void RemoveAllCameras();

	void DrawScreenQuad(GLuint final_scene);

	/* Wireframe Toogle */
	void ToggleWireframeDraw();
	bool GetWireframeActive();
	void WireframeSetFillMode() const;
	void WireframeSetLineMode() const;

	/* Debug Drawing */
	void DrawLineAt(const Vector2& start, const Vector2& end, const Engine::Color& color);
	void DrawCircleAt(Vector2 pos, int precision, float diameter, Engine::Color color);
	void DrawRectangleAt(Vector2 pos, Vector2 size, Engine::Color color);
	void DrawRectangleAt(Vector2 pos, float width, float height, Engine::Color color);
	void DrawOrientedRectangleAt(Vector2 pos, float width, float height, float angle, Engine::Color color);
	void SortObjects();

	/* Draw all the debug data */
	void DrawDebugLines(CameraComp* _cam = nullptr);
	void DrawRunTimeDebugLines();

	/* Blending */
	void SetCustomBlendMode(unsigned _mode);
	void SetBlendFunction(unsigned _function);
	void SetBlendingParameters(unsigned _source_factor, unsigned _dest_factor);
	void SetBlendEquation(unsigned _source_factor, unsigned _function, unsigned _dest_factor);

	unsigned GetBlendingFunc();
	unsigned GetBlendingSourceParam();
	unsigned GetBlendingDestParam();
	void GetBlendingEquation(unsigned* _func, unsigned* _source, unsigned* _dest);

	/* Serialization */
	void FromJson(nlohmann::json & _j);
	void ToJson(nlohmann::json & _j);

	/* Settings Gettors */
	bool GetDrawingOutLine();
	int GetColorFlags();
	Engine::Color& GetShadowColor();

	float							mHighestRenderable{ 0 };

	// TODO: Translucent and non-translucent objects
	// We can sort all Translucent by the Z order (before drawing each frame)
	// We can sort the "Solid" objects by texture and run Z-Ordering by opengL 
private:
	std::map<Space*, std::vector<renderable*>>			mEditorRenderables;

	std::map<Space*, std::vector<std::vector<IComp*>*>>	mRenderables;
	std::map<Space*, std::vector<CameraComp*>>			mCameras;

	std::map<Space*, std::vector<std::vector<IComp*>*>>	mShadows;
	std::map<Space*, std::vector<std::vector<IComp*>*>>	mShadowEmitters;

	std::map<Space*, std::vector<std::vector<IComp*>*>>		mOutlineds;

	Vector2 mLastCenter;
	Vector2 mLastView;

	bool get_comps = false;

	bool							mbDrawingOutline{false};
	ShaderProgram*					mOutlineShader;

	/* Frequently used models and shaders */
	Model							mQuad;
	Model							mScreenQuad;

	ShaderProgram*					mQuadShader;
	ShaderProgram*					mScreenShader;
	ShaderProgram*					mTextShader;

	/* Debug Lines */
	std::vector<Line>				mLines;			// Actual data
	unsigned						mLineCount;		// Line count (overflow purposes)

	Model							mLineMesh;		// Model
	ShaderProgram*					mLineShader;	// Shader

	// Activate / Deactivate wireframe drawing
	bool							mbWireframeActive;
	GLuint							mCurrentFrameBuffer;

	/* Blending */
	unsigned						mBlendingFunction;
	unsigned						mBlendingSource;
	unsigned						mBlendingDestination;

	/* Extra settings */
	int								ColorEdit_Flags;
	Engine::Color					mGeneralShadowColor;
};

#define RenderMgr (RenderManager::Instance())

/* TODO: Do this in a good way, and check for errors when 
creating textures, shaders, throw exceptions etc. */
#define check_gl_error() _check_gl_error(__FILE__, __LINE__)
void  _check_gl_error(const char * file, int line);

float get_gl_depth(int x, int y);
float get_gl_depth(Vector2 _pos);

unsigned get_gl_stencil(int x, int y);
unsigned get_gl_stencil(Vector2 _pos);