#include "RenderManager.h"

Vector2 PointCord(int i, int precision, float radius) {
	Vector2 point = { 0,0 };
	point.x = radius * cos(2 * PI * i / (float)precision);
	point.y = radius * sin(2 * PI * i / (float)precision);
	return point;
}

void RenderManager::ToggleWireframeDraw()
{
	if (mbWireframeActive)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		mbWireframeActive = false;
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		mbWireframeActive = true;
	}
}

bool RenderManager::GetWireframeActive()
{
	return mbWireframeActive;
}

void RenderManager::WireframeSetFillMode() const
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void RenderManager::WireframeSetLineMode() const
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void RenderManager::DrawLineAt(const Vector2 & start, const Vector2 & end,
	const Engine::Color & color)
{
	if (mLineCount < MAX_DEBUG_LINES)
	{
		mLines.push_back(Line(Vector3(start.x, start.y, 0), Vector3(end.x, end.y, 0), color));
		mLineCount++;
	}
}

void RenderManager::DrawCircleAt(Vector2 pos, int precision, float diameter, 
	Engine::Color color)
{
	float radius = diameter / 2;

	/*We are drawing a regular polygon of i sides. For that, each side is a line
	  that needs to be drawn. For that, starting from 0 degrees, we are going
	  to build a side of the polygon each iteration of the loop. The parameter
	  that changes from iteration to iteration is the angle the first point is at*/
	for (int i = 0; i < precision; i++)
	{
		//First point coord
		Vector2 point1 = PointCord(i, precision, radius);

		//Second point coord
		Vector2 point2 = PointCord(i + 1, precision, radius);

		//Draw the line that links both points
		DrawLineAt(Vector2(point1.x + pos.x, point1.y + pos.y),
				   Vector2(point2.x + pos.x, point2.y + pos.y), color);
	}
}

void RenderManager::DrawRectangleAt(Vector2 pos, Vector2 size, Engine::Color color)
{
	// Call the other function
	DrawRectangleAt(pos, size.x, size.y, color);
}

void RenderManager::DrawRectangleAt(Vector2 pos, float width, float height, Engine::Color color)
{
	//Draw the 4 lines that a rectangle requires. The two horizontals, and the two verticals
	DrawLineAt(Vector2(-width / 2 + pos.x,  height / 2 + pos.y), Vector2( width / 2 + pos.x,  height / 2 + pos.y), color);
	DrawLineAt(Vector2(-width / 2 + pos.x,  height / 2 + pos.y), Vector2(-width / 2 + pos.x, -height / 2 + pos.y), color);
	DrawLineAt(Vector2(-width / 2 + pos.x, -height / 2 + pos.y), Vector2( width / 2 + pos.x, -height / 2 + pos.y), color);
	DrawLineAt(Vector2( width / 2 + pos.x,  height / 2 + pos.y), Vector2( width / 2 + pos.x, -height / 2 + pos.y), color);
}

void RenderManager::DrawOrientedRectangleAt(Vector2 pos, float width, float height,
	float angle, Engine::Color color)
{
	//Draw the 4 lines that a rectangle requires. The two horizontals, and the two verticals
	Vector2 topLeft(-width / 2, height / 2);
	Vector2 topRight(width / 2, height / 2);
	Vector2 botLeft(-width / 2, -height / 2);
	Vector2 botRight(width / 2, -height / 2);
	
	Matrix33 rotate = Matrix33::RotRad(angle);

	Vector2 topLeftR = rotate * topLeft;
	Vector2 topRightR = rotate * topRight;
	Vector2 botLeftR = rotate * botLeft;
	Vector2 botRightR = rotate * botRight;

	DrawLineAt(topLeftR + pos, topRightR + pos, color);
	DrawLineAt(topLeftR + pos, botLeftR + pos, color);
	DrawLineAt(botLeftR + pos, botRightR + pos, color);
	DrawLineAt(topRightR + pos, botRightR + pos, color);
}