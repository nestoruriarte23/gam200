#include <glew.h>
#include "../Game/Game.h"
#include "Camera.h"

Viewport::Viewport(const Vector2 & pos, const Vector2 & dimensions)
{
	mPos = pos;
	mDimensions = dimensions;
}

Matrix44 Viewport::GetNDCToViewport()
{
	/* TODO: Check this, in theory we should not do anything because vp is 
	set through OpenGL and NDC is obviously normalized, but just in case */
	return Matrix44();
}

Matrix44 Viewport::GetViewportToNDC()
{
	Matrix44 InvScale = Matrix44::Scale(Vector3(2 / mDimensions.x, 
		2 / mDimensions.y, 0));

	return InvScale;
}

Matrix44 Viewport::GetWindowToNDC()
{
	Matrix44 InvPos = Matrix44::Translate(Vector3(-mPos.x, -mPos.y, 0));
	Matrix44 InvScale = Matrix44::Scale(Vector3(2 / mDimensions.x, 2 / mDimensions.y, 1));
	
	return InvScale * InvPos;
}

void Viewport::SetProperties(Vector2 pos, Vector2 size)
{
	mPos = pos + size / 2 - Game->GetWindowSize() / 2;
	mDimensions = size;

	SetOpenGLViewport();
}

void Viewport::SetOpenGLViewport() const
{
	Vector2 pos(mPos - mDimensions / 2 + Game->GetWindowSize() / 2);

	glEnable(GL_SCISSOR_TEST);
	glViewport((int)(pos.x), (int)(pos.y),
		(int)(mDimensions.x), (int)(mDimensions.y));
	glScissor((int)(pos.x), (int)(pos.y),
		(int)(mDimensions.x), (int)(mDimensions.y));
}

bool Viewport::operator==(const Viewport& v) const {
	return v.mPos == mPos && v.mDimensions == mDimensions;
}

bool Viewport::operator!=(const Viewport& v) const {
	return v.mPos != mPos && v.mDimensions != mDimensions;
}

void Viewport::FromJson(nlohmann::json & _j)
{
	mPos.x = _j["mPos"]["x"];
	mPos.y = _j["mPos"]["y"];
	mDimensions.x = _j["mDimensions"]["x"];
	mDimensions.y = _j["mDimensions"]["y"];
}

void Viewport::ToJson(nlohmann::json & _j)
{
	_j["mPos"]["x"] = mPos.x;
	_j["mPos"]["y"] = mPos.y;
	_j["mDimensions"]["x"] = mDimensions.x;
	_j["mDimensions"]["y"] = mDimensions.y;
}

Viewport Viewport::WholeScreen()
{
	return Viewport(Vector2(0, 0), Game->GetWindowSize());
}


Camera::Camera() : mViewport(Vector2(0, 0), Game->GetWindowSize()), 
	mPos(0, 0), mOrientation(0) 
{
	mViewScale = Game->GetWindowSize();
}

Camera::Camera(const Vector2 & pos, const Vector2 & scale, float rotation,
	const Vector2 & viewportPos, const Vector2 & viewportScale)
	: mViewport(viewportPos, viewportScale)
{
	mPos = pos;
	mViewScale = scale;
	mOrientation = rotation;
}

bool Camera::Initialize()
{
	AlignWithViewport(&mViewport);
	return true;
}

void Camera::AlignWithViewport(Viewport * vp)
{
	mPos = vp->GetPos();
	mViewScale = vp->GetSize();

	mOrientation = 0.0f;
}

Matrix44 Camera::GetWorldToViewport()
{
	Matrix44 wtondc = GetWorldToNDC();

	return wtondc;
}

Matrix44 Camera::GetViewportToWorld()
{
	return GetNDCToWorld() * mViewport.GetViewportToNDC();
}


void Camera::RotateByDeg(f32 angle)
{
	mOrientation +=	(f32)ToRadians(angle);
}

void Camera::RotateByRad(f32 angle)
{
	mOrientation += angle;
}

void Camera::ScaleBy(f32 ratio)
{
	if (ratio >= 1.0f || ratio < 1.0f &&
		mViewScale.x > 2.0f && mViewScale.y > 2.0f)
	{
		mViewScale *= ratio;
	}
}

void Camera::PrintInfo()
{
	// Camera Properties
	std::cout										<< std::endl
				<< "CAMERA PROPERTIES: "			<< std::endl
				<< "Position: "		<< mPos			<< std::endl
				<< "Dimensions: "	<< mViewScale	<< std::endl
				<< "Rotation: "		<< mOrientation	<< std::endl

	// Viewport Properties
				<< "VIEWPORT PROPERTIES: "					<< std::endl
				<< "Position: "		<< mViewport.GetPos()	<< std::endl
				<< "Dimensions: "	<< mViewport.GetSize()	<< std::endl;
}

Matrix44 Camera::GetWorldToNDC()
{
	Matrix44 InvTranslate = Matrix44::Translate(Vector3(-mPos.x, -mPos.y, 0));
	Matrix44 InvRotate = Matrix44::Rotate(-mOrientation);
	Matrix44 InvScale = Matrix44::Scale(Vector3(2 / mViewScale.x, 
		2 / mViewScale.y, 1));

	return InvScale * InvRotate * InvTranslate;
}

Matrix44 GetWorldToNDC(const Camera& _cam)
{
	Matrix44 InvTranslate = Matrix44::Translate(Vector3(-_cam.mPos.x, -_cam.mPos.y, 0));
	Matrix44 InvRotate = Matrix44::Rotate(-_cam.mOrientation);
	Matrix44 InvScale = Matrix44::Scale(Vector3(2 / _cam.mViewScale.x,
		2 / _cam.mViewScale.y, 1));

	return InvScale * InvRotate * InvTranslate;
}

Matrix44 Camera::GetNDCToWorld()
{
	Matrix44 Translate = Matrix44::Translate(Vector3(mPos.x, mPos.y, 0));
	Matrix44 Rotate = Matrix44::Rotate(mOrientation);
	Matrix44 Scale = Matrix44::Scale(Vector3(mViewScale.x / 2, mViewScale.y / 2, 0));

	return Translate * Rotate * Scale;
}
