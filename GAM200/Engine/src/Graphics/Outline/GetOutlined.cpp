#include "GetOutlined.h"

#include "../Shadow.h"
#include "../RenderManager.h"
#include "../../Level/Editor/ImGUI_Basics.h"
#include "../../Level/Editor/ImGui/imgui.h"
#include "../../System/GameObjectManager/GameObject.h"

void GetOutlined::Initialize()
{
	//RenderMgr->AddOutlined(this, mOwner->GetSpace());

	for (unsigned i = 0; i < mOwner->GetComps().size(); i++)
	{
		mRenderable = dynamic_cast<renderable*>(mOwner->GetComps()[i]);

		if (mRenderable && dynamic_cast<Shadow*>(mRenderable) == nullptr)
			break;
	}
}

void GetOutlined::Shutdown()
{
	//RenderMgr->RemoveOutlined(this);
}

bool GetOutlined::Edit()
{
	bool changed = false;
	ImGui::PushID("GetOutlined");

	if (ImGui::TreeNode("GetOutlined"))
	{
		ImGui::PopID();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
		}
	}

	ImGui::PopID();
	return changed;
}

renderable * GetOutlined::GetRenderable()
{
	return mRenderable;
}

GetOutlined * GetOutlined::Clone()
{
	GetOutlined* _new = DBG_NEW GetOutlined(*this);

	return _new;
}

void GetOutlined::FromJson(nlohmann::json & _j)
{
}

void GetOutlined::ToJson(nlohmann::json & _j)
{
	_j["_type"] = "GetOutlined";
}

bool GetOutlined::are_the_same_type(IComp const & lhs)
{
	if (dynamic_cast<const GetOutlined*>(&lhs)) {
		return true;
	}
	return false;
}

void GetOutlined::operator=(IComp & _comp)
{
}

bool GetOutlined::equal_to(IComp const & other) const
{
	if (GetOutlined const* p = dynamic_cast<GetOutlined const*>(&other)) {
		return mRenderable == p->mRenderable;
	}
	else {
		return false;
	}
}
