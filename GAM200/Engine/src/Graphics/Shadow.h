#pragma once

#include "RenderableComp.h"

class Shadow : public renderable
{
	RTTI_DECLARATION_INHERITED(Shadow, renderable);

public:
	Shadow();
	~Shadow();

	void Initialize() override;
	void Shutdown() override;

	bool Edit() override;
	void Render() override;

	Shadow* Clone() override;

	void FromJson(nlohmann::json & _j) override;
	void ToJson(nlohmann::json & _j) override;

	bool are_the_same_type(IComp const& lhs) override;
	void operator=(IComp& _comp);

	Vector2					mOffset;
	Vector2					mRelativeSize;
protected:
	bool equal_to(IComp const& other) const override;

	bool					mbReceiveShadow;
	bool					mbUseGlobalShadow;
	TResource<Texture>*		mTexture;

	friend class RenderManager;
};
