#pragma once

#include "renderablecomp.h"
//#include "spine/spine.h"

class Shadow;

namespace spine
{
	class Atlas;
	class SkeletonData;
	class AnimationStateData;

	class Skeleton;
	class AnimationState;
}

struct SpineData 
{
	spine::Atlas* atlas = 0;
	spine::SkeletonData* skelData = 0;
	spine::AnimationStateData* animStateData = 0;

	TResource<Texture>*		texture{nullptr};		

	void operator=(SpineData & _data);

	void DeleteData();
	~SpineData();
};

class SpineAnimation : public renderable
{
	RTTI_DECLARATION_INHERITED(SpineAnimation, renderable);

public:
	SpineAnimation();

	void Render() override;
	bool Edit() override;

	void Initialize() override;
	void Shutdown() override;

	void Update();
	void ReloadSpineData();
	void ComputeModel(Texture*&);

	void SetCurrentAnim(std::string _anim_name, bool _looping = true);
	std::string GetCurrentAnimation();
	void ToogleVisibility() { mbVisible = !mbVisible; }
	void SetVisibility(bool _visible) { mbVisible = _visible; }
	void SetFlipX(bool _value) { mbFlipX = _value; }
	spine::AnimationState* GetAnimationState() { return mAnimState;  }

	SpineAnimation * Clone();

	void FromJson(nlohmann::json & _j);
	void ToJson(nlohmann::json & _j);
	void operator=(IComp&);

	bool are_the_same_type(IComp const& lhs);

	float					mRelSpeed;

protected:
	virtual bool equal_to(IComp const& other) const;

	// Spine Instance Data
	spine::Skeleton*		mSkeleton = 0;
	spine::AnimationState*	mAnimState = 0;

	Vector2					mRelSize;
	bool					mbPause;
	bool					mbFlipX;

	Shadow*					mShadow;

	std::string				mCurrentAnimation;

	// Spine shared resources
	TResource<SpineData>*	mSpineData;
};