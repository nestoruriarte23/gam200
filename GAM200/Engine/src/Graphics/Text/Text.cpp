#include "Text.h"
#include "..//..//Game/Game.h"
#include "..//..//System/GameObjectManager/GameObject.h"
#include "../../Engine/src/Level/Editor/ImGUI_Basics.h"
#include "../../Engine/src/Level/Editor/ImGui/imgui.h"
#include "../../Engine/src/ResourceManager/ActualResourceManager.h"
#include "../RenderManager.h"

void TextComp::FromJson(nlohmann::json & _j)
{
	nlohmann::json jname = _j["DisplayText"];
	SetText(jname.get<std::string>().c_str());

	if (_j.find("Visible") != _j.end())
		mbVisible = _j["Visible"];
	jname = _j["Font Name"];
	font_name = jname.get<std::string>().c_str();
	text_size = _j["Text Size"];
	text_color.FromJson(_j["Text Color"]);

	if (_j.find("mbVisible") != _j.end())
		mbVisible = _j["mbVisible"];
}

void TextComp::ToJson(nlohmann::json & _j)
{
	_j["_type"] = "TextComp";

	_j["Visible"] = mbVisible;
	_j["DisplayText"] = text;
	_j["mbVisible"] = mbVisible;
	_j["Font Name"] = font_name;
	_j["Text Size"] = text_size;
	text_color.ToJson(_j["Text Color"]);
}

bool TextComp::are_the_same_type(IComp const & lhs)
{
	if (dynamic_cast<const Font*>(&lhs)) {
		return true;
	}
	return false;
}

bool TextComp::equal_to(IComp const & other) const
{
	if (TextComp const* p = dynamic_cast<TextComp const*>(&other)) {
		return ((displayText == p->displayText) && (text == p->text));
	}
	else {
		return false;
	}
}

void TextComp::Initialize()
{
	displayText = dynamic_cast<TResource<Font>*>(gActualResourceMgr.GetResource("..//Resources//Font//Erroak_regular.ttf"));
	font_name = displayText->GetFilename();
	mShaderProgram = RenderMgr->GetTextShaderProgram();
	mModel = RenderMgr->GetBasicQuad();

	renderable::Initialize();
}

void TextComp::Render()
{

	if (!mbVisible)
		return;

	//////////////////////////////////////////////////
	//	OVERKILL, 1 QUAD PER LETTER, HAS TO BE FIXED	//
	//////////////////////////////////////////////////

	///------------ALLOCATING THE NECCESARY MEMORY-------///
	GLuint VAO, VBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	check_gl_error();

	///-------------ENABLE BLENDING AND GETTING THE PROJECTION MATRIX --------------//
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	check_gl_error();

	// use program and set uniforms
	glUseProgram(mShaderProgram->GetHandle());
	GLuint loc = glGetUniformLocation(mShaderProgram->GetHandle(), "textColor");
	glUniform3f(loc, text_color.r, text_color.g, text_color.b);
	
	// pass transform
	Matrix44 DrawMatrix;
	if (GameObject* owner = this->GetOwner()) {
		/* Set the Transform of the object */
		Transform2D mObjTransf(0, 0,
			text_size, text_size,
			owner->mRotation);

		/* Get the 44 matrix from the Transform */
		DrawMatrix = mObjTransf.GetMatrix();
		DrawMatrix.AddTranslation(Vector3(owner->mPosition.x,
			owner->mPosition.y, 0));
	}
	mShaderProgram->SetMatrix44("mtxModel", DrawMatrix.v);

	check_gl_error();

	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(VAO);

	check_gl_error();

	// Iterate through all characters
	std::string::const_iterator c;
	float temp_x = 0.0f;
	float temp_y = 0.0f;
	//text_size = 1.0f;

	float offset = 0.0;
	for (auto c : text)
	{
		Glyph ch = displayText->Get()->getChar(c);
		offset += (ch.mSize.x + ch.mOffset.x*2) * text_size;
	}
	offset /= 2;

	for(auto c = text.begin(); c != text.end(); c++)
	{
		Glyph ch = displayText->Get()->getChar(*c);
		
		GLfloat xpos = temp_x + ch.mOffset.x * text_size - offset;	//temp_x + ch.mOffset.x * text_size;		//ORIGIN POSITION ON THE QUAD
		GLfloat ypos = temp_y - ch.mSize.y/2 * text_size;//temp_y - (ch.mSize.y - ch.mOffset.y) * text_size;	//ORIGIN POSITION ON THE QUAD
		//ypos -= ypos / 2;

		GLfloat w = ch.mSize.x * text_size;	//QUAD SIZE
		GLfloat h = ch.mSize.y * text_size;	//QUAD SIZE
		// Update VBO for each character
		GLfloat vertices[6][4] = {
			{ xpos,     ypos + h,   0.0, 0.0 },//GENERATING VERTICES
			{ xpos,     ypos,       0.0, 1.0 },//GENERATING VERTICES
			{ xpos + w, ypos,       1.0, 1.0 },//GENERATING VERTICES
											   //GENERATING VERTICES
			{ xpos,     ypos + h,   0.0, 0.0 },//GENERATING VERTICES
			{ xpos + w, ypos,       1.0, 1.0 },//GENERATING VERTICES
			{ xpos + w, ypos + h,   1.0, 0.0 } //GENERATING VERTICES
		};

		// Render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, ch.mCharacterID);
		// Update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		size_t len = sizeof(vertices);
		glBufferSubData(GL_ARRAY_BUFFER, 0, len, vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);

		check_gl_error();

		// Now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		temp_x += (ch.mNextChar>> 6) * text_size; // Bitshift by 6 to get value in pixels (2^6 = 64)
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);

	check_gl_error();
}

bool TextComp::Edit()
{

	bool changed = false;
	ImGui::PushID(GetUID());

	if (ImGui::TreeNode("TextComp"))
	{
		ImGui::Checkbox("Visible", &mbVisible);
		ImGui::DragFloat("Size", &text_size, 0.05f);
		char tempName[200]{0};

		//////--------------Fix string modification ------------------//////
		for (unsigned i = 0; i < text.size(); i++)
			tempName[i] = text[i];

		ImGui::InputText("String", tempName, IM_ARRAYSIZE(tempName));

		text = tempName;

		auto selected_item = 0;
		auto& all_fonts = gActualResourceMgr.GetAllFilenamesOfType(FILETYPE::TextType);

		if (ImGui::BeginCombo("Font", all_fonts[selected_item].size() == 0 ? "" : all_fonts[selected_item].data()))
		{
			for (unsigned n = 0; n < all_fonts.size(); n++)
			{
				bool is_selected = (selected_item == n);
				if (ImGui::Selectable(all_fonts[n].data(), is_selected))
				{
					selected_item = n;
					displayText = gActualResourceMgr.GetResource<Font>(all_fonts[n]);
					changed = true;
				}
				
				if (is_selected)
				{
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}

		changed = ImGui::ColorEdit4(" Color", (float*)&text_color,
			RenderMgr->GetColorFlags()) || changed;

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
		}
	}

	ImGui::PopID();
	return changed;
}
