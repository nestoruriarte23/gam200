#pragma once

#include <string>
#include "..//..//ResourceManager/ActualResourceManager.h"
#include "..//RenderableComp.h"
#include "Glyph.h"

class Font
{
public:
	~Font() { mAllCharacters.clear(); }
	Glyph getChar(char indx) { return mAllCharacters[indx]; }
	void InsertGlyph(std::pair<char, Glyph> to_add) {mAllCharacters.insert(to_add);}

private:
	std::map<char, Glyph> mAllCharacters;
};

class TextComp : public renderable
{
public:
	
	RTTI_DECLARATION_INHERITED(TextComp, renderable);

	TextComp(): mbVisible(false) {}

	void SetText(const char* text_) { text = text_; }

	void FromJson(nlohmann::json & _j);
	void ToJson(nlohmann::json & _j);

	bool are_the_same_type(IComp const& lhs);
	TextComp* Clone() { return DBG_NEW TextComp(); }


	void operator=(IComp& _go) {}

	bool equal_to(IComp const& other) const;

	void Initialize();
	void Render();
	bool Edit();

	TResource<Font>* displayText;

	std::string text = "HELLO THERE!";
	std::string font_name;
	float text_size = 10.0f;
	Engine::Color text_color;
	bool mbVisible;
	PROP_Val(int, verga, 0);
};

