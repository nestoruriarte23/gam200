#pragma once
#include "..//RenderableComp.h"


struct Glyph
{
	unsigned   mCharacterID;
	Vector2	   mSize;
	Vector2	   mOffset;
	int        mNextChar;

};