#include "Shadow.h"
#include "RenderManager.h"
#include "../Game/Game.h"
#include "../Level/Scene/Scene.h"
#include "../Level/Editor/ImGUI_Basics.h"
#include "../Level/Editor/ImGui/imgui.h"
#include "../System/GameObjectManager/GameObject.h"
#include "../ResourceManager/ActualResourceManager.h"

Shadow::Shadow() : mOffset(0,0), mRelativeSize{1,1}
{
	mShaderProgram = RenderManager::Instance()->GetQuadShaderProgram();
	mModel = RenderManager::Instance()->GetBasicQuad();

	mTexture = gActualResourceMgr.GetResource<Texture>
		("..//Resources//Textures//Shadow2.png");
		//("..//Resources//Textures//Shadow550x350.png");
}

Shadow::~Shadow() {}

void Shadow::Initialize()
{
	mbEmitOutline = false;

	renderable::Initialize();
	RenderMgr->AddShadow(this, mOwner->GetSpace());
}

void Shadow::Shutdown()
{
	renderable::Shutdown();
	RenderMgr->RemoveShadow(this);
}

bool Shadow::Edit()
{
	bool changed = false;
	ImGui::PushID("Shadow");

	if (ImGui::TreeNode("Shadow"))
	{
		changed = (ImGui::ColorEdit4(" ModColor", mModColor.v, RenderMgr->GetColorFlags())) || changed;
		changed = ImGui::Checkbox(" Visible", &mbVisible) || changed;

		changed = ImGui::DragFloat2(" Offset", mOffset.v) || changed;
		changed = ImGui::DragFloat2(" RelSize", mRelativeSize.v, 0.006f, 0.0f, 5.0f) || changed;

		changed = ImGui::Checkbox(" UseGeneralShadow", &mbUseGlobalShadow) || changed;

		changed = ImGui::Checkbox(" ReceiveShadow ", &mbReceiveShadow) || changed;

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
		}
	}

	ImGui::PopID();
	return changed;
}

void Shadow::Render()
{
	if (!Game->onEditor || !mbVisible)
		return;

	GameObject* owner = this->GetOwner();
	if (owner != NULL) {
		/* Set the Transform of the object */
		Transform2D mObjTransf(0, 0,
			owner->mScale.x * mRelativeSize.x, owner->mScale.y * mRelativeSize.y,
			owner->mRotation);

		/* Get the 44 matrix from the Transform */
		Matrix44 DrawMatrix = mObjTransf.GetMatrix();
		DrawMatrix.AddTranslation(Vector3(owner->mPosition.x + mOffset.x,
			owner->mPosition.y + mOffset.y, 0));

		/* TODO: Check the different texture units */
		glActiveTexture(GL_TEXTURE0);

		RenderManager::Instance()->BindModel(mModel);

		/* Set the right texture (TODO: Optimize by sorting) */
		if (mTexture && mTexture->Get())
			glBindTexture(GL_TEXTURE_2D, mTexture->Get()->mHandle);
		else
			glBindTexture(GL_TEXTURE_2D, 0);

		/* Get the memory (GPU?) location of the variable for the mtx */
		GLint loc = glGetUniformLocation(mShaderProgram->GetHandle(), "mtxModel");
		glUniformMatrix4fv(loc, 1, GL_TRUE, DrawMatrix.v);

		loc = glGetUniformLocation(mShaderProgram->GetHandle(), "ourTexture");
		glUniform1i(loc, 0);

		loc = glGetUniformLocation(mShaderProgram->GetHandle(), "modColor");
		if (!mbUseGlobalShadow)
			glUniform4f(loc, mModColor.r, mModColor.g, mModColor.b, mModColor.a);
		else
		{
			auto& global = RenderMgr->GetShadowColor();
			glUniform4f(loc, global.r, global.g, global.b, global.a);
		}

		/* Draw the actual triangles */
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		RenderManager::Instance()->UnbindModel();
	}
}

Shadow * Shadow::Clone()
{
	Shadow* _new = DBG_NEW Shadow(*this);

	return _new;
}

void Shadow::FromJson(nlohmann::json & _j)
{
	mShaderProgram = RenderManager::Instance()->GetQuadShaderProgram();
	mTexture = gActualResourceMgr.GetResource<Texture>(_j["mTextureName"]);

	mRelativeSize.x = _j["mRelativeSize"]["x"];
	mRelativeSize.y = _j["mRelativeSize"]["y"];
	
	mOffset.x = _j["mOffset"]["x"];
	mOffset.y = _j["mOffset"]["y"];

	if (_j.find("mbReceiveShadow") != _j.end())
		mbReceiveShadow = _j["mbReceiveShadow"];
	else
		mbReceiveShadow = true;

	if (_j.find("mbUseGlobalShadow") != _j.end())
		mbUseGlobalShadow = _j["mbUseGlobalShadow"];
	else
		mbUseGlobalShadow = true;

	if (_j.find("mbVisible") != _j.end())
		mbVisible = _j["mbVisible"];

	if (_j.find("mModColor") != _j.end())
		mModColor.FromJson(_j["mModColor"]);
	else
		mModColor = Engine::Color(1, 1, 1, 1);
}

void Shadow::ToJson(nlohmann::json & _j)
{
	_j["_type"] = "Shadow";

	_j["mTextureName"] = mTexture->GetFilename();
	_j["mbReceiveShadow"] = mbReceiveShadow;
	_j["mbUseGlobalShadow"] = mbUseGlobalShadow;

	_j["mRelativeSize"];
	_j["mRelativeSize"]["x"] = mRelativeSize.x;
	_j["mRelativeSize"]["y"] = mRelativeSize.y;

	_j["mOffset"];
	_j["mOffset"]["x"] = mOffset.x;
	_j["mOffset"]["y"] = mOffset.y;

	_j["mbVisible"] = mbVisible;
	mModColor.ToJson(_j["mModColor"]);
}

bool Shadow::are_the_same_type(IComp const & lhs)
{
	if (dynamic_cast<const Shadow*>(&lhs)) {
		return true;
	}
	return false;
}

void Shadow::operator=(IComp & _comp)
{
	Shadow & shadow = *dynamic_cast<Shadow*>(&_comp);
	mTexture = gActualResourceMgr.GetResource<Texture>(shadow.mTexture->GetFilename());

	renderable::operator=(static_cast<renderable&>(_comp));

	mOffset = shadow.mOffset;
	mRelativeSize = shadow.mRelativeSize;
	mbReceiveShadow = shadow.mbReceiveShadow;
	mbVisible = shadow.mbVisible;
	mbUseGlobalShadow = shadow.mbUseGlobalShadow;

	mModColor = shadow.mModColor;
}

bool Shadow::equal_to(IComp const & other) const
{
	if (Shadow const* p = dynamic_cast<Shadow const*>(&other)) {
		return mTexture == p->mTexture;
	}
	else {
		return false;
	}
}
