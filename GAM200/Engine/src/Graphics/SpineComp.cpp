
#include "../ResourceManager/ActualResourceManager.h"
#include "RenderManager.h"
#include "Color.h"
#include "Shadow.h"
#include "../System/GameObjectManager/GameObject.h"
#include "../Utilities/Time/FrameTime.h"
#include "../Game/Game.h"

#include "spine/spine.h"
#include "SpineComp.h"

#include "../Utilities/MyDebug/LeakDetection.h"

#pragma region Spine Extension

#include "spine/Extension.h"
using spine::SpineExtension;

namespace spine
{
	SpineExtension *getDefaultExtension()
	{
		return new spine::DefaultSpineExtension();
	}
}

void FreeSpineExtension()
{
	delete SpineExtension::getInstance();
}

#pragma endregion

SpineAnimation::SpineAnimation() 
	: renderable()
	, mSkeleton(0)
	, mAnimState(0)
	, mRelSize(1,1)
	, mRelSpeed(1)
	, mbPause(false)
{
	// Defaut animation, when adding the animation component to an object
	auto first_anim = gActualResourceMgr.GetAllFilenamesOfType(FILETYPE::AnimationType).front();

	mSpineData = gActualResourceMgr.GetResource<SpineData>(first_anim);

	mShaderProgram = RenderManager::Instance()->GetQuadShaderProgram();
}

void SpineAnimation::Render()
{
	if (!mbVisible) return;

	ShaderProgram* myShader = RenderMgr->GetDrawingOutline() ?
		RenderMgr->GetOutlineShader() : mShaderProgram;

	mSkeleton->getRootBone()->setScaleX(mbFlipX ? -1.0f : 1.0f);

	// clear the model
	mModel->ClearModel();

	Texture * texInUse = nullptr;		

	/* Compute the modulation color based on the shadows */
	if (mShadow && !(Game->onEditor)) 
		RenderMgr->ComputeAlphaBasedOnShadow(mOwner, mShadow, this);

	check_gl_error();

	GLuint loc = glGetUniformLocation(myShader->GetHandle(), "modColor");

	check_gl_error();

	glUniform4f(loc, mModColor.r, mModColor.g, mModColor.b, mModColor.a);

	check_gl_error();

	ComputeModel(texInUse);

	//texInUse = mSpineData->Get()->texture ? mSpineData->Get()->texture->Get() : nullptr;

	// upload to the gpu
	mModel->UploadToGPU();

	/* Draw the model with the right texture (the camera already set) */
	RenderManager::Instance()->DrawModel(mOwner, mModel, texInUse, 
		myShader, mRelSize);
}

#include "../Level/Editor/ImGUI_Basics.h"
#include "../Level/Editor/ImGui/imgui.h"

bool SpineAnimation::Edit()
{
	bool changed = false;
	ImGui::PushID("Animation");

	if (ImGui::TreeNode("Animation"))
	{
		/* Choose between Spine files / .anims */
		auto& animations = gActualResourceMgr.GetAllFilenamesOfType
		(FILETYPE::AnimationType);

		std::vector<std::string>::iterator anim_it =
			std::find(animations.begin(), animations.end(), mSpineData->GetFilename());

		if (ImGui::BeginCombo(" SpineFile", anim_it->data()))
		{
			for (auto it = animations.begin(); it != animations.end(); ++it)
			{
				bool is_selected = (anim_it == it);
				if (ImGui::Selectable(it->data(), is_selected))
				{
					anim_it = it;
					mSpineData = gActualResourceMgr.GetResource<SpineData>(*anim_it);
					ReloadSpineData();
					mModel->ClearModel();
					changed = true;
				}

				if (is_selected)
				{
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}

		if (ImGui::BeginCombo(" Animation", mCurrentAnimation.data()))
		{
			auto animations = mSpineData->Get()->skelData->getAnimations();
			for (u32 i = 0; i < animations.size(); ++i) 
			{
				bool is_selected = (mCurrentAnimation == animations[i]->getName().buffer());

				if (ImGui::Selectable(animations[i]->getName().buffer(), is_selected))
				{
					mAnimState->setAnimation(0, animations[i], true);
					mCurrentAnimation = animations[i]->getName().buffer();
					changed = true;
				}

				if (is_selected)
				{
					ImGui::SetItemDefaultFocus();
				}
			}
			ImGui::EndCombo();
		}

		changed = ImGui::DragFloat2(" Relative Size", mRelSize.v, 0.03f, 0.0f, 20.0f) || changed;
		changed = ImGui::DragFloat(" Relative Speed", &mRelSpeed, 0.015f, 0.0f, 20.0f) || changed;
		changed = ImGui::Checkbox(" Pause", &mbPause) || changed;
		changed = ImGui::Checkbox(" FlipX", &mbFlipX) || changed;

		changed = EditRenderable() || changed;

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}

	ImGui::PopID();
	return changed;
}

void SpineAnimation::Initialize()
{
	/* Create model */
	mModel = DBG_NEW Model();

	// Sanity check
	assert(mSpineData->Get() && mSpineData->Get()->skelData 
		&& mSpineData->Get()->atlas);

	mShadow = mOwner->get_component_type<Shadow>();

	// create the spine instance data
	ReloadSpineData();

	renderable::Initialize();

	if (mCurrentAnimation == "")
		return;

	auto animations = mSpineData->Get()->skelData->getAnimations();
	for (u32 i = 0; i < animations.size(); ++i) {
		if (mCurrentAnimation == animations[i]->getName().buffer())
		{
			mAnimState->setAnimation(0, animations[i], true);
			return;
		}
	}
}

void SpineAnimation::Shutdown()
{
	if (mModel) {
		mModel->ClearModel();
		delete mModel; mModel = nullptr;
	}

	renderable::Shutdown();
}

void SpineAnimation::Update()
{
	if (mbPause)
		return;

	// get frame time
	f32 dt = (f32)TimeManager::Instance()->GetDt();

	if (!mAnimState) return;

	// update animation and apply to the skeleton
	mAnimState->update(dt * mRelSpeed);
	mAnimState->apply(*mSkeleton);

	// update the world transform of the whole skeleton (bone hierarchy)
	mSkeleton->updateWorldTransform();
}

void SpineAnimation::ReloadSpineData()
{
	if (mSkeleton)  delete mSkeleton;
	if (mAnimState) delete mAnimState;

	// create the skeleton
	mSkeleton = new spine::Skeleton(mSpineData->Get()->skelData);

	// create the animation state
	mAnimState = new spine::AnimationState(mSpineData->Get()->animStateData);

	// set default animation
	if (mSpineData->Get()->skelData->getAnimations().size())
		mAnimState->setAnimation(0, mSpineData->Get()->skelData->getAnimations()[0], true);

	// set default skin
	if (mSpineData->Get()->skelData->getSkins().size())
		mSkeleton->setSkin(mSpineData->Get()->skelData->getSkins()[0]);
}

void SpineAnimation::ComputeModel(Texture*& texInUse)
{
	//unsigned misses = 0;

	/* Compute model this frame */
	for (size_t i = 0, n = mSkeleton->getSlots().size(); i < n; ++i)
	{
		spine::Slot* slot = mSkeleton->getDrawOrder()[i];

		// Fetch the currently active attachment, continue
		// with the next slot in the draw order if no
		// attachment is active on the slot
		spine::Attachment* attachment = slot->getAttachment();
		if (!attachment)
		{
			//misses++;
			continue;
		}

		check_gl_error();

		// todo: the blending mode, see http://esotericsoftware.com/spine-cpp#Implementing-Rendering

		 // Calculate the tinting color based on the skeleton's color
		// and the slot's color. Each color channel is given in the range [0-1]
		spine::Color skeletonColor = mSkeleton->getColor();
		spine::Color slotColor = slot->getColor();
		Engine::Color tint(skeletonColor.r * slotColor.r, skeletonColor.g * slotColor.g, skeletonColor.b * slotColor.b, skeletonColor.a * slotColor.a);

		// draw region attachement
		if (attachment->getRTTI().isExactly(spine::RegionAttachment::rtti))
		{
			// hack 
			static float vertex_positions[8];

			// cast the attachment to the correct region attachment
			spine::RegionAttachment * region = dynamic_cast<spine::RegionAttachment*>(attachment);

			// get the texture for that region
			TResource<Texture>* resource = (TResource<Texture>*)(((spine::AtlasRegion*)region->getRendererObject())->page->getRendererObject());
			if (texInUse != resource->Get()) // new texture
			{
				// TODO
				// draw whatever you have so far. 
				// clear the model
				// and continue. 

				mModel->UploadToGPU();

				RenderManager::Instance()->DrawModel(this->mOwner, mModel,
					texInUse, RenderMgr->GetDrawingOutline() ?
					RenderMgr->GetOutlineShader() : mShaderProgram);

				mModel->ClearModel();

				texInUse = resource->Get();
			}

			// get the region vertices in world 
			// note: here x and y are consecutive, so:
			// vertex_positions[0] represents the x value of the first vertex
			// vertex_positions[1] represents the y value of the first vertex. 
			// vertex_positions[2] represents the x value of the second vertex
			// vertex_positions[3] represents the y value of the second vertex. 
			// etc...
			region->computeWorldVertices(slot->getBone(), vertex_positions, 0, 2);

			// add the whole quad to the model
			Vertex tmp;
			tmp.SetVertexColor(tint);

			tmp.SetPosition(Vector2(vertex_positions[0], vertex_positions[1]));
			tmp.SetTexCoord(Vector2(region->getUVs()[0], 1 - region->getUVs()[1]));
			mModel->AddVertex(tmp); mModel->AddIndex();

			tmp.SetPosition(Vector2(vertex_positions[2], vertex_positions[3]));
			tmp.SetTexCoord(Vector2(region->getUVs()[2], 1 - region->getUVs()[3]));
			mModel->AddVertex(tmp); mModel->AddIndex();

			tmp.SetPosition(Vector2(vertex_positions[4], vertex_positions[5]));
			tmp.SetTexCoord(Vector2(region->getUVs()[4], 1 - region->getUVs()[5]));
			mModel->AddVertex(tmp); mModel->AddIndex();

			tmp.SetPosition(Vector2(vertex_positions[0], vertex_positions[1]));
			tmp.SetTexCoord(Vector2(region->getUVs()[0], 1 - region->getUVs()[1]));
			mModel->AddVertex(tmp); mModel->AddIndex();

			tmp.SetPosition(Vector2(vertex_positions[4], vertex_positions[5]));
			tmp.SetTexCoord(Vector2(region->getUVs()[4], 1 - region->getUVs()[5]));
			mModel->AddVertex(tmp); mModel->AddIndex();

			tmp.SetPosition(Vector2(vertex_positions[6], vertex_positions[7]));
			tmp.SetTexCoord(Vector2(region->getUVs()[6], 1 - region->getUVs()[7]));
			mModel->AddVertex(tmp); mModel->AddIndex();
		}

		if (attachment->getRTTI().isExactly(spine::MeshAttachment::rtti))
		{
			// hack 

			// cast the attachment to the correct region attachment
			spine::MeshAttachment * mesh = dynamic_cast<spine::MeshAttachment*>(attachment);

			//(TResource<Texture>*)(((spine::AtlasRegion*)region->getRendererObject())->page->getRendererObject());
			TResource<Texture>* resource = (TResource<Texture>*)((spine::AtlasRegion*)mesh->getRendererObject())->page->getRendererObject();
			if (texInUse != resource->Get()) // new texture
			{
				// TODO
				// draw whatever you have so far. 
				// clear the model
				// and continue. 

				mModel->UploadToGPU();

				RenderManager::Instance()->DrawModel(this->mOwner, mModel,
					texInUse, RenderMgr->GetDrawingOutline() ?
					RenderMgr->GetOutlineShader() : mShaderProgram);

				mModel->ClearModel();

				texInUse = resource->Get();
			}

			size_t numVertices = mesh->getWorldVerticesLength();
			float * vertex_positions = DBG_NEW float[numVertices];

			mesh->computeWorldVertices(*slot, 0, numVertices, vertex_positions, 0, 2);

			// triangles is an array of indices of the vertices in 'vertex_positions' that 
			// is used to construct the triangles of the mesh attachment
			auto& triangles = mesh->getTriangles();

			// construct the triangle mesh using the indices and the vertex array
			Vertex tmp;
			for (size_t j = 0; j < triangles.size(); ++j)
			{
				unsigned short vIdx = triangles[j];

				// copy the vertex position (note: here we multiply by 2 because for each vertex we have 2 floats (x,y))
				tmp.SetPosition(Vector2(vertex_positions[vIdx * 2],
					vertex_positions[vIdx * 2 + 1]));

				// copy the vertex uvs 
				// (note 1: here we multiply by 2 because for each vertex we have 2 floats (x,y))
				// (note 2: here we take the uvs directly from the mesh attachment, because their values doesn't change with the animation)
				tmp.SetTexCoord(Vector2(mesh->getUVs()[vIdx * 2],
					1.0f - mesh->getUVs()[vIdx * 2 + 1]));

				// set the color
				tmp.SetVertexColor(tint);

				// add to the model
				mModel->AddVertex(tmp);
				mModel->AddIndex();
			}

			delete[] vertex_positions;
		}
	}

	//std::cout << "Missed attachments: " << misses << std::endl;
}

void SpineAnimation::SetCurrentAnim(std::string _anim_name, bool _looping)
{
	mAnimState->setAnimation(0, _anim_name.data(), _looping);
	mCurrentAnimation = _anim_name;
}

std::string SpineAnimation::GetCurrentAnimation()
{
	return mCurrentAnimation;
}

SpineAnimation * SpineAnimation::Clone()
{
	SpineAnimation* _new = DBG_NEW SpineAnimation(*this);

	_new->mModel = DBG_NEW Model();
	_new->mSkeleton = nullptr;
	_new->mAnimState = nullptr;

	return _new;
}

void SpineAnimation::FromJson(nlohmann::json & _j)
{
	mCurrentAnimation = _j["mCurrentAnimation"];

	mRelSize.x = _j["mRelSize"]["x"];
	mRelSize.y = _j["mRelSize"]["y"];
	
	mRelSpeed = _j["mRelSpeed"];
	mbPause = _j["mbPause"];

	if (_j.find("mbFlipX") != _j.end())
	mbFlipX = _j["mbFlipX"];

	mSpineData = gActualResourceMgr.GetResource<SpineData>(_j["mAnimName"]);

	FromJsonRenderable(_j);
}

void SpineAnimation::ToJson(nlohmann::json & _j)
{
	_j["_type"] = "SpineAnimation";

	_j["mAnimName"]	= mSpineData->GetFilename();
	_j["mCurrentAnimation"] = mCurrentAnimation;

	_j["mRelSize"];
	_j["mRelSize"]["x"] = mRelSize.x;
	_j["mRelSize"]["y"] = mRelSize.y;

	_j["mRelSpeed"] = mRelSpeed;
	_j["mbPause"] = mbPause;
	_j["mbFlipX"] = mbFlipX;

	ToJsonRenderable(_j);
}

void SpineAnimation::operator=(IComp& _comp) {
	SpineAnimation & spine = *dynamic_cast<SpineAnimation*>(&_comp);

	mRelSize = spine.mRelSize;

	mRelSpeed = spine.mRelSpeed;
	mbPause = spine.mbPause;

	mSpineData = gActualResourceMgr.GetResource<SpineData>(spine.mSpineData->GetFilename());

	if (mSpineData != spine.mSpineData) ReloadSpineData();
	mModel->ClearModel();

	auto animations = mSpineData->Get()->skelData->getAnimations();

	for (unsigned i = 0; i < animations.size(); i++) {
		if (animations[i]->getName().buffer() == spine.GetCurrentAnimation()) {
			mCurrentAnimation = animations[i]->getName().buffer();
			mAnimState->setAnimation(0, animations[i], true);
		}
	}

	mShaderProgram = spine.mShaderProgram;
	mModColor = spine.mModColor;
	mbVisible = spine.mbVisible;

	mbEmitOutline = spine.mbEmitOutline;
}


bool SpineAnimation::are_the_same_type(IComp const & lhs)
{
	if (dynamic_cast<const SpineAnimation*>(&lhs)) {
		return true;
	}
	return false;
}

bool SpineAnimation::equal_to(IComp const & other) const
{
	if (SpineAnimation const* p = dynamic_cast<SpineAnimation const*>(&other)) {
		return mSpineData->GetFilename() == p->mSpineData->GetFilename();
	}
	else {
		return false;
	}
}

void SpineData::DeleteData()
{
	if (atlas) {
		delete atlas; atlas = 0;
	}
	if (skelData) {
		delete skelData; skelData = 0;
	}
	if (animStateData) {
		delete animStateData; animStateData = 0;
	}
}

void SpineData::operator=(SpineData & _data) {

};

SpineData::~SpineData()
{
	DeleteData();
}
