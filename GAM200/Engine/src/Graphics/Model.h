#pragma once

#include "../Utilities/Containers.h"
#include "Vertex.h"
#include <glew.h>

class Model
{
	friend class RenderManager;

	/* TODO: At some point, maybe not this year, make the Upload to GPU
	generic for any vertex configuration, just through the vertex properties */

public:
	Model();
	virtual ~Model();
	void AddVertex(const Vertex & vert) { mVertices.push_back(vert); }
	void AddParticleVertex(const ParticleVertex& vert) { mParticleVertices.push_back(vert); }
	void AddIndices(const unsigned index) { mIndices.push_back(index); }
	void AddIndices(const unsigned * indices, unsigned size);
	void AddIndex();

	void ClearModel();
	void UploadToGPU();
	void UploadParticleModelToGPU();
	void ReloadToGPU();
	int GetSize();

	void SetColor(Engine::Color _color);

	void SaveHandle(GLuint* destination);
	GLuint GetHandle();

	void CreateBasicQuad(float);
	void CreateLineMesh();
	void CreateParticleQuad();

private:
	std::vector<Vertex>			mVertices;
	std::vector<ParticleVertex>	mParticleVertices;
	std::vector<unsigned int>	mIndices;

	GLuint						mHandle;
	GLuint						mVertexBuffer;
	GLuint						mIndexBuffer;
	GLuint						mOffsetArray{0};

	friend class ParticleSystem;
};