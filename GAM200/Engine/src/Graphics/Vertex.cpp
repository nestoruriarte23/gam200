#include "Vertex.h"

void Vertex::SetPosition(const Vector2 & _pos)
{
	mPos.x = _pos.x;
	mPos.y = _pos.y;
	mPos.z = 0;
}

void Vertex::SetPosition(const Vector3 & _pos)
{
	mPos = _pos;
}

void Vertex::SetTexCoord(const Vector2 & _coords)
{
	mUV = _coords;
}
