#pragma once

class Window;
class GameStateManager;

bool SystemInit(Window* myWindow, const char* windowName, unsigned xSize, unsigned ySize, bool fullscreen);

void SystemExit(Window* myWindow, GameStateManager* gsm);

void SystemGameLoop();

void SystemHideConsole(bool hide);

void SystemHideCursor(bool hide);
