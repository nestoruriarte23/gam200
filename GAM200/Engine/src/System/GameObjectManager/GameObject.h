#pragma once
#include <algorithm>
#include "../../Utilities/Math/MyMath.h"
#include "..//..//Utilities/Containers.h"
#include "..//..//Serializer/SerializeFactory.h"
#include "..//..//Components/Components.h"

// forward declarations
class renderable;
class Shadow;
class IBase;
struct Space;

class GameObject : public IBase
{
		RTTI_DECLARATION_INHERITED(GameObject, IBase);

	public:

		GameObject();
		
		virtual ~GameObject();

		// (Variables of transform comp) (Should be made properties)
		Vector2 mPosition;
		Vector2 mSortingPos;
		Vector2 mScale;
		float  mRotation;
		bool  mbSerializable = true;
		bool  mbArchetype = false;
		std::string  mArchetypeName;
		Tags mTag;
		std::string  mRegion;

		// State Methods
		virtual void SetEnabled(bool enabled); // Call Set Enabled on all components
		virtual void Initialize();	// Calls initialize on all components
		virtual void InitializeCameras();	// Calls initialize on all components
		virtual void Shutdown();
		virtual void Move(Vector2& direction, float speed);
		virtual void Move(Vector2& old_pos, Vector2& new_pos);
		// --------------------------------------------------------------------
		//----------------------JSON-------------------------------------------
		virtual void ToJson(nlohmann::json& j);
		void ToJsonArchetype(nlohmann::json& j);
		virtual void FromJson(nlohmann::json& j);
		void FromJsonArchetype(nlohmann::json& j);
		void UpdateArchetype(GameObject& _go);
#pragma region// COMPONENT MANAGEMENT

		// Getters
		Space* GetSpace();
		u32 GetCompCount() const;
		IComp* GetComp(u32 index)const;
		IComp* GetComp(const char* type)const;
		IComp* GetComp(const Rtti& type)const;
		IComp* GetCompName(const char* compName, const char* compType = NULL)const;
		// template
		template<class T>
		T* GetComp(const char* name = NULL);

		template<class T>
		T* GetCompDerived(const char* name = NULL);

		template<typename T>
		T* NewComp(const char* name = NULL);

		template <typename T>
		T* get_component_type()const
		{
			//create a pointer as the same type as the one provided
			T* Type = NULL;
			//iterate throw all the components
			for (unsigned i = 0; i< mComps.size(); i++)
			{
				//cast the component to the one provided
				Type = dynamic_cast<T*>(mComps[i]);
				//if it exists returns it
				if (Type)
					return Type;
			}
			//otherwise return null
			return NULL;
		}

		void SetRegion(std::string _name);

		// Add/Remove by address
		IComp* AddComp(IComp* pComp);
		// Add/Remove by address
		IComp* AddCompNoInitialize(IComp* pComp);

		void RemoveComp(IComp* pComp);

		// Removes first component encoutered that match the search criteria
		void RemoveCompType(const char* compType);
		void RemoveCompType(const Rtti& compType);
		void RemoveCompName(const char* compName, const char* compType = NULL);

		// Removes all components encoutered that match the search criteria
		void RemoveAllCompType(const char* compType);
		void RemoveAllCompType(const Rtti& compType);
		void RemoveAllCompName(const char* compName, const char* compType = NULL);

		// Remove all components
		void RemoveAllComp();

#pragma endregion

		// Remove the object
		void Destroy();

		GameObject * Clone();

		GameObject * CloneNoInitialize();
		// debug only!!
		std::vector<IComp*>& GetComps() { return mComps; }
		const std::vector<IComp*>& GetComps() const { return mComps; }

		std::string mSceneSpace;

		void operator=(GameObject& _go);
	protected:
		std::vector<IComp*> mComps;
		bool mbEnabled;
};

#ifndef FOR_EACH
#define FOR_EACH(itName, container)												\
		for(auto itName = (container).begin(); itName != (container).end(); ++itName)	

#ifndef FOR_REV_EACH
#define FOR_REV_EACH(itName, container)												\
for (auto itName = (container).rbegin(); itName != (container).rend(); ++itName)
#endif

#endif 