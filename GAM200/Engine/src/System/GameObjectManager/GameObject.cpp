#pragma once
#include "../../GameStateManager/GameStateManager.h"
#include "../../Level/Level.h"
#include "../../Components/System.h"
#include "../../Components/Base.h"
#include "../../Utilities/Containers.h"
#include "../../Utilities/Math/MyMath.h"
#include "../../Engine/Engine.h"
#include "../../Level/Editor/Editor.h"
#include "../../Level/Editor/ImGUI_Basics.h"
#include "../../Level/Editor/ImGui/imgui.h"
#include "../../Components/Components.h"
#include "../../Level/Scene/Scene.h"

#include "GameObject.h"

#include "../../Utilities/MyDebug/LeakDetection.h"

#include "../../EventSystem/TypeInfo.h"
#include "../../Graphics/RenderableComp.h"

using json = nlohmann::json;

GameObject::GameObject() : IBase()
{
	mRotation = 0;
	mbEnabled = false;
	mTag = GameObjects;
	//ObjMgr->AddObject(this);
}
GameObject::~GameObject()
{
	// Free the memory of the components
	RemoveAllComp();
}

// ----------------------------------------------------------------------------
#pragma region// STATE METHODS

void GameObject::SetEnabled(bool enabled) // Call Set Enabled on all components
{
	// call base method
	mbEnabled = enabled;

	// delegate to components
	FOR_EACH(it, mComps)
		(*it)->SetEnabled(enabled);
}
void GameObject::Initialize()
{	
	// Initialize all comps 
	FOR_EACH(it, mComps)
	{
		(*it)->Initialize();
	}
}

void GameObject::InitializeCameras()
{
	CameraComp* object_camera = get_component_type<CameraComp>();

	if (object_camera != nullptr)
		object_camera->Initialize();

}
void GameObject::Shutdown()
{
	// shutdown all comps 
	FOR_EACH(it, mComps)
		(*it)->Shutdown();
}

void GameObject::Move(Vector2& direction, float speed)
{
	Vector2 old_pos = mPosition;

	Vector2 oldGridPos(floorf(old_pos.x / GRID_SIZE), floorf(old_pos.y / GRID_SIZE));
	Vector2 oldKeyVec(oldGridPos.x * GRID_SIZE, oldGridPos.y * GRID_SIZE);

	int old_key = static_cast<int>(oldKeyVec.x * 623.19 + oldKeyVec.y);

	Vector2 newPos = mPosition += direction * speed;

	Vector2 newGridPos(floorf(newPos.x / GRID_SIZE), floorf(newPos.y / GRID_SIZE));
	Vector2 newKeyVec(newGridPos.x * GRID_SIZE, newGridPos.y * GRID_SIZE);

	int new_key = static_cast<int>(newKeyVec.x * 623.19 + newKeyVec.y);

	if (old_key == new_key)
		return;

	Space* spaceOwner = GetSpace();

	auto grid = spaceOwner->grid_;

	auto found = std::find(spaceOwner->grid_[old_key].first.begin(), spaceOwner->grid_[old_key].first.end(), this);
	
	if(found != spaceOwner->grid_[old_key].first.end())
		spaceOwner->grid_[old_key].first.erase(found);
	
	for (auto it : mComps)
	{
		auto test = type_of(*it);

		auto found = std::find(spaceOwner->grid_[old_key].second[type_of(*it)].begin(), spaceOwner->grid_[old_key].second[type_of(*it)].end(), it);
	
		if (found != spaceOwner->grid_[old_key].second[type_of(*it)].end())
		{
			spaceOwner->grid_[old_key].second[type_of(*it)].erase(found);

			if (spaceOwner->grid_[old_key].second[type_of(*it)].size() == 0)
			{
				spaceOwner->grid_[old_key].second.erase(type_of(*it));
			}
		}
	}

	spaceOwner->AddObjectToGrid(*this, new_key);
}

void GameObject::Move(Vector2& old_pos, Vector2& new_pos)
{
	Vector2 oldGridPos(floorf(old_pos.x / GRID_SIZE), floorf(old_pos.y / GRID_SIZE));
	Vector2 oldKeyVec(oldGridPos.x * GRID_SIZE, oldGridPos.y * GRID_SIZE);

	int old_key = static_cast<int>(oldKeyVec.x * 623.19 + oldKeyVec.y);

	Vector2 newGridPos(floorf(new_pos.x / GRID_SIZE), floorf(new_pos.y / GRID_SIZE));
	Vector2 newKeyVec(newGridPos.x * GRID_SIZE, newGridPos.y * GRID_SIZE);

	int new_key = static_cast<int>(newKeyVec.x * 623.19 + newKeyVec.y);

	if (old_key == new_key)
		return;

	Space* spaceOwner = GetSpace();

	auto grid = spaceOwner->grid_;

	auto found = std::find(spaceOwner->grid_[old_key].first.begin(), spaceOwner->grid_[old_key].first.end(), this);

	if (found != spaceOwner->grid_[old_key].first.end())
		spaceOwner->grid_[old_key].first.erase(found);

	for (auto it : mComps)
	{
		auto test = type_of(*it);

		auto found = std::find(spaceOwner->grid_[old_key].second[type_of(*it)].begin(), spaceOwner->grid_[old_key].second[type_of(*it)].end(), it);

		if (found != spaceOwner->grid_[old_key].second[type_of(*it)].end())
		{
			spaceOwner->grid_[old_key].second[type_of(*it)].erase(found);

			if (spaceOwner->grid_[old_key].second[type_of(*it)].size() == 0)
			{
				spaceOwner->grid_[old_key].second.erase(type_of(*it));
			}
		}
	}

	spaceOwner->AddObjectToGrid(*this, new_key);
}

#pragma endregion

// ----------------------------------------------------------------------------
//-----------------------------JSONN-------------------------------------------
void GameObject::ToJson(json& _j)
{
	
	//save position
	_j["mPosition"];
	_j["mPosition"]["x"] = mPosition.x;
	_j["mPosition"]["y"] = mPosition.y;

	//save rotation
	_j["mRotation"] = mRotation;
	//save scale
	_j["mScale"];
	_j["mScale"]["x"] = mScale.x;
	_j["mScale"]["y"] = mScale.y;
	//save the name
	_j["name"] = GetName();

	_j["Tag"] = mTag;

	_j["mSceneSpace"] = mSceneSpace;
	//save all components
	_j["mSortingPos"]["x"] = mSortingPos.x;
	_j["mSortingPos"]["y"] = mSortingPos.y;

	if (mbArchetype) {
		_j["mArchetypeName"] = mArchetypeName;
	}


	for (auto & c : mComps) {
		json j;
		c->ToJson(j);
		_j["Components"].push_back(j);
	}


	if (GetComp("Sprite")) {
		Sprite* s = dynamic_cast<Sprite*>(GetComp("Sprite"));

		if (s->GetTextureName() == "..//Resources//Textures//helecho doble.png") {
			mArchetypeName = "HelechoD";
			mbArchetype = true;
			Editor::archetypeObjects["HelechoD"].push_back(this);
		}

		if (s->GetTextureName() == "..//Resources//Textures//helecho 1.png") {
			mArchetypeName = "Helecho";
			mbArchetype = true;
			Editor::archetypeObjects["Helecho"].push_back(this);
		}
	}

}

void GameObject::ToJsonArchetype(nlohmann::json& _j) {

	_j["mPosition"]["x"] = mPosition.x;
	_j["mPosition"]["y"] = mPosition.y;

	_j["name"] = GetName();

	_j["Tag"] = mTag;

	_j["mSceneSpace"] = mSceneSpace;

	_j["mSortingPos"]["x"] = mSortingPos.x;
	_j["mSortingPos"]["y"] = mSortingPos.y;
}

void GameObject::FromJson(json& _j)
{

	//load position
	mPosition.x = _j["mPosition"]["x"];
	mPosition.y = _j["mPosition"]["y"];

	//save rotation
	mRotation = _j["mRotation"];
	//save scale
	mScale.x = _j["mScale"]["x"];
	mScale.y = _j["mScale"]["y"];
	//save the name
	json jname = _j["name"];
	SetName(jname.get<std::string>().c_str());

	if (_j.find("mSceneSpace") != _j.end()) {
		jname = _j["mSceneSpace"];
		mSceneSpace = jname.get<std::string>();
	}

	mbArchetype = false;

	if (_j.find("mArchetypeName") != _j.end()) {
		jname = _j["mArchetypeName"];
		mArchetypeName = jname.get<std::string>();
		mbArchetype = true;
	}
		
	//mSpaceName = _j["mSpaceName"].get<const char *>();
	mSortingPos.x = _j["mSortingPos"]["x"];
	mSortingPos.y = _j["mSortingPos"]["y"];

	mTag = _j["Tag"];

	if (_j.find("Components") != _j.end()){
		json & components = *_j.find("Components");
		for (auto it = components.begin(); it != components.end(); ++it){
			json & compVal = *it;
			std::string str = compVal["_type"].get<std::string>();
			IComp * c = factory->Create(str.c_str());
			c->mOwner = this;
			c->FromJson(compVal);
			AddCompNoInitialize(c);
		}
	}

	//if (GetSpace() != nullptr)
		//Initialize();
}

void GameObject::FromJsonArchetype(nlohmann::json& _j) {

	mPosition.x = _j["mPosition"]["x"];
	mPosition.y = _j["mPosition"]["y"];

	json jname = _j["name"];
	SetName(jname.get<std::string>().c_str());

	mTag = _j["Tag"];

	if (_j.find("mSceneSpace") != _j.end()) {
		jname = _j["mSceneSpace"];
		mSceneSpace = jname.get<std::string>();
	}

	mSortingPos.x = _j["mSortingPos"]["x"];
	mSortingPos.y = _j["mSortingPos"]["y"];
}

struct CompareTypes {
	CompareTypes() : comp(nullptr) {};

	CompareTypes(IComp * _comp) : comp(_comp) {};

	bool operator()(IComp const * _comp) {
		return comp->are_the_same_type(*_comp);
	}

	IComp * comp;
};

void GameObject::UpdateArchetype(GameObject& _go) {

	//set rotation
	mRotation = _go.mRotation;
	//set scale
	mScale = _go.mScale;

	for (unsigned i = 0; i < mComps.size(); i++) {
		std::vector<IComp *>::iterator it;
		if ((it = std::find_if(_go.mComps.begin(), _go.mComps.end(), CompareTypes(mComps[i]))) != _go.mComps.end()) {
			*mComps[i] = *(*it);
		}
	}


	//Can be optimized
	if (_go.mComps.size() < mComps.size()) {
		for (unsigned i = 0; i < mComps.size(); i++) {
			std::vector<IComp *>::iterator it;
			if ((it = std::find_if(_go.mComps.begin(), _go.mComps.end(), CompareTypes(mComps[i]))) == _go.mComps.end()) {
				RemoveComp(mComps[i]);
			}
		}
	}
	else if (_go.mComps.size() > mComps.size()) {
		for (unsigned i = 0; i < _go.mComps.size(); i++) {
			std::vector<IComp *>::iterator it;
			if ((it = std::find_if(mComps.begin(), mComps.end(), CompareTypes(_go.mComps[i]))) == mComps.end()) {
				AddComp(_go.mComps[i]->Clone());
				//mComps.back()->Initialize();
			}
		}
	}

}

void GameObject::operator=(GameObject& _go) {
	//set position
	mPosition = _go.mPosition;
	//set rotation
	mRotation = _go.mRotation;
	//set scale
	mScale= _go.mScale;

	for (unsigned i = 0; i < mComps.size(); i++) {
		std::vector<IComp *>::iterator it;
		if ((it = std::find_if(_go.mComps.begin(), _go.mComps.end(), CompareTypes(mComps[i]))) != _go.mComps.end()) {
			*mComps[i] = *(*it);
		}
	}


	//Can be optimized
	if (_go.mComps.size() < mComps.size()) {
		for (unsigned i = 0; i < mComps.size(); i++) {
			std::vector<IComp *>::iterator it;
			if ((it = std::find_if(_go.mComps.begin(), _go.mComps.end(), CompareTypes(mComps[i]))) == _go.mComps.end()) {
				RemoveComp(mComps[i]);
			}
		}
	} else if (_go.mComps.size() > mComps.size()) {
		for (unsigned i = 0; i < _go.mComps.size(); i++) {
			std::vector<IComp *>::iterator it;
			if ((it = std::find_if(mComps.begin(), mComps.end(), CompareTypes(_go.mComps[i]))) == mComps.end()) {
				AddComp(_go.mComps[i]->Clone());
				mComps.back()->Initialize();
			}
		}
	}
}


#pragma region// COMPONENT MANAGEMENT

// Find Component
u32 GameObject::GetCompCount() const
{
	return mComps.size();
}
IComp* GameObject::GetComp(u32 index) const
{
	if (index < GetCompCount())
		return mComps[index];
	return NULL;
}
IComp* GameObject::GetComp(const char* type) const
{
	// go throught the components and look for the same type
	for (auto it = mComps.begin(); it != mComps.end(); ++it)
	{
		std::string typeName = (*it)->GetType().GetName();
		if (typeName == type)
			return (*it);
	}
	return NULL;
}

IComp* GameObject::GetComp(const Rtti& type) const
{
	// go throught the components and look for the same type
	for (auto it = mComps.begin(); it != mComps.end(); ++it)
	{
		if ((*it)->GetType().IsExactly(type))
			return (*it);
	}
	return NULL;
}
IComp* GameObject::GetCompName(const char* compName, const char* compType) const
{
	for (auto it = mComps.begin(); it != mComps.end(); ++it)
	{
		// found a match with the name
		if (strcmp((*it)->GetName(), compName) == 0)
		{
			// not same type -> continue
			if (compType && strcmp(compType, (*it)->GetType().GetName()) != 0)
				continue;
			// same type or don't care about type -> return
			return (*it);
		}
	}
	return NULL;
}

// Add/Remove by address
IComp* GameObject::AddComp(IComp* pComp)
{
	if (pComp) {
		pComp->mOwner = this;
		for (IComp * comp : mComps) {
			if (comp->are_the_same_type(*pComp)) {
				return pComp;
			}
		}
		pComp->Initialize();
		mComps.push_back(pComp);
	}
	return pComp;
}

IComp* GameObject::AddCompNoInitialize(IComp* pComp) {
	if (pComp) {
		pComp->mOwner = this;
		for (IComp * comp : mComps) {
			if (comp->are_the_same_type(*pComp)) {
				return pComp;
			}
		}
		mComps.push_back(pComp);
	}
	return pComp;
}

void GameObject::RemoveComp(IComp* pComp)
{
	if (!pComp)
		return;
	//pComp->Shutdown(); //TODO: make sure that we indeed don't need that
	// NOTE this will create memory leaks.
	for (auto it = mComps.begin(); it != mComps.end(); ++it)
	{
		if ((*it) == pComp)
		{
			//pComp->mOwner = NULL;
			pComp->Shutdown();
			mComps.erase(it);
			delete pComp;
			return;
		}
	}
}

// Removes first component encoutered that match the search criteria
void GameObject::RemoveCompType(const char* compType)
{
	RemoveComp(GetComp(compType));
}
void GameObject::RemoveCompType(const Rtti& compType)
{
	RemoveComp(GetComp(compType));
}
void GameObject::RemoveCompName(const char* compName, const char* compType)
{
	RemoveComp(GetCompName(compName, compType));
}

// Removes all components encoutered that match the search criteria
void GameObject::RemoveAllCompType(const char* compType)
{
	IComp* pComp = GetComp(compType);
	while (pComp)
	{
		RemoveComp(pComp);
		pComp = GetComp(compType);
	}
}
void GameObject::RemoveAllCompType(const Rtti& compType)
{
	IComp* pComp = GetComp(compType);
	while (pComp)
	{
		RemoveComp(pComp);
		pComp = GetComp(compType);
	}
}
void GameObject::RemoveAllCompName(const char* compName, const char* compType)
{
	IComp* pComp = GetCompName(compName, compType);
	while (pComp)
	{
		RemoveComp(pComp);
		pComp = GetCompName(compName, compType);
	}
}

void GameObject::RemoveAllComp()
{
	while (mComps.size())
	{
		mComps.back()->Shutdown();
		auto dsadsa = RenderMgr->GetEditorRenderables();
		delete mComps.back();
		dsadsa = RenderMgr->GetEditorRenderables();
		mComps.back() = nullptr;
		mComps.pop_back();//Double call to destructor
		dsadsa = RenderMgr->GetEditorRenderables();
	}
}

void GameObject::Destroy()
{
	//ObjMgr->DestroyObject(this);
}

GameObject * GameObject::Clone() {
	GameObject * go = DBG_NEW GameObject(*this);
	go->mUID = go->mLastUID++;
	go->mComps.clear();
	for (unsigned i = 0; i < mComps.size(); i++) {
		go->AddCompNoInitialize(mComps[i]->Clone());
	}
	for (unsigned i = 0; i < mComps.size(); i++) {
		go->mComps[i]->Initialize();
	}
	//go->Initialize();
	go->mName = mName + "(Clone)";
	return go;
}

GameObject * GameObject::CloneNoInitialize() {
	GameObject * go = DBG_NEW GameObject(*this);
	go->mUID = go->mLastUID++;
	go->mComps.clear();
	for (unsigned i = 0; i < mComps.size(); i++) {
		go->AddCompNoInitialize(mComps[i]->Clone());
	}
	go->mName = mName + "(Clone)";
	return go;
}

template<class T>
T* GameObject::GetComp(const char* compName)
{
	// go throught the components and look for the same type
	for (auto it = mComps.begin(); it != mComps.end(); ++it)
	{
		if ((*it)->GetType().IsExactly(T::TYPE()))
		{
			// not same name -> continue
			if (compName && strcmp(compName, (*it)->GetName()) != 0)
				continue;

			// same name or don't care about the name
			// return
			return (T*)(*it);
		}
	}
	return NULL; // not found
}

template<class T>
T* GameObject::GetCompDerived(const char* compName)
{
	// go throught the components and look for the same type
	for (auto it = mComps.begin(); it != mComps.end(); ++it)
	{
		if ((*it)->GetType().IsDerived(T::TYPE()))
		{
			// not same name -> continue
			if (compName && strcmp(compName, (*it)->GetName()) != 0)
				continue;

			// same name or don't care about the name
			// return
			return (T*)(*it);
		}
	}
	return NULL; // not found
}

template<typename T>
T* GameObject::NewComp(const char* compName) {
	T* newComp = DBG_NEW T;
	if (compName)
		newComp->SetName(compName);
	auto added_comp = dynamic_cast<T*>(AddComp(newComp));
	added_comp->Initialize();
	return added_comp;
}

void GameObject::SetRegion(std::string _name) {
	mRegion = _name;
}

Space* GameObject::GetSpace()
{
	return GSM->level->demo_scene->FindSpace(mSceneSpace.c_str());
}

#pragma endregion
