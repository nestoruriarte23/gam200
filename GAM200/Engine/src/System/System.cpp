#include "System.h"
#include "..//Window/Window.h"
#include "..//GameStateManager/GameStateManager.h"
#include "..//Input/Input.h"
#include "..//Sound/Audio.h"
#include "..//Graphics/RenderManager.h"
#include "..//Physics/PhysicsSystem.h"
#include "..//Physics/CollisionSystem.h"
#include "..//../GamePlay/src/CameraLogic/CameraLogic.h"
#include "..//Level/Editor/Editor.h"
#include "..//Level/Scene/Scene.h"
#include "..//ResourceManager/ActualResourceManager.h"
#include "..//StateMachines/State.h"
#include "..//StateMachines/StateMachine.h"
#include "..//Level/Level.h"
#include "../../Settings.h"

#ifdef FAST_LOAD
#include "../ResourceManager/Importer/TextureImporter.h"
#endif // FAST_LOAD

bool SystemInit(Window* myWindow, const char * windowName, unsigned xSize, unsigned ySize, bool fullscreen)
{
	if(myWindow == NULL || myWindow->Init(windowName, xSize, ySize, fullscreen) == -1)
		return false;

	RenderMgr->Initialize();
	PhysMgr->Initialize();
	CollMgr->Initialize();
	gAudioMgr->Initialize();
	gAudioMgr->PlaySounds();
	factory->Initialize();
	gActualResourceMgr.Initialize();
	StMgr->Initialize();
	ParticleMgr->Initialize();
		
	return true;
}

void SystemExit(Window* myWindow, GameStateManager* gsm)
{
	if (myWindow)
	{
		SDL_GL_MakeCurrent(myWindow->GetSDLWindow(), NULL);
		SDL_GL_DeleteContext(myWindow->GetRenderContext());
	}

	delete gsm;
	gsm = nullptr;

	ParticleMgr->Shutdown();
	CameraMgr->ShutDown();
	PhysMgr->ShutDown();
	RenderMgr->Free();
	CollMgr->ShutDown();

	if (InputManager)
		delete InputManager;
	if (PhysMgr)
		delete PhysMgr;
	if (CollMgr)
		delete CollMgr;


	factory->ReleaseInstance();
	gActualResourceMgr.UnloadAllResources();
	gActualResourceMgr.ReleaseInstance();
	ParticleMgr->ReleaseInstance();
	StMgr->ReleaseInstance();
	TimeManager::ReleaseInstance();
	Camera::ReleaseInstance();

	gAudioMgr->StopAll();
	gAudioMgr->Shutdown();

	if (gAudioMgr)
		delete gAudioMgr;

}

void SystemGameLoop()
{
	while (GSM->isRunning)
	{
		if (GSM->currentLevel != RESTART)
		{
			GSM->SetLevel(GSM->currentLevel);
			GSM->Load();
		}

		GSM->Init();

		if(Editor::LevelName == "LevelWithSpaces")
			CameraMgr->Initialize();

		#ifdef FAST_LOAD
		MultiLoader->UploadReadyTexturesToGPU();
		#endif // FAST_LOAD

		RenderMgr->GetEditorRenderables();

		while (GSM->currentLevel == GSM->nextLevel)
		{
			InputManager->StartFrame();

			TimeManager::Instance()->StartFrame();

			if(!Game->onTitle)
				GSM->HandleEvents();		// Include input processing
			
			//get objects

			if (!Game->onEditor)
			{
				if (Editor::LevelName == "LevelWithSpaces")
					CameraMgr->Update();

				if(!GSM->isPaused())
				{
					PhysMgr->Update();
					//CollMgr->Update(GSM->level->demo_scene->arenaObj, GSM->level->demo_scene->GetObjInScreen());
					StMgr->Update();
					LogicManager::Instance()->Update();
				}
			}

			CollMgr->Update();

			RenderMgr->Update();
			gAudioMgr->Update();

			if (!GSM->isPaused())
			{
				RenderMgr->UpdateRenderables();
				ParticleMgr->Update(TimeManager::Instance()->GetDt());
			}

			GSM->Update();

			if (GSM->nextLevel != QUIT_GAME)
				GSM->Render();

			GSM->level->demo_scene->ClearLists();

			GSM->level->demo_scene->FreeDestroyedObjects();

			Game->mCurrentFrame++;
		}

		GSM->Free();

		EventDispatcher::get_instance().clear();

		if (GSM->nextLevel != RESTART)
			GSM->Unload();

		GSM->currentLevel = GSM->nextLevel;
	}

}

void SystemHideConsole(bool hide)
{
	//SDL_Show
}

void SystemHideCursor(bool hide)
{
	if(hide)
		SDL_ShowCursor(SDL_DISABLE);
	else
		SDL_ShowCursor(SDL_ENABLE);
}
