//#include <iostream>
//#include <string.h>
//#include "ResourceManager.h"
//#include "..\ResourceManager\ResourceManager.h"
//#include "../../Graphics/RenderManager.h"
//
///* BIG TODO: Destroy this and change to the actual resource manager, 
//at this point this cpp is probably already unused but further 
//testing is required */
//
//ResourceManager::ResourceManager() {}
//
//TexturePtr ResourceManager::LoadTexture(std::string mFileName)
//{
//	/* Sanity check for a valid name */
////	if (mFileName == NULL) return mTextureHandlers.begin()++;
//	// TODO, Maybe set up a dummy texture for NULL names so the 
//	// program does not crash && Sanity check itself
//
//	/* Sanity check if the texture is already loaded */
//	try { mTextures.at(mFileName); }
//
//	/* If not (throws out_of_range error) load the texture */
//	catch (const std::out_of_range&)
//	{
//
//		/* Create the int holders for stb_image.h */
//		int width, height, ColorChannels;
//
//		/* Flip Vertically if necessary to match OpenGL coord system */
//		stbi_set_flip_vertically_on_load(true);
//
//		/* Load the data, also get the properties of the image */
//		unsigned char *TexData = stbi_load(mFileName.c_str(), 
//			&width, &height, &ColorChannels, 0);
//		if (TexData == nullptr)//Error check
//		{
//			std::cout << "Error when loading texture from file: " + mFileName << std::endl;
//		}
//
//		// hard break
//		if (!TexData)__debugbreak();
//
//		/* Create the OpenGL handler */
//		GLuint texture;
//		glGenTextures(1, &texture);
//		check_gl_error();
//
//		/* Add the texture handler to the resource manager */
//		mTextureHandlers.push_front(texture);
//		mTextures.insert(std::make_pair(mFileName, mTextureHandlers.begin()++));
//
//		/* Bind said texture handler so the next texture commands will act on it */
//		glBindTexture(GL_TEXTURE_2D, texture);
//		check_gl_error();
//
//		/* Set texture look-up parameters */
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//		check_gl_error();
//
//		/* Load the tex data onto the OpenGL handler */
//		glTexImage2D(GL_TEXTURE_2D, 0, ColorChannels == 3 ? GL_RGB : GL_RGBA, width, height, 0, ColorChannels == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, TexData);
//		//glGenerateMipmap(GL_TEXTURE_2D);
//		check_gl_error();
//
//		/* Free the data */
//		stbi_image_free(TexData);
//	}
//
//	/* Save the name */
//	RenderManager::Instance()->SaveTextureName(mFileName);
//
//	/* Return the pointer to the GLuint that holds the texture */
//	return mTextureHandlers.begin()++;
//}
//
//TexturePtr ResourceManager::GetTextureID(std::string mFileName)
//{
//	/* TODO: Dummy Texture && Sanity check */
//	return mTextures[mFileName];
//}
//
//TexturePtr ResourceManager::GetTextureID(const char* mFileName)
//{
//	std::string str(mFileName);
//
//	/* TODO: Dummy Texture && Sanity check */
//	return mTextures[str];
//}