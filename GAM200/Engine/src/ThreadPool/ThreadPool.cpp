#include "ThreadPool.h"

#ifdef FAST_LOAD // Trying to multithread loading //

ThreadPool::ThreadPool() : stop{ false }
{
	for (int i = std::thread::hardware_concurrency() - 1; i > 0; --i)
	{
		workers.push_back(new Worker(*this));
		actual_threads.push_back(std::thread(*workers.back()));
	}
}

void Worker::operator()()
{
	std::function<void()> task;
	while (true)
	{
		{   // acquire lock
			std::unique_lock<std::mutex>
				lock(pool.queue_mutex);

			working = false;	// Need to know if a task is being executed

			// look for a work item
			while (!pool.stop && pool.tasks.empty())
			{ // if there are none wait for notification
				pool.condition.wait(lock);
			}

			if (pool.stop) // exit if the pool is stopped
				return;

			// get the task from the queue
			task = pool.tasks.front();
			pool.tasks.pop_front();

			working = true;
		}   // release lock

		// execute the task
		task();
	}
}

#endif // FAST_LOAD