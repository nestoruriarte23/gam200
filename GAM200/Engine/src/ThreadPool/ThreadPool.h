#pragma once

#include "../../Settings.h"

#ifdef FAST_LOAD

#include <thread>
#include <mutex>
#include "../Utilities/Math/MyMath.h"
#include "../Serializer/SerializeFactory.h"

class ThreadPool;

class Worker
{
public:
	Worker(ThreadPool& _pool) : pool(_pool) { }
	void operator()();

	bool ImWorking() const { return working; }

private:
	ThreadPool& pool;
	bool working{ false };
};

class ThreadPool
{
	MAKE_SINGLETON(ThreadPool)

public:

	~ThreadPool()
	{
		// stop all threads
		stop = true;
		condition.notify_all();

		// join them
		for (size_t i = 0; i < actual_threads.size(); ++i)
			actual_threads[i].join();

		for (Worker* worker : workers)
		{
			delete worker;
		}
		workers.clear();
	}

	template<class FUNCTION>
	void enqueue(FUNCTION _func)
	{
		{ // acquire lock
			std::unique_lock<std::mutex> lock(queue_mutex);

			// add the task
			tasks.push_back(std::function<void()>(_func));
		} // release lock

		// wake up one thread
		condition.notify_one();
	}

	std::list< std::function<void()> > tasks;
private:
	friend class Worker;

	std::vector< std::thread >	actual_threads;
	std::vector< Worker* >		workers;

	std::condition_variable condition;
	std::mutex queue_mutex;
	bool stop;
};

#endif // FAST_LOAD