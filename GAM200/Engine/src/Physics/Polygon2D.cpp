#include "../Utilities/Math/MyMath.h"
#include "Polygon2D.h"


//void Polygon2D::Draw(u32 color, Matrix33 * vtx_transform) const
//{
//	// can't draw polygon if we don't have at least
//	// 2 vertices
//	if (mVertices.size() <= 1)
//		return;
//
//	// set transform if necessary transform 
//	if (vtx_transform != NULL) {
//		Matrix33 mat = *vtx_transform;
//		AEGfxSetransform(&mat);
//	}
//
//	// Draw line between each vertices
//	for (u32 i = 0; i < mVertices.size() - 1; ++i)
//	{
//		auto & v1 = mVertices[i];
//		auto & v2 = mVertices[i + 1];
//		AEGfxLine(v1.x, v1.y, 0, color, v2.x, v2.y, 0, color);
//	}
//	// Draw last line from last to first vertex
//	auto & v1 = mVertices[0];
//	auto & v2 = mVertices[mVertices.size() - 1];
//	AEGfxLine(v1.x, v1.y, 0, color, v2.x, v2.y, 0, color);
//
//	// force draw on graphics system
//	AEGfxFlush();
//}

Polygon2D::Polygon2D(Transform2D* box)
{
	Vector2 vecs[4];
	Matrix33 rotMatrix = Matrix33::RotRad(box->mOrientation);

	vecs[0] = Vector2(box->mPosition.x + box->mScale.x / 2, box->mPosition.y + box->mScale.y / 2);
	vecs[1] = Vector2(box->mPosition.x + box->mScale.x / 2, box->mPosition.y - box->mScale.y / 2);
	vecs[2] = Vector2(box->mPosition.x - box->mScale.x / 2, box->mPosition.y + box->mScale.y / 2);
	vecs[3] = Vector2(box->mPosition.x - box->mScale.x / 2, box->mPosition.y - box->mScale.y / 2);

	AddVertex(rotMatrix*vecs[0]);
	AddVertex(rotMatrix*vecs[2]);
	AddVertex(rotMatrix*vecs[3]);
	AddVertex(rotMatrix*vecs[1]);
}
Polygon2D Polygon2D::MakeQuad()
{
	// result polygon
	Polygon2D res(4);

	// create vertices
	res[0] = { -0.5f, 0.5f };
	res[1] = { -0.5f, -0.5f };
	res[2] = { 0.5f, -0.5f };
	res[3] = { 0.5f, 0.5f };

	return res;
}

Polygon2D Polygon2D::MakePentagon()
{
	return MakeStandardPoly(5);
}

Polygon2D Polygon2D::MakeHexagon()
{
	return MakeStandardPoly(6);
}

Polygon2D Polygon2D::MakeSeptagon()
{
	return MakeStandardPoly(7);
}

Polygon2D Polygon2D::MakeOctagon()
{
	return MakeStandardPoly(8);
}

Polygon2D Polygon2D::MakeStandardPoly(u32 side)
{
	Polygon2D res(side);
	res[0] = { 0.5f, 0.0f };
	f32 alpha = (2.0f*PI) / (f32)side;
	for (u32 i = 1; i < side; ++i) {

		// set current point to previous
		// before rotation
		res[i] = res[i - 1];
		
		// compute rotation vars
		f32 tmp = res[i].x;
		f32 cA = cosf(alpha);
		f32 sA = sinf(alpha);

		// apply rotation to get _new point
		res[i].x = cA * res[i].x - sA * res[i].y;
		res[i].y = sA * tmp + cA * res[i].y;
	}
	return res;
}

Polygon2D::Polygon2D() {
	// Do nothing
}

Polygon2D::Polygon2D(u32 size)
{
	// Call SetSize method
	SetSize(size);
}

void Polygon2D::AddVertex(const Vector2 & vtx)
{
	// Add the _new vertex
	mVertices.push_back(vtx);
}

Vector2 & Polygon2D::operator[](u32 idx) 
{ 
	// Return the vertex at the specified position
	return mVertices[idx]; 
}

void Polygon2D::Clear()
{
	// Clear mVertices
	mVertices.clear();
}

u32 Polygon2D::GetSize() const
{ 
	// Get the size of the vector using size method
	return mVertices.size(); 
}

void Polygon2D::SetSize(u32 size)
{
	// Resize the vector using the method designed for it
	mVertices.resize(size);
}

std::vector<Vector2> Polygon2D::GetTransformedVertices(const Matrix33 &mat_transform) const
{
	// Create a vector with all the vertices multiplied
	std::vector<Vector2> newVertices;

	// Fill the _new vector with all the vertcies
	for (Vector2 i : mVertices)
	{
		// Add each vertix multiplied by the matrix
		newVertices.push_back(mat_transform * i);
	}

	// Return mVertices (it will create a copy)
	return newVertices; 
}
