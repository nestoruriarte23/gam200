#include "Collisions.h"

bool StaticPointToStaticCircle(Vector2 *P, Vector2 *Center, float Radius)
{
	// Check if the distance from the center of the circle to the point 
	// (squared) is bigger than the radius (squared) or not
	if ((*P - *Center).LengthSq() < Radius * Radius)
		// If radius is bigger (or equal), it is colliding
		return true;
	else
		// Otherwise, it is not
		return false;
}

bool StaticPointToStaticEllpise(const Vector2& _point, const Vector2& _ecenter, float _xaxis, float _yaxis, float* _intersection)
{
	auto _xaxissqr = _xaxis * _xaxis;
	auto _yaxissqr = _yaxis * _yaxis;
	auto _xdiffsqr = (_point.x - _ecenter.x)*(_point.x - _ecenter.x);
	auto _ydiffsqr = (_point.y - _ecenter.y)*(_point.y - _ecenter.y);

	*_intersection = (_xdiffsqr * _yaxissqr) + (_ydiffsqr * _xaxissqr);
	*_intersection /= (_xaxissqr*_yaxissqr);

	return *_intersection < 1;
}

bool StaticPointToStaticRect(Vector2 *Pos, Vector2 *Rect, float Width, float Height)
{
	// Check if the given point is between the boundaries (for all 4 sides)

	// To the right of the left-most side and...
	if (Pos->x > Rect->x - Width / 2 &&
		// To the left of the right-most side and...
		Pos->x < Rect->x + Width / 2 &&
		// Higher than the bottom of the AABB and...
		Pos->y > Rect->y - Height / 2 &&
		// Lower than the top of the AABB
		Pos->y < Rect->y + Height / 2)
	{
		// If all conditions are met, return "Colliding" (=true)
		return true;
	}
	else
		// If any condition fail, it is not colliding
		return false;
}

bool StaticPointToOrientedRect(const Vector2 *Pos, Vector2 *Rect, float Width, float Height, float AngleRad)
{
	// Translate the point, respect to the rectangle
	Vector2 PointTranslated = (*Pos) - *Rect;

	// Undo the rotation the OBB has, and apply it to the point
	Vector2 PointRotated = Matrix33::RotRad(-AngleRad + PI / 2).MultPoint(PointTranslated);

	// Then just use the AABB vs Point Test, with the _new point 
	// and the rectangle located at (0,0)
	return StaticPointToStaticRect(&PointRotated, &Vector2(0, 0), Height, Width);
}

bool StaticCircleToStaticCircle(Vector2 *Center0, float Radius0, Vector2 *Center1, float Radius1)
{
	// Apply the "growing method" , this means creating a bigger circle 
	// that its radius is the sum of the original two, and just use the 
	// Point vs Circle test
	return StaticPointToStaticCircle(Center0, Center1, Radius0 + Radius1);
}

bool StaticCircleToStaticCircle(const Vector3& Center0, float Radius0, const Vector3& Center1, float Radius1)
{
	// Apply the "growing method" , this means creating a bigger circle 
	// that its radius is the sum of the original two, and just use the 
	// Point vs Circle test

	Vector2 center_diff(Center0.x - Center1.x, Center0.y - Center1.y);
	float add = Radius0 + Radius1;

	return center_diff.LengthSq() < (add * add);
}

bool StaticRectToStaticRect(Vector2 *Rect0, float Width0,
	float Height0, Vector2 *Rect1, float Width1, float Height1)
{
	// Once again, we can apply the "growth" method and then just use the
	// AABB vs Point test. There are other methods, but this one is really 
	// fast and easy to use.
	return StaticPointToStaticRect(Rect0, Rect1, Width0 + Width1, Height0 + Height1);
}

bool StaticRectToStaticCirlce(Vector2 * Rect, float Width,
	float Height, Vector2* Center, float Radius)
{
	/* We take the idea of finding the closest point from the AABB to the
	circle. And, once found, just apply the Point vs Circle test. To find
	that closest point, we just clump it, as done below. (or just use AEClump)*/

	// Get the a copy of the center of the circle, now we are going to modify it
	Vector2 rectClosestPoint = *Center;

	// Clump x - axis

	// If the circle is to the right of the AABB, we set the center.x to be
	// the x-coord of the right-most side
	if (rectClosestPoint.x < Rect->x - Width / 2)
		rectClosestPoint.x = Rect->x - Width / 2;

	// Else if the circle is to the left of the AABB, we set the center.x 
	// to be the x-coord of the left-most side
	else if (rectClosestPoint.x > Rect->x + Width / 2)
		rectClosestPoint.x = Rect->x + Width / 2;

	// Otherwise, the circle is in between the right and left side. So we 
	// don't need to change the value we had in any way. (We shoudn't either)



	// Clump y - axis

	// We can apply the exact same idea to the y axis
	if (rectClosestPoint.y < Rect->y - Height / 2)
		rectClosestPoint.y = Rect->y - Height / 2;
	else if (rectClosestPoint.y > Rect->y + Height / 2)
		rectClosestPoint.y = Rect->y + Height / 2;

	// Now we can just apply the Point vs Circle test, as we stated before
	return StaticPointToStaticCircle(&rectClosestPoint, Center, Radius);
}

bool OrientedRectToStaticCirlce(Vector2 * Rect, float Width,
	float Height, float AngleRad, Vector2* Center, float Radius)
{
	// We compute the position of the circle respect to OBB, taking in account 
	// the translation and rotation of the square (as we also did in the OBB vs Point test)
	Vector2 CircleForRectPos = Matrix33::RotRad(-AngleRad).MultPoint((*Center) - *Rect);

	// Then we can just apply the StaticRect (AABB) to Circle test
	return StaticRectToStaticCirlce(&CircleForRectPos, Width, Height, &Vector2(0, 0), Radius);
}

bool StaticCircleToStaticCircleEx(Vector2 * Center1, float Radius1, Vector2 * Center2, float Radius2, Contact * pResult)
{
	// Use the other test (from prevoius assignment) to check for overlap
	if (StaticCircleToStaticCircle(Center1, Radius1, Center2, Radius2))
	{
		// If it is colliding

		// Check for valid pointer to store contact info
		if (pResult != NULL)
		{
			// Compute the normal from Circle1 to Circle2 (do not normalize yet)
			// First can compute penetration
			pResult->mNormal = *Center2 - *Center1;

			// Penetration is the difference between the sum of radius and real distance
			pResult->mPenetration = Radius1 + Radius2 - pResult->mNormal.Length();

			// NOw, normalize the normal
			pResult->mNormal.NormalizeThis();

			// Point of intersection, computed through the vector that joins the centers
			pResult->mPi = *Center1 + pResult->mNormal * Radius1;
		}

		// Either the pointer is valid or not, there is collision
		return true;
	}
	else
		// Otherwise, it is not
		return false;
}

bool StaticRectToStaticCircleEx(Vector2 * Rect, float Width, float Height, Vector2 * Center, float Radius, Contact * pResult)
{
	/* We take the idea of finding the closest point from the AABB to the
	circle. And, once found, just apply the Point vs Circle test. To find
	that closest point, we just clump it, as done below. (or just use AEClump)*/

	// Get the a copy of the center of the circle, now we are going to modify it
	Vector2 rectClosestPoint = *Center;

	// Clump x - axis

	// If the circle is to the right of the AABB, we set the center.x to be
	// the x-coord of the right-most side
	if (rectClosestPoint.x < Rect->x - Width / 2)
		rectClosestPoint.x = Rect->x - Width / 2;

	// Else if the circle is to the left of the AABB, we set the center.x 
	// to be the x-coord of the left-most side
	else if (rectClosestPoint.x > Rect->x + Width / 2)
		rectClosestPoint.x = Rect->x + Width / 2;

	// Otherwise, the circle is in between the right and left side. So we 
	// don't need to change the value we had in any way. (We shoudn't either)

	// Clump y - axis

	// We can apply the exact same idea to the y axis
	if (rectClosestPoint.y < Rect->y - Height / 2)
		rectClosestPoint.y = Rect->y - Height / 2;
	else if (rectClosestPoint.y > Rect->y + Height / 2)
		rectClosestPoint.y = Rect->y + Height / 2;

	// Use the other test (from prevoius assignment) to check for overlap
	if (StaticPointToStaticCircle(&rectClosestPoint, Center, Radius))
	{
		// If it is colliding

		// Check for valid pointer to store contact info
		if (pResult != NULL)
		{
			if (Center->x != rectClosestPoint.x || Center->y != rectClosestPoint.y)
			{
				// Compute the normal from Circle1 to Circle2 (do not normalize yet)
				// First can compute penetration
				pResult->mNormal = *Center - rectClosestPoint;

				// Penetration is the difference between the sum of radius and real distance
				pResult->mPenetration = Radius - pResult->mNormal.Length();

				// NOw, normalize the normal
				pResult->mNormal.NormalizeThis();

				// Point of intersection, computed through the vector that joins the centers
				pResult->mPi = rectClosestPoint;
			}

			// If the clump results in the original point, then the circe
			else
			{
				// Calculate the distance from center of rect to center of circle
				Vector2 CenterDistances(*Center - *Rect);

				// Calculate the x penetration and y penetration, using the centerDistance 
				// and dimensions of the AABB
				f32 xPen = (Width / 2 - abs(CenterDistances.x));
				f32 yPen = (Height / 2 - abs(CenterDistances.y));

				// Depending on which is the minimum penetration... (it is never a diagonal)
				if (xPen < yPen)
				{
					// If it is the x, Normal direction is set by centerDistances x 
					// (Either positive or negative in x)
					pResult->mNormal = Vector2(CenterDistances.x, 0).Normalize();

					// Penetration is just the xPen and the radius
					pResult->mPenetration = Vector2(Radius + xPen, 0).Length();
				}
				else
				{
					// If it is the y, Normal direction is set by centerDistances.y 
					// (Either positive or negative in y)
					pResult->mNormal = Vector2(0, CenterDistances.y).Normalize();

					// Penetration is just the yPen and the radius
					pResult->mPenetration = Vector2(0, Radius + yPen).Length();
				}

				// Set the point of intersection to be the center of the circle
				pResult->mPi = *Center;
			}
		}

		// Either the pointer is valid or not, there is collision
		return true;
	}
	else
		// Otherwise, it is not
		return false;
}

bool StaticOBBToStaticCircleEx(Transform2D * OBB, Vector2 * Center, float Radius, Contact * pResult)
{
	// We compute the position of the circle respect to OBB, taking in account 
	// the translation and rotation of the square (as we also did in the OBB vs Point test)
	Vector2 CircleForRectPos = Matrix33::RotRad(-OBB->mOrientation).MultPoint((*Center) - OBB->mPosition);

	// Then we can just apply the StaticRect (AABB) to Circle test, and get the result
	// info into pResult (No need to check if pResult NULL at this point since
	// it will be done at RectVsCircleEx). Save the info of Collided or not at "Collided"
	bool Collided = StaticRectToStaticCircleEx(&Vector2(0, 0), OBB->mScale.x, OBB->mScale.y,
		&CircleForRectPos, Radius, pResult);

	// Sanity Check for pResult
	if (pResult != NULL)
	{
		// We have to modify the results from pResult since those are from an AABB
		// centered at 0,0

		// Undo the Rotation of the normal
		pResult->mNormal = Matrix33::RotRad(OBB->mOrientation) *  pResult->mNormal;

		// Undo the translation of the Point of intersection
		pResult->mPi = Matrix33::RotRad(OBB->mOrientation).MultVec(pResult->mPi)
			+ OBB->mPosition;
	}

	// Return the value calculated previously
	return Collided;
}

bool StaticRectToStaticRectEx(Vector2 *pos1, Vector2 *size1, Vector2 *pos2, Vector2 *size2, Contact * pResult)
{
	// We will check for Non - Separating axises (there can only be two since
	// we are working with AABB vs AABB. Either x-axis or y-axis)

	// First we get the distance of the centers from the 2 figures
	Vector2 CenterDistance = *pos2 - *pos1;

	// Check for x-axis and y-axis separations.
	f32 xAxisSeparation = size1->x / 2 + size2->x / 2 - abs(CenterDistance.x);
	f32 yAxisSeparation = 0;

	// A negative xAxis separation means no-collition since it would mean that distance
	// from two centers is bigger than the sum of the sizes of the sides, and so we can 
	// ensure they are not colliding. And so, we only check y-axis if positive separation.
	if (xAxisSeparation < 0)
		return false;
	else
	{
		// Calculate ySeparation
		yAxisSeparation = size1->y / 2 + size2->y / 2 - abs(CenterDistance.y);

		// Same logic as before, negative separation means no collition
		if (yAxisSeparation < 0)
			return false;
	}

	// Once we reach this point, if we have not return false yet, there is collition for 
	// sure. We will return true at the end.

	// Check for a valid pResult address
	if (pResult != NULL)
	{
		// Magnitude would be the the minimum between the xSeparation and ySeparation
		if (xAxisSeparation < yAxisSeparation)
		{
			// Set the penetration
			pResult->mPenetration = xAxisSeparation;

			// The normal, accordingly, will be horizontal
			pResult->mNormal = Vector2(1, 0);

			// Whether it is in the positive direction or negative, will depend on the
			// relative position of the figures
			if (pos1->x > pos2->x)
				pResult->mNormal *= -1;
		}
		else
		{
			// Set the penetration
			pResult->mPenetration = yAxisSeparation;

			// The normal, accordingly, will be vertical
			pResult->mNormal = Vector2(0, 1);

			// Whether it is in the positive direction or negative, will depend on the
			// relative position of the figures
			if (pos1->y > pos2->y)
				pResult->mNormal *= -1;
		}

		// First of all, calculate the point of intersection, it will be necessary
		// for calculating the normal and its magnitude

		// Compute all the positions for the corners of AABB 2
		Vector2 box2Corners[4] = {
			Vector2(pos2->x - size2->x / 2, pos2->y + size2->y / 2),
			Vector2(pos2->x - size2->x / 2, pos2->y - size2->y / 2),
			Vector2(pos2->x + size2->x / 2, pos2->y - size2->y / 2),
			Vector2(pos2->x + size2->x / 2, pos2->y + size2->y / 2)
		};

		// For each corner (4) in box 2
		for  (Vector2 i : box2Corners)
		{
			// Check if contained in AABB 1.
			if (StaticPointToStaticRect(&i, pos1, size1->x, size1->y))
			{
				// If so, that is the point of intersection
				pResult->mPi = i;

				// No need to search any more
				return true;
			}
		}

		// At this point we know that both figures are colliding, but not a single
		// corner of AABB 2 is inside of the first one. so We try the other way around

		// Compute all the positions for the corners of AABB 1
		Vector2 box1Corners[4] = {
			Vector2(pos1->x - size1->x / 2, pos1->y + size1->y / 2),
			Vector2(pos1->x - size1->x / 2, pos1->y - size1->y / 2),
			Vector2(pos1->x + size1->x / 2, pos1->y - size1->y / 2),
			Vector2(pos1->x + size1->x / 2, pos1->y + size1->y / 2)
		};

		// For each corner (4) in box 1
		for (Vector2 i : box1Corners)
		{
			// Check if contained in AABB 1.
			if (StaticPointToStaticRect(&i, pos2, size2->x, size2->y))
			{
				// If so, that is the point of intersection
				pResult->mPi = i;

				// No need to search any more
				return true;
			}
		}

		// At this point, no corner of any of the figures is inside the other figure
		// But we know there is a collition, so we just set the first corner (top - 
		// right of AABB 1) as the default point of intersection.
		pResult->mPi = box1Corners[0];

	}
	// Whatever the contact info is, we know there is a collition, return true. This 
	// part of the code should never be reached, but just in case, to ensure there is
	// a correct bool return for collition.
	return true;
}

bool OrientedRectToOrientedRectEx(Transform2D * OBB1, Transform2D * OBB2, Contact * pResult)
{
	// We are going to apply SAT (Separatin axis theorem). It states that if we
	// can find an axis where two figures are clearly sparated, therefore, they are
	// not colliding. Otherwise, we need to keep checking until we find an axis or 
	// there no more axises to check. In that case, we can safely state that the
	// two figures are not colliding.

	// Just in case, empty pResults->mPenetration, as we will be using it later for
	// some checks
	if (pResult != NULL)
		pResult->mPenetration = 0.0f;

	// First we get the distance of the centers from the 2 figures
	Vector2 CenterDistance = OBB1->mPosition - OBB2->mPosition;

	// We compute all the normals that we are going to need, in this case, just
	// 4 normals. Two of each figure (because of simetry).
	Vector2 OBBnormalsNormalized[4] = {
		// First OBB
		Vector2(cos(OBB1->mOrientation), sin(OBB1->mOrientation)),  // normal[0]
		OBBnormalsNormalized[0].Perp(),								// normal[1]

																	// Second OBB
																	Vector2(cos(OBB2->mOrientation), sin(OBB2->mOrientation)),	// normal[2]
																	OBBnormalsNormalized[2].Perp(),								// normal[3]
	};

	// We compute all the normals that we are going to need, in this case, just
	// 4 normals. Two of each figure (because of simetry).
	Vector2 OBBNormalsScaled[4] = {
		// First OBBs
		OBBnormalsNormalized[0] * OBB1->mScale.x / 2,
		OBBnormalsNormalized[1] * OBB1->mScale.y / 2,

		// Second OBBs
		OBBnormalsNormalized[2] * OBB2->mScale.x / 2,
		OBBnormalsNormalized[3] * OBB2->mScale.y / 2,
	};

	// Compute the following with every normal of the array until a separating
	// axis found.
	for (int i = 0; i < 4; ++i)
	{
		// Get the square length of the Distance between centers projected
		float ProjCenterDistance = (CenterDistance.Project(OBBnormalsNormalized[i])).Length();

		// Add up the projections of the segments //

		// Create the variable
		float SegmentProjectionTotal = 0.0f;

		// Add all the projections
		for (Vector2 j : OBBNormalsScaled)
		{
			// Projection of the normal scaled onto the nornalized vector
			SegmentProjectionTotal += (j.Project(OBBnormalsNormalized[i])).Length();
		}

		// After all the math, we analyze the results //

		// If distance of centers is greater than the addition of segments, there
		// is certainly not collition. 
		f32 DistanceDifference = SegmentProjectionTotal - ProjCenterDistance;

		// Therefore, if the difference is smaller than 0, We can return false.
		if (DistanceDifference <= 0)
			return false;

		// Otherwise there could still be collition.
		else
		{
			// We are looking for the minimum penetration, so we check if we have 
			// already an smaller Penetration (or no penetration yet)
			if (pResult != NULL && (pResult->mPenetration > DistanceDifference ||
				pResult->mPenetration == 0.0f))
			{
				// If this one is smaller, set it as the penetration
				pResult->mPenetration = DistanceDifference;

				// Also set the normal being used as the one for the intersecion (it will recieve
				// some changes on the sign depending on some factors later explained)
				pResult->mNormal = OBBnormalsNormalized[i];

				// Check the relative positio of the two OBBs using the dot product
				// If negative, we will need to change the normal to match reality
				if (OBBnormalsNormalized[i].Dot(OBB2->mPosition - OBB1->mPosition) < 0)
					pResult->mNormal *= -1;
			}
		}
	}

	// If the code reaches this point, there is collition for sure as all normals
	// have already been tested. And we will return true at the end, after calculating
	// the intersection point.

	// Sanity check
	if (pResult != NULL)
	{
		// Compute all the positions for the corners of AABB 2
		Vector2 box2Corners[4] = {
			Vector2(OBB2->mPosition + OBBNormalsScaled[2] + OBBNormalsScaled[3]),
			Vector2(OBB2->mPosition - OBBNormalsScaled[2] + OBBNormalsScaled[3]),
			Vector2(OBB2->mPosition - OBBNormalsScaled[2] - OBBNormalsScaled[3]),
			Vector2(OBB2->mPosition + OBBNormalsScaled[2] - OBBNormalsScaled[3])
		};

		// For each corner (4) in box 2
		for (unsigned i = 0; i < 4; ++i)
		{
			// Check if contained in AABB 1.
			if (StaticPointToOrientedRect(&box2Corners[i], &OBB1->mPosition, OBB1->mScale.x,
				OBB1->mScale.y, OBB1->mOrientation))
			{
				// If so, that is the point of intersection
				pResult->mPi = box2Corners[i];

				// No need to search any more
				return true;
			}
		}

		// Compute all the positions for the corners of AABB 2
		Vector2 box1Corners[4] = {
			Vector2(OBB1->mPosition + OBBNormalsScaled[0] + OBBNormalsScaled[1]),
			Vector2(OBB1->mPosition - OBBNormalsScaled[0] + OBBNormalsScaled[1]),
			Vector2(OBB1->mPosition - OBBNormalsScaled[0] - OBBNormalsScaled[1]),
			Vector2(OBB1->mPosition + OBBNormalsScaled[0] - OBBNormalsScaled[1])
		};

		// For each corner (4) in box 1
		for (unsigned i = 0; i < 4; ++i)
		{
			// Check if contained in AABB 1.
			if (StaticPointToOrientedRect(&box1Corners[i], &OBB2->mPosition, OBB2->mScale.x,
				OBB2->mScale.y, OBB2->mOrientation))
			{
				// If so, that is the point of intersection
				pResult->mPi = box1Corners[i];

				// No need to search any more
				return true;
			}
		}

		// At this point, no corner of any of the figures is inside the other figure
		// But we know there is a collition, so we just set the first corner (top - 
		// right of AABB 1) as the default point of intersection.
		pResult->mPi = box1Corners[0];
	}

	// We can just state that collition is true at this point as explained before.
	return true;
}

bool PolygonToPolygon(Polygon2D * p1, Transform2D * tr1, Polygon2D * p2,
	Transform2D * tr2, Contact * pResult)
{
	// Just in case, empty pResults->mPenetration, as we will be using it later for
	// some checks
	if (pResult != NULL)
		pResult->mPenetration = 0.0f;




	// Compute the normals and store them into a vector
	std::vector<Vector2> normalsVector;


	// Normals from Polygon 1 //

	// Compute the transform matrix
	Matrix33 transformPoly1 = Matrix33::Translate(tr1->mPosition.x, tr1->mPosition.y) *
		Matrix33::RotRad(tr1->mOrientation) * Matrix33::Scale(tr1->mScale.x, tr1->mScale.y);

	std::vector<Vector2> verticesPoly1 = p1->GetTransformedVertices(transformPoly1);
	int size = verticesPoly1.size();

	// Add all the normals of body 1
	for (int i = 0; i < size; ++i)
	{
		// Add each normal of body 1
		normalsVector.push_back((verticesPoly1[i] - verticesPoly1[(i + 1) % size]).Perp());
	}


	// Normals from Polygon 2 //

	// Compute the transform matrix
	Matrix33 transformPoly2 = Matrix33::Translate(tr2->mPosition.x, tr2->mPosition.y) *
		Matrix33::RotRad(tr2->mOrientation) * Matrix33::Scale(tr2->mScale.x, tr2->mScale.y);

	std::vector<Vector2> verticesPoly2 = p2->GetTransformedVertices(transformPoly2);
	size = verticesPoly2.size();

	// Add all the normals of body 2
	for (int i = 0; i < size; ++i)
	{
		// Add each normal of body 2
		normalsVector.push_back((verticesPoly2[i] - verticesPoly2[(i + 1) % size]).Perp());
	}


	// SAT //

	pResult->mPenetration = 0.0f;

	// All the relevant variables for later (are initialized to 0,0 in the constructors)
	Vector2 minProjectionA;
	Vector2 maxProjectionA;
	Vector2 minProjectionB;
	Vector2 maxProjectionB;

	// For each normal
	for (Vector2 i : normalsVector)
	{
		//AEGfxLine(-10 * i.x, -10 * i.y, 0, 0xFFFFFF00, 10 * i.x, 10 * i.y, 0, 0xFFFFFF00);

		minProjectionA.x = minProjectionA.y = 0.0f;
		maxProjectionA.x = maxProjectionA.y = 0.0f;
		minProjectionB.x = minProjectionB.y = 0.0f;
		maxProjectionB.x = maxProjectionB.y = 0.0f;

		// Polygon 1
		for (Vector2 poly1 : verticesPoly1)
		{
			// Get the projection of the vertix onto the normal
			Vector2 thisProj = poly1.Project(i)/* tr1->mPosition.Project(i)*/;
			//Vector2 thisProj = /*poly1.Project(i)*/ tr1->mPosition.Project(i);

			// To get the max and min points, we can compare just an axis, since all the points
			// are placed onto the same line (x axis for example). When minProjA is still 0,0
			// just set a default value.
			if (minProjectionA.x == 0.0f && minProjectionA.y == 0.0f)
			{
				// Set this projected point as the default
				minProjectionA = maxProjectionA = thisProj;
			}
			else
			{
				// Compare the min
				if (thisProj.y < minProjectionA.y)
					minProjectionA = thisProj;

				// Compare the max
				else if (thisProj.y > maxProjectionA.y)
					maxProjectionA = thisProj;
			}
		}

		// Polygon 2
		for (Vector2 poly2 : verticesPoly2)
		{
			// Get the projection of the vertix onto the normal
			Vector2 thisProj = poly2.Project(i) /* tr2->mPosition.Project(i)*/;

			// To get the max and min points, we can compare just an axis, since all the points
			// are placed onto the same line (x axis for example). When minProjA is still 0,0
			// just set a default value.
			if (minProjectionB.x == 0.0f && minProjectionB.y == 0.0f)
			{
				// Set this projected point as the default
				minProjectionB = maxProjectionB = thisProj;
			}
			else
			{
				// Compare the min
				if (thisProj.y < minProjectionB.y)
					minProjectionB = thisProj;

				// Compare the max
				else if (thisProj.y > maxProjectionB.y)
					maxProjectionB = thisProj;
			}
		}

		/*AEGfxRect(minProjectionA.x, minProjectionA.y, 0, 5, 5, 0xFFFF00FF);
		AEGfxRect(maxProjectionA.x, maxProjectionA.y, 0, 5, 5, 0xFFFF00FF);
		AEGfxRect(minProjectionB.x, minProjectionB.y, 0, 5, 5, 0xFFFF00FF);
		AEGfxRect(maxProjectionB.x, maxProjectionB.y, 0, 5, 5, 0xFFFF00FF);*/

		// Check for overlap
		if (maxProjectionA.y < minProjectionB.y || maxProjectionB.y < minProjectionA.y)
			return false;

		// Get the minimum overlap this iteration (with this normal)

		// Get the vectors between the relative extremes (just two cases)
		Vector2 minBmaxA = minProjectionB - maxProjectionA;
		Vector2 minAmaxB = maxProjectionB - minProjectionA;

		// Variable used right after, for storing whether minBmaxA is 
		// shorter than minAmaxB or not
		Vector2 minOverlapThisNormal;

		// Compare the lenghts squared 
		if (minAmaxB.LengthSq() > minBmaxA.LengthSq() )
			// Set the minOverlap variable
			minOverlapThisNormal = minBmaxA;
		else
			minOverlapThisNormal = minAmaxB;

		// Check if the pResult is set yet or otherwise, if the
		// _new overlap is smaller than the one already set
		if (pResult->mPenetration == 0.0f || minOverlapThisNormal.LengthSq() <
			pResult->mPenetration * pResult->mPenetration)
		{
			// Set both the normal and the penetration amount
			pResult->mNormal = minOverlapThisNormal.Normalize();
			pResult->mPenetration = minOverlapThisNormal.Length();
		}
	}

	// Relative positions between figures matter, to change the direction of the normal
	if (pResult->mNormal.Dot(tr2->mPosition - tr1->mPosition))
		pResult->mNormal *= -1;

	return true;
}