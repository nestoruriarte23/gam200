#pragma once
#include <stdio.h>
#include <vector>
#include "../Utilities/Math/MyMath.h"


struct Polygon2D
{
private:
	// Vertices of the polygon
	std::vector<Vector2>	mVertices;
public:

	Polygon2D();
	Polygon2D(u32 size);
	Polygon2D(Transform2D* box);

	void AddVertex(const Vector2 & vtx);
	Vector2 &operator[](u32 idx);
	void Clear();
	u32	GetSize() const;
	void SetSize(u32 size);
	std::vector<Vector2> GetTransformedVertices(const Matrix33 &mat_transform) const;
	//void Draw(u32 color, Matrix33 * vtx_transform = NULL) const;
	static Polygon2D MakeQuad();
	static Polygon2D MakePentagon();
	static Polygon2D MakeHexagon();
	static Polygon2D MakeSeptagon();
	static Polygon2D MakeOctagon();
	static Polygon2D MakeStandardPoly(u32 side);
};
