#ifndef RAYCAST_H_
#define RAYCAST_H_

#include <list>
#include "LineSegment2D.h"
#include "BoxCollider.h"
#include "../Components/Components.h"
#include "../System/GameObjectManager/GameObject.h"


struct Space;
class Ray 
{

public:
	Ray() : mOrigin(0,0), mDirection(1, 0), limit(2000), rot(0) {}
	Ray(Vector2 org, Vector2 dir, float lim, float rotation);
	virtual ~Ray() {}

	void DrawRay();

	Vector2 mOrigin;
	Vector2 mDirection;
	float rot;
	float limit;
	Vector2 mOutPi;

	std::list<GameObject*> mCastedObjects;
};

//! Performs a raycast test against a line.
f32 RayCastLine(const Vector2 & origin, const Vector2 & dir, const LineSegment2D & line, Vector2 * outPi);
//! Performs a raycast test against an obb.
f32 RayCastRect(const Vector2 & origin, const Vector2 & dir, const Transform2D & rect, Vector2 * outPi);
f32 RayCastRect(Ray & ray, BoxCollider * box, Vector2 * outPi);
//! Performs a raycast test against a circle.
f32 RayCastCircle(const Vector2 & origin, const Vector2 & dir, const Vector2 & circle, f32 radius, Vector2 * outPi);

//! Performs a raycast test against a line.
f32 RayCastLine(const Ray & ray, const LineSegment2D & line, Vector2 * outPi);
//! Performs a raycast test against an obb.
f32 RayCastRect(const Ray & ray, const Transform2D &rect, Vector2 * outPi);
f32 RayCastRect(const Ray & ray, const BoxCollider &go, Vector2 * outPi);
//! Performs a raycast test against a circle.
f32 RayCastCircle(const Ray & ray, const Vector2 & circle, f32 radius, Vector2 * outPi);

const std::vector<GameObject*> CastRay(const Vector2& _origin,
	const Vector2& _direction, float _length, Space* _space, Tags _tag, std::vector<GameObject*>& mResult);
const std::vector<GameObject*> CastRay(const Ray& _ray, Space* _space, Tags _tag, std::vector<GameObject*>& mResult);

#endif