#pragma once
#include "../Physics/PhysicsSystem.h"
#include "../System/GameObjectManager/GameObject.h"
#include "RigidBody.h"
#include "BoxCollider.h"
#include "../Utilities/Time/FrameTime.h"
#include "../../Engine/src/Level/Editor/ImGUI_Basics.h"
#include "../../Engine/src/Level/Editor/ImGui/imgui.h"
#include "../Utilities/MyDebug/LeakDetection.h"

RigidBody::RigidBody() : mInvMass(1.0f), mDrag(0.980f), mColliderState(DYNAMIC)
{
	mGravity = Vector2(0, 0);
}

void AutoComputeBodyInvMass(RigidBody * body)
{
	f32 magic_density = 0.00658f;
	body->mInvMass = (body->mOwner->mScale.x * body->mOwner->mScale.y * magic_density);
}

void RigidBody::Initialize()
{
	Space* mspace = mOwner->GetSpace();
	PhysMgr->attach(mOwner->GetSpace(),this);

	//mGravity = Vector2(0, -100);
	
	if (mColliderState == DYNAMIC)
		AutoComputeBodyInvMass(this);
	else
		mInvMass = 0.0f;

	BoxCollider* mBox = mOwner->get_component_type<BoxCollider>();
	if (mBox)
	{
		mBox->mRigidBody = this;
		mBox->Reattach((EKinematics)mColliderState);
	}
}

void RigidBody::Shutdown()
{
	PhysMgr->removeComp(mOwner->GetSpace(),this);
	BoxCollider* mBox = mOwner->get_component_type<BoxCollider>();
	if (mBox)
	{
		mBox->Reattach(STATIC);
	}
}

void RigidBody::Dragging()
{
	mVelocity = Vector2();
}



void RigidBody::Update()
{
	if (mInvMass)
		AddForce(mGravity / mInvMass);

	// Integrate physics
	f32 dt = 0.0166667f;
	//f32 dt = static_cast<f32>(TimeManager::Instance()->GetDt());
	Integrate(dt);
}

void RigidBody::AddForce(Vector2 force)
{
	mAcceleration += force;
}

void RigidBody::AddVelocity(Vector2 vel)
{
	mVelocity = vel;
}

void RigidBody::Integrate(float timeStep)
{
	// Check if objct is static
	if (mInvMass == 0.0f)
		return;

	// Compute the acceleration (before its just a Forces Vector)
	mAcceleration *= mInvMass;

	// Integrate: Get velocity with acceleration
	mVelocity += mAcceleration * timeStep;
	mVelocity *= mDrag;

	// Get position with velocity
	mOwner->mPosition += mVelocity * timeStep;

	// Reset the acceleration
	mAcceleration = Vector2(0.0f, 0.0f);
}

bool RigidBody::Edit()
{
	bool changed = false;
	ImGui::PushID("RigidBody");

	if (ImGui::TreeNode("RigidBody"))
	{
		#pragma region // Actual Editing Code //

		changed = ImGui::DragFloat2("Velocity", mVelocity.v) || changed;
		changed = ImGui::DragFloat2("Acceleration", mAcceleration.v) || changed;
		changed = ImGui::DragFloat2("Gravity", mGravity.v) || changed;
		changed = ImGui::DragFloat("Inverse Mass", &mInvMass) || changed;
		changed = ImGui::DragFloat("Drag Effect", &mDrag) || changed;

		int state = 0;
		const char * dyn[3] = { "Dynamic", "Static", "Kinematic" };
		if (ImGui::Combo("CollState", &mColliderState, dyn, 3, 6)) {
			BoxCollider* mBox = mOwner->get_component_type<BoxCollider>();
			switch (mColliderState)
			{
			case 0:
				if (mBox)
					mBox->Reattach(DYNAMIC);
				mInvMass = 1.0f;
				break;
			case 1:
				if (mBox)
					mBox->Reattach(STATIC);
				mInvMass = 0.0f;
				break;
			case 2:
				if (mBox)
					mBox->Reattach(KINEMATIC);
				mInvMass = 0.0f;
				break;
			}
			changed = false;
		}
		//
		//if (ImGui::Combo(ngo10, &colType, ngo30, 3, 6)) {
		//
		//}

		#pragma endregion

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}

	ImGui::PopID();
	return changed;
}

void RigidBody::FromJson(nlohmann::json & _j) {

	mGravity.x = _j["mGravity"]["x"];
	mGravity.y = _j["mGravity"]["y"];

	mInvMass = _j["mInvMass"];

	mDrag = _j["mDrag"];

	mColliderState = _j["mColliderState"];
}

void RigidBody::ToJson(nlohmann::json & _j) {

	_j["mGravity"];
	_j["mGravity"]["x"] = mGravity.x;
	_j["mGravity"]["y"] = mGravity.y;

	_j["mInvMass"] = mInvMass;

	_j["mDrag"] = mDrag;

	_j["mColliderState"] = mColliderState;

	_j["_type"] = "RigidBody";

	//EKinematics mBodyState = DYNAMIC;
}

void RigidBody::operator=(IComp &_comp) {
	RigidBody & rigidBody = *dynamic_cast<RigidBody*>(&_comp);

	mGravity = rigidBody.mGravity;

	mInvMass = rigidBody.mInvMass;

	mDrag = rigidBody.mDrag;

	mColliderState = rigidBody.mColliderState;
}

IComp * RigidBody::Clone() 
{ 
	return DBG_NEW RigidBody(*this);
}

bool RigidBody::are_the_same_type(IComp const& lhs) {
	if (dynamic_cast<const RigidBody*>(&lhs)) {
		return true;
	}
	return false;
}

bool RigidBody::equal_to(IComp const& other) const {
	if (RigidBody const* p = dynamic_cast<RigidBody const*>(&other)) {
		return mPrevPosition == p->mPrevPosition && mVelocity == p->mVelocity
			&& mAcceleration == p->mAcceleration && mGravity == p->mGravity
			&& mInvMass == p->mInvMass && mDrag == p->mDrag;
	}
	else {
		return false;
	}
}


/*void RigidBody::operator=(IComp & _comp) {
	RigidBody _rigidBody = *dynamic_cast<RigidBody *>(&_comp);
}*/