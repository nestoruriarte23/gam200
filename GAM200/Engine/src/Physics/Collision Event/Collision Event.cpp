#include "Collision Event.h"

CollisionEvent::CollisionEvent(BoxCollider * object1, BoxCollider * object2): object1_(object1), object2_(object2) { }
BoxCollider* CollisionEvent::GetBoxCollider1() { return object1_; }
BoxCollider* CollisionEvent::GetBoxCollider2() { return object2_; }
