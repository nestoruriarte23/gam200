#pragma once

#include "..//..//EventSystem/Event.h"

class BoxCollider;

class CollisionEvent : public Event
{
	public:
		CollisionEvent(BoxCollider* object1, BoxCollider* object2);
		BoxCollider* GetBoxCollider1();
		BoxCollider* GetBoxCollider2();
		
		BoxCollider* object1_;
		BoxCollider* object2_;
};