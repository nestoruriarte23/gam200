#include <algorithm>
#include "../Physics/CollisionSystem.h"
#include "Raycast.h"
#include "../../Engine/src/Level/Editor/ImGUI_Basics.h"
#include "../../Engine/src/Level/Editor/ImGui/imgui.h"
#include "../Graphics/RenderManager.h"
#include "../Utilities/MyDebug/LeakDetection.h"
#include "../Level/Space/Space.h"


Ray::Ray(Vector2 org, Vector2 dir, float lim = 2000.0f, float rotation = 0) : mOrigin(org), mDirection(dir), limit(lim), rot(rotation)
{}

void Ray::DrawRay()
{
	Vector2 end = Vector2(mDirection * limit)+ mOrigin;
	RenderManager::Instance()->DrawLineAt(mOrigin, end, Engine::Color::rosa);
}
/*!
 \brief	Performs a raycast test against a line

 \param origin	Origin of the ray
 \param dir 	Direction of the ray (not necessarily normalized).
 \param line	Line segment to raycast against.
 \param outPi	Intersection point output
 
 \return	the time of intersection as a factor of dir (if dir is normalized, then 
			this is the same as the distance.
*/
f32 RayCastLine(const Vector2 & origin, const Vector2 & dir, const LineSegment2D & line, Vector2 * outPi)
{
	float ti = (line.mN*line.mP0 - line.mN*origin) / (line.mN*dir); // Getting time of intersection
	Vector2 pi = origin + dir * ti;									// Getting point of intersection
	if ((line.mP0 - pi)*(line.mP1 - pi) > 0)						// If not true positive
		ti = FLT_MAX;													// ti is set to max
	if(outPi)														// If outPi exists
		*outPi = origin + dir * ti;										// Set the outPi
	return ti;														// Return time of intersection
}

/*!
 \brief	Performs a raycast test against an obb.

 \param origin	Origin of the ray
 \param dir 	Direction of the ray (not necessarily normalized).
 \param rect	Rect to raycast against.
 \param outPi	Intersection point output
 
 \return	the time of intersection as a factor of dir (if dir is normalized, then 
			this is the same as the distance.
*/
f32 RayCastRect(const Vector2 & origin, const Vector2 & dir, const Transform2D & rect, Vector2 * outPi)
{

	Vector2 n1 = Matrix33::RotRad(rect.mOrientation) * Vector2(rect.mScale.x/2, 0); // Get normal1 of OBB
	Vector2 n2 = Matrix33::RotRad(rect.mOrientation) * Vector2(0, rect.mScale.y/2); // Get normal2 of OBB
	Vector2 vertex[4];						// Vertexes of OBB
	vertex[0] = rect.mPosition - n1 + n2;		 // Top-Left
	vertex[1] = rect.mPosition + n1 + n2;		 // Top-Right
	vertex[2] = rect.mPosition + n1 - n2;		 // Bottom-Right
	vertex[3] = rect.mPosition - n1 - n2;		 // Bottom-Left
	LineSegment2D side[4];					// Sides of OBB
	side[0] = LineSegment2D(vertex[1], vertex[0]); // Top
	side[1] = LineSegment2D(vertex[2], vertex[1]); // Left
	side[2] = LineSegment2D(vertex[3], vertex[2]); // Bottom
	side[3] = LineSegment2D(vertex[0], vertex[3]); // Right
	float minTi = FLT_MAX;	// Setting min time of intersection to FLT_MAX
	float ti;				// Variable of time of intersection
	for (int i = 0; i < 4; i++) // Loop that goes through all sides of the OBB
	{
		ti = RayCastLine(origin, dir, side[i], NULL); // Getting the ti of a side
		if (ti > 0 && ti < minTi) // If true time value and smaller than minTi
			minTi = ti; // Set the _new min value
		/*else if (ti > 0)
			std::cout << "Desde dentro\n";*/
	}
	if (outPi) // If outPi exists
		*outPi = origin + dir * minTi; // Set the outPi
	return minTi; // Return time of intersection
}

f32 RayCastRect(Ray & ray, BoxCollider * box, Vector2 * outPi)
{
	Vector2 origin(ray.mOrigin);
	Vector2 dir(ray.mDirection);
	GameObject *object(box->GetOwner());
	std::list<GameObject*>::iterator it = find(ray.mCastedObjects.begin(), ray.mCastedObjects.end(), object);
	if (it != ray.mCastedObjects.end()) // The object is on the list
		return FLT_MAX;
	Vector2 n1 = Matrix33::RotRad(object->mRotation) * Vector2(object->mScale.x / 2, 0); // Get normal1 of OBB
	Vector2 n2 = Matrix33::RotRad(object->mRotation) * Vector2(0, object->mScale.y / 2); // Get normal2 of OBB
	Vector2 vertex[4];						// Vertexes of OBB
	vertex[0] = object->mPosition - n1 + n2;		 // Top-Left
	vertex[1] = object->mPosition + n1 + n2;		 // Top-Right
	vertex[2] = object->mPosition + n1 - n2;		 // Bottom-Right
	vertex[3] = object->mPosition - n1 - n2;		 // Bottom-Left
	LineSegment2D side[4];					// Sides of OBB
	side[0] = LineSegment2D(vertex[1], vertex[0]); // Top
	side[1] = LineSegment2D(vertex[2], vertex[1]); // Left
	side[2] = LineSegment2D(vertex[3], vertex[2]); // Bottom
	side[3] = LineSegment2D(vertex[0], vertex[3]); // Right
	float minTi = FLT_MAX;	// Setting min time of intersection to FLT_MAX
	float ti;				// Variable of time of intersection
	for (int i = 0; i < 4; i++) // Loop that goes through all sides of the OBB
	{
		ti = RayCastLine(origin, dir, side[i], NULL); // Getting the ti of a side
		if (ti < minTi && ti > 0) // If true time value and smaller than minTi
			minTi = ti; // Set the _new min value
	}
	if (outPi) // If outPi exists
	{
		*outPi = origin + dir * minTi; // Set the outPi
		ray.mCastedObjects.push_back(object);;
	}
	return minTi; // Return time of intersection
}

/*!
 \fn		RayCastCircle
 \brief	Performs a raycast test against a circle.

 \param origin	Origin of the ray
 \param dir 	Direction of the ray (not necessarily normalized).
 \param circle	Circle center
 \param radius	Circle radius.
 \param outPi	Intersection point output
 
 \return	the time of intersection as a factor of dir (if dir is normalized, then 
			this is the same as the distance.
*/
f32 RayCastCircle(const Vector2 & origin, const Vector2 & dir, const Vector2 & circle, f32 radius, Vector2 * outPi)
{
	float A = dir*dir;												// Member of quadratic function
	float B = -dir * 2 *(circle - origin);							// Member of quadratic function
	float C = (circle - origin)*(circle - origin) - radius*radius;	// Member of quadratic function
	float delta = B*B - 4 * A*C;									// Get delta of cuadratic function
	float t1 = FLT_MAX, t2;											// Solutions of cuadratic function (t1 set to FLT_MAX)
	if (delta == 0)										// If one result from cuadratic function
		t1 = -B / (2 * A);									// Get time of intersection
	if (delta > 0)										// If two results from cuadratic function
	{
		t1 = (-B+sqrt(delta)) / (2 * A);					// Get time of intersection 1
		t2 = (-B-sqrt(delta)) / (2 * A);					// Get time of intersection 2
		if(t1 >= 0 && t2 >= 0)								// If neither t1 and t2 is less than 0
			t1 = std::min(t1, t2);								// Store in t1 the min of the two
		if (t1 < 0 && t2 >= 0)								// If t1 is less than 0
			t1 = t2;											// Store in t1 t2
		if (t1 < 0 && t2 < 0)								// If both times are less than 0
			t1 = FLT_MAX;										// Store FLT_MAX in t1
	}
	if (outPi)											// If outPi exists
		*outPi = origin + dir * t1;							// Set the outPi
	return t1;											// Return time of intersection
}


/*!
 \brief	Performs a raycast test against a circle. Simple wrapper around
		the function above
*/
f32 RayCastCircle(const Ray & ray, const Vector2 & circle, f32 radius, Vector2 * outPi)
{
	return RayCastCircle(ray.mOrigin, ray.mDirection, circle, radius, outPi);
}

/*!
 \brief	Performs a raycast test against an obb. Simple wrapper around
		the function above
*/
f32 RayCastRect(const Ray & ray, const Transform2D &rect, Vector2 * outPi)
{
	return RayCastRect(ray.mOrigin, ray.mDirection, rect, outPi);
}

/*!
 \brief	Performs a raycast test against a line. Simple wrapper around
		the function above
*/
f32 RayCastLine(const Ray & ray, const LineSegment2D & line, Vector2 * outPi)
{
	return RayCastLine(ray.mOrigin, ray.mDirection, line, outPi);
}



bool CheckForBoxWithTag(GameObject* mObj, Tags _tag, const Vector2 _origin, float length)
{
	if (mObj->mTag == _tag && mObj->get_component_type<BoxCollider>())
	{
		Vector2 mDistance = mObj->mPosition - _origin;
		if (mDistance.Length() < length)
			return true;
	}
	return false;
}

const std::vector<GameObject*> CastRay(const Vector2& _origin,
	const Vector2& _direction, float _length, Space* _space, Tags _tag, std::vector<GameObject*>& mResult)
{
	float offset = 100.0f;
	std::vector<GameObject*> mObjs;
	/* ADDED AN OFFSET */
	std::copy_if(_space->space_objects.begin(), _space->space_objects.end(), std::back_inserter(mObjs), std::bind(CheckForBoxWithTag, std::placeholders::_1, _tag, _origin, _length + offset));

	Ray mRay(_origin, _direction, _length);
	Vector2 outPi;
	//std::vector<GameObject*> mResult;
	
	for (GameObject* object : mObjs)
	{
		BoxCollider* mBox = object->get_component_type<BoxCollider>();
		Transform2D mTrans = Transform2D(Vector2(object->mPosition.x + mBox->mOffset.x, object->mPosition.y + mBox->mOffset.y), mBox->mCollSize, object->mRotation);

		if (RayCastRect(mRay, mTrans, &outPi) != FLT_MAX)
		{
			if (std::find(mResult.begin(), mResult.end(), object) == mResult.end())
				mResult.push_back(object);
		}
	}

	return mResult;
}

const std::vector<GameObject*> CastRay(const Ray& _ray, Space* _space, Tags _tag, std::vector<GameObject*>& mResult)
{
	return CastRay(_ray.mOrigin, _ray.mDirection, _ray.limit, _space, _tag, mResult);
}

