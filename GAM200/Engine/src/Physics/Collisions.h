#pragma once
#include <stdio.h>
#include "..//Utilities/Math/Transform2D.h"
#include "..//Physics/Polygon2D.h"

struct Contact
{
	Vector2	mPi;
	Vector2	mNormal;
	float	mPenetration = 0.0f;
};

bool StaticPointToStaticCircle(Vector2 *P, Vector2 *Center, float Radius);
bool StaticPointToStaticEllpise(const Vector2& _point, const Vector2& _ecenter, float _xaxis, float _yaxis, float* _intersection);
bool StaticPointToStaticRect(Vector2 *Pos, Vector2 *Rect, float Width, float Height);
bool StaticPointToOrientedRect(const Vector2 *Pos, Vector2 *Rect, float Width, float Height, float AngleRad);

bool StaticCircleToStaticCircle(Vector2 *Center0, float Radius0, Vector2 *Center1, float Radius1);
bool StaticCircleToStaticCircle(const Vector3& Center0, float Radius0, const Vector3& Center1, float Radius1);
bool StaticRectToStaticRect(Vector2 *Rect0, float Width0, float Height0, Vector2 *Rect1, float Width1, float Height1);
bool StaticRectToStaticCirlce(Vector2 * Rect, float Width, float Height, Vector2* Center, float Radius);
bool OrientedRectToStaticCirlce(Vector2 * Rect, float Width, float Height, float AngleRad, Vector2* Center, float Radius);
bool StaticCircleToStaticCircleEx(Vector2 * Center1, float Radius1, Vector2 * Center2, float Radius2, Contact * pResult);
bool StaticRectToStaticCircleEx(Vector2 * rectPos, float rectWidth, float rectHeight, Vector2 * circlePos, float radius, Contact * pResult);
bool StaticOBBToStaticCircleEx(Transform2D * OBB, Vector2 * Center, float Radius, Contact * pResult);
bool StaticRectToStaticRectEx(Vector2 *pos1, Vector2 *size1, Vector2 *pos2, Vector2 *size2, Contact * pResult);
bool OrientedRectToOrientedRectEx(Transform2D * OBB1, Transform2D * OBB2, Contact * pResult);
bool PolygonToPolygon(Polygon2D * p1, Transform2D * tr1, Polygon2D * p2, Transform2D * tr2, Contact * pResult);