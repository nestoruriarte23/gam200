#pragma once
#include "../Utilities/Math/MyMath.h"
#include "../Components/Components.h"

class GameObject;

enum EKinematics
{
	DYNAMIC,
	STATIC,
	KINEMATIC
};

class RigidBody : public IComp
{

	RTTI_DECLARATION_INHERITED(RigidBody, IComp);

	public:
		RigidBody();
		virtual ~RigidBody() {}

		// physics data
		EKinematics mBodyState = DYNAMIC;
		Vector2	mPrevPosition;
		Vector2	mVelocity;
		Vector2	mAcceleration;
		Vector2	mGravity;
		f32		mInvMass;
		f32		mDrag;

		bool Edit();
		void FromJson(nlohmann::json & j);
		void ToJson(nlohmann::json & j);
		virtual void operator=(IComp &);
		IComp * Clone();

		void Initialize();
		void Shutdown();
		void Dragging();

		// integrate
		void Integrate(float timeStep);

		// adding force
		void AddForce(Vector2 force);
		void AddVelocity(Vector2 vel);

		// update
		virtual void Update();

		bool are_the_same_type(IComp const& lhs);

		//virtual void operator=(IComp & _comp);

	protected:
		virtual bool equal_to(IComp const& other) const;
		
	private:
		int mColliderState = DYNAMIC;
};
