#pragma once
#include "../Physics/CollisionSystem.h"
#include "../System/GameObjectManager/GameObject.h"
#include "Collisions.h"
#include "BoxCollider.h"
#include "../../Engine/src/Level/Editor/ImGUI_Basics.h"
#include "../../Engine/src/Level/Editor/ImGui/imgui.h"
#include "..//EventSystem/TypeInfo.h"
#include "Collision Event/Collision Event.h"
#include "../Graphics/RenderManager.h"
#include "../Input/Input.h"
#include "../Level/Editor/ImGUI/imgui.h"
#include "../Utilities/MyDebug/LeakDetection.h"
#include "Raycast.h"

bool EditableCollider::editing_collider = false;
static bool EditButtoon = false;
using json = nlohmann::json;

BoxCollider::BoxCollider() : ghost(false), mRigidBody(NULL),  mRotation(0)
{
	  
	mCollSize = Vector2(0, 0);
}

BoxCollider::BoxCollider(BoxCollider & _boxCollider) {
	//Vector2 mColliderPos;
	mCollSize = _boxCollider.mCollSize;
	mOffset = _boxCollider.mOffset;
	mRotation = _boxCollider.mRotation;

	ghost = _boxCollider.ghost;

	mRigidBody = _boxCollider.mRigidBody;
	mCollisionShape = _boxCollider.mCollisionShape;
	mColliderState = _boxCollider.mColliderState;
	mPolygon = _boxCollider.mPolygon;
	mColliderEdit = _boxCollider.mColliderEdit;
	mColliderEdit.mBox = this;
	
}

BoxCollider::~BoxCollider()
{
	for (auto it : properties.properties)
	{
		//delete it.second;
	}
}

void BoxCollider::Initialize()
{
	RigidBody* mRB = mOwner->get_component_type<RigidBody>();
	if (mRB)
	{
		mRigidBody = mRB;
		mColliderState = mRigidBody->mBodyState;
	}
	
	//CollMgr->attach(mOwner->GetSpace(), this);
	//mColliderPos = mOwner->mPosition + mOffset;
	if(mCollSize == Vector2(0,0))
		mCollSize = mOwner->mScale;
}

void BoxCollider::Update()
{
	
}

void BoxCollider::Reattach(EKinematics CollState)
{
	//CollMgr->removeComp(mOwner->GetSpace(), this);
	//mColliderState = CollState;
	//CollMgr->attach(mOwner->GetSpace(), this);
}

void BoxCollider::Shutdown()
{
	//CollMgr->removeComp(mOwner->GetSpace(),this);
}

void BoxCollider::ComputeAABB(Vector2 * outPos, Vector2 * outSize)
{
	// Diferenciate between the 3 posible shapes
	if (mCollisionShape == CSHAPE_AABB)
	{
		// Position is just the position of the original shape.
		*outPos = mOwner->mPosition+mOffset;

		// As this shape it is already an AABB, there is no need
		// to change the original Scale.
		*outSize = mCollSize;

		// Can just return at this point
		return;
	}

	else if (mCollisionShape == CSHAPE_CIRCLE)
	{
		// Position is just the position of the original shape.
		*outPos = mOwner->mPosition + mOffset;

		// As we can check in the Render function, radius is computed as the x
		// component of the Scale, and, therefore, the AABB surronding should
		// be a Square wich sides are equal to the twice the radius.
		*outSize = Vector2(mCollSize.x * 2, mCollSize.x * 2);

		// Can just return at this point
		return;
	}

	else if (mCollisionShape == CSHAPE_OBB)
	{
		// Position is just the position of the original shape.
		*outPos = mOwner->mPosition + mOffset;

		// Compute the rotation matrix with the rotation of the object
		Matrix33 rotMatrix = Matrix33::RotRad(mOwner->mRotation);

		// Get two corners of the Square, in this case, the ones to the right
		Vector2 topLeftCorner(mCollSize.x, -mCollSize.y);
		Vector2 bottomLeftCorner(mCollSize.x, mCollSize.y);

		// Rotate those corners to get their real position
		Vector2 topLeftRotated = rotMatrix * topLeftCorner;
		Vector2 bottomLeftRotated = rotMatrix * bottomLeftCorner;

		// Check (with absolute values) which one is higher either on the 
		// x or on the y axis, return that value. This way, that is the maximum
		// value the OBB will take on that axis and therefore creates an AABB
		// that fits perfectly.

		// Checks for X axis
		if (abs(topLeftRotated.x) > abs(bottomLeftRotated.x))
			outSize->x = abs(topLeftRotated.x);
		else
			outSize->x = abs(bottomLeftRotated.x);

		// Checks for Y axis
		if (abs(topLeftRotated.y) > abs(bottomLeftRotated.y))
			outSize->y = abs(topLeftRotated.y);
		else
			outSize->y = abs(bottomLeftRotated.y);
	}

	// If any other shape is inputed no if would be entered and therefore 
	// this function would do nothing.
}

void BoxCollider::FromJson(nlohmann::json & _j) {
	
	mCollSize.x = _j["mCollSize"]["x"];
	mCollSize.y = _j["mCollSize"]["y"];

	if (_j.find("mOffset") != _j.end())
	{
		mOffset.x = _j["mOffset"]["x"];
		mOffset.y = _j["mOffset"]["y"];
	}

	//properties.FromJson(_j, properties);

	//mRotation = _j["mRotation"];

	if (_j.find("ghost") != _j.end())
		ghost = _j["ghost"];
	else
		ghost = false;

	if (_j.find("mCollisionShape") != _j.end())
	mCollisionShape = _j["mCollisionShape"];
}

void BoxCollider::ToJson(nlohmann::json & _j) {
	_j["mCollSize"];
	_j["mCollSize"]["x"] = mCollSize.x;
	_j["mCollSize"]["y"] = mCollSize.y;


	_j["mOffset"];
	_j["mOffset"]["x"] = mOffset.x;
	_j["mOffset"]["y"] = mOffset.y;

	_j["ghost"] = ghost;
	/*_j["Properties"];
	properties.ToJson(_j["Properties"]);*/

	//_j["mRotation"] = mRotation;

	IComp::ToJson(_j);

	_j["mCollisionShape"] = mCollisionShape;
	_j["_type"] = "BoxCollider";

}

void BoxCollider::operator=(IComp & _comp) {
	BoxCollider & boxCollider = *dynamic_cast<BoxCollider*>(&_comp);

	mCollSize = boxCollider.mCollSize;
	mOffset = boxCollider.mOffset;
	
	properties = boxCollider.properties;

	mCollisionShape = boxCollider.mCollisionShape;
}

bool BoxCollider::Edit()
{
	bool changed = false;
	DrawCollider();

	ImGui::PushID("BoxCollider");

	if (ImGui::TreeNode("BoxCollider"))
	{
		changed = ImGui::DragFloat2("Offset", mOffset.v) || changed;
		changed = ImGui::DragFloat2("Scale", mCollSize.v, 2.0f) || changed;

		changed = ImGui::Checkbox("Ghost", &ghost) || changed;

		if (ImGui::Button("EDIT"))
		{
			EditableCollider::editing_collider = EditableCollider::editing_collider == true ? false : true;
			mColliderEdit = EditableCollider(this, mCollSize, mRotation);
		}
			
		if (EditableCollider::editing_collider)
			EditCollider();

		int shape = mCollisionShape -1;
		const char* dyn[4] = { "AABB","CIRCLE", "POLYGON", "OBB" };
		if (ImGui::Combo("Shape", &shape, dyn, 4, 6)) {
			switch (shape)
			{
			case 0:
				mCollisionShape = CSHAPE_AABB;
				break;
			case 3:
				mCollisionShape = CSHAPE_OBB;
				break;
			case 1:
				mCollisionShape = CSHAPE_CIRCLE;
				mCollSize.x = mCollSize.y;
				break;
			case 2:
				mCollisionShape = CSHAPE_POLYGON;
				break;
			}
			changed = true;
		}

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}

	ImGui::PopID();
	return changed;
}

IComp * BoxCollider::Clone()
{
	BoxCollider* _new = DBG_NEW BoxCollider(*this);

	//_new->event_handler.

	return _new;
}

bool BoxCollider::are_the_same_type(IComp const& lhs) {
	if (dynamic_cast<const BoxCollider*>(&lhs)) {
		return true;
	}
	return false;
}

bool BoxCollider::equal_to(IComp const& other) const {
	if (BoxCollider const* p = dynamic_cast<BoxCollider const*>(&other)) {
		return  mCollSize == p->mCollSize
			&& mOffset == p->mOffset && mColliderState == p->mColliderState
			&& mCollisionShape == p->mCollisionShape;
	}
	else {
		return false;
	}
}

void BoxCollider::EditCollider()
{	
	mColliderEdit.Check();
	mColliderEdit.TransformCollider();
}

void BoxCollider::DrawCollider()
{
	Vector2 Pos(mOwner->mPosition + mOffset);
	switch (mCollisionShape)
	{
	case CSHAPE_AABB:
		RenderManager::Instance()->DrawOrientedRectangleAt(Pos, mCollSize.x, mCollSize.y, 0, Engine::Color::green);
		break;
	case CSHAPE_OBB:
		RenderManager::Instance()->DrawOrientedRectangleAt(Pos, mCollSize.x, mCollSize.y, mOwner->mRotation, Engine::Color::green);
		break;
	case CSHAPE_CIRCLE:
		RenderManager::Instance()->DrawCircleAt(Pos, 100, mCollSize.x, Engine::Color::green);
		break;
	case CSHAPE_POLYGON:
		break;
	default:
		break;
	}
}
void BoxCollider::EditAABB()
{
	RenderManager::Instance()->DrawOrientedRectangleAt(Vector2(mOwner->mPosition.x+mOffset.x, mOwner->mPosition.y + mOffset.y), mCollSize.x, mCollSize.y, 0, Engine::Color::green);
	mColliderEdit.Check();
	mColliderEdit.TransformCollider();
}

void BoxCollider::EditOBB()
{
	RenderManager::Instance()->DrawOrientedRectangleAt(Vector2(mOwner->mPosition.x + mOffset.x, mOwner->mPosition.y + mOffset.y), mCollSize.x, mCollSize.y, mOwner->mRotation, Engine::Color::green);
	mColliderEdit.Check();
	mColliderEdit.TransformCollider();
}

void BoxCollider::EditCircle()
{
	RenderManager::Instance()->DrawCircleAt(Vector2(mOwner->mPosition.x + mOffset.x, mOwner->mPosition.y + mOffset.y), 100, mCollSize.x, Engine::Color::green);
	mColliderEdit.Check();
	mColliderEdit.TransformCollider();
}

void BoxCollider::EditPolygon()
{

}


void EditableCollider::Check()
{
	ImGuiIO& io = ImGui::GetIO();
	Matrix33 rotate;

	if(mBox->mCollisionShape == CSHAPE_OBB)
		rotate = Matrix33::RotRad(mBox->mOwner->mRotation);
	else 
		rotate = Matrix33::RotRad(0);
	Vector2 top_scalator = rotate * Vector2(mBox->mCollSize.x / 2, mBox->mCollSize.y / 2);
	Vector2 right_scalator = rotate * Vector2(-mBox->mCollSize.x / 2, mBox->mCollSize.y / 2);
	Vector2 left_scalator = rotate * Vector2(-mBox->mCollSize.x / 2, -mBox->mCollSize.y / 2);
	Vector2 bot_scalator = rotate * Vector2(mBox->mCollSize.x / 2, -mBox->mCollSize.y / 2);

	RenderManager::Instance()->DrawOrientedRectangleAt(mBox->mOwner->mPosition + mBox->mOffset + top_scalator, 4, 4, mBox->mOwner->mRotation, Engine::Color::turquesa);
	RenderManager::Instance()->DrawOrientedRectangleAt(mBox->mOwner->mPosition + mBox->mOffset + right_scalator, 4, 4, mBox->mOwner->mRotation, Engine::Color::turquesa);
	RenderManager::Instance()->DrawOrientedRectangleAt(mBox->mOwner->mPosition + mBox->mOffset + bot_scalator, 4, 4, mBox->mOwner->mRotation, Engine::Color::turquesa);
	RenderManager::Instance()->DrawOrientedRectangleAt(mBox->mOwner->mPosition + mBox->mOffset + left_scalator, 4, 4, mBox->mOwner->mRotation, Engine::Color::turquesa);

	if (MouseDown(MouseInput::LEFT) && !io.WantCaptureMouse)
	{
		if (!visual_scalator)
		{
			if (StaticPointToOrientedRect(&InputManager->GetMouse(), &(mBox->mOwner->mPosition + mBox->mOffset + top_scalator), 8, 8, mBox->mOwner->mRotation))
			{
				visual_scalator = true;
				Scalating_state = is_top_scalating;
				scalator_origin = InputManager->GetMouse();
				original_scale = mBox->mCollSize;
				original_offset = mBox->mOffset;
			}
			else if (StaticPointToOrientedRect(&InputManager->GetMouse(), &(mBox->mOwner->mPosition + mBox->mOffset + right_scalator),8, 8, mBox->mOwner->mRotation))
			{
				visual_scalator = true;
				Scalating_state = is_right_scalating;
				scalator_origin = InputManager->GetMouse();
				original_scale = mBox->mCollSize;
				original_offset = mBox->mOffset;
			}
			else if (StaticPointToOrientedRect(&InputManager->GetMouse(), &(mBox->mOwner->mPosition + mBox->mOffset + bot_scalator), 8, 8, mBox->mOwner->mRotation))
			{
				visual_scalator = true;
				Scalating_state = is_bot_scalating;
				scalator_origin = InputManager->GetMouse();
				original_scale = mBox->mCollSize;
				original_offset = mBox->mOffset;
			}
			else if (StaticPointToOrientedRect(&InputManager->GetMouse(), &(mBox->mOwner->mPosition + mBox->mOffset + left_scalator), 8, 8, mBox->mOwner->mRotation))
			{
				visual_scalator = true;
				Scalating_state = is_left_scalating;
				scalator_origin = InputManager->GetMouse();
				original_scale = mBox->mCollSize;
				original_offset = mBox->mOffset;
			}
		}

		if (!visual_scalator)
		{
			if (!StaticPointToOrientedRect(&InputManager->GetMouse(), &(mBox->mOwner->mPosition + mBox->mOffset), mBox->mCollSize.x, mBox->mCollSize.y, mBox->mRotation))
			{
				EditableCollider::editing_collider = false;
			}
		}
	}
	
}

void EditableCollider::TransformCollider()
{
	
	if (visual_scalator)
	{
		if (MouseDown(MouseInput::LEFT))
		{
			switch (Scalating_state)
			{
			case is_top_scalating:
				mBox->mCollSize = original_scale + (InputManager->GetMouse() - scalator_origin);
				mBox->mOffset = original_offset + (InputManager->GetMouse() - scalator_origin) / 2;
				break;

			case is_right_scalating:
				mBox->mCollSize.x = original_scale.x + (scalator_origin.x - InputManager->GetMouse().x);
				mBox->mCollSize.y = original_scale.y - (scalator_origin.y - InputManager->GetMouse().y);
				mBox->mOffset = original_offset + (InputManager->GetMouse() - scalator_origin) / 2;
				break;

			case is_bot_scalating:
				mBox->mCollSize.x = original_scale.x - (scalator_origin.x - InputManager->GetMouse().x);
				mBox->mCollSize.y = original_scale.y + (scalator_origin.y - InputManager->GetMouse().y);
				mBox->mOffset = original_offset - (scalator_origin-InputManager->GetMouse()) / 2;
				break;

			case is_left_scalating:
				mBox->mCollSize = original_scale + (scalator_origin - InputManager->GetMouse());
				mBox->mOffset = original_offset - (scalator_origin - InputManager->GetMouse()) / 2;
				break;

			default:
				break;
			}
			if (mBox->mCollisionShape == CSHAPE_CIRCLE)
				mBox->mCollSize.x = mBox->mCollSize.y;
		}
		else
			visual_scalator = false;
	}
}






