#pragma once
#include "../System/GameObjectManager/GameObject.h"
#include "Collisions.h"
#include "CollisionSystem.h"
#include "Collision Event/Collision Event.h"
#include "../Graphics/RenderManager.h"
#include "../Components/CompMngrs/LogicBase.h"
#include <algorithm>



CollisionSystem::CollisionSystem() {}

bool CollisionSystem::Initialize()
{
	// default
	mCollisionIterations = 1;

	for (u32 i = 0; i < CSHAPE_INDEX_MAX; ++i)
		mCollisionTests[i] = NULL;
	
	mCollisionTests[CSHAPE_CIRCLE | CSHAPE_CIRCLE] = CollideCircles;
	mCollisionTests[CSHAPE_AABB | CSHAPE_AABB] = CollideAABBs;
	mCollisionTests[CSHAPE_OBB | CSHAPE_OBB] = CollideOBBs;
	mCollisionTests[CSHAPE_CIRCLE | CSHAPE_AABB] = CollideAABBToCircle;
	mCollisionTests[CSHAPE_OBB | CSHAPE_AABB] = CollideOBBs;
	mCollisionTests[CSHAPE_CIRCLE | CSHAPE_OBB] = CollideOBBToCircle;

	return true;
}

void CollisionSystem::Update()
{
	std::map<Space*, std::vector<std::vector<IComp*>*>> mInScreen = Game->GetCurrScene()->GetComponentsInScreen<BoxCollider>();

	for (auto it = mInScreen.begin(); it != mInScreen.end(); it++)//spaces
	{
		for (auto it2 = (*it).second.begin(); it2 != (*it).second.end(); it2++)//vectors
		{
			for (auto it3 = (*it2)->begin(); it3 != (*it2)->end(); it3++)//vector inside the vector
			{
				BoxCollider* temp = reinterpret_cast<BoxCollider*>((*it3));

				if (temp->mColliderState == STATIC)
				{
					mColliders[((*it).first)].second.push_back(temp);
				}
				else
				{
					mColliders[((*it).first)].first.push_back(temp);
				}
			}
		}
	}

	CollideAllBodies();
	LaunchEvents();
	ClearBodies();
}

void CollisionSystem::Update(std::pair<std::list<BoxCollider*>, std::list<BoxCollider*>> in_screen, Space* wanted)
{
	CollideInScreenBodies(in_screen, wanted);
}

void CollisionSystem::ShutDown()
{
	ClearBodies();
}

void CollisionSystem::Reattach(BoxCollider* obj, EKinematics state)
{
	removeComp(obj->mOwner->GetSpace(), obj);
	obj->mColliderState = state;
	attach(obj->mOwner->GetSpace(), obj);
}

void CollisionSystem::attach(Space* mSpace, BoxCollider* mBox)
{
	switch (mBox->mColliderState)
	{
	case DYNAMIC:
		if (!mBox->mOwner->get_component_type<RigidBody>())
			return;
		mColliders[mSpace].first.push_back(mBox);
		break;
	case STATIC:
		mColliders[mSpace].second.push_back(mBox);
		break;
	case KINEMATIC:
		mColliders[mSpace].second.push_back(mBox);
		break;
	default:
		break;
	}
}

void CollisionSystem::removeComp(Space* mSpace, BoxCollider *obj)
{
	if (!mSpace) return;
	if(obj->mColliderState == DYNAMIC)
		mColliders[mSpace].first.remove(obj);
	else
		mColliders[mSpace].second.remove(obj);
}

void CollisionSystem::ClearBodies()
{
	for (auto it = mColliders.begin(); it != mColliders.end(); it++)
	{
		while (!it->second.first.empty())
			it->second.first.remove(it->second.first.back());
		while (!it->second.second.empty())
			it->second.second.remove(it->second.second.back());
		it->second.first.clear();
		it->second.second.clear();
	}
	mColliders.clear();

}

CollisionFn CollisionSystem::GetCollisionFn(BoxCollider * b1, BoxCollider * b2)
{
	u32 collision_index = b1->mCollisionShape | b2->mCollisionShape;
	return mCollisionTests[collision_index];
}

bool CollideOBBtoPolygon(BoxCollider* body1, BoxCollider* body2, Contact* c)
{
	BoxCollider* Polygon = body1->mCollisionShape == CSHAPE_POLYGON ? body1 : body2;
	BoxCollider* OBB = body1->mCollisionShape == CSHAPE_POLYGON ? body2 : body1;

	Transform2D PolyTrans = Transform2D(Vector2(Polygon->mOwner->mPosition.x + Polygon->mOffset.x, Polygon->mOwner->mPosition.y + Polygon->mOffset.y), Polygon->mCollSize, Polygon->mOwner->mRotation);
	Transform2D OBBTrans = Transform2D(Vector2(OBB->mOwner->mPosition.x + OBB->mOffset.x, OBB->mOwner->mPosition.y + OBB->mOffset.y), OBB->mCollSize, OBB->mOwner->mRotation);

	Polygon2D poly1 = Polygon2D::MakeStandardPoly(10);
	Polygon2D poly2 = Polygon2D::MakeStandardPoly(10);

	Matrix33 mat = PolyTrans.GetMatrix();
	Matrix33 mat2 = OBBTrans.GetMatrix();
	for (int i = 0; i < 9; i++)
	{
		
		Vector2 vtx = mat*poly1[i];
		Vector2 vtx2 = mat * poly1[i+1];
		RenderManager::Instance()->DrawLineAt(vtx, vtx2, Engine::Color::red);

		
	}
	Vector2 vtx = mat * poly1[0];
	Vector2 vtx2 = mat * poly1[9];
	RenderManager::Instance()->DrawLineAt(vtx, vtx2, Engine::Color::red);

	for (int i = 0; i < 9; i++)
	{
		Vector2 vtx3 = mat2 * poly2[i];
		Vector2 vtx4 = mat2 * poly2[i + 1];
		RenderManager::Instance()->DrawLineAt(vtx3, vtx4, Engine::Color::green);
	}
	Vector2 vtx3 = mat2 * poly2[0];
	Vector2 vtx4 = mat2 * poly2[9];
	RenderManager::Instance()->DrawLineAt(vtx3, vtx4, Engine::Color::green);


	return PolygonToPolygon(&poly1, &PolyTrans, &poly2, &OBBTrans, c);
}

bool CollidePolygons(BoxCollider* body1, BoxCollider* body2, Contact* c)
{
	Transform2D trans1 = Transform2D(Vector2(body1->mOwner->mPosition.x + body1->mOffset.x, body1->mOwner->mPosition.y + body1->mOffset.y), body1->mCollSize, body1->mOwner->mRotation);
	Transform2D trans2 = Transform2D(Vector2(body2->mOwner->mPosition.x + body2->mOffset.x, body2->mOwner->mPosition.y + body2->mOffset.y), body2->mCollSize, body2->mOwner->mRotation);

	Polygon2D poly1 = body1->mPolygon;
	Polygon2D poly2 = body2->mPolygon;

	return PolygonToPolygon(&poly1, &trans1, &poly2, &trans2, c);
}

bool CollideCircles(BoxCollider* body1, BoxCollider* body2, Contact * c)
{
	Vector2 pos1 = body1->mOwner->mPosition + body1->mOffset;
	Vector2 pos2 = body2->mOwner->mPosition + body2->mOffset;

	return StaticCircleToStaticCircleEx(&pos1, body1->mCollSize.x/2, &pos2, body2->mCollSize.x/2, c);
}

bool CollideAABBs(BoxCollider* body1, BoxCollider* body2, Contact * c)
{
	Vector2 p1 = body1->mOwner->mPosition + body1->mOffset, s1 = body1->mCollSize,
			p2 = body2->mOwner->mPosition + body2->mOffset, s2 = body2->mCollSize;
	return StaticRectToStaticRectEx(&p1, &s1, &p2, &s2, c);
}

bool CollideOBBs(BoxCollider* body1, BoxCollider* body2, Contact * c)
{
	Transform2D obb1(body1->mOwner->mPosition + body1->mOffset, body1->mCollSize, body1->mOwner->mRotation);
	Transform2D obb2(body2->mOwner->mPosition + body2->mOffset, body2->mCollSize, body2->mOwner->mRotation);
	return OrientedRectToOrientedRectEx(&obb1, &obb2, c);
}

bool CollideAABBToCircle(BoxCollider* body1, BoxCollider* body2, Contact * c)
{
	// which is which
	BoxCollider * rect = body1->mCollisionShape == CSHAPE_AABB ? body1 : body2;
	BoxCollider * circle = body1->mCollisionShape == CSHAPE_CIRCLE ? body1 : body2;

	if (StaticRectToStaticCircleEx(&Vector2(rect->mOwner->mPosition + rect->mOffset), rect->mCollSize.x, rect->mCollSize.y, &Vector2(circle->mOwner->mPosition + circle->mOffset), circle->mCollSize.x/2, c))
	{
		if (circle == body1) // flip normal to match our convention
			c->mNormal = -c->mNormal;
		return true;
	}
	return false;
}

bool CollideOBBToCircle(BoxCollider* body1, BoxCollider* body2, Contact * c)
{

	// which is which
	Transform2D obb = body1->mCollisionShape == CSHAPE_OBB
		? Transform2D(body1->mOwner->mPosition + body1->mOffset, body1->mCollSize, body1->mOwner->mRotation)
		: Transform2D(body2->mOwner->mPosition + body2->mOffset, body2->mCollSize, body2->mOwner->mRotation);

	BoxCollider * circle = body1->mCollisionShape == CSHAPE_CIRCLE ? body1 : body2;
	if (StaticOBBToStaticCircleEx(&obb, &Vector2(circle->mOwner->mPosition + circle->mOffset), circle->mCollSize.x / 2, c))
	{
		if (circle == body1) // flip normal to match our convention
			c->mNormal = -c->mNormal;
		return true;
	}
	return false;
}

void CollisionSystem::ResolveStaticContactPenetration(BoxCollider * body1, Contact * contact)
{
	// Compute the _new positions of the bodies
	body1->mOwner->mPosition -= contact->mNormal * contact->mPenetration;
}

void CollisionSystem::ResolveStaticContactVelocity(RigidBody * body1, Contact * contact)
{
	// Relative Velocity betweeen the two bodies
	Vector2 relativeVelocity( - body1->mVelocity);

	// Separating velocity
	f32 separating = relativeVelocity * contact->mNormal;

	// If negative, already separating
	if (separating > 0)
		return;

	// Calculate the separating increment involving the relative velocities
	// and the normal of the collition
	f32 separatingIncrement = (-separating) * DFLT_RESTITUTION;
	separatingIncrement -= separating;

	// Update the velocities
	body1->mVelocity -= contact->mNormal * separatingIncrement;
}

void CollisionSystem::ResolveContactPenetration(RigidBody * body1, RigidBody * body2, Contact * contact)
{
	// Compute the influence of each mass.
	f32 MassInfluence1 = body1->mInvMass / (body1->mInvMass + body2->mInvMass);
	f32 MassInfluence2 = body2->mInvMass / (body1->mInvMass + body2->mInvMass);

	// Compute the _new positions of the bodies
	body1->mOwner->mPosition -= contact->mNormal * contact->mPenetration * MassInfluence1*MassInfluence1;
	body2->mOwner->mPosition += contact->mNormal * contact->mPenetration * MassInfluence2*MassInfluence2;
}

void CollisionSystem::ResolveContactVelocity(RigidBody * body1, 
	RigidBody * body2, Contact * contact)
{
	// Relative Velocity betweeen the two bodies
	Vector2 relativeVelocity(body2->mVelocity - body1->mVelocity);

	// Separating velocity
	f32 separating = relativeVelocity * contact->mNormal;

	// If negative, already separating
	if (separating > 0)
		return;

	// Calculate the separating increment involving the relative velocities
	// and the normal of the collition
	f32 separatingIncrement = (-separating) * DFLT_RESTITUTION;
	separatingIncrement -= separating;

	// Compute the influence of each mass.
	f32 MassInfluence1 = body1->mInvMass / (body1->mInvMass + body2->mInvMass);
	f32 MassInfluence2 = body2->mInvMass / (body1->mInvMass + body2->mInvMass);

	// Update the velocities
	body1->mVelocity -= contact->mNormal * separatingIncrement * 
		MassInfluence1* MassInfluence1;
	body2->mVelocity += contact->mNormal * separatingIncrement * 
		MassInfluence2 *MassInfluence2;
}

void CollisionSystem::CollideAllBodies()
{
	std::for_each(mColliders.begin(), mColliders.end(),
		[&](auto it)
		{
			//printf("%d \n", it.second.first.size());
			std::for_each(it.second.first.begin(), it.second.first.end(),
				[&](BoxCollider* dyn1)
				{
					std::for_each(it.second.first.begin(), it.second.first.end(),
						[&](BoxCollider* dyn2)
						{
							if (dyn1 == dyn2)
								return;

							// Contact Info, to be computed
							Contact pContact;

							if (GetCollisionFn(dyn1, dyn2)(dyn1, dyn2, &pContact))
							{
								CollisionEvent mCollision(dyn1, dyn2);
								mEvents.push_back(mCollision);

								if ((dyn1)->ghost == true || (dyn2)->ghost == true)
									return;
								// Resolve Velocity just once
								// Increment the amount of collitions this frame
								mCollisionsThisFrame++;

								//if (dyn1->mRigidBody == nullptr || dyn2->mRigidBody == nullptr) return;
								// Resolve the velocity
								ResolveContactVelocity((dyn1)->mRigidBody, (dyn2)->mRigidBody, &pContact);

								// Resolve the position
								ResolveContactPenetration((dyn1)->mRigidBody, (dyn2)->mRigidBody, &pContact);
							}
						});

					std::for_each(it.second.second.begin(), it.second.second.end(),
						[&](BoxCollider* stc)
						{
							// Contact Info
							Contact pContact;

							if (GetCollisionFn(dyn1, stc)(dyn1, stc, &pContact))
							{

								CollisionEvent mCollision(dyn1, stc);
								mEvents.push_back(mCollision);

								if ((dyn1)->ghost == true || (stc)->ghost == true)
									return;
								
								// Increment the amount of collitions this frame
								mCollisionsThisFrame++;

								//EventDispatcher::get_instance().trigger_collision_event(mCollision);
								//if (!dyn1 || !stc) return;
								// Resolve the velocity
								ResolveStaticContactVelocity((dyn1)->mRigidBody, &pContact);
								
								// Resolve the position
								ResolveStaticContactPenetration((dyn1), &pContact);
							}
						});
				});
		});

}

void CollisionSystem::LaunchEvents()
{

	EventDispatcher& dispather = EventDispatcher::get_instance();

	std::for_each(mEvents.begin(), mEvents.end(),
		[&](CollisionEvent& event)
		{
			dispather.trigger_collision_event(event);
		});
	mEvents.clear();
}

void CollisionSystem::CollideInScreenBodies(std::pair<std::list<BoxCollider*>, std::list<BoxCollider*>> objects, Space* arena)
{

	// For an N amount of iterations, resolve collitions
	for (unsigned n = 0; n < mCollisionIterations; ++n)
	{
		// For every dynamic object
		for (std::list<BoxCollider*>::iterator dyn1 = objects.first.begin(); dyn1 != objects.first.end(); ++dyn1)
		{
			// There will be two cases, first of all, the checks with the
			// other dynamic objects, and then, the static ones

			// Check with every other dynamic object
			for (std::list<BoxCollider*>::iterator dyn2 = dyn1; dyn2 != objects.first.end(); ++dyn2)
			{
				// Sanity check
				Property<bool>* ghost_1 = dynamic_cast<Property<bool>*>((*dyn1)->properties.properties.find("ghost")->second);
				Property<bool>* ghost_2 = dynamic_cast<Property<bool>*>((*dyn2)->properties.properties.find("ghost")->second);

				if (*dyn1 == *dyn2 || (*dyn2)->ghost == true || (*dyn1)->ghost == true)
					continue;

				// Contact Info, to be computed
				Contact pContact;

				if (GetCollisionFn(*dyn1, *dyn2)(*dyn1, *dyn2, &pContact))
				{
					// Resolve Velocity just once.
					if (n == 0)
					{
						// Increment the amount of collitions this frame
						mCollisionsThisFrame++;

						// Resolve the velocity
						ResolveContactVelocity((*dyn1)->mRigidBody, (*dyn2)->mRigidBody, &pContact);
					}
					// Resolve the position
					ResolveContactPenetration((*dyn1)->mRigidBody, (*dyn2)->mRigidBody, &pContact);
				}
			}

			// With the static objects...
			for (std::list<BoxCollider*>::iterator stc = objects.second.begin(); stc != objects.second.end(); ++stc)
			{
				// Contact Info
				Contact pContact;

				if (GetCollisionFn(*dyn1, *stc)(*dyn1, *stc, &pContact))
				{
					//Property<bool>* ghost_1 = dynamic_cast<Property<bool>*>((*dyn1)->properties.properties.find("ghost")->second);
					//Property<bool>* ghost_2 = dynamic_cast<Property<bool>*>((*stc)->properties.properties.find("ghost")->second);

					if ((*dyn1)->ghost == true || (*stc)->ghost == true)
						continue;
					// Resolve Velocity just once.
					if (n == 0)
					{
						// Increment the amount of collitions this frame
						mCollisionsThisFrame++;

						CollisionEvent mCollision(*dyn1, *stc);
						EventDispatcher::get_instance().trigger_event(mCollision);

						// Resolve the velocity
						ResolveStaticContactVelocity((*dyn1)->mRigidBody, &pContact);
					}
					// Resolve the position
					ResolveStaticContactPenetration((*dyn1), &pContact);
				}
			}
		}
	}
}
