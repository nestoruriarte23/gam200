#pragma once
#include "../Utilities/Math/MyMath.h"
#include "../Physics/RigidBody.h"
#include "../Components/Components.h"
#include "..//EventSystem/EventDispatcher.h"
#include "..//Physics/Collision Event/Collision Event.h"
#include "Polygon2D.h"


enum ECollisionShape
{
	CSHAPE_POLYGON = 3,
	CSHAPE_AABB = 1,	// 001
	CSHAPE_CIRCLE = 2,	// 010
	CSHAPE_OBB = 4,	// 100
	CSHAPE_INDEX_MAX = (CSHAPE_OBB | CSHAPE_CIRCLE) + 1
};

struct EditableCollider
{
	EditableCollider() {}
	EditableCollider(BoxCollider* Collider, Vector2 scale,  float mRot) : 
					mBox(Collider), original_rotation(mRot), original_scale(scale) {}

	enum {is_top_scalating, is_right_scalating, is_bot_scalating, is_left_scalating} Scalating_state;

	void Check();

	void TransformCollider();

	BoxCollider* mBox;
	static bool editing_collider;
	bool visual_scalator = false;
	Vector2 original_scale;
	Vector2 scalator_origin;
	Vector2 original_offset;
	float original_rotation = 0;

};

class BoxCollider : public IComp
{
	RTTI_DECLARATION_INHERITED(BoxCollider, IComp);
public:
	BoxCollider();
	BoxCollider(BoxCollider & _boxCollider);
	virtual ~BoxCollider();

	virtual void Initialize();
	virtual void Update();
	virtual void Shutdown();

	void Reattach(EKinematics CollState);

	void ComputeAABB(Vector2 * outPos, Vector2 * outSize);
	void EditCollider();
	void DrawCollider();
	void EditAABB();
	void EditOBB();
	void EditCircle();
	void EditPolygon();

	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	virtual void operator=(IComp &);
	bool Edit();
	IComp * Clone();
	bool are_the_same_type(IComp const& lhs);
protected:
	virtual bool equal_to(IComp const& other) const;
public:
	//Vector2 mColliderPos;
	Vector2 mCollSize;
	Vector2 mOffset;
	float mRotation;
	
	bool ghost = false;
	//PROP(bool, ghost);

	RigidBody* mRigidBody = NULL;
	ECollisionShape mCollisionShape = CSHAPE_AABB;
	EKinematics mColliderState = STATIC;
	Polygon2D mPolygon;
	EditableCollider mColliderEdit;
};

