#pragma once
#include "Collisions.h"
#include "PhysicsSystem.h"

PhysicsSystem::PhysicsSystem() {}


bool PhysicsSystem::Initialize() { return true; }

void PhysicsSystem::Update()
{
	for (auto it = mRigidBodys.begin(); it != mRigidBodys.end(); it++)
	{
		for (auto bodys = it->second.begin() ; bodys != it->second.end(); bodys++)
		{
				(*bodys)->Update();
		}
	}
}

void PhysicsSystem::ShutDown()
{
	for (std::map<Space*, std::list<RigidBody*>>::iterator it = mRigidBodys.begin(); it != mRigidBodys.end(); it++)
	{
		/*for (auto rb = it->second.begin(); rb != it->second.end(); rb++)
			it->second.remove(*rb);*/
		while(!it->second.empty())
			it->second.remove(it->second.back()); 
		it->second.clear();
	}
	mRigidBodys.clear();
}

void PhysicsSystem::attach(Space* mSpace, RigidBody* obj)
{
	if (!obj)
		return;

	mRigidBodys[mSpace].push_back(obj);

}

void PhysicsSystem::removeComp(Space* mSpace, RigidBody * obj)
{
	if (!mSpace) return;
	mRigidBodys[mSpace].remove(obj);
}
