#include "LineSegment2D.h"
// ---------------------------------------------------------------------------

/*!
\brief		Constructs a dummy line segment.
\details	Sets all values to zero
*/
LineSegment2D::LineSegment2D():
	mP0(Vector2(0, 0)),
	mP1(Vector2(0, 0)),
	mN(Vector2(0, 0)),
	mNdotP0(0)
{}

/*! ---------------------------------------------------------------------------

\brief	Constructs a 2D line segment's data using 2 points

\details:
- Edge is defined as E = P1-P0
- mN is the outward normal and is defined as mN = -E.Perp().Normalize();
- mNdotP0 = the dot product of the normal with p0.

---------------------------------------------------------------------------*/
LineSegment2D::LineSegment2D(const Vector2 & p0, const Vector2 &p1) :
	mP0(p0),
	mP1(p1),
	mN(-(p1 - p0).Perp().Normalize()),
	mNdotP0(mN * p0)
{}


/*!
 \brief	This function determines the distance separating a point from a line
 
 \return	The distance. Note that the returned value should be:
			- Negative if the point is in the line's inside half plane
			- Positive if the point is in the line's outside half plane
			- Zero if the point is on the line
*/
float StaticPointToStaticLineSegment(Vector2 *P, LineSegment2D *LS)
{
	return LS->mN**P - LS->mN*LS->mP0; // Retutns distance between P and LS
}

/*!
 \brief	Given a point P and an array of LineSegment2D, determines if a point
			is contained by all line segments (ie. inside half plane of all segments).
 
 \return	true if the point is inside all line segments, false otherwise.
*/
bool PointInLineSegments(Vector2 *P, LineSegment2D *LS, u32 count)
{
	for (unsigned i = 0; i < count; i++) // Loop that goes through all the LSs
		if (StaticPointToStaticLineSegment(P, LS + i) > 0) // If the point is outside
			return false; // Return false
	return true; // If it is inside all LSs, return true
}