#pragma once 
#include <list>
#include "../Utilities/Math/MyMath.h"
#include "../Components/System.h"
#include "RigidBody.h"
#include "BoxCollider.h"
#include "../EventSystem/EventDispatcher.h"
#include "Raycast.h"

class Ray;


// Collision restitution for velocity resolution
#define DFLT_RESTITUTION 0.908f;

struct Contact;
bool CollideCircles(BoxCollider* body1, BoxCollider* body2, Contact * c);
bool CollideAABBs(BoxCollider* body1, BoxCollider* body2, Contact * c);
bool CollideOBBs(BoxCollider* body1, BoxCollider* body2, Contact * c);
bool CollideAABBToCircle(BoxCollider* body1, BoxCollider* body2, Contact * c);
bool CollideOBBToCircle(BoxCollider* body1, BoxCollider* body2, Contact * c);
bool CollidePolygons(BoxCollider* body1, BoxCollider* body2, Contact* c);
bool CollideOBBtoPolygon(BoxCollider* body1, BoxCollider* body2, Contact* c);
// typedef for function pointer CollisionFn
typedef bool(*CollisionFn)(BoxCollider*, BoxCollider*, Contact *);


struct CollisionSystem : public ISystem
{
	MAKE_SINGLETON(CollisionSystem);
	RTTI_DECLARATION_INHERITED(CollisionSystem, ISystem);

	// Member Variables
	std::list<BoxCollider*> mStaticBodies;
	std::list<BoxCollider*> mDynamicBodies;

	// Collision iterations
	u32 mCollisionIterations;
	u32 mCollisionsThisFrame; // collisions this frame

								//dynamics					//statics
	std::map<Space*, std::pair<std::list<BoxCollider*>, std::list<BoxCollider*>>> mColliders;
	std::vector<CollisionEvent> mEvents;
	// Collision Tests -They are added to the collision system at initialize. 
	// (see CollisionSystem::Init) for more details.
	CollisionFn mCollisionTests[CSHAPE_INDEX_MAX];

	// System Functions
	virtual bool Initialize();
	virtual void Update();
	virtual void Update(std::pair<std::list<BoxCollider*>, std::list<BoxCollider*>> in_screen, Space* wanted);
	virtual void ShutDown();

	// Rigid Body Management
	void attach(Space* mSpace, BoxCollider* mBox);
	void Reattach(BoxCollider* obj, EKinematics state);
	void removeComp(Space* mSpace, BoxCollider *obj);
	void ClearBodies();

	// findeing the collision tests
	CollisionFn GetCollisionFn(BoxCollider * b1, BoxCollider * b2);

	// Collides and resolve all rigidbodies 
	void CollideAllBodies();
	void LaunchEvents();
	void CollideInScreenBodies(std::pair<std::list<BoxCollider*>, std::list<BoxCollider*>> objects, Space* arena);

	// Exposed solver
	void ResolveContactPenetration(RigidBody * obj1, RigidBody * obj2, Contact * contact);
	void ResolveContactVelocity(RigidBody * obj1, RigidBody * obj2, Contact * contact);
	void ResolveStaticContactPenetration(BoxCollider * body1, Contact * contact);
	void ResolveStaticContactVelocity(RigidBody * body1, Contact * contact);

	EventDispatcher& mDispatcher = EventDispatcher::get_instance();
};

#define CollMgr (CollisionSystem::Instance())