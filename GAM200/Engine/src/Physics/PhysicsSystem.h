#pragma once 
#include <list>
//#include "../Utilities/Math/MyMath.h"
#include "../Components/System.h"
#include "RigidBody.h"

struct Contact;
// Collision restitution for velocity resolution
#define DFLT_RESTITUTION 0.908f;


struct PhysicsSystem : public ISystem
{
	MAKE_SINGLETON(PhysicsSystem);
	RTTI_DECLARATION_INHERITED(PhysicsSystem, ISystem);

	// Member Variables
	std::map<Space*, std::list<RigidBody*>> mRigidBodys;

	// System Functions
	virtual void Update();
	virtual bool Initialize();
	virtual void ShutDown();
	// Rigid Body Management
	void attach(Space* mSpace, RigidBody* obj);
	void removeComp(Space* mSpace, RigidBody*obj);
};

#define PhysMgr (PhysicsSystem::Instance())