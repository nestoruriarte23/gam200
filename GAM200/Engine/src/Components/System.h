// ----------------------------------------------------------------------------
// Project: GAM200
// File:	System.h
// Purpose:	System class header.
//
// Copyright DigiPen Institute of Technology
// ----------------------------------------------------------------------------
#pragma once 

//#include "../Utilities/Containers.h"
#include "Base.h"
#include "../Utilities/MyDebug/LeakDetection.h"


	// ----------------------------------------------------------------------------
	// System class will be the base class for all the system we want to implement
	// in the engine.
	// ----------------------------------------------------------------------------
	class ISystem : public IBase
	{
		RTTI_DECLARATION_INHERITED(ISystem, IBase);

	public:
		// ----------------------------------------------------------------------------
		// Functions meant to be overwritten with the functionality of each system.
		// ----------------------------------------------------------------------------
		virtual bool Initialize() { return true; };		
		virtual void Update() {};
		virtual ~ISystem() {}

		// Singleton support
		virtual ISystem** RetrieveInstancePtr() { return nullptr; }
		virtual ISystem* RetrieveInstance() { return nullptr; }
	};


// Add this macro to make the class singleton. Although there are better ways
// to implement the singleton pattern, this one is quick and easy. Warning!
// the default, copy constructor and assignment operators are made private
#define MAKE_SINGLETON(classname)\
	public:\
	static classname** InstancePtr()\
	{\
		static classname * singleton = nullptr;\
		if(singleton == nullptr)\
			singleton = DBG_NEW classname();\
		return &singleton;\
	}\
	static classname* Instance()\
	{\
		classname * singleton = *InstancePtr();\
		return singleton;\
	}\
	virtual ISystem** RetrieveInstancePtr()\
	{\
		return reinterpret_cast<ISystem**>(classname::InstancePtr());\
	}\
	virtual ISystem* RetrieveInstance()\
	{\
		return dynamic_cast<ISystem*>(classname::Instance());\
	}\
	static void ReleaseInstance()\
	{\
		delete Instance();\
		(*InstancePtr()) = NULL;\
	}\
	private:\
	classname();\
	classname(const classname &);\
	const classname & operator=(const classname &);
