// ----------------------------------------------------------------------------
// Project: GAM200
// File:	Components.cpp
// Purpose:	IComp API
//
// Copyright DigiPen Institute of Technology
// ----------------------------------------------------------------------------
#include "Components.h"
#include "../System/GameObjectManager/GameObject.h"

/* Global variable for IBase */
u32	IBase::mLastUID = 0;

	// ----------------------------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------------------------
	IComp::IComp()
		: mbEnabled(true)
	{}
	IComp::~IComp()
	{}
	// ----------------------------------------------------------------------------
	// PUBLIC - State Methods - By default they do nothing in this component.
	// ----------------------------------------------------------------------------
	void IComp::Initialize()
	{}
	void IComp::Update()
	{}
	void IComp::Shutdown()
	{}

	// ----------------------------------------------------------------------------
	// Return wheter the component is enabled or not.
	// ----------------------------------------------------------------------------
	bool IComp::IsEnabled()
	{
		return mbEnabled;
	}

	// ----------------------------------------------------------------------------
	// Set the component to enabled or disabled.
	// ----------------------------------------------------------------------------
	void IComp::SetEnabled(bool enabled)
	{
		mbEnabled = enabled;
	}

	// ----------------------------------------------------------------------------
	// Edit function for ImGui editing
	// ----------------------------------------------------------------------------
	bool IComp::Edit() { return false; }

	/*void IComp::operator=(IComp & _comp) {
		mbEnabled = _comp.mbEnabled;
		mUID = _comp.mUID;
	}*/
