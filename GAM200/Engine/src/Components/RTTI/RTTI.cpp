// ----------------------------------------------------------------------------
// Project: GAM200
// File:	RTTI.cpp
// Purpose:	RTTI API.
//
// Copyright DigiPen Institute of Technology
// ----------------------------------------------------------------------------
#include "RTTI.h"


	std::map<std::string, Rtti> Rtti::Types;

	const Rtti & Rtti::RttiAdd(const char * typeName, const char * parentName)
	{
		// TODO: insert return statement here

		// parent isn't assumed to exists. so this might be the first time
		// the parent type is encountered; (it depends on the call). 
		auto & parentIt = Types[parentName];
		parentIt.mName = parentName;

		std::map<std::string, Rtti>::iterator it = Types.find(typeName);
		if (it == Types.end()) {

			auto & ref = Types[typeName];
			ref.mpBaseType = &parentIt;
			ref.mName = typeName;

			// restore iterator
			it = Types.find(typeName);
		}

		// add to parent
		parentIt.mChildren[typeName] = &it->second;

		return it->second;
	}

	// ----------------------------------------------------------------------------
	// Default constructor. Initializes everything to null values.
	// ----------------------------------------------------------------------------

	Rtti::Rtti(): mName("no_name")	, mpBaseType(nullptr)
	{}
	// ----------------------------------------------------------------------------
	// Custom constructor.
	// ----------------------------------------------------------------------------
	Rtti::Rtti(const char * name, const Rtti * pBaseType)
	{
		// store the name
		mName = name;
		// remove the "class " prefix, returned by typeid().name() function
		mName = mName.substr(mName.find_first_of(" ") + 1);

		// store the pointer to the base type
		mpBaseType = pBaseType;
	}


	// ----------------------------------------------------------------------------
	// Name getter.
	// ----------------------------------------------------------------------------
	const char * Rtti::GetName() const
	{
		return mName.c_str();
	}

	// ----------------------------------------------------------------------------
	// Compares the component type with the type of the passed component.
	// ----------------------------------------------------------------------------
	bool Rtti::IsExactly(const Rtti & otherType) const
	{
		return this == &otherType;
	}

	// ----------------------------------------------------------------------------
	// Check if the component has been derived from the passed component.
	// ----------------------------------------------------------------------------
	bool Rtti::IsDerived(const Rtti & otherType) const
	{
		const Rtti * temp = this;
		while (temp)
		{
			if (temp == &otherType)
			{
				return true;
			}

			temp = temp->mpBaseType;
		}
		return false;
	}
