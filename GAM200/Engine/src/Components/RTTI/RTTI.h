// ----------------------------------------------------------------------------
// Project: GAM200
// File:	RTTI.h
// Purpose:	RTTI system header. It contains the class declaration for the
//			RTTI interface and all the required macros to use it.
//
// Copyright DigiPen Institute of Technology
// ----------------------------------------------------------------------------
#pragma once

#include <string>
#include <vector>
#include <map>


	// ----------------------------------------------------------------------------
	// RTTI class
	// ----------------------------------------------------------------------------
	class Rtti
	{

	// ----------------------------------------------------------------------------
	// Private members of the RTTI.
	// ----------------------------------------------------------------------------
	private:

		std::string		mName;
		const Rtti	* mpBaseType;
		
		// map with pointers to all the included components on the RTTI.
		std::map<std::string, Rtti*> mChildren;

	public:
		// map of strings to store all the names of the components.
		static std::map<std::string, Rtti> Types;

		// ----------------------------------------------------------------------------
		// Templated function to add a new component type to the RTTI system.
		// ----------------------------------------------------------------------------
		template<typename type> static const Rtti &  RttiAdd()
		{
			return RttiAdd(GetTypeName<type>().c_str(), "NULL");
		}

		// ----------------------------------------------------------------------------
		// Templated function to add a new component type to the RTTI system 
		// based on the parent component.
		// ----------------------------------------------------------------------------
		template<typename type, typename parent> static const Rtti &  RttiAdd()
		{
			return RttiAdd(GetTypeName<type>().c_str(), GetTypeName<parent>().c_str());
		}

		static const Rtti & RttiAdd(const char * typeName, const char * parentName);

	public:
		// ----------------------------------------------------------------------------
		// Constructors for the RTTI system.
		// ----------------------------------------------------------------------------
		Rtti();
		Rtti(const char * name, const Rtti * pBaseType);

		// getters
		const char * GetName() const;

		// compares this with address of otherType
		bool IsExactly(const Rtti & otherType) const;

		// walks up the hierarchy comparing with address of otherType
		bool IsDerived(const Rtti & otherType) const;

		// convert from c++ rtti to ours
		template<typename T>
		static std::string GetTypeName()
		{
			// remove class
			std::string out = typeid(T).name();
			std::size_t c = out.find("class ");
			while (c != std::string::npos)
			{
				auto s = out.begin() + c;
				auto e = s + strlen("class ");
				out.replace(s, e, "");
				c = out.find("class ");
			}
			// remove struct
			c = out.find("struct ");
			while (c != std::string::npos)
			{
				auto s = out.begin() + c;
				auto e = s + strlen("struct ");
				out.replace(s, e, "");
				c = out.find("struct ");
			}
			return out;
		}
	};
 

// ----------------------------------------------------------------------------
// Use this macro each time we want to create a new class which derives
// from another one. I will automatically call all the necessary functions
// to make our RTTI system compatible with that new class.
// ----------------------------------------------------------------------------
#define RTTI_DECLARATION_INHERITED(thisType, parentType)											\
	public:									\
		virtual const Rtti& GetType() const						\
		{	return Rtti::RttiAdd<thisType, parentType>();\
		}\
		static const Rtti& TYPE()\
{return Rtti::RttiAdd<thisType, parentType>();\
}\
// ----------------------------------------------------------------------------
// Use this macro each time we want to create a new class which is the base
// class for other ones. I will automatically call all the necessary functions
// to make our RTTI system compatible with that new class.
// ----------------------------------------------------------------------------
#define RTTI_DECLARATION_BASECLASS(thisType)											\
	public:									\
		virtual const Rtti& GetType() const						\
		{	return Rtti::RttiAdd<thisType>();											\
		}\
		static const Rtti& TYPE()\
{return Rtti::RttiAdd<thisType>();\
}\


// ----------------------------------------------------------------------------