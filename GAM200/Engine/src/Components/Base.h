#pragma once
// ----------------------------------------------------------------------------
// Project: GAM200
// File:	Base.h
// Purpose:	Header of the base interface class.
//
// Copyright DigiPen Institute of Technology
// ----------------------------------------------------------------------------
#include <string>				// string
#include "../../Types.h"
#include "RTTI\RTTI.h"			// RTTI


	// ----------------------------------------------------------------------------
	// CLASS:	IBase
	// PURPOSE:	This class provides the base for all the classes in the engine 
	//			It provides basic services such as RTTI and Messages, smart pointers, etc...
	// ------------------------------------------------------------------------
	class IBase
	{
		// RTTI'S system macro to declare this new class in the RTTI system.
		RTTI_DECLARATION_BASECLASS(IBase);
	public:
		// public virtual destructor 
		virtual ~IBase() {}
		// ----------------------------------------------------------------------------
#pragma region// NAME & UID
	public:
		// ----------------------------------------------------------------------------
		// Purpose: Get the name of the class name.
		// ----------------------------------------------------------------------------
		const char * GetName() { return mName.data(); }

		// ----------------------------------------------------------------------------
		// Purpose: Set the name of the class.
		// ----------------------------------------------------------------------------
		void SetName(const char * name) { 
			mName = name; 
		}

		// ----------------------------------------------------------------------------
		// Purpose: Get the unique id of the class.
		// ----------------------------------------------------------------------------
		u32			 GetUID() { return mUID; }
		const u32	 GetUID() const { return mUID; } ;
	protected:
		std::string mName;	// Non-Unique name
		u32			mUID;	// Unique number ID
#pragma endregion

	protected:
		static u32	mLastUID;	// Unique number ID
		IBase() {
			mUID = ++mLastUID;
		} // only accessible from child classes. can't construct an IBase explicitly.
	};

// ----------------------------------------------------------------------------