// ----------------------------------------------------------------------------
// Project: GAM200
// File:	Components.h
// Purpose:	IComp API header.
//
// Copyright DigiPen Institute of Technology
// ----------------------------------------------------------------------------
#pragma once
#include "../../Types.h"
#include "RTTI\RTTI.h"
#include "Base.h"
#include "..\Serializer\SerializeFactory.h"

class GameObject;

// ----------------------------------------------------------------------------
// \class	IComp 
// \brief	Base component class, meant to be used as an interface.
class IComp : public IBase
{
	RTTI_DECLARATION_INHERITED(IComp, IBase);
//	friend class GameObject;

public:
	IComp();
	virtual ~IComp() = 0;

	// State Methods
		// Enabled gettor.
	bool IsEnabled();
		// Enabled setter.
	virtual void SetEnabled(bool enabled);

	// Overridable methods - each component should implement their own versions of those
	virtual void Initialize();		// Called when the owner object is about to be initalized.
	virtual void Update();			// Called by the system at each update.
	virtual void Shutdown();		// Called by the owner object when destroyed.

	// TODO: Return the owner when GameObject implemented.
	GameObject* GetOwner(void) const {return mOwner;}

	//Edit function to be override by child class, 
	//to be able to edit the component with ImGui
	virtual bool Edit();

	virtual void Dragging() {}
	virtual void Selected() {}

	//Clone function for deep copys in case is necesary
	virtual IComp * Clone() = 0;

	//From/To Json function to be override by child class,
	//in order to save and load from file
	virtual void FromJson(nlohmann::json & j) {};
	virtual void ToJson(nlohmann::json & j) {};
	virtual void operator=(IComp &) {};

	//friend comparision operator to be able to check if two components are the same
	friend bool operator==(IComp const& lhs, IComp const& rhs) {
		return lhs.equal_to(rhs);
	}

	virtual bool are_the_same_type(IComp const& lhs) = 0;

	//virtual void operator=(IComp & _comp);



protected:

	virtual bool equal_to(IComp const& other) const = 0;


public:
	PropertyMap properties;
	// TODO: Add when GameObject implemented.
	GameObject			*mOwner; // owner object
	bool				mbEnabled;
};




// ----------------------------------------------------------------------------