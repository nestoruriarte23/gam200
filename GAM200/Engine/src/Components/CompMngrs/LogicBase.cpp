
#include "LogicBase.h"

LogicManager::LogicManager() 
{
	//std::cout << "Logic Manager creatated" << std::endl;
}

LogicManager::~LogicManager()
{
	//std::cout << "Logic Component Count on manager destruction: " << mComps.size() << std::endl;
	//std::cout << "Logic Manager destroyed" << std::endl;
}

void LogicManager::Update()
{
	for (std::list<IComp*>::iterator it = mComps.begin(); it != mComps.end(); ++it)
	{
		if ((*it)->mbEnabled)
			(*it)->Update();
	}

	while (!mDeadComps.empty())
	{
		mComps.remove(mDeadComps.back());
		mDeadComps.pop_back();
	}
}

bool LogicManager::Initialize()
{
	for (IComp* comp : mComps)
	{
		comp->Initialize();
	}
		

	return true;
}

void LogicManager::Shutdown()
{
	while (!mComps.empty())
	{
		mComps.back()->Shutdown();
		mComps.pop_back();
	}

	std::cout << "Logic Component Count on manager shutdown: " 
		<< mComps.size() << std::endl;
}

void LogicManager::AddLogicComponent(IComp * logicComp)
{
	mComps.push_back(logicComp);
}

void LogicManager::RemoveComp(IComp* logicComp)
{
	for (auto comp = mComps.begin(); comp != mComps.end(); ++comp)
		if (logicComp == *comp)
		{
			// This should propably be called through the destructor
			// of the compoent, so no delete (TODO?)

			comp = mComps.erase(comp);
			delete logicComp;

			break;
		}
}

void LogicManager::RemoveCompNoDelete(IComp * logicComp)
{
	for (auto comp = mComps.begin(); comp != mComps.end(); ++comp)
		if (logicComp == *comp)
		{
			// This should propably be called through the destructor
			// of the compoent, so no delete (TODO?)

			mComps.erase(comp);
			mDeadComps.push_back(logicComp);

			break;
		}
}

void LogicManager::ClearComps()
{
	// TODO? : Clear Comps will be usually called through the scene/level destructor
	// which already calls the destructor of the components and removes the objects 
	// from the vector (So, theoretically and hopefully no work to be done here)

	//std::cout << "Logic Component Count on clear call: " << mComps.size() << std::endl;
}

ILogic::ILogic()
{
	LogicManager::Instance()->AddLogicComponent(this);
}

ILogic::~ILogic()
{
	EventDispatcher::get_instance().unsubscribe_collision(*this, mOwner->GetUID());
	LogicManager::Instance()->RemoveCompNoDelete(this);
}

void ILogic::FromJson(nlohmann::json & _j)
{
	// TODO: With Property System
}

void ILogic::ToJson(nlohmann::json & _j)
{
	// TODO: With Property System
}

#include "../../Level/Editor/ImGUI_Basics.h"
#include "../../Level/Editor/ImGui/imgui.h"
#include "../../System/GameObjectManager/GameObject.h"

bool ILogic::Edit()
{
	bool changed = false;
	ImGui::PushID("LogicComponent");

	if (ImGui::TreeNode("LogicComponent"))
	{
		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
		}
	}

	ImGui::PopID();
	return changed;
}

void ILogic::Initialize()
{
	
}

/*void ILogic::CollisionDetected(const CollisionEvent& coll)
{
	BoxCollider* mBox = mOwner->get_component_type<BoxCollider>();
	if (coll.object1_ == mBox)
		OnCollisionStarted(coll.object2_);
	else if (coll.object2_ == mBox)
		OnCollisionStarted(coll.object1_);
}*/

void ILogic::Shutdown()
{
	EventDispatcher::get_instance().unsubscribe_collision(*this, mOwner->GetUID());
}

void ILogic::handle_event(const Event& event)
{																
	//event_handler.handle(event);								
}										
bool ILogic::Compare(Listener* to_compare)
{																	
	ILogic* listener = dynamic_cast<ILogic*>(to_compare);

	if (mOwner->GetUID() == listener->mOwner->GetUID())				
		return true;												

	return false;													
}

void ILogic::subscribe_collision_event()
{

	EventDispatcher& event_dispatcher = EventDispatcher::get_instance();
	event_dispatcher.subscribe_collison(*this, mOwner->GetUID());
}