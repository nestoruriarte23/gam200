#pragma once

#include <vector>

#include "../System.h"
#include "../Components.h"

class PhysComp : public IComp
{

};

class PhysManager : public IBase
{
	MAKE_SINGLETON(PhysManager);
	RTTI_DECLARATION_INHERITED(PhysManager, IBase);

public:
	void Initialize();
	void Update();
	void ShutDown();

	template <typename T>
	T* get_component_type()const
	{
		//create a pointer as the same type as the one provided
		T* Type = NULL;
		//iterate throw all the components
		for (unsigned i = 0; i< mComp.size(); i++)
		{
			//cast the component to the one provided
			Type = dynamic_cast<T*>(mComp[i]);
			//if it exists returns it
			if (Type)
				return Type;
		}
		//otherwise return null
		return NULL;
	}

private:
	std::vector<PhysComp*> mComp;
};
#define PhysMgr (PhysManager::Instance())