#pragma once

#include <list>
#include "../Components.h"
#include "../../Engine/Engine.h"
#include "../../Utilities/MyDebug/LeakDetection.h"

class ILogic : public IComp, public Listener
{
	RTTI_DECLARATION_INHERITED(ILogic, IComp);

public:
	ILogic();
	~ILogic();

	void FromJson(nlohmann::json& _j);
	void ToJson(nlohmann::json& _j);

	virtual bool Edit();				// TODO: Property System + Editor????
	virtual void Initialize();			// Does nothing
	virtual void Shutdown();			// Does nothing
	virtual void Update() = 0;

	virtual bool are_the_same_type(IComp const& lhs) = 0;

	void handle_event(const Event& event);
	bool Compare(Listener* to_compare);

	virtual void OnCollisionStarted(const BoxCollider* coll) 
	{

	}	//the behaviour of this function will be defined by the user
	//void CollisionDetected(const CollisionEvent& coll);
protected:
	virtual bool equal_to(IComp const& other) const = 0;

	void subscribe_collision_event();
	//EventHandler event_handler;
};

class LogicManager : public ISystem
{
	RTTI_DECLARATION_INHERITED(LogicManager, ISystem);
	MAKE_SINGLETON(LogicManager);

public:
	~LogicManager();

	virtual void Update();
	virtual bool Initialize();
	virtual void Shutdown();

	// component management
	void AddLogicComponent(IComp * logicComp);
	void RemoveComp(IComp * logicComp);
	void RemoveCompNoDelete(IComp * logicComp);
	void ClearComps();

private:
	std::list<IComp *> mComps;
	std::list<IComp *> mDeadComps;
};

/* WARNING: After the macro following statements are public */
#define LOGIC_COMPONENT( component )					\
public:													\
component * Clone()										\
{														\
	return DBG_NEW component(*this);					\
}														\
bool Edit() override											 \
{																 \
	bool changed = false;										 \
	ImGui::PushID(#component);									 \
																 \
	if (ImGui::TreeNode(#component))							 \
	{															 \
		ImGui::TreePop();										 \
	}															 \
	else														 \
	{															 \
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);			 \
		if (ImGui::Button("", ImVec2(14, 14))) {				 \
			mOwner->RemoveComp(this);							 \
		}														 \
	}															 \
																 \
	ImGui::PopID();												 \
	return changed;												 \
}																 \
														\
void FromJson(nlohmann::json & _j){}					\
void ToJson(nlohmann::json & _j) {						\
	_j["_type"] = #component;							\
}														\
bool are_the_same_type(IComp const& lhs) {				\
	if (dynamic_cast<const component*>(&lhs)) {			\
		return true;									\
	}													\
	return false;										\
}														\
protected:																	\
bool equal_to(IComp const& other) const {									\
	if (component const* p = dynamic_cast<component const*>(&other)) 		\
		return true;														\
	return false;															\
}																			\
public:

