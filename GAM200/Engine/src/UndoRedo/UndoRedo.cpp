#include <string>
#include <iostream>
#include <fstream>
#include "UndoRedo.h"
#include "../Serializer/SerializeFactory.h"

#include "../Utilities/MyDebug/LeakDetection.h"

Undo::Undo() {}

void Undo::SaveOldObject(GameObject * _go, int _ID, int _action) {
	std::string name("undo" + std::to_string(undoStackSize));
	serializer->SaveStackUndo(name.c_str(), _go, _ID, _action);
	redo->ClearRedoStack();
	undoStackSize++;
}

GameObject * Undo::UndoChange(int & _id, int & _action) {
	GameObject * go = DBG_NEW GameObject();
	undoStackSize--;
	std::string name("undo" + std::to_string(undoStackSize));
	_id = serializer->LoadStackUndo(name.c_str(), go, _action);
	return go;
}

void Undo::UndoMultiStackChange(std::vector<std::pair<int, Vector2>>& _objects) {
	undoStackSize--;
	std::string name("undo" + std::to_string(undoStackSize));
	serializer->LoadStackUndo(name.c_str(), _objects);
}

void Undo::UndoMultiStackChangeObj(std::vector<GameObject*>& _objects) {
	undoStackSize--;
	std::string name("undo" + std::to_string(undoStackSize));
	serializer->LoadStackUndo(name.c_str(), _objects);
}

int Undo::GetAction(){
	nlohmann::json j;

	std::string name("undo" + std::to_string(undoStackSize-1));

	std::string path;
	path += "../Resources/temp/";
	path += name;
	path += ".json";

	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();
	
		return j["action"];
	}
	return NULL;
}

int Undo::GetUndoStackSize() {
	return undoStackSize;
}

void Undo::ClearUndoStack() {
	for (int i = undoStackSize; i >= 0; i--) {
		std::string s("..\\Resources\\temp\\undo" + std::to_string(i) + ".json");
		remove(s.c_str());
	}
	undoStackSize = 0;
}

void Undo::UndoSaveMultiStack(std::vector<std::pair<int, Vector2>>& _objects) {
	std::string name("undo" + std::to_string(undoStackSize));
	serializer->SaveStackUndo(name.c_str(), _objects, (int)Action::MULTIPLEEDIT);
	redo->ClearRedoStack();
	undoStackSize++;
}

void Undo::UndoSaveMultiStackObj(std::vector<GameObject*>& _objects, int action) {
	std::string name("undo" + std::to_string(undoStackSize));
	serializer->SaveStackUndo(name.c_str(), _objects, action);
	redo->ClearRedoStack();
	undoStackSize++;
}

Redo::Redo() {}

void Redo::SaveOldObject(GameObject * _go, int _ID, int _action) {
	std::string name("redo" + std::to_string(redoStackSize));
	serializer->SaveStackUndo(name.c_str(), _go, _ID, _action);
	redoStackSize++;
}


void Redo::RedoMultiStackChange(std::vector<std::pair<int, Vector2>>& _objects) {
	redoStackSize--;
	std::string name("redo" + std::to_string(redoStackSize));
	serializer->LoadStackUndo(name.c_str(), _objects);
}

void Redo::RedoMultiStackChangeObj(std::vector<GameObject*>& _objects) {
	redoStackSize--;
	std::string name("redo" + std::to_string(redoStackSize));
	serializer->LoadStackUndo(name.c_str(), _objects);
}

GameObject * Redo::RedoChange(int & _id, int & _action) {
	GameObject * go = DBG_NEW GameObject();
	redoStackSize--;
	std::string name("redo" + std::to_string(redoStackSize));
	_id = serializer->LoadStackUndo(name.c_str(), go, _action);
	return go;
}

int Redo::GetRedoStackSize() {
	return redoStackSize;
}

int Redo::GetAction() {
	nlohmann::json j;

	std::string name("redo" + std::to_string(redoStackSize - 1));

	std::string path;
	path += "../Resources/temp/";
	path += name;
	path += ".json";

	std::ifstream inFile(path.c_str());
	if (inFile.good() && inFile.is_open()) {
		inFile >> j;
		inFile.close();

		return j["action"];
	}
	return NULL;
}

void Redo::ClearRedoStack() {
	for (int i = redoStackSize; i >= 0; i--) {
		std::string s("..\\Resources\\temp\\redo" + std::to_string(i) + ".json");
		remove(s.c_str());
	}
	redoStackSize = 0;
}

void Redo::RedoSaveMultiStack(std::vector<std::pair<int, Vector2>>& _objects) {
	std::string name("redo" + std::to_string(redoStackSize));
	serializer->SaveStackUndo(name.c_str(), _objects, (int)Action::MULTIPLEEDIT);
	redoStackSize++;
}


void Redo::RedoSaveMultiStackObj(std::vector<GameObject*>& _objects, int action) {
	std::string name("redo" + std::to_string(redoStackSize));
	serializer->SaveStackUndo(name.c_str(), _objects, action);
	redoStackSize++;
}


void clearAllSacks() {
	redo->ClearRedoStack();
	undo->ClearUndoStack();
}