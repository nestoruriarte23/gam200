#pragma once
#include "../Components/System.h"
#include "..//System/GameObjectManager/GameObject.h"

enum class Action { NONE, ADDOBJECT, DELETEOBJECT, ADDCOMPONENT, DELETECOMPONENT, MULTIPLEEDIT, MULTIPLEDELETE, MULTIPLEADD};

class Undo {
	MAKE_SINGLETON(Undo);

	public:
		void SaveOldObject(GameObject * _go, int _ID, int _action);
		GameObject * UndoChange(int & _id, int & _action);
		void UndoMultiStackChange(std::vector<std::pair<int, Vector2>>& _objects);
		void UndoMultiStackChangeObj(std::vector<GameObject*>& _objects);
		int GetUndoStackSize();
		int GetAction();
		void ClearUndoStack();
		void UndoSaveMultiStack(std::vector<std::pair<int, Vector2>>& _objects);
		void UndoSaveMultiStackObj(std::vector<GameObject*>& _objects, int action);
	private:
		int undoStackSize = 0;
};

class Redo {
	MAKE_SINGLETON(Redo);

public:
	void SaveOldObject(GameObject * _go, int _ID, int _action);
	GameObject * RedoChange(int & _id, int & _action);
	void RedoMultiStackChange(std::vector<std::pair<int, Vector2>>& _objects);
	void RedoMultiStackChangeObj(std::vector<GameObject*>& _objects);
	int GetRedoStackSize();
	int GetAction();
	void ClearRedoStack();
	void RedoSaveMultiStack(std::vector<std::pair<int, Vector2>>& _objects);
	void RedoSaveMultiStackObj(std::vector<GameObject*>& _objects, int action);
private:
	int redoStackSize = 0;
};

void clearAllSacks();

#define undo (Undo::Instance())
#define redo (Redo::Instance())