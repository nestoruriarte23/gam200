#include "StateManager.h"
#include "../../GamePlay/src/PlayerLogic/PlayerLogic.h"
#include "../../GamePlay/src/GaltxaLogic/GaltxagorriLogic.h"
#include "../../GamePlay/src/CompanionLogic/CompanionLogic.h"
#include "../../GamePlay/src/LamiaLogic/LamiaLogic.h"
#include "../../GamePlay/src/SorginaLogic/SorginaLogic.h"
#include "../../GamePlay/EnemyLogic.h"
#include "../../GamePlay/src/CompanionLogic/CompanionLogic.h"

StateManager::StateManager()
{}

bool StateManager::Initialize()
{
	// Player States
	mStateTypeNames.push_back("PlayerIdleState");
	mStateTypeNames.push_back("PlayerRunState");
	mStateTypeNames.push_back("PlayerHammerHitState");
	mStateTypeNames.push_back("PlayerSuperHammerHitState");
	mStateTypeNames.push_back("PlayerHammerSpinState");
	mStateTypeNames.push_back("PlayerTackleState");
	mStateTypeNames.push_back("PlayerDeathState");

	// Global Enemy States
	mStateTypeNames.push_back("EnemyDirectionState");

	// Galtzagorri States
	mStateTypeNames.push_back("GaltzagorriIddleState");
	mStateTypeNames.push_back("GaltzagorriRunState");
	mStateTypeNames.push_back("GaltzagorriAttackState");
	mStateTypeNames.push_back("GaltzagorriDeathState");
	mStateTypeNames.push_back("Galtzagorri_HammerHit");
	mStateTypeNames.push_back("Galtzagorri_SuperHammerHit");
	mStateTypeNames.push_back("Galtzagorri_HammerSpin");
	mStateTypeNames.push_back("Galtzagorri_Tackle");

	// Lamia States
	mStateTypeNames.push_back("LamiaChaseState");
	mStateTypeNames.push_back("LamiaAttackState");
	mStateTypeNames.push_back("LamiaRangeState");
	mStateTypeNames.push_back("Lamia_HammerHit");
	mStateTypeNames.push_back("Lamia_SuperHammerHit");
	mStateTypeNames.push_back("Lamia_HammerSpin");

	// Sorgina States
	mStateTypeNames.push_back("SorginaIddleState");
	mStateTypeNames.push_back("SorginaAttackState");
	mStateTypeNames.push_back("SorginaAttackTargetState");
	mStateTypeNames.push_back("SorginaBufferState");

	// Companion States
	mStateTypeNames.push_back("CompanionMoveState");
	mStateTypeNames.push_back("CompanionHeal");
	mStateTypeNames.push_back("CompanionExplosion");

	// Combo States
	mStateTypeNames.push_back("ComboSys");;
	return true;
}

void StateManager::Shutdown()
{
	mActors.clear();
}

void StateManager::attach(Actor* obj)
{
	if (!obj)
		return;
	mActors.push_back(obj);
}
void StateManager::detach(Actor* obj)
{
	if (!obj)
		return;
	for (u32 i = 0; i < mActors.size(); i++)
	{
		if (mActors[i] == obj)
			mActors.erase(mActors.begin() + i);
	}
}

State * StateManager::CreateStateByStateTypeName(const std::string & stateType, const std::string & stateName)
{
	// Player States
	if (stateType == "PlayerIdleState")
		return DBG_NEW PlayerIdleState(stateName.c_str());
	if (stateType == "PlayerRunState")
		return DBG_NEW PlayerRunState(stateName.c_str());
	if (stateType == "PlayerHammerHitState")
		return DBG_NEW PlayerHammerHitState(stateName.c_str());
	if (stateType == "PlayerSuperHammerHitState")
		return DBG_NEW PlayerSuperHammerHitState(stateName.c_str());
	if (stateType == "PlayerTackleState")
		return DBG_NEW PlayerTackleState(stateName.c_str());
	if (stateType == "PlayerHammerSpinState")
		return DBG_NEW PlayerHammerSpinState(stateName.c_str());
	if (stateType == "PlayerDeathState")
		return DBG_NEW PlayerDeathState(stateName.c_str());

	// Global Enemy States
	if (stateType == "EnemyDirectionState")
		return DBG_NEW EnemyDirectionState(stateName.c_str());

	// Galtzagorri States
	if (stateType == "GaltzagorriIddleState")
		return DBG_NEW GaltzagorriIddleState(stateName.c_str());
	if (stateType == "GaltzagorriRunState")
		return DBG_NEW GaltzagorriRunState(stateName.c_str());
	if (stateType == "GaltzagorriAttackState")
		return DBG_NEW GaltzagorriAttackState(stateName.c_str());
	if (stateType == "GaltzagorriDeathState")
		return DBG_NEW GaltzagorriDeathState(stateName.c_str());
	if (stateType == "Galtzagorri_HammerHit")
		return DBG_NEW Galtzagorri_HammerHit(stateName.c_str());
	if (stateType == "Galtzagorri_SuperHammerHit")
		return DBG_NEW Galtzagorri_SuperHammerHit(stateName.c_str());
	if (stateType == "Galtzagorri_HammerSpin")
		return DBG_NEW Galtzagorri_HammerSpin(stateName.c_str());
	if (stateType == "Galtzagorri_Tackle")
		return DBG_NEW Galtzagorri_Tackle(stateName.c_str());

	// Lamia States
	if (stateType == "LamiaChaseState")
		return DBG_NEW LamiaChaseState(stateName.c_str());
	if (stateType == "LamiaAttackState")
		return DBG_NEW LamiaAttackState(stateName.c_str());
	if (stateType == "LamiaRangeState")
		return DBG_NEW LamiaRangeState(stateName.c_str());
	if (stateType == "Lamia_HammerHit")
		return DBG_NEW Lamia_HammerHit(stateName.c_str());
	if (stateType == "Lamia_SuperHammerHit")
		return DBG_NEW Lamia_SuperHammerHit(stateName.c_str());
	if (stateType == "Lamia_HammerSpin")
		return DBG_NEW Lamia_HammerSpin(stateName.c_str());

	// Sorgina States
	if (stateType == "SorginaIddleState")
		return DBG_NEW SorginaIddleState(stateName.c_str());
	if (stateType == "SorginaAttackState")
		return DBG_NEW SorginaAttackState(stateName.c_str());
	if (stateType == "SorginaAttackTargetState")
		return DBG_NEW SorginaAttackTargetState(stateName.c_str());
	if (stateType == "SorginaBufferState")
		return DBG_NEW SorginaBufferState(stateName.c_str());

	// Companion States
	if (stateType == "CompanionMoveState")
		return DBG_NEW CompanionMoveState(stateName.c_str());
	if (stateType == "CompanionHeal")
		return DBG_NEW CompanionHeal(stateName.c_str());
	if (stateType == "CompanionExplosion")
		return DBG_NEW CompanionExplosion(stateName.c_str());

	// Combo States
	if (stateType == "ComboSys")
		return DBG_NEW ComboSys(stateName.c_str());
	return nullptr;
}

void StateManager::Update()
{
	for (u32 i = 0; i < mActors.size(); ++i)
		mActors[i]->Update();
}
