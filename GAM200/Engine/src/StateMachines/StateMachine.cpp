
#include "State.h"
#include "Actor.h"
#include "StateMachine.h"
#include "../Level/Editor/ImGUI_Basics.h"
#include "../Level/Editor/ImGui/imgui.h"

#include "../../GamePlay/src/PlayerLogic/PlayerLogic.h"
#include "../../GamePlay/src/GaltxaLogic/GaltxagorriLogic.h"
#include "../../GamePlay/src/LamiaLogic/LamiaLogic.h"
#include "../../GamePlay/EnemyLogic.h"
#include "../../GamePlay/src/CompanionLogic/CompanionLogic.h"
#include "..//Input/InputEvent.h"
#include <stdlib.h>

using nlohmann::json;
// ----------------------------------------------------------------------------
// STATE MACHINE
// ----------------------------------------------------------------------------

StateMachine::StateMachine()
{
	// Sets all the values to NULL
	mActor = NULL;
	mCurrentState = NULL;
	mNextState = NULL;
	mInitialState = NULL;
}

StateMachine::StateMachine(Actor * actor)
{
	// Sets all the values to NULL, but the actor to the address passed as parameter
	mActor = actor;
	mCurrentState = NULL;
	mNextState = NULL;
	mInitialState = NULL;
}

//StateMachine::StateMachine(StateMachine & stmch)
//{
//	mActor = stmch.mActor;
//	mCurrentState = NULL;
//	mNextState = NULL;
//	mInitialState = NULL;
//}

StateMachine::~StateMachine()
{
	// Call clear
	Clear();
}

void StateMachine::AddState(State * state)
{
	// Check if 'state' is NULL
	if (state == NULL)
		return;

	// Check for duplicated name
	for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); ++it)
	{
		if ((*it)->mName == state->mName)
			return;
	}

	// Set the actor and the owner state machine
	state->mActor = mActor;
	state->mOwnerStateMachine = this;

	// Add the state to the vector
	mStates.push_back(state);
}

void StateMachine::RemoveState(State * state)
{
	// Sanity check for valid pointer
	if (state == NULL)
		return;

	// Remove the state from the list
	for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); ++it)
	{
		if ((*it) == state)
		{
			delete *it;
			mStates.erase(it);
			break;
		}
	}
}

void StateMachine::Clear()
{
	mCurrentState = NULL;	// New (part IV)
	mInitialState = NULL;	// New (part IV)
	mNextState = NULL;	// New (part IV)

	while (mStates.size() != 0)
	{
		delete mStates.back();
		mStates.pop_back();
	}

	mStates.clear(); // Frees mStates from iterations
}

State* StateMachine::GetState(const char * stateName)
{
	if (stateName == NULL)
		return NULL;

	// Iterate thorough all the states
	for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); ++it)
	{
		// Check if names are equal
		if ((*it)->mName == stateName)
			// If a match found, return the state
			return *it;
	}

	return NULL;
}

void StateMachine::SetInitState(State * state)
{
	// Iterate thorough all the states
	for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); ++it)
	{
		// Check if there is the state
		if ((*it) == state)
		{
			// If a match found, return the state
			mInitialState = state;

			// Once found and set, we can just return
			return;
		}
	}
}

void StateMachine::SetInitState(const char * stateName)
{
	// Set the Initial state to the one found (No need to check if the
	// state, becuase if it were not, GetState returns NULL, so we can just set
	// mInitialState with no problem)
	mInitialState = GetState(stateName);
}

void StateMachine::ChangeState(State * state)
{
	// Check if 'state' is NULL
	if (state == NULL)
		return;

	// Look if the state is contained in this state machine.
	for (std::vector<State*>::iterator it = mStates.begin(); it != mStates.end(); ++it)
	{
		// If a match id found
		if ((*it) == state)
		{
			// Set the next state
			mNextState = state;

			// Can just return here, our job is done
			return;
		}
	}
}

void StateMachine::ChangeState(const char * stateName)
{
	// Find the state that has said name
	State * tempNextState = GetState(stateName);

	// Check if the state has been found (aka check if not NULL)
	if (tempNextState != NULL)
	{
		// If calid pointer, set the state
		mNextState = tempNextState;
	}
}

void StateMachine::Update()
{
	// Update the state
	if (mCurrentState != NULL)
		mCurrentState->InternalUpdate();

	// Look for a change of state
	if (mCurrentState != mNextState)
	{
		// Call exit of the old state
		if (mCurrentState != NULL)
			mCurrentState->InternalExit();

		// Update which is the current state
		mCurrentState = mNextState;

		if (mCurrentState != NULL)
			// Call enter on the _new state
			mCurrentState->InternalEnter();
	}
}

void StateMachine::Reset()
{
	// Sanity check
	if (mInitialState != NULL)
	{
		if (mCurrentState != NULL)
			mCurrentState->InternalExit();

		// Call a state change to the initial one
		mCurrentState = mInitialState;
		mNextState = mInitialState;

		if (mCurrentState != NULL)
			mCurrentState->InternalEnter();
	}
}

void StateMachine::FromJson(nlohmann::json & j)
{
	// read state machine name
	std::string tmpName = j["Name"].get<std::string>();
	SetName(tmpName.c_str());

	// find the state array node
	auto it = j.find("States");
	if (it != j.end()) // found it
	{
		// get a refernce to the json node using the iterator 
		json &stateArrayNode = *it;

		// loop through all children nodes in the array
		for (auto it2 = stateArrayNode.begin();
			it2 != stateArrayNode.end(); ++it2)
		{
			// get arefence to the json node
			json & stateNode = *it2;

			// read the type
			tmpName = stateNode["_type"].get<std::string>();

			// create state using the state name
			State * newState = StMgr->CreateStateByStateTypeName(tmpName, tmpName);
			SuperState * sst = dynamic_cast<SuperState*>(newState);

			// add to the state machine
			if (sst == NULL)
				this->AddState(newState);
			else
			{
				this->AddState(sst);
				sst->FromJson(j);
			}

			// serialize
			newState->FromJson(stateNode);

			// make this machine the owner of the state
			newState->mOwnerStateMachine = this;

			if (j["CurrentStateName"].get<std::string>().c_str())
			{
				SetInitState(j["CurrentStateName"].get<std::string>().c_str());
				ChangeState(j["CurrentStateName"].get<std::string>().c_str());
			}
		}
	}
}

void StateMachine::ToJson(nlohmann::json & j)
{
	j["Name"] = GetName();
	if (mInitialState)
		j["CurrentStateName"] = mInitialState->GetType().GetName();

	// write the states as an array
	json &stateArrayNode = j["States"];
	for (u32 i = 0; i < mStates.size(); ++i)
	{
		// create json node
		json stateNode;

		// write the type of the state using RTTI
		stateNode["_type"] = mStates[i]->GetType().GetName();

		SuperState * sst = dynamic_cast<SuperState*>(mStates[i]);
		// write the state data into the json node
		mStates[i]->ToJson(stateNode);
		if (sst != NULL)
		{
			sst->ToJson(j);
		}

		// add the state json ndoe to the json array
		stateArrayNode.push_back(stateNode);
	}
}

StateMachine* StateMachine::Clone()
{
	StateMachine* machine = DBG_NEW StateMachine();
	machine->mName = mName;

	for (unsigned i = 0; i < mStates.size(); i++)
	{
		State* newState = mStates[i]->Clone();
		machine->AddState(newState);

		if (mStates[i] == mCurrentState)
			machine->mCurrentState = newState;

		if (mStates[i] == mNextState)
			machine->mNextState = newState;

		if (mStates[i] == mInitialState)
			machine->mInitialState = newState;
	}

	return machine;
}

int global_machine_counter = 1;
StateMachine * Actor::AddStateMachine()
{
	StateMachine *stmc = DBG_NEW StateMachine();
	mBrain.push_back(stmc);
	stmc->mActor = this;
	char /*name[2], */mname[20];
	strcpy_s(mname, sizeof mname, "Change Name");
	mBrain.back()->SetName(mname);
	return mBrain.back();
}