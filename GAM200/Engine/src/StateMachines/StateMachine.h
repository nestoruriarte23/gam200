#pragma once
#ifndef STATE_MACHINE_H_
#define STATE_MACHINE_H_


#include "../Components/Components.h"
#include "../../Engine/Engine.h"
#include "../../Engine/src/Game/Game.h"

//#include "StateManager.h"

#include <string>
#include <vector>
#include <typeinfo>

struct State;
struct SuperState;
struct Actor;

struct StateMachine : public IBase
{

	RTTI_DECLARATION_INHERITED(StateMachine, IBase);

	// ------------------------------------------------------------------------
	// DATA

	//! State container. This class owns the memory for the states and thus should free it when destroyed. 
	std::vector<State*> mStates;

	//! Pointer to the game object that owns the state machine.
	Actor * mActor;

	//! Pointer to the current active state in the state machine. 
	State * mCurrentState;

	//! Pointer to the next active state. Same as mCurrentState unless state change is requested. 
	State * mNextState;

	//! Pointer to the initial state of the state machine. This is the active state when the state  machine is reset. 
	State * mInitialState;

	// ------------------------------------------------------------------------
	// METHODS

	StateMachine();
	StateMachine(Actor * actor);
	~StateMachine();

	//
	// CONTAINER MANAGEMENT
	//

	void AddState(State * state);
	void RemoveState(State * state);
	void Clear();
	State* GetState(const char * stateName);
	void SetInitState(State * state);
	void SetInitState(const char * stateName);
	void ChangeState(State * state);
	void ChangeState(const char * stateName);
	void Update();
	void Reset();

	virtual StateMachine* Clone();

	virtual void FromJson(nlohmann::json & j);
	virtual void ToJson(nlohmann::json & j);

};

#endif