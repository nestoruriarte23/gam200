#include "../Level/Editor/ImGUI_Basics.h"
#include "../Level/Editor/ImGui/imgui.h"
#include "..\Serializer\SerializeFactory.h"

using nlohmann::json;
#include "Actor.h"


// ----------------------------------------------------------------------------
// ACTOR
// ----------------------------------------------------------------------------

// Helper Global variables for the Add State buttons
bool addClicked = false;
bool addSuperClicked = false;
Actor::Actor() :IComp()
{
	// add default state machine
	AddStateMachine();
	mBrain[0]->SetName("Default State Machine");
	StMgr->attach(this);
}

Actor::Actor(Actor& actor) {

	// remove all state machines
	while (mBrain.size() != 0)
	{
		delete mBrain.back();
		mBrain.pop_back();
	}
	mBrain.clear();

	// find the state array node
	auto it = actor.mBrain.begin();
	if (it != actor.mBrain.end()) // found it
	{
		// get a refernce to the json node using the iterator 
		// loop through all children nodes in the array
		for (auto it2 = actor.mBrain.begin();
			it2 != actor.mBrain.end(); ++it2)
		{

			//add _new state machine
			//std::cout << "DEBUG SM : " << this << std::endl;
			StateMachine *machine = (*it2)->Clone();

			machine->mActor = this;

			mBrain.push_back(machine);
		}
	}
}

Actor::~Actor()
{
	while (mBrain.size() != 0)
	{
		delete mBrain.back();
		mBrain.pop_back();
	}
}

void Actor::Initialize()
{
	//mBrain[0]->mInitialState->LogicEnter();
}

void Actor::Shutdown()
{
	StMgr->detach(this);
}

void Actor::Update()
{
	for (u32 i = 0; i < mBrain.size(); ++i)
		mBrain[i]->Update();
}

/******************************************************************************************
	Helper function to avoid repeating code on the 'Add State' button for adding states
	to StateMachines or SuperStates. They should do mostly the same, but add the states
	to the correct given statemachines.
******************************************************************************************/
void Actor::AddState(StateMachine * stmch, bool super)
{

	//if ((addClicked && !super) || (addSuperClicked && super))
	//{
	//	
	//		if (!super)
	//			addClicked = false;
	//		else
	//			addSuperClicked = false;
	//	}
	//}

	//if (ImGui::Button("Add State")) {
	//	if (!super)
	//		addClicked = true;
	//	else
	//		addSuperClicked = true;
	//}
}


/******************************************************************************************
	Helper function to avoid repeating code while printing all the states of a StateMachine
	or a SuperState.
******************************************************************************************/


namespace internal
{
	bool new_state_popup_opened = false;
	StateMachine * new_state_selected_stmch = 0;

	bool NewStatePopup()
	{
		bool changed = false;
		if (new_state_popup_opened) {
			ImGui::OpenPopup("New State Popup");
			new_state_popup_opened = false;
		}

		if (ImGui::BeginPopupModal("New State Popup")) {

			// filter to choose the state
			static ImGuiTextFilter filter;
			filter.Draw();

			// show a list of the registered states in the state manager
			std::vector<std::string>& stateNames = StMgr->mStateTypeNames;
			for (unsigned int i = 0; i < stateNames.size(); ++i)
			{
				// if passes filter, show
				if (filter.PassFilter(stateNames[i].c_str()))
				{
					ImGui::Selectable(stateNames[i].c_str());
					if (ImGui::IsItemClicked())
					{

						// state is selected add by state name and close popup
						State *state = StMgr->CreateStateByStateTypeName(stateNames[i], stateNames[i]);
						new_state_selected_stmch->AddState(state);
						changed = true;
						ImGui::CloseCurrentPopup();
					}
				}
			}

			ImGui::EndPopup();
		}
		return changed;
	}

	void PrintStateList(StateMachine * stmch)
	{
		for (unsigned i = 0; i < stmch->mStates.size(); i++)
		{
			ImGui::PushID(stmch->mStates[i]->mName.c_str());
			SuperState * sst = dynamic_cast<SuperState*>(stmch->mStates[i]);
			if (sst == NULL)
			{
				ImGui::LabelText("", stmch->mStates[i]->mName.c_str());
				ImGui::SameLine();
				if (ImGui::Button("Remove") && stmch->mStates.size() != 1) {
					stmch->RemoveState(stmch->mStates[i]);
				}
				else
				{
					stmch->mStates[i]->Edit();
					stmch->mStates[i]->Selected();
				}
			}
			else
			{
				if (ImGui::CollapsingHeader(stmch->mStates[i]->mName.c_str())) // SuperState name
				{
					ImGui::Indent();
					if (ImGui::SmallButton("+")) {
						internal::new_state_popup_opened = true;
						internal::new_state_selected_stmch = &sst->mInternalStateMachine;
					}
					PrintStateList(&sst->mInternalStateMachine);
					if (ImGui::Button("Remove SuperState", ImVec2(ImGui::GetWindowSize().x*0.5f, 0.0f)) && stmch->mStates.size() != 1) {
						stmch->RemoveState(stmch->mStates[i]);
					}
					ImGui::Unindent();
				}
			}
			ImGui::PopID();
		}
	}

	void ShowStateMachineGui(std::vector<StateMachine*> * brain, u32 counter, StateMachine * stmch)
	{
		// Pushing and ID to differentiate between the 
		//ImGui::PushID("StateMachine");

		// user wants to add a state
		if (ImGui::SmallButton("+")) {
			internal::new_state_popup_opened = true;
			internal::new_state_selected_stmch = stmch;
		}
		ImGui::SameLine();
		if (ImGui::TreeNode(stmch->GetName()))
		{
			// show initial state
			if (stmch->mStates.size() != 0)
			{
				int initialIndex = 0;// mStates.size() - 1;
				std::vector<const char *> trueStateNames;
				for (unsigned i = 0; i < stmch->mStates.size(); i++) {
					trueStateNames.push_back(stmch->mStates[i]->mName.c_str());
					if (stmch->mStates[i] == stmch->mInitialState)
						initialIndex = i;
				}

				const char* const* InitialStateArray = &(trueStateNames[0]);
				if (ImGui::Combo("Initial State", &initialIndex, InitialStateArray, (int)stmch->mStates.size(), 6)) {
					stmch->SetInitState(InitialStateArray[initialIndex]);
				}
				if (stmch->mStates.size() == 1)
					stmch->SetInitState(stmch->mStates[0]);
			}

			// show the states
			ImGui::Text("States"); ImGui::SameLine();
			ImGui::Separator();
			PrintStateList(stmch);

			ImGui::TreePop();
		}
		// user wants to delete the state machine
		if (ImGui::SmallButton("x") && brain->size() > 1)
		{
			brain->erase(brain->begin() + counter);
		}
		ImGui::SameLine();
		ImGui::Text("Remove State Machine");
	}
} // namespace internal

bool Actor::Edit()
{
	bool changed = false;
	ImGui::PushID("Actor");

	if (ImGui::TreeNode("Actor"))
	{
		for (u32 i = 0; i < mBrain.size(); ++i)
		{
			char smname[40], counter[2];
			_itoa_s(i, counter, sizeof counter, 10);
			strcpy_s(smname, sizeof smname, "State Machine ");
			strcat_s(smname, sizeof smname, counter);

			ImGui::PushID(smname);

			char * tempName[20] = { const_cast<char *>(mBrain[i]->GetName()) };
			ImGui::InputText("", *tempName, IM_ARRAYSIZE(tempName));
			ImGui::SameLine();

			if (ImGui::Button("Change Name")) {
				if (tempName != NULL && tempName[0] != '\0') {
					mBrain[i]->SetName(*tempName);
					changed = true;
				}
			}
			char temp[40];
			strcpy_s(temp, sizeof temp, mBrain[i]->GetName());
			strcpy_s(smname, sizeof smname, temp);

			bool expanded = ImGui::CollapsingHeader(smname);
			if (expanded)
			{
				//ImGui::PushID("");
				internal::ShowStateMachineGui(&mBrain, i, mBrain[i]);
			}
			ImGui::PopID();
		}

		if (ImGui::Button("Add State Machine"))
		{
			// do something
			AddStateMachine();
		}

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}

	ImGui::PopID();

	changed = internal::NewStatePopup() || changed;
	//ImGui::DragFloat2("Collider Position", mPosition.v);

	return changed;
}

void Actor::FromJson(nlohmann::json & j) {

	// remove all state machines
	while (mBrain.size() != 0)
	{
		delete mBrain.back();
		mBrain.pop_back();
	}
	mBrain.clear();

	// find the state array node
	auto it = j.find("State Machines");
	if (it != j.end()) // found it
	{
		// get a refernce to the json node using the iterator 
		json &smArrayNode = *it;

		// loop through all children nodes in the array
		for (auto it2 = smArrayNode.begin();
			it2 != smArrayNode.end(); ++it2)
		{
			// get arefence to the json node
			json & smNode = *it2;

			//add _new state machine
			//std::cout << "DEBUG SM : " << this << std::endl;
			StateMachine *machine = DBG_NEW StateMachine(this);
			mBrain.push_back(machine);

			// serialize
			mBrain.back()->FromJson(smNode);
		}
	}
}

void Actor::ToJson(nlohmann::json & j) {


	j["_type"] = "Actor";

	// write the states as an array
	json &smArrayNode = j["State Machines"];
	for (u32 i = 0; i < mBrain.size(); ++i)
	{
		// create json node
		json smNode;

		// write the state data into the json node
		mBrain[i]->ToJson(smNode);

		// add the state json ndoe to the json array
		smArrayNode.push_back(smNode);
	}
}

void Actor::operator=(IComp& _comp) {


	//TODO

	Actor & actor = *dynamic_cast<Actor*>(&_comp);

	// remove all state machines
	while (mBrain.size() != 0)
	{
		delete mBrain.back();
		mBrain.pop_back();
	}
	mBrain.clear();

	// find the state array node
	auto it = actor.mBrain.begin();
	if (it != actor.mBrain.end()) // found it
	{
		// get a refernce to the json node using the iterator 
		// loop through all children nodes in the array
		for (auto it2 = actor.mBrain.begin();
			it2 != actor.mBrain.end(); ++it2)
		{

			//add _new state machine
			//std::cout << "DEBUG SM : " << this << std::endl;
			StateMachine *machine = (*it2)->Clone();

			machine->mActor = this;

			mBrain.push_back(machine);
		}
	}
}

IComp * Actor::Clone()
{
	return DBG_NEW Actor(*this);
}

bool Actor::are_the_same_type(IComp const& lhs) {
	if (dynamic_cast<const Actor*>(&lhs)) {
		return true;
	}
	return false;
}

bool Actor::equal_to(IComp const& other) const {
	if (Actor const* p = dynamic_cast<Actor const*>(&other)) {
		return mBrain == p->mBrain;
	}
	else {
		return false;
	}
}

