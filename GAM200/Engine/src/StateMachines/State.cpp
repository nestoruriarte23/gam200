#include "../Level/Editor/ImGUI_Basics.h"
#include "../Level/Editor/ImGui/imgui.h"


#include "State.h"


// ----------------------------------------------------------------------------
// STATE
// ----------------------------------------------------------------------------


State::State(const char * name)
{
	// Set the name
	if (name == NULL)
		mName = "unnamed";
	else
		mName = name;

	// Set the rest of the variables to NULL
	mOwnerStateMachine = NULL;
	mActor = NULL;
	mTimeInState = 0.0f;

	//StMgr->attach(this);
}

State::State(const State& st)
{
	mName = st.mName;
	mOwnerStateMachine = NULL;
	mActor = NULL;
	mTimeInState = 0.0f;
}

void State::InternalEnter()
{
	// Initialize the timer
	mTimeInState = 0.0f;

	// Call Logic Enter
	LogicEnter();
}

void State::InternalExit()
{
	// Call Logic Exit
	LogicExit();
}

void State::InternalUpdate()
{
	// Update the timer
	mTimeInState += static_cast<f32>(TimeManager::Instance()->GetDt());

	// Call Logic Update()
	LogicUpdate();
}


// ----------------------------------------------------------------------------
// SUPER STATE (STATE AS A STATE MACHINE)
// ----------------------------------------------------------------------------

SuperState::SuperState(const char * name, Actor * actor) :State(name)
{
	// Set the actor of the internal state machine
	mInternalStateMachine.mActor = actor;

	// Also the actor of this state
	mActor = actor;
}

void SuperState::InternalEnter()
{
	// Iterate thorugh all the Internal states
	for (std::vector<State*>::iterator it = mInternalStateMachine.mStates.begin();
		it != mInternalStateMachine.mStates.end(); ++it)
	{
		// Set the actor
		(*it)->mActor = mInternalStateMachine.mActor;
	}

	// Reset the internal state machine
	mInternalStateMachine.Reset();

	// Call the internal Enter of the state
	State::InternalEnter();
}

void SuperState::InternalUpdate()
{
	// Update the internal Machine
	mInternalStateMachine.Update();

	// Call the internal Update
	State::InternalUpdate();

	// Look for a change of state
	if (mInternalStateMachine.mCurrentState != mInternalStateMachine.mNextState)
	{
		// Call exit of the old state
		if (mInternalStateMachine.mCurrentState != NULL)
			mInternalStateMachine.mCurrentState->InternalExit();

		// Update which is the current state
		mInternalStateMachine.mCurrentState = mInternalStateMachine.mNextState;

		if (mInternalStateMachine.mCurrentState != NULL)
			// Call enter on the _new state
			mInternalStateMachine.mCurrentState->InternalEnter();
	}
}