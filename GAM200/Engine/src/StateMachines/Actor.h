#pragma once
#ifndef ACTOR_H_
#define ACTOR_H_ 

#include "../Components/Components.h"
#include "../../Engine/Engine.h"
#include "../../Engine/src/Game/Game.h"

#include "StateMachine.h"

struct Actor : public IComp
{

	RTTI_DECLARATION_INHERITED(Actor, IComp);

	//! Use a vector to support concurrent state machine
	std::vector<StateMachine*>  mBrain;

	/*
	TODO
	For the correct way to call ToJson and FromJson, each object should have its own Actor component
	They should have Actors that are children of the main actor and that in the json files, they have hard
	coded their statemachines, such as their states are hardcoded for each object individualy
	It may be necessary to even hardcode their states, which seems crazy to me

	*/

	//! Adds one state machine to be used by default.
	Actor();
	Actor(Actor & actor);
	~Actor();
	void Initialize() override;
	void Shutdown() override;

	//! Updates all state machine sequentially.
	void Update() override;

	void AddState(StateMachine * stmch, bool super);
	bool Edit();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	IComp * Clone();

	StateMachine* AddStateMachine();

	// @SUGGESTION
	/*! Add extra functionality to easily manage the actor's state machines
		and their respective states. For example, the following functions could be
		useful:

		State * GetState(stateMachineName, stateName);
		State * AddState(stateMachineName, newState);
		void  ChangeState(stateMachineName, nextState);

		State * AddState(newState); // adds to default state machine.
		State * GetState(stateName); // gets state from default state machine.
		boid ChangeState(nextState); // changes state in default state machine.

		etc...
	*/

	virtual bool are_the_same_type(IComp const& lhs);

	virtual void operator=(IComp & _comp);



protected:

	virtual bool equal_to(IComp const& other) const;
};


#endif