#pragma once
#ifndef STATE__H_
#define STATE__H_

#include "../Components/Components.h"
#include "../../Engine/Engine.h"
#include "../../Engine/src/Game/Game.h"

#include "StateMachine.h"

#include <string>

struct State
{
	RTTI_DECLARATION_BASECLASS(State);

	// ------------------------------------------------------------------------
	// DATA

	//! Name of the state.
	std::string mName;
	//! Pointer to the state machine that owns this state.
	StateMachine * mOwnerStateMachine;
	//! Pointer to the game object that owns the state machine.
	Actor * mActor;
	//! Specifies how long this state has been active.
	float mTimeInState;

	// ------------------------------------------------------------------------
	// METHODS

	State(const char * name);
	State(const State& st);
	virtual ~State() {}
	virtual void LogicEnter() {}
	virtual void LogicExit() {}
	virtual void LogicUpdate() {}
	virtual void InternalEnter();
	virtual void InternalExit();
	virtual void InternalUpdate();

	virtual void Edit() {}
	virtual void Selected() {}
	virtual void FromJson(nlohmann::json & j) {}
	virtual void ToJson(nlohmann::json & j) {}
	virtual State* Clone() { return nullptr; }

};



struct SuperState : public State
{
	// ------------------------------------------------------------------------
	// DATA

	//! Internal state machine. InternalUpdate calls Update
	StateMachine mInternalStateMachine;

	// ------------------------------------------------------------------------
	// METHODS

	SuperState(const char * name, Actor * actor);
	virtual void InternalEnter();
	virtual void InternalUpdate();

	virtual void FromJson(nlohmann::json & j) {}
	virtual void ToJson(nlohmann::json & j) {}
};

#endif