#pragma once
#ifndef STATE_MANAGER_H_
#define STATE_MANAGER_H_

#include <string>

#include "../Components/Components.h"
#include "../../Engine/Engine.h"
#include "../../Engine/src/Game/Game.h"

#include "Actor.h"

class StateManager : public ISystem
{
	MAKE_SINGLETON(StateManager);
	RTTI_DECLARATION_INHERITED(StateManager, ISystem);

public:
	// Member Variables
	std::vector<Actor*> mActors;

	virtual bool Initialize();
	virtual void Shutdown();
	virtual void Update();

	// Actor Management
	void attach(Actor* obj);
	void detach(Actor* obj);

	// HELPER FOR EDITOR
	// function to create all the possible states we have in the game. 
	// hack for now, later it should use the factory
	std::vector<std::string>mStateTypeNames; // for editor to display combo
	State * CreateStateByStateTypeName(const std::string & stateType, const std::string & stateName);

};
#define StMgr (StateManager::Instance())


#endif