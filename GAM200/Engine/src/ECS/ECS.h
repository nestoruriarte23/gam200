/*#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>
#include <bitset>
#include <array>

class Component;
class Entity;

using Component_ID = std::size_t;

inline Component_ID GetComponentTypeID()
{
	static Component_ID lastID = 0;
	return lastID++;
}

template <typename T> inline Component_ID GetComponentTypeID() noexcept
{
	static Component_ID typeID = GetComponentTypeID();
	return typeID;
}

constexpr std::size_t maxComponents = 32;

using ComponentBitSet = std::bitset<maxComponents>;
using ComponentArray = std::array<Component*, maxComponents>;

class Component
{
	public:
		Entity* entity;

		virtual void Init() {}
		virtual void Update() {}
		virtual void Draw() {}

		virtual ~Component() {}

};

class Entity
{
	private:
		bool active = true;
		std::vector<std::unique_ptr<Component>> components;

		ComponentArray componentsArray;
		ComponentBitSet componentBitSet;

	public:

		void Update();
		void Draw();
		bool IsActive() { return active; }
		void Destroy() { active = false; }

		template <typename T>
		bool HasComponents() const;


		template <typename T, typename...TArgs>
		T& AddComponent(TArgs&&... mArgs);

		template <typename T>
		T& GetComponent() const;

};

template<typename T>
inline bool Entity::HasComponents() const
{
	return componentBitSet[GetComponentTypeID<T>];
}

template<typename T, typename ...TArgs>
inline T & Entity::AddComponent(TArgs && ...mArgs)
{
	T* comp(new T(std::forward<TArgs>(mArgs)...));
	comp->entity = this;
	std::unique_ptr<Component> uPtr{ comp };
	components.emplace_back(std::move(uPtr));

	componentsArray[GetComponentTypeID<T>()] = comp;
	componentBitSet[GetComponentTypeID<T>()] = true;

	comp->Init();
	return *comp;

}

template<typename T>
inline T & Entity::GetComponent() const
{
	auto ptr(componentsArray[GetComponentTypeID<T>()]);

	return *static_cast<T*>(ptr);
}

class Manager
{
	private:
		std::vector<std::unique_ptr<Entity>> entities;

	public:

		void Update();
		void Draw();

		void Refresh();
		Entity& AddEntity();
		
};
*/