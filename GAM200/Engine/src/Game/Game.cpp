#include <ctime>
#include <cstdlib>

#include "Game.h"
#include "../System/GameObjectManager/GameObject.h"
#include "../Level/Scene/Scene.h"
#include "../Level/Space/Space.h"
#include "../window/Window.h"

GameClass::GameClass()
{
	srand(static_cast <unsigned> (time(0)));
	mWindow = DBG_NEW Window();
}

GameClass::~GameClass()
{
	delete mWindow;
}

Window * GameClass::GetWindow()
{
	return mWindow;
}

SDL_Window * GameClass::GetSDLWindow()
{ 
	return mWindow->GetSDLWindow();
}

Vector2 GameClass::GetWindowSize()
{
	return mWindow->GetWindowSize();
}

Vector2 GameClass::GetStartingWindowSize()
{
	return mWindow->GetStartingWindowSize();
}

void GameClass::ChangeResolution(int w, int h)
{
	mWindow->ChangeResolution(w, h);
}

void GameClass::ToogleFullscreen()
{
	mWindow->ToogleFullscreen();
}

void GameClass::SetScene(Scene * _scene)
{
	mCurrentScene = _scene;
}

Scene * GameClass::GetCurrScene()
{
	return mCurrentScene;
}

void GameClass::DestroyObject(GameObject * _go)
{
	/* Esto lo quita de la lista */
	mCurrentScene->FindSpace(_go->mSceneSpace.data())->DeleteObject(_go->GetUID());

	/* Y esto lo borra */
	//delete _go;
}

void GameClass::CreateObject(GameObject* obj)
{
	if (mCurrentScene->GetDefault()) {
		mCurrentScene->GetDefault()->AddObject(*obj);
	} else {
		mCurrentScene->scene_spaces[0]->AddObject(*obj);
	}
	//obj->Initialize();
}
void GameClass::AddObjectInSpace(GameObject* obj)
{
	obj->GetSpace()->AddObject(*obj);
	//obj->Initialize();
}

/*! Assumes Archetype file has already the correct space set */
GameObject* GameClass::CreateArchetype(std::string _archetype_name)
{
	GameObject * _new_go = DBG_NEW GameObject();

	serializer->LoadArchetype(_archetype_name.data(), _new_go);

	_new_go->GetSpace()->AddObject(*_new_go);
	_new_go->Initialize();

	return _new_go;
}