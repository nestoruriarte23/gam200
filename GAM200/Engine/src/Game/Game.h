#pragma once

#include "../Components/System.h"
#include "../Utilities/Math/Vector2.h"

class GameObject;
struct Scene;

class Window;
struct SDL_Window;

class GameClass : public ISystem
{
	MAKE_SINGLETON(GameClass);

public:
	~GameClass();

	Window*					GetWindow();
	SDL_Window*				GetSDLWindow();
	Vector2					GetWindowSize();
	Vector2					GetStartingWindowSize();
	void					ChangeResolution(int w, int h);
	void					ToogleFullscreen();

	void					SetScene(Scene* _scene);
	Scene*					GetCurrScene();

	void					DestroyObject(GameObject* _go);
	void					CreateObject(GameObject* obj);
	void					AddObjectInSpace(GameObject* obj);
	GameObject*				CreateArchetype(std::string _archetype_name);

	bool					onEditor;
	bool					onTitle = false;
	int						mCurrentFrame = 0;

private:
	Window*					mWindow;
	Scene*					mCurrentScene;
};

#define Game (GameClass::Instance())

