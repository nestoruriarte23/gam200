﻿#pragma once

#include <string>
#include <list>
#include "../Components/Components.h"
#include "../System/GameObjectManager/GameObject.h"
#include "../../Engine/src/Level/Editor/ImGUI_Basics.h"
#include "../../Engine/src/Level/Editor/ImGui/imgui.h"
#include "../Graphics/RenderableComp.h"
#include "../Utilities/Math/MyMath.h"

#define MAX_VOICE_COUNT 100	// ! CANNOT BE ZERO

namespace FMOD
{
	class Sound;
	class Channel;
	class System;
}

struct Sound
{
	FMOD::Sound		*pSound;
	std::string		filename;

	Sound();
};

class Voice
{
	// friends have access to privates
	friend class AudioManager;
public:
	// Constructors/Destructor
	Voice();
	~Voice();

	// Setters/Getters
	void SetVolume(float vol);
	void SetPause(bool paused);
	void SetLoop(bool isLoop);
	bool IsPaused();
	bool IsValid();
	bool IsLooping();
	float GetVolume();

	// STL
	typedef std::list<Voice *> PTR_LIST;
private:
	FMOD::Channel	*pChannel;
	float			volume;
	bool			isLooping;
	bool			isPaused;
};

class SoundEmitter : public IComp
{
	RTTI_DECLARATION_INHERITED(SoundEmitter, IComp);

	friend class AudioManager;

public:
	SoundEmitter();
	~SoundEmitter();
	SoundEmitter(SoundEmitter & emitter) : voice(emitter.voice), isLooping(emitter.isLooping),
		isPaused(emitter.isPaused), cue(emitter.cue), sfx(emitter.sfx) {}
	SoundEmitter(Sound * sound);

	void SetVoiceVariables();

	bool Edit();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);

	void operator=(IComp&);

	bool are_the_same_type(IComp const& lhs) override;
	IComp * Clone();

	void PlayCue(bool paused = false, bool lopp = false);
	void SetCue(std::string new_cue);

protected:
	bool equal_to(IComp const& other) const override;

private:
	Voice *			voice;
	float			volume;
	bool			isLooping;
	bool			isPaused;
	bool			play;

	Sound * cue;
	bool sfx;
};

class SoundListener : public IComp
{

	RTTI_DECLARATION_INHERITED(SoundListener, IComp);

public:
	SoundListener();
	~SoundListener();
	SoundListener(float max, float min);

	// Getters and Setters
	float GetMaxLimit();
	float GetMinLimit();
	float GetRate();
	void SetMaxLimit(float limit);
	void SetMinLimit(float limit);

	bool Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	void operator=(IComp&);

	bool are_the_same_type(IComp const& lhs) override;

	IComp * Clone();

protected:
	virtual bool equal_to(IComp const& other) const;

private:
	float maxVolLimit; // In this limit forward (< maxVolLimit) vol is 1
	float minVolLimit; // In this limit forward (> minVolLimit) vol is 0
	float rate; // Distance between maxVolLimit and minVolLimit
};

class AudioManager : public IBase
{
	MAKE_SINGLETON(AudioManager);
	RTTI_DECLARATION_INHERITED(AudioManager, IBase);

	friend class SoundEmitter;
public:
	~AudioManager();

	bool Initialize();
	void Update();
	void Shutdown();
	void PlaySounds();

	// Sound Management
	Sound * CreateSound(std::string filename);
	void	FreeSound(Sound * pSound);
	Voice * Play(Sound * pSound, bool paused = false);
	Voice * Loop(Sound * pSound, bool paused = false);

	FMOD::System * GetFMOD();
	void AddSound(Sound * pSound);
	void RemoveSound(Sound * pSound);

	// Stop All
	void StopAll();
	void FreeThisVoice(Voice *pVoice);

	// Logic of lists
	void attach(SoundEmitter* emitter);
	void attach(SoundListener* listener);
	void reattach(SoundEmitter* emitter);
	void reattach(SoundListener* listener);
	void deattach(SoundEmitter* emitter);
	void deattach(SoundListener* listener);

private:

	// Helper methods
	void						AllocVoices();
	void						FreeVoices();

	// Voice manager... adds to free and removes from used
	Voice *						GetFreeVoice();

	// FMOD System
	FMOD::System*				pFMOD = NULL;

	// keep track of the sounds created 
	unsigned int				soundCount = 0;
	unsigned int				voiceCount = MAX_VOICE_COUNT;

	// Storing sound info for the component
	std::vector<Sound*>				soundList;
	std::vector<std::string>		namesList;

	// SoundEmitter and SoundListener lists
	std::list<SoundEmitter*>	emitterlist;
	std::list<SoundListener*>	listenerlist;

	// Volumes of different types of voices
	float defaultVolume;
	float sfxVolume;
	float globalVolume;

	// Voice Management List (equivalent to dead list and free list)
	Voice *						voices = 0;
	Voice::PTR_LIST				freeVoices;
	Voice::PTR_LIST				usedVoices;

};

// GLOBAL Audio Manager
#define gAudioMgr (AudioManager::Instance())