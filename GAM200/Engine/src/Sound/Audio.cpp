﻿#include "Audio.h"
#include "..//System/GameObjectManager/GameObject.h"
#include "../Graphics/RenderManager.h"
#include "../Graphics/Color.h"
#include <iostream>
#include <fmod.hpp>

#include "../Utilities/MyDebug/LeakDetection.h"

#define MAX_VOICE_COUNT 100	// ! CANNOT BE ZERO

// Sound class functions
#pragma region
Sound::Sound() : pSound(NULL) {}
#pragma endregion

// Voice class functions
#pragma region

Voice::Voice() : pChannel(NULL), isLooping(false), isPaused(true), volume(1.0f) {}
Voice::~Voice() {}

void Voice::SetVolume(float vol)
{
	if (pChannel == NULL)
		return;

	volume = vol;

	if (volume < 0.0f)
		volume = 0.0f;
	else if (volume > 1.0f)
		volume = 1.0f;

	pChannel->setVolume(volume);
}

float Voice::GetVolume() { return volume; }

void Voice::SetPause(bool paused)
{
	if (pChannel == NULL)
		return;
	isPaused = paused;
	pChannel->setPaused(paused);
}

bool Voice::IsPaused() { return isPaused; }

void Voice::SetLoop(bool isLoop)
{
	if (pChannel == NULL)
		return;

	isLooping = isLoop;
	if (isLooping)
		pChannel->setLoopCount(-1);
	else
		pChannel->setLoopCount(0);
}

bool Voice::IsValid()
{
	if (pChannel == NULL)
		return false;

	bool pl;
	pChannel->isPlaying(&pl);
	return pl;
}

bool Voice::IsLooping() { return isLooping; }

#pragma endregion

// SoundEmitter class component functions
#pragma region

SoundEmitter::SoundEmitter() : voice(0), cue(0), volume(1.0f), isPaused(0), isLooping(0), sfx(0)
{
	gAudioMgr->attach(this);
	voice = gAudioMgr->GetFreeVoice();
}

SoundEmitter::~SoundEmitter()
{
	gAudioMgr->deattach(this);
}

SoundEmitter::SoundEmitter(Sound* sound) : voice(0), cue(sound), volume(1.0f), isPaused(0), isLooping(0), sfx(0) {}

void SoundEmitter::SetVoiceVariables()
{
	//*voice = Voice();
	voice->SetVolume(volume);
	voice->SetLoop(isLooping);
	voice->SetPause(isPaused);
}

bool SoundEmitter::Edit()
{
	bool changed = false;
	ImGui::PushID("SoundEmitter");

	if (ImGui::TreeNode("SoundEmitter"))
	{
		//std::list<SoundEmitter*> test = gAudioMgr->emitterlist;

		int soundIndex = 0;
		std::vector<std::string> sounds = gAudioMgr->namesList;
		const char ** soundNames = DBG_NEW const char*[sounds.size()];
		for (unsigned i = 0; i < sounds.size(); ++i) {
			soundNames[i] = sounds[i].c_str();
		}
		if (ImGui::Combo(" Sound", &soundIndex, soundNames, (int)sounds.size(), 6)) {
			cue = gAudioMgr->soundList[soundIndex];
			voice = gAudioMgr->GetFreeVoice();
			changed = true;
		}
		if (ImGui::DragFloat(" Volume", &volume))
		{
			if (volume < 0.0f)
				volume = 0.0f;
			if (volume > 1.0f)
				volume = 1.0f;
			changed = true;
		}
		changed = ImGui::Checkbox("Play", &play) || changed;
		ImGui::SameLine();
		changed = ImGui::Checkbox("Loop", &isLooping) || changed;
		ImGui::SameLine();
		changed = ImGui::Checkbox("SFX", &sfx) || changed;

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}

	ImGui::PopID();

	return changed;
}

void SoundEmitter::PlayCue(bool paused, bool loop)
{
	if (!loop)
		voice = gAudioMgr->Play(cue, paused);
	else
		voice = gAudioMgr->Loop(cue, paused);
}
void SoundEmitter::SetCue(std::string new_cue)
{
	if (new_cue == cue->filename)
		return;
	for (unsigned i = 0; i < gAudioMgr->soundCount; i++)
	{
		if (new_cue == gAudioMgr->soundList[i]->filename)
		{
			gAudioMgr->FreeThisVoice(voice);
			cue = gAudioMgr->soundList[i];
			voice = gAudioMgr->GetFreeVoice();
			return;
		}
	}
}

void SoundEmitter::FromJson(nlohmann::json & _j)
{
	//voice = _j["voice"];
	volume = _j["volume"];
	isLooping = _j["isLooping"];
	isPaused = _j["isPaused"];
	play = _j["play"];
	for (unsigned i = 0; i < gAudioMgr->namesList.size(); i++)
		if (gAudioMgr->namesList[i] == _j["cue"]["filename"])
			cue = gAudioMgr->soundList[i];
	sfx = _j["sfx"];
}

void SoundEmitter::ToJson(nlohmann::json & _j)
{
	//_j["SoundEmitter"]["voice"] = voice;
	_j["volume"] = volume;
	_j["isLooping"] = isLooping;
	_j["isPaused"] = isPaused;
	_j["cue"]["filename"];
	if (cue) {
		_j["cue"]["filename"] = cue->filename;
	}

	_j["play"] = play;
	_j["sfx"] = sfx;

	_j["_type"] = "SoundEmitter";
}

void SoundEmitter::operator=(IComp& _comp) {
	SoundEmitter & emitter = *dynamic_cast<SoundEmitter*>(&_comp);

	volume = emitter.volume;
	isLooping = emitter.isLooping;
	isPaused = emitter.isPaused;
	for (unsigned i = 0; i < gAudioMgr->namesList.size(); i++)
		if (gAudioMgr->namesList[i] == emitter.cue->filename)
			cue = gAudioMgr->soundList[i];
	sfx = emitter.sfx;
}

bool SoundEmitter::are_the_same_type(IComp const& lhs) {
	if (dynamic_cast<const SoundEmitter*>(&lhs)) {
		return true;
	}
	return false;
}

IComp * SoundEmitter::Clone() { return DBG_NEW SoundEmitter(*this); }

bool SoundEmitter::equal_to(IComp const& other) const {
	if (SoundEmitter const* p = dynamic_cast<SoundEmitter const*>(&other)) {
		//TODO
		return true;
	}
	else {
		return false;
	}
}


#pragma endregion

// SoundListener class component functions
#pragma region

SoundListener::SoundListener() : maxVolLimit(500), minVolLimit(100), rate(maxVolLimit - minVolLimit)
{
	gAudioMgr->attach(this);
}

SoundListener::SoundListener(float max, float min) : maxVolLimit(max), minVolLimit(min), rate(max - min)
{
	gAudioMgr->attach(this);
}

SoundListener::~SoundListener()
{
	gAudioMgr->deattach(this);
}

float SoundListener::GetMaxLimit() { return maxVolLimit; }

float SoundListener::GetMinLimit() { return minVolLimit; }

float SoundListener::GetRate() { return rate; }

void SoundListener::SetMaxLimit(float limit) { maxVolLimit = limit; }

void SoundListener::SetMinLimit(float limit) { minVolLimit = limit; }

bool SoundListener::Edit()
{
	bool changed = false;
	ImGui::PushID("SoundListener");

	if (ImGui::TreeNode("SoundListener"))
	{
		if (ImGui::DragFloat(" Min Limit", &minVolLimit)) {
			if (minVolLimit >= maxVolLimit)
				minVolLimit = maxVolLimit - 0.01f;
			changed = true;
		}
		if (ImGui::DragFloat(" Max Limit", &maxVolLimit)) {
			if (maxVolLimit <= minVolLimit)
				maxVolLimit = minVolLimit + 0.01f;
			changed = true;
		}
		rate = maxVolLimit - minVolLimit;

		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}

	ImGui::PopID();

	return changed;
}

void SoundListener::Selected()
{
	GameObject * owner = GetOwner();
	Vector2 pos(owner->mPosition.x, owner->mPosition.y);
	RenderManager::Instance()->DrawCircleAt(pos, 100, minVolLimit * 2, Engine::Color::white);
	RenderManager::Instance()->DrawCircleAt(pos, 100, maxVolLimit * 2, Engine::Color::white);
}

void SoundListener::FromJson(nlohmann::json & _j)
{
	maxVolLimit = _j["maxVolLimit"];
	minVolLimit = _j["minVolLimit"];
	rate = _j["rate"];
}

void SoundListener::ToJson(nlohmann::json & _j)
{
	_j["maxVolLimit"] = maxVolLimit;
	_j["minVolLimit"] = minVolLimit;
	_j["rate"] = rate;

	_j["_type"] = "SoundListener";
}

void SoundListener::operator=(IComp& _comp) {
	SoundListener & listener = *dynamic_cast<SoundListener*>(&_comp);

	maxVolLimit = listener.maxVolLimit;
	minVolLimit = listener.minVolLimit;
	rate = listener.rate;
}


bool SoundListener::are_the_same_type(IComp const& lhs) {
	if (dynamic_cast<const SoundListener*>(&lhs)) {
		return true;
	}
	return false;
}

IComp * SoundListener::Clone() { return DBG_NEW SoundListener(*this); }

bool SoundListener::equal_to(IComp const& other) const {
	if (SoundListener const* p = dynamic_cast<SoundListener const*>(&other)) {
		//TODO
		return true;
	}
	else {
		return false;
	}
}


#pragma endregion

// AudioManager class functions
#pragma region

// Global
//AudioManager gAudioMgr;

// --------------------------------------------------------------
AudioManager::AudioManager()
	: pFMOD(NULL)
	, soundCount(0)
	, voiceCount(MAX_VOICE_COUNT)
	, voices(0)
	, defaultVolume(1.0f)
	, sfxVolume(1.0f)
	, globalVolume(1.0f)
{}

AudioManager::~AudioManager()
{
	Shutdown();
}
bool	AudioManager::Initialize()
{
	FMOD_RESULT result;

	result = FMOD::System_Create(&pFMOD);
	if (result != FMOD_OK)
	{
		//AE_ASSERT_MESG(result == FMOD_OK, "AUDIO: Error creating FMOD system! (%d) %s\n", result, FMOD_ErrorString(result));
		return false;
	}

	// Initialize FMOD
	result = pFMOD->init(MAX_VOICE_COUNT, FMOD_INIT_NORMAL, 0);
	if (result != FMOD_OK)
	{
		pFMOD->release();
		pFMOD = NULL;
		return false;
	}

	AllocVoices();

	return true;
}

void AudioManager::PlaySounds()
{
	for (std::list<SoundEmitter*>::iterator i = emitterlist.begin(); i != emitterlist.end(); i++)
	{
		SoundEmitter * emitter = *i;
		emitter->SetVoiceVariables();
		if (!emitter->play)
			continue;
		if (!emitter->isLooping)
			emitter->voice = gAudioMgr->Play(emitter->cue, emitter->isPaused);
		else
			emitter->voice = gAudioMgr->Loop(emitter->cue, emitter->isPaused);
		//emitter->voice = gAudioMgr->Play(emitter->cue);
	}
}

void	AudioManager::Shutdown()
{
	if (NULL == pFMOD)
		return;

	pFMOD->release();
	FreeVoices();
	pFMOD = NULL;
}
void	AudioManager::Update()
{
	if (NULL == pFMOD)
		return;

	// Update FMOD -> play audio
	pFMOD->update();

	// loop through the SoundEmitters
	for (std::list<SoundListener*>::iterator i = listenerlist.begin(); i != listenerlist.end(); i++)
	{
		SoundListener * listener = *i;
		GameObject * lisOwner = listener->GetOwner();
		for (std::list<SoundEmitter*>::iterator j = emitterlist.begin(); j != emitterlist.end(); j++)
		{
			SoundEmitter * emitter = *j;
			GameObject * emOwner = emitter->GetOwner();
			Vector2 emPos(emOwner->mPosition.x, emOwner->mPosition.y);
			Vector2 lisPos(lisOwner->mPosition.x, lisOwner->mPosition.y);
			float distance = emPos.Distance(lisPos);
			float vol;
			if (distance > listener->GetMaxLimit())
				vol = 0.0f;
			else if (distance < listener->GetMinLimit())
				vol = 1.0f;
			else
				/***************************************************************************
				Distance to lis-Distance too close to not be 1-----------------Volume to get
				Max Distance to hear something--------------------------------------Volume=1

				(Distance to lis-Distance too close to not be 1)*(Volume=1)
				Volume to get = ------------------------------------------------------------
				Max Distance to hear something

				This will make that when reaching the limits of the listener, the volume
				will increase, instead of decrease. To solve that, we substract the volume
				to 1, which is the maximun.
				***************************************************************************/
				vol = 1 - (distance - listener->GetMinLimit()) / listener->GetRate();
			emitter->voice->SetVolume(emitter->volume*vol);
			if (emitter->sfx)
				emitter->voice->SetVolume(emitter->voice->volume*sfxVolume);
			else
				emitter->voice->SetVolume(emitter->voice->volume*defaultVolume);
			emitter->voice->SetVolume(emitter->voice->volume*globalVolume);
			//emitter->volume = emitter->voice->volume;
		}
	}

	// loop through the voices
	for (Voice::PTR_LIST::iterator it = usedVoices.begin(); it != usedVoices.end(); ++it)
	{
		// this voice is no longer playing
		if (!(*it)->IsValid())
		{
			// erase and push to free list
			freeVoices.push_back(*it);
			it = usedVoices.erase(it);
			if (it == usedVoices.end())
				break;
		}
	}

}
FMOD::System * AudioManager::GetFMOD() { return this->pFMOD; }
// --------------------------------------------------------------

void AudioManager::FreeThisVoice(Voice *pVoice)
{
	if (NULL == pFMOD || pVoice == NULL)
		return;

	pVoice->pChannel->stop();
	usedVoices.remove(pVoice);
	freeVoices.push_back(pVoice);
}
Voice *	AudioManager::GetFreeVoice()
{
	if (NULL == pFMOD)
		return NULL;
	if (freeVoices.empty())
		return NULL;

	Voice * pv = freeVoices.back();
	freeVoices.pop_back();
	usedVoices.push_back(pv);

	return pv;
}
void	AudioManager::AllocVoices()
{
	if (NULL == pFMOD)
		return;

	// free the voices if necessary
	FreeVoices();

	// allocate _new array of voices
	voiceCount = MAX_VOICE_COUNT;
	voices = DBG_NEW Voice[voiceCount];

	// push all onto the free voices
	for (unsigned int i = 0; i < voiceCount; ++i)
		freeVoices.push_back(voices + i);
}
void	AudioManager::FreeVoices()
{
	if (NULL == pFMOD)
		return;
	if (voices)
	{
		delete[] voices;
		voiceCount = 0;
		freeVoices.clear();
		usedVoices.clear();
	}
}

// --------------------------------------------------------------
void AudioManager::attach(SoundEmitter* emitter)
{
	if (!emitter)
		return;
	emitterlist.push_back(emitter);
}
void AudioManager::attach(SoundListener* listener)
{
	if (!listener)
		return;
	listenerlist.push_back(listener);
}
void AudioManager::reattach(SoundEmitter* emitter)
{
	deattach(emitter);
	//SoundEmitter * persistent_emitter = _new SoundEmitter(*emitter);
	attach(emitter);
}
void AudioManager::reattach(SoundListener* listener)
{
	deattach(listener);
	attach(listener);
}
void AudioManager::deattach(SoundEmitter* emitter)
{
	emitterlist.remove(emitter);
}
void AudioManager::deattach(SoundListener* listener)
{
	listenerlist.remove(listener);
}


// --------------------------------------------------------------
Sound *	AudioManager::CreateSound(std::string filename)
{
	if (NULL == pFMOD)
		return NULL;

	// Allocate _new memory for the sound
	Sound * pSound = DBG_NEW Sound();
	pFMOD->createSound(filename.data(), FMOD_LOOP_NORMAL | FMOD_2D, 0, &pSound->pSound);

	// save the name of the 
	pSound->filename = filename;

	// error check
	if (pSound->pSound == NULL)
	{
		// make sure to delete the sound pointer
		delete pSound;
		return NULL;
	}

	soundList.push_back(pSound);
	namesList.push_back(filename);	// Save the filenames
	++soundCount;	// Stats update
	return pSound;
}
void AudioManager::AddSound(Sound * pSound)
{
	soundList.push_back(pSound);
	namesList.push_back(pSound->filename);	// Save the filenames
	++soundCount;	// Stats update
}
void		AudioManager::FreeSound(Sound * pSound)
{
	if (NULL == pFMOD)
		return;
	if (!pSound)
		return;

	if (pSound->pSound)
	{
		pSound->pSound->release();
		pSound->pSound = 0;
	}

	RemoveSound(pSound);

	// Stats update
	--soundCount;
	delete pSound;
}
void AudioManager::RemoveSound(Sound * pSound)
{
	for (unsigned i = 0; i < namesList.size(); i++)
	{
		if (namesList[i] == pSound->filename)
		{
			namesList.erase(namesList.begin() + i);
			soundList.erase(soundList.begin() + i);
			break;
		}
	}
	// Stats updat
	--soundCount;
}
Voice *	AudioManager::Play(Sound * pSound, bool paused)
{
	// make sure we can actually play the sound
	if (pFMOD == NULL || pSound == NULL)
		return NULL;

	// look for a free voice
	Voice * pVoice = GetFreeVoice();

	// this voice is valid
	if (pVoice)
	{
		pFMOD->playSound(pSound->pSound, 0, paused, &pVoice->pChannel);
		pVoice->SetPause(paused);
		pVoice->SetLoop(false);
	}

	// Return the voice (either NULL or correct)
	return pVoice;
}
Voice *	AudioManager::Loop(Sound * pSound, bool paused)
{
	Voice * pv = Play(pSound, paused);
	if (pv)
		pv->SetLoop(true);
	return pv;
}
void		AudioManager::StopAll()
{
	if (NULL == pFMOD)
		return;

	// loop through the voices
	while (!usedVoices.empty())
	{
		// erase and push to free list
		usedVoices.back()->pChannel->stop();
		freeVoices.push_back(usedVoices.back());
		usedVoices.pop_back();
	}
	pFMOD->update();
}

#pragma endregion
