#pragma once

#include "Importer/Importer.h"
#include "../Components/System.h"
#include <sys/stat.h>
#include <iostream>
#include <filesystem>
#include <map>

typedef struct stat FileStatus;

struct Sound;
class Texture;
class Importer;

using namespace std;
using namespace std::experimental::filesystem;


enum FILETYPE
{
	DummyType,//for unknow types
	TextureType,//.png
	FolderType,//no extension
	//SHADER,//.shader
	AnimationType,//.anim
	SoundType,//.ogg
	TextType//.ttf
};

#pragma region IResource

class IResource
{
	public:
		IResource(std::string filename_ = "Dummy");
		virtual ~IResource() { filename.clear(); };
		virtual std::string GetFilename();
		virtual void SetFilename(std::string filename_);
		virtual FileStatus GetModifiedTime();

	protected:
		std::string filename;
		FileStatus modifiedTime;
};

template <typename T>
class TResource : public IResource
{
public:
	TResource(std::string filename);
	~TResource();
	T* Get();
	void SetResource(T* pResource);

private:
	T* resource {nullptr};
};

#pragma endregion

#pragma region ResourceManager

class ActualResourceManager
{
	MAKE_SINGLETON(ActualResourceManager);

	public:
		~ActualResourceManager();

		void Initialize();
		void UpdateFiles();
		void Update();

		void LoadFromTXT(std::string _file);

		FILETYPE GetExtension(std::string filename);
		IResource* GetResource(std::string filename);

		template <typename T>
		TResource<T>* GetResource(std::string filename);

		Importer* GetImporter(FILETYPE type);

		std::vector<std::string>& GetAllFilenamesOfType(FILETYPE extension);


		IResource* LoadResource(std::string filename);

		template <typename T>
		IResource* LoadResourceOfType(FILETYPE type, std::string filename);

		void UnloadResource(std::string filename);
		void UnloadAllResources();

		IResource* ReloadResource(std::string filename);

		template <typename T>
		TResource<T>* ReloadResource(std::string filename);

		template <typename T>
		T* LoadResource(std::string filename);

		void LoadData(std::string foldername);

	private:

		std::map<FILETYPE, std::vector<std::string>>			filenames;
		std::map<FILETYPE, std::map<std::string, IResource*>>	resources;
		std::map<FILETYPE, Importer*>							importers;

};

template<typename T>
inline TResource<T> * ActualResourceManager::GetResource(std::string filename)
{
	auto stored_res = GetResource(filename);

	TResource<T>* myResource = stored_res ? dynamic_cast<TResource<T>*>(stored_res) : DBG_NEW TResource<T>(filename);

	return (myResource);
}

template<typename T>
inline TResource<T> * ActualResourceManager::ReloadResource(std::string filename)
{
	UnloadResource(filename);
	LoadResource(filename);

	return (GetResource<T>(filename));
}

template <typename T>
inline IResource * ActualResourceManager::LoadResourceOfType(FILETYPE type, std::string filename)
{
	auto myImporter = GetImporter(type);

	return myImporter->Load(filename);
}

#define gActualResourceMgr (*ActualResourceManager::Instance())

#pragma endregion

#pragma region TResource


template<typename T>
inline TResource<T>::TResource(std::string filename)
	: IResource(filename) {}

template<typename T>
inline TResource<T>::~TResource()
{
	FILETYPE extension = gActualResourceMgr.GetExtension(filename);
	Importer* myImporter = gActualResourceMgr.GetImporter(extension);

	myImporter->Unload(filename);

	resource = nullptr;
}

template<typename T>
inline T * TResource<T>::Get() { return resource; }

template<typename T>
inline void TResource<T>::SetResource(T * pResource)
{
	if (pResource != nullptr)
		resource = pResource;
}

#pragma endregion

