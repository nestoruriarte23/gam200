#include "ActualResourceManager.h"
#include "../Graphics/SpineComp.h"
#include "../Utilities/MyDebug/MyDebug.h"
#include "../Utilities/MyDebug/LeakDetection.h"

using namespace std;
using namespace std::experimental::filesystem;

#pragma region IRESOURCE

IResource::IResource(std::string filename_) : filename(filename_) 
{
	if (filename_ != "Dummy") {

		stat(filename.data(), &modifiedTime);
	}
}

std::string IResource::GetFilename() { return filename; }

void IResource::SetFilename(std::string filename_)
{
	filename = filename_;
}

FileStatus IResource::GetModifiedTime() { return modifiedTime; }

#pragma endregion



#pragma region ACTUALRESOURCEMANAGER

ActualResourceManager::ActualResourceManager()
{
	importers[TextureType] = DBG_NEW TextureImporter();
	importers[FolderType] = DBG_NEW FolderImporter();
	importers[AnimationType] = DBG_NEW AnimImporter();
	importers[DummyType] = DBG_NEW DummyImporter();
	importers[SoundType] = DBG_NEW SoundImporter();
	importers[TextType] = DBG_NEW TextImporter();
}

ActualResourceManager::~ActualResourceManager()
{
	UnloadAllResources();

	delete importers[TextureType];
	importers[TextureType] = nullptr;

	delete importers[FolderType];
	importers[FolderType] = nullptr;
	//
	//delete importers[Shader];
	//importers[Shader] = nullptr;

	delete importers[AnimationType];
	importers[AnimationType] = nullptr;

	delete importers[DummyType];
	importers[DummyType] = nullptr;

	delete importers[SoundType];
	importers[SoundType] = nullptr;

	delete importers[TextType];
	importers[TextType] = nullptr;

}

void ActualResourceManager::Initialize()
{
	LoadFromTXT("..//Resources//to_load.TXT");
}

void ActualResourceManager::UpdateFiles()
{
	FileStatus data;
	for (auto i : resources)
	{
		for (auto j = i.second.begin(); j != i.second.end(); j++)
		{
			stat(j->first.data(), &data);

			if (data.st_mtime != j->second->GetModifiedTime().st_mtime)
			{
				ReloadResource(j->first);
			}
		}
	}
}

void ActualResourceManager::Update()
{
	UpdateFiles();
}

void ActualResourceManager::LoadFromTXT(std::string _file)
{
	std::string path;
	std::ifstream myfile;

	if (_file.rfind("/") != std::string::npos)
		path = _file.substr(0, _file.rfind("/") + 1);

	myfile.open(_file);

	if (myfile.is_open())
	{
		std::string temp;
		while (getline(myfile, temp))
		{
			LoadResource(path + temp);
		}

		myfile.close();
		return;
	}

	else cout << "Unable to open file";
	return;
}

FILETYPE ActualResourceManager::GetExtension(std::string filename)
{
	std::string extension;

	extension = filename.substr(filename.rfind('.') + 1);

	if (extension == "png" || extension == "jpg" || extension == "bmp")
		return TextureType;

	//if (extension == "shader")
	//	return Shader

	if (extension == "anim")
		return AnimationType;

	if (extension == "ogg" || extension == "mp3" || extension == "wav")
		return SoundType;

	if (extension == "ttf")
		return TextType;

	if (extension[0] == '/')
		return FolderType;

	return DummyType;
}

IResource* ActualResourceManager::GetResource(std::string filename)
{
	FILETYPE type = GetExtension(filename);

	resources;
	auto resource = resources[type].find(filename);
	if (resource != resources[type].end())
		return resource->second;
	else
		return nullptr;
}

Importer * ActualResourceManager::GetImporter(FILETYPE type)
{
	switch (type)
	{
		case TextureType:
			return importers[TextureType];

		case FolderType:
			return importers[FolderType];

		//case Shader:
		//	return importers[Shader];

		case AnimationType:
			return importers[AnimationType];

		case SoundType:
			return importers[SoundType];

		case TextType:
			return importers[TextType];
	};

	return importers[DummyType];
}

std::vector<std::string>& ActualResourceManager::GetAllFilenamesOfType(FILETYPE extension)
{
	return filenames[extension];
}

IResource * ActualResourceManager::LoadResource(std::string filename)
{
	FILETYPE extension = GetExtension(filename);
	auto myImporter = GetImporter(extension);


	IResource* loadedResource = myImporter->Load(nullptr, filename);

	if (extension != FolderType)
	{
		resources[extension].insert(pair<std::string, IResource*>(filename, loadedResource));

		if (std::find(filenames[extension].begin(), filenames[extension].end(), filename) == filenames[extension].end())
			filenames[extension].push_back(filename);
	}

	return loadedResource;
}

void ActualResourceManager::UnloadResource(std::string filename)
{
	FILETYPE extension = GetExtension(filename);

	auto it = resources[extension].find(filename);

	if (it != resources[extension].end())
	{
		/* Remove the string from the filenames map */
		auto file_it = std::find(filenames[extension].begin(), filenames[extension].end(), filename);
		if (file_it != filenames[extension].end())
			filenames[extension].erase(file_it);

		TResource<Texture>* TexResource = nullptr;
		TResource<Sound>* SoundResource = nullptr;
		TResource<SpineData>* AnimResource = nullptr;

		switch (extension)
		{
			case TextureType:
				TexResource = dynamic_cast<TResource<Texture>*>(it->second);
				delete TexResource;
				resources[extension].erase(it);
				break;

			case AnimationType:
				AnimResource = dynamic_cast<TResource<SpineData>*>(it->second);
				delete AnimResource;
				resources[extension].erase(it);
				break;

			case FolderType:
				GetImporter(extension)->Unload(filename);
				break;

			case SoundType:
				SoundResource = dynamic_cast<TResource<Sound>*>(it->second);
				delete SoundResource;
				resources[extension].erase(it);
				break;

			case TextType:
				//TextResource = dynamic_cast<TResource<Text>*>(it->second);
				//delete TextResource;
				//resources[extension].erase(it);
				break;

			case DummyType:
				delete it->second;
				resources[extension].erase(it);
				break;
		}
	}
}

void ActualResourceManager::UnloadAllResources()
{
	resources;
	for (auto i : resources)
	{
		for (auto j : i.second)
		{
			UnloadResource(j.first);
		}
	}
}

IResource * ActualResourceManager::ReloadResource(std::string filename)
{
	auto myImporter = GetImporter(GetExtension(filename));

	myImporter->Unload(filename);

	auto loaded_resource = GetResource(filename);

	return myImporter->Load(loaded_resource, filename.data());
}

void ActualResourceManager::LoadData(std::string foldername)
{
	for (auto i : directory_iterator(foldername))
	{
		LoadResource((i.path()).generic_string());
	}
}

#pragma endregion
