#include "Importer.h"
#include "stb_image.h"
#include "..//ActualResourceManager.h"
#include "..//..//Sound/Audio.h"
#include "../../Graphics/RenderableComp.h"
#include "../../Graphics/RenderManager.h"
#include "../../Utilities/MyDebug/LeakDetection.h"
#include "../..//Graphics/Text/Text.h"
#include "..//..//Graphics/Text/Glyph.h"
#include "../../../Settings.h"
#include <ft2build.h>
#include FT_FREETYPE_H


#include <stdlib.h>
#include <fstream>
#include <fmod.hpp>

using namespace std;
using namespace std::experimental::filesystem;

#pragma region SOUNDIMPORTER

IResource * SoundImporter::Load(IResource* target_resource, std::string filename)
{
	TResource<Sound>* soundResource;

	if (target_resource != nullptr)
		soundResource = static_cast<TResource<Sound>*>(target_resource);
	else
		soundResource = DBG_NEW TResource<Sound>(filename);

	soundResource->SetResource(new Sound());

	FMOD::System* pFMOD = gAudioMgr->GetFMOD();

	if (NULL == pFMOD)
		return NULL;

	pFMOD->createSound(filename.data(), FMOD_LOOP_NORMAL | FMOD_2D, 0, &soundResource->Get()->pSound);

	// save the name of the 
	soundResource->Get()->filename = filename;

	// error check
	if (soundResource->Get()->pSound == NULL)
	{
		// make sure to delete the sound pointer
		delete soundResource->Get();
		return NULL;
	}

	gAudioMgr->AddSound(soundResource->Get());

	return soundResource;
}

void SoundImporter::Unload(std::string filename)
{
	TResource<Sound>* soundResource = reinterpret_cast<TResource<Sound>*>(gActualResourceMgr.GetResource(filename));

	FMOD::System* pFMOD = gAudioMgr->GetFMOD();

	if (NULL == pFMOD)
		return;
	if (!soundResource->Get()->pSound)
		return;

	if (soundResource->Get()->pSound)
	{
		soundResource->Get()->pSound->release();
		soundResource->Get()->pSound = 0;
	}

	gAudioMgr->RemoveSound(soundResource->Get());

	delete soundResource->Get();
}

#pragma endregion

IResource * DummyImporter::Load(IResource* target_resource, std::string filename) { return nullptr; }

void DummyImporter::Unload(std::string filename) { }

#pragma region SPINE_ANIM IMPORTER

#include "spine/spine.h"
#include "../../Graphics/SpineComp.h"

static int ReadAnimDataFromFile(const std::string& _file, std::string& _atlas, std::string& _skel)
{
	std::string temp;
	std::ifstream myfile;

	std::string path;

	if (_file.rfind("/") != std::string::npos)
		path = _file.substr(0, _file.rfind("/") + 1);

	myfile.open(_file);

	if (myfile.is_open())
	{
		while (getline(myfile, temp))
		{
			std::string ext = temp.substr(temp.rfind('.') + 1);
			if (ext == "atlas")
				_atlas = path + temp;
			else if (ext == "json")
				_skel = path + temp;
			else
			{
				cout << "Unkown file found loading " << _file << endl;
				break;
			}
		}

		myfile.close();

		if (_atlas == "" || _skel == "")
		{
			cout << "Unable to retrieve all necesary data for " 
				<< _file << endl;
			return -1;
		}
		else return 0;
	}

	else cout << "Unable to open file  \"" << _file << "\"\n (Line: " 
		<< __LINE__ << " File: " << __FILE__ << ")" << std::endl;
	return -1;
}

class SpineTextureLoader : public spine::TextureLoader
{
	// load the texture
	void load(spine::AtlasPage& page, const spine::String& path) override {

		// here you should create a texture resource (assuming you have a resource manager). 
		texture = static_cast<TResource<Texture>*>(gActualResourceMgr.LoadResource(path.buffer()));
		/*Texture * tex = gActualResourceMgr.GetResource<Texture>(path.buffer())->Get();

		texture = tex;*/

		// sanity check and error message

#ifndef FAST_LOAD

		if (!texture || !texture->Get()) {
			std::cout << "ERROR! Couldn't load Spine atlas page " << path.buffer() << "\n";
			return;
		}

#endif // !FAST_LOAD

		// pass the texture pointer to the page. Spine doesn't actually do anything with it, 
		// it will be passed back to us when drawing the skeleton. 
		page.setRendererObject(texture);

		// set the texture width and height

#ifndef FAST_LOAD

		page.width = texture->Get()->mWidth;
		page.height = texture->Get()->mHeight;

#endif // !FAST_LOAD

	}
	void unload(void* texture) override {

	}

public:
	TResource<Texture>*	texture;	// TODO: Avoid this?
};

static SpineTextureLoader spine_loader;

IResource * AnimImporter::Load(IResource* target_resource, std::string filename)
{
	std::string atlas_name;
	std::string skeleton_name;

	if (ReadAnimDataFromFile(filename, atlas_name, skeleton_name) < 0)
		return nullptr;

	/* Create the actual resource */
	TResource<SpineData>* animResource;

	if (target_resource != nullptr)
		animResource = static_cast<TResource<SpineData>*>(target_resource);
	else
		animResource = DBG_NEW TResource<SpineData>(filename);

	animResource->SetResource(DBG_NEW SpineData);

	/* Load atlas */
	animResource->Get()->atlas = new spine::Atlas(atlas_name.c_str(), &spine_loader);
	if (!animResource->Get()->atlas) {
		cout << "ERROR! Couldn't load the atlas " << atlas_name << "\n";
		return nullptr;
	}

	animResource->Get()->texture = spine_loader.texture;	// TODO: Avoid this

	/* Load Skeleton */
	spine::SkeletonJson json(animResource->Get()->atlas);
	animResource->Get()->skelData = json.readSkeletonDataFile(skeleton_name.c_str());
	if (!animResource->Get()->skelData) {
		cout << "ERROR! Loading skel data Json: " << json.getError().buffer() << "\n";
		return nullptr;
	}

	/* Create the animation state data */
	animResource->Get()->animStateData 
		= new spine::AnimationStateData(animResource->Get()->skelData);

	/* If reached this point, everything went fine */
	return animResource;
}

void AnimImporter::Unload(std::string filename)
{
	TResource<SpineData>* animResource = static_cast<TResource<SpineData>*>(gActualResourceMgr.GetResource(filename));

	delete animResource->Get();
}

#pragma endregion

#pragma region FOLDERIMPORTER

IResource * FolderImporter::Load(IResource* target_resource, std::string foldername)
{
	Unload(foldername);
	for (auto i : directory_iterator(foldername))
	{
		gActualResourceMgr.LoadResource((i.path()).generic_string());
	}

	return nullptr;
}

void FolderImporter::Unload(std::string foldername)
{
	for (auto i : directory_iterator(foldername))
	{
		gActualResourceMgr.UnloadResource((i.path()).generic_string());
	}
}

#pragma endregion

IResource * TextImporter::Load(IResource * target_resource, std::string filename)
{
	TResource<Font>* font_resource = DBG_NEW TResource<Font>(filename);

	font_resource->SetResource(DBG_NEW Font);

	Font* info = font_resource->Get();

	FT_Library ft;

	if (FT_Init_FreeType(&ft))
	{
		std::cout << "ERROR: In File TextRenderer.cpp, function LoadFont: Font not initialized" << std::endl;
		return nullptr;
	}

	FT_Face faceOfFont;

	if (FT_New_Face(ft, filename.c_str(), 0, &faceOfFont))
	{
		std::cout << "ERROR: In File TextRenderer.cpp, function LoadFont: Font not found :_(" << std::endl;
		return nullptr;
	}

	FT_Set_Pixel_Sizes(faceOfFont, 0, 100);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	for (GLubyte c = 0; c < 128; c++)
	{
		if (FT_Load_Char(faceOfFont, c, FT_LOAD_RENDER))
		{
			std::cout << "ERROR: In File TextRenderer.cpp, function LoadFont: Character not found :_(" << std::endl;
			continue;
		}

		unsigned newTexture;

		glGenTextures(1, &newTexture);

		glBindTexture(GL_TEXTURE_2D, newTexture);

		glTexImage2D
		(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			faceOfFont->glyph->bitmap.width,
			faceOfFont->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			faceOfFont->glyph->bitmap.buffer
		);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		Glyph ch =
		{
			newTexture,
			Vector2((float)faceOfFont->glyph->bitmap.width, (float)faceOfFont->glyph->bitmap.rows),
			Vector2((float)faceOfFont->glyph->bitmap_left,  (float)faceOfFont->glyph->bitmap_top),
			faceOfFont->glyph->advance.x
		};

		info->InsertGlyph(std::pair<char, Glyph>(c, ch));
	}

	FT_Done_Face(faceOfFont);
	FT_Done_FreeType(ft);

	return font_resource;
}

void TextImporter::Unload(std::string filename)
{
	delete (dynamic_cast<TResource<Font>*>(gActualResourceMgr.GetResource(filename))->Get());
}
