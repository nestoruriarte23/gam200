#include "TextureImporter.h"
#include "Importer.h"
#include "stb_image.h"
#include "..//ActualResourceManager.h"
#include "..//..//Sound/Audio.h"
#include "../../Graphics/RenderableComp.h"
#include "../../Graphics/RenderManager.h"
#include "../../Utilities/MyDebug/LeakDetection.h"

static unsigned loaded_textures = 0;

#ifdef FAST_LOAD

#include "../../ThreadPool/ThreadPool.h"

MultithreadedLoader::MultithreadedLoader() {}

bool MultithreadedLoader::UploadReadyTexturesToGPU()
{
	while (true)
	{
		while (!mLoadedTextures.empty())
		{
			auto texture_info = mLoadedTextures.top();
			mLoadedTextures.pop();

			auto textureResource = static_cast<TResource<Texture>*>(texture_info.mTarget);
			textureResource->SetResource(DBG_NEW Texture());

			/* Error check */
			if (texture_info.rawData == nullptr)
			{
				std::cout << "Error when loading texture from file: " + textureResource->GetFilename() << std::endl;
			}

			/* Create the OpenGL handler */
			GLuint textureHandle;
			glGenTextures(1, &textureHandle);

			/* Bind said texture handler so the next texture commands will act on it */
			glBindTexture(GL_TEXTURE_2D, textureHandle);

			/* Set texture look-up parameters */
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			std::string resource_name = textureResource->GetFilename();
			if (resource_name.find("Shadow") != std::string::npos)
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			}
			else
			{
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			}

			/* Save the height and width */
			textureResource->Get()->mHeight = texture_info.mHeight;
			textureResource->Get()->mWidth = texture_info.mWidth;

			/* Add the texture handler to the resource manager */
			textureResource->Get()->mHandle = textureHandle;

			/* Load the tex data onto the OpenGL handler */
			glTexImage2D(GL_TEXTURE_2D, 0, texture_info.mColorChannels == 3 ? GL_RGB : GL_RGBA,
				texture_info.mWidth, texture_info.mHeight, 0, texture_info.mColorChannels == 3 ?
				GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, texture_info.rawData);

			//std::cout << "Texture Loaded" << std::endl;

			glGenerateMipmap(GL_TEXTURE_2D);

			/* Free the data */
			stbi_image_free(texture_info.rawData);

			//std::cout << "GPUd: " << textureResource->GetFilename() << std::endl;
		}

		if (mToLoad > 0) std::this_thread::sleep_for(std::chrono::microseconds(250));
		else break;
	}

	ThreadPool::ReleaseInstance();

	return mToLoad == 0;
}

void MultithreadedLoader::PushLoadedInfo(const TextureInfo& _info)
{
	mLock.lock();

	mLoadedTextures.push(_info);

	mLock.unlock();
}

void MultithreadedLoader::AddTask()
{
	mLock.lock();

	mToLoad++;

	mLock.unlock();
}
void MultithreadedLoader::FinishTask()
{
	mLock.lock();

	mToLoad--;

	mLock.unlock();
}

IResource * TextureImporter::Load(IResource* target_resource, std::string filename)
{
	TResource<Texture>* textureResource;

	if (target_resource != nullptr)
		textureResource = static_cast<TResource<Texture>*>(target_resource);
	else
		textureResource = DBG_NEW TResource<Texture>(filename);

	MultiLoader->AddTask();
	//std::cout << MultiLoader->mToLoad << " textures: " << filename << std::endl;

	ThreadPool::Instance()->enqueue(
	[filename, textureResource]()
	{
		/* Create the int holders for stb_image.h */
		int width, height, ColorChannels;

		/* Flip Vertically if necessary to match OpenGL coord system */
		stbi_set_flip_vertically_on_load(true);

		/* Load the data, also get the properties of the image */
		unsigned char *TexData = stbi_load(filename.c_str(),
			&width, &height, &ColorChannels, 0);

		if (TexData == nullptr)
			std::cout << "Error stbi_load: " << filename << std::endl;

		MultiLoader->PushLoadedInfo(TextureInfo{ textureResource, height, width, ColorChannels, TexData });
		MultiLoader->FinishTask();
	}
	);

	return textureResource;
}

#endif // FAST_LOAD

#ifndef FAST_LOAD

IResource * TextureImporter::Load(IResource* target_resource, std::string filename)
{
	TResource<Texture>* textureResource;

	if (target_resource != nullptr)
		textureResource = static_cast<TResource<Texture>*>(target_resource);
	else
		textureResource = DBG_NEW TResource<Texture>(filename);


	/* Create the int holders for stb_image.h */
	int width, height, ColorChannels;

	/* Flip Vertically if necessary to match OpenGL coord system */
	stbi_set_flip_vertically_on_load(true);

	/* Load the data, also get the properties of the image */
	unsigned char *TexData = stbi_load(filename.c_str(),
		&width, &height, &ColorChannels, 0);

	if (TexData == nullptr)//Error check
	{
		std::cout << "Error when loading texture from file: " + filename << std::endl;
	}
	/*else
		std::cout << "Total: " << ++loaded_textures << " Texture Loaded: " << filename << std::endl;*/


	textureResource->SetResource(DBG_NEW Texture());

	/* Set texture look-up parameters */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	if (filename.find("Shadow") != std::string::npos)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}

	/* Save the height and width */
	textureResource->Get()->mHeight = height;
	textureResource->Get()->mWidth = width;

	/* Create the OpenGL handler */
	GLuint textureHandle;
	glGenTextures(1, &textureHandle);

	/* Add the texture handler to the resource manager */
	textureResource->Get()->mHandle = textureHandle;

	/* Bind said texture handler so the next texture commands will act on it */
	glBindTexture(GL_TEXTURE_2D, textureHandle);

	/* Load the tex data onto the OpenGL handler */
	glTexImage2D(GL_TEXTURE_2D, 0, ColorChannels == 3 ? GL_RGB : GL_RGBA, width, height, 0, ColorChannels == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, TexData);
	glGenerateMipmap(GL_TEXTURE_2D);

	/* Free the data */
	stbi_image_free(TexData);

	return textureResource;
}

#endif // FAST_LOAD

void TextureImporter::Unload(std::string filename)
{
	glDeleteTextures(1, &(gActualResourceMgr.GetResource<Texture>(filename)->Get()->mHandle));
	//std::cout << "Total: " << --loaded_textures << " Texture Unloaded: " << filename << std::endl;
	delete gActualResourceMgr.GetResource<Texture>(filename)->Get();
}
