#pragma once
#include <string>

class IResource;

#pragma region IMPORTER

class Importer
{
public:
	virtual IResource* Load(IResource* target_resource, std::string filename) = 0;
	virtual void Unload(std::string filename) = 0;
};

class DummyImporter : public Importer
{
private:
	IResource* Load(IResource* target_resource, std::string filename);
	void Unload(std::string filename);
};

class FolderImporter : public Importer
{
private:
	IResource* Load(IResource* target_resource, std::string filename);
	void Unload(std::string filename);
};

class SoundImporter : public Importer
{
private:
	IResource* Load(IResource* target_resource, std::string filename);
	void Unload(std::string filename);
};

class TextureImporter : public Importer
{
private:
	IResource* Load(IResource* target_resource, std::string filename);
	void Unload(std::string filename);
};

class ShaderImporter : public Importer
{
private:
	IResource* Load(IResource* target_resource, std::string filename);
	void Unload(std::string filename);
};

class AnimImporter : public Importer
{
private:
	IResource* Load(IResource* target_resource, std::string filename);
	void Unload(std::string filename);
};

class TextImporter : public Importer
{
private:
	IResource* Load(IResource* target_resource, std::string filename);
	void Unload(std::string filename);
};

#pragma endregion