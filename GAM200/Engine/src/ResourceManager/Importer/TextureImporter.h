#pragma once

#include "../../../Settings.h"

#ifdef FAST_LOAD

#include <stack>
#include <mutex>
#include "../../Serializer/SerializeFactory.h"

class IResource;

struct TextureInfo
{
	TextureInfo(IResource* _target, int _height, int _width, int _channels, unsigned char *	_data) 
		: mTarget{ _target }, mHeight{ _height }, mWidth{ _width }, mColorChannels{ _channels }, rawData{ _data } {}

	IResource*			mTarget;
	int					mHeight;
	int					mWidth;
	int					mColorChannels;
	unsigned char *		rawData;
};

class MultithreadedLoader
{
	MAKE_SINGLETON(MultithreadedLoader)

public:
	bool UploadReadyTexturesToGPU();

	void PushLoadedInfo(const TextureInfo& _info);

	void AddTask();
	void FinishTask();

	int						mToLoad{ 0 };

private:
	std::stack<TextureInfo> mLoadedTextures;
	std::mutex				mLock;

};

#define MultiLoader MultithreadedLoader::Instance() 

#endif // FAST_LOAD