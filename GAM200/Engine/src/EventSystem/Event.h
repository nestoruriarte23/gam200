/*!
*****************************************************************************
\file    event.hh
\author  Nestor Uriarte
\par     DP email: nestor.uriarte@lauro20.org
\par     Course: CS225
\par     Programming Assignment #2
\date    18/10/2018

\brief
    This file has the classes Event, MemberFunctionHandler, EventHandler.
*******************************************************************************/
#pragma once
#include <string>
#include <typeinfo>
#include <map>
#include "../Utilities/MyDebug/LeakDetection.h"

    class Event
    {
        public:
            virtual ~Event() {}
        private:
    };
    
    class HandlerFunction
    {
        public:
            virtual ~HandlerFunction(){}
            void handle(const Event& event);
        private:
            virtual void call(const Event& event) = 0;
    };
    
    template<typename T, typename EVENT>
    class MemberFunctionHandler : public HandlerFunction
    {
        public:
			~MemberFunctionHandler() { object_ = nullptr; }
            //defining the function pointer
            typedef void (T::*MemberFunction)(const EVENT &);
            
            MemberFunctionHandler(T* object, MemberFunction function)
            :object_(object), function_(function){}
        
        private:
        
        T* object_;
        MemberFunction function_;
        
        virtual void call(const Event& event)
        {
            (object_ ->* function_)(static_cast<const EVENT &>(event));
        }
        
    };
    
    class EventHandler
    {
        public:
            ~EventHandler();
            
            template<typename T, typename EVENT>
            void register_handler(T& object,void (T::*function)(const EVENT&));
            
            void handle(const Event& event);
            
        private:
            std::map<std::string, HandlerFunction* > handlers;
        
    };
/*!
*****************************************************************************
    \fn void register_handler(T& object,void (T::*function)(const EVENT&))
    
    \brief 
        A template function to store the objects that will handle an event

  \param T& object
    A constant referenece to a run time know object
  \param void (T::*function)(const EVENT&)
    a function pointer

  \return void
    Does not return anything
*******************************************************************************/    
    template<typename T, typename EVENT>
    void EventHandler::register_handler(T& object,void (T::*function)(const EVENT&))
    {
        handlers[typeid(EVENT).name()] = DBG_NEW MemberFunctionHandler<T, EVENT>(&object, function);
    }