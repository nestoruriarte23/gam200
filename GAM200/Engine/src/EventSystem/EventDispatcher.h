/*!
*****************************************************************************
\file    event_dispatcher.hh
\author  Nestor Uriarte
\par     DP email: nestor.uriarte@lauro20.org
\par     Course: CS225
\par     Programming Assignment #2
\date    18/10/2018

\brief
    This file contains the classes Listener, EventDispatcher and the function 
    trigger_event
*******************************************************************************/
#pragma once
#include "Event.h"
#include "TypeInfo.h"
#include "../../Types.h"
#include <vector>
#include <map>
	
    class Listener
    {
        public:
            virtual ~Listener(){}
            virtual void handle_event(const Event& event) = 0;
            
        private:
            
    };
    
	class CollisionEvent;
    class EventDispatcher
    {
        public:
            EventDispatcher();
            
            std::map<TypeInfo, std::vector<Listener *> > subscribers_map;
            static EventDispatcher& get_instance(){ return instance; }
            void subscribe(Listener & listener, const TypeInfo & type);
			void unsubscribe(Listener & listener, const TypeInfo & type);
            friend std::ostream & operator<<(std::ostream & os,EventDispatcher & event);
               
            void clear();
            void trigger_event(const Event& event);


#pragma region// COLLISION EVENTS

			std::map<u32, std::vector<Listener*> > collision_map;
			void subscribe_collison(Listener& listener, const u32& id);
			void unsubscribe_collision(Listener& listener,const u32& id);
			void trigger_collision_event(const CollisionEvent& event);

#pragma endregion
            
        private:
            EventDispatcher(const EventDispatcher& ){}
            
            static EventDispatcher instance;//static variable
    };
    
    void trigger_event(const Event& event);