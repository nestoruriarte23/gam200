/*!
*****************************************************************************
\file    event_dispatcher.cc
\author  Nestor Uriarte
\par     DP email: nestor.uriarte@lauro20.org
\par     Course: CS225
\par     Programming Assignment #2
\date    18/10/2018

\brief
    This file contains the implementation for the functions on the file 
    event_dispatcher.hh
    
  -EventDispatcher()
    The default constructor of the class
    
  -void subscribe(Listener & listener,const TypeInfo & type)
    subscribes the objects to react to a specific event
    
  -std::ostream & operator<<(std::ostream & os,EventDispatcher & event)
    overload of the << operator
    
  -void clear()
    function that cleans the map
    
  -void trigger_event(const Event& event)
    function that triggers an event
    
  -void trigger_event(const Event& event)
    A function inside of the cs225 namespace that triggers an event
    
  -void unsubscribe(Listener & listener,const TypeInfo & type) 
    unsubscribes the objects to stop reacting to a specific event
*******************************************************************************/
#include "TypeInfo.h"
#include "EventDispatcher.h"
#include "../Components/CompMngrs/LogicBase.h"
#include "../Physics/Collision Event/Collision Event.h"
#include <algorithm>

        
    EventDispatcher EventDispatcher::instance;//initializing the static variable
/*!
*****************************************************************************
    \fn bool EventDispatcher()
    
    \brief 
        default constructor of the class

  \param 
    it takes no arguments
    
  \return void
    Does not return anything
*******************************************************************************/
    EventDispatcher::EventDispatcher(){}
/*!
*****************************************************************************
    \fn void ssubscribe(Listener & listener,const TypeInfo & type)
    
    \brief 
        Returns if the position that we want to move is valid or not.

  \param Listener & listener
    A reference to the event listener
  \param const TypeInfo & type
    A constant reference to a TypeInfo object

  \return void
    Does not return anything
*******************************************************************************/    
    void EventDispatcher::subscribe(Listener & listener,const TypeInfo & type)
    {
        subscribers_map[type].push_back(&listener);
    }
/*!
*****************************************************************************
    \fn std::ostream & operator<<(std::ostream & os,EventDispatcher & event)
    
    \brief 
        overload of the << operator

  \param std::ostream & os
    A reference to an output stream
  \param EventDispatcher & event
    A reference to an event object

  \return bool
    if is valid or not
*******************************************************************************/    
    std::ostream & operator<<(std::ostream & os,EventDispatcher & event)
    {
        map<TypeInfo, std::vector<Listener *> >::iterator it;
           
        for (it = event.subscribers_map.begin(); it != event.subscribers_map.end(); it++)
        {
               os<<"The event type "<<it -> first.get_name()
               <<" has the following subscribers:\n";
               
               for(unsigned int i = 0;i < it -> second.size(); i++)
               {
                   os<<"\tAn instance of type "
                   <<type_of(*(it -> second[i])).get_name()<<"\n";
               }
        }
        return os;
    }
 /*!
*****************************************************************************
    \fn void clear()
    
    \brief 
        cleans the map

  \param 
    it takes no arguments

  \return void
    Does not return anything
*******************************************************************************/      
       void EventDispatcher::clear()
       {
           subscribers_map.clear();//clearing the map
		   collision_map.clear();
       }

/*!
*****************************************************************************
    \fn void trigger_event(const Event& event)
    
    \brief 
        function that triggers an event

  \param const Event& event
    A constant reference to an event object

  \return void
    Does not return anything
*******************************************************************************/    
    void EventDispatcher::trigger_event(const Event& event)
    {
		//check if the event is stored in the map
		std::map<TypeInfo, std::vector<Listener*> >::iterator iterator_;
		iterator_ = subscribers_map.find(type_of(event));
		if (iterator_ != subscribers_map.end())
		{
			std::for_each(iterator_->second.begin(), iterator_->second.end(),
				[&](Listener* listener)
				{
					listener->handle_event(event);
				});
		}
    }


/*!
*****************************************************************************
    \fn void trigger_event(const Event& event)
    
    \brief 
        A function inside of the cs225 namespace that triggers an event

  \param const Event& event
    A constant reference to an event object

  \return void
    Does not return anything
*******************************************************************************/    
    void trigger_event(const Event& event)
    {
        //getting an EventDispatcher instance
        EventDispatcher & dispatcher = EventDispatcher::get_instance();
        
        //calling the trigger function inside the instance
        dispatcher.trigger_event(event);
    }
/*!
*****************************************************************************
    \fn void unsubscribe(Listener & listener,const TypeInfo & type)
    
    \brief 
        Returns if the position that we want to move is valid or not.

  \param Listener & listener
    A reference to the event listener to unsubscribe
  \param const TypeInfo & type
    A constant reference to a TypeInfo object

  \return void
    Does not return anything
*******************************************************************************/
    
    void EventDispatcher::unsubscribe(Listener & listener,const TypeInfo & type)
    {
        vector<Listener* >::iterator it;//creating a iterator
        
        it = subscribers_map[type].begin();//setting it to the beggining of the vecotr
        
        while(it != subscribers_map[type].end())//while is not pointing at the end
        {
			std::string name1 = type_of(**it).get_name();
			std::string name2 = type_of(listener).get_name();
            //checking if is the one i need to remove
            if((type_of(**it).get_name()) == (type_of(listener).get_name()))
            {
				subscribers_map[type].erase(it);//removing it

				break;//breaking from the loopW
            }
            it++;//mkaing it point to the next element
        }
        
        
    }


	void EventDispatcher::subscribe_collison(Listener& listener,const u32& id)
	{
		collision_map[id].push_back(&listener);
	}

	void EventDispatcher::unsubscribe_collision(Listener& listener,const u32& id)
	{
		collision_map.erase(id);
	}


	void EventDispatcher::trigger_collision_event(const CollisionEvent& event)
	{
		if (!event.object1_->mOwner || !event.object2_->mOwner) return;

		u32 ID_1 = event.object1_->mOwner->GetUID();
		u32 ID_2 = event.object2_->mOwner->GetUID();

		//std::vector<Listener*>::iterator it = collision_map[ID_1];
		std::for_each(collision_map[ID_1].begin(), collision_map[ID_1].end(),
			[&](Listener* listener)
			{
				ILogic* mComp = dynamic_cast<ILogic*>(listener);
				if (mComp)mComp->OnCollisionStarted(event.object2_);
			});

		std::for_each(collision_map[ID_2].begin(), collision_map[ID_2].end(),
			[&](Listener* listener)
			{
				ILogic* mComp = dynamic_cast<ILogic*>(listener);
				if (mComp)mComp->OnCollisionStarted(event.object1_);
			});
	}