/*!
*****************************************************************************
\file    event.cc
\author  Nestor Uriarte
\par     DP email: nestor.uriarte@lauro20.org
\par     Course: CS225
\par     Programming Assignment #2
\date    18/10/2018

\brief
    this file contains the implementation for the functions for the file and
    functions on the event.hh file.
    
    -void HandlerFunction::handle(const Event& event)
        A function to access the Call function of the NVI
        
    -~EventHandler()
        The destructor of the class
        
    -void EventHandler::handle(const Event& event)
        A function to call a specific handle function depending which object
        has to handle it
*******************************************************************************/
#include "Event.h"
#include <map>
/*!
*****************************************************************************
    \fn void HandlerFunction::handle(const Event& event)
    
    \brief 
        A function to access the Call function of the NVI

  \param const Event& event
    A constant referenece to a Event object

  \return void
    Does not return anything
*******************************************************************************/
    void HandlerFunction::handle(const Event& event){ call(event);}
/*!
*****************************************************************************
    \fn ~EventHandler()
    
    \brief 
        The destructor of the class

  \param it takes no arguments

  \return 
    Does not return anything
*******************************************************************************/     
    EventHandler::~EventHandler()
    {
		//std::map<std::string, HandlerFunction* >::iterator it;//creating an iterator
		/* for (auto it = handlers.begin(); it != handlers.end(); it++)//going through the map
		  handlers.erase(it);*/
		while (!handlers.empty())
		{
			delete handlers.begin()-> second; //deallocating the memory
			handlers.erase(handlers.begin());
		}
    }
/*!
*****************************************************************************
    \fn bool is_valid_move(const BoardPosition& pos)
    
    \brief 
        A function to call a specific handle function depending which object
        has to handle it

  \param const Event& event
    A constant referenece to a Event object

  \return void
    Does not return anything
*******************************************************************************/    
    void EventHandler::handle(const Event& event)
    {
        std::map<std::string, HandlerFunction* >::iterator iterator_;//creating the iterator
        iterator_ = handlers.find(typeid(event).name());//finding a specific object
        if(iterator_ != handlers.end())//a check to know that it was found
        {
            iterator_ -> second ->handle(event);//calling the handle function of the object
        }
    }