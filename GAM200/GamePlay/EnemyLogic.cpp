#include "EnemyLogic.h"

void EnemyLogic::Initialize()
{
	attacked_with = AttackID::Nothing;
	destroyable_object = mOwner->get_component_type<DestroyableObject>();
	trigger = false;
}
void EnemyLogic::Update()
{}
void EnemyLogic::Shutdown()
{}
void EnemyLogic::attacked(AttackID id)
{
	attacked_with = id;

	if (destroyable_object)
		destroyable_object->Hit();
}

//////////////////////////////////////////////////////////////////////////
//							ENEMY ATTACK STATE							//
//////////////////////////////////////////////////////////////////////////

void EnemyDirectionState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
}
void EnemyDirectionState::LogicUpdate()
{
	movement = Vector2(player->mPosition.x, player->mPosition.y) - Vector2(mOwner->mPosition.x, mOwner->mPosition.y);
}
void EnemyDirectionState::LogicExit()
{}



//////////////////////////////////////////////////////////////////////////
//							DESTROYABLE OBJECT							//
//////////////////////////////////////////////////////////////////////////

void DestroyableObject::Initialize()
{
	damaged = false;
	mSprite = mOwner->get_component_type<Sprite>();
	Timer = 0.0f;
	alpha = 1.0f;
}

void DestroyableObject::Update()
{
	if (damaged)
	{
		Timer += TimeManager::Instance()->GetDt();
		if (Timer > 2)
		{
			damaged = false;
			Game->DestroyObject(mOwner);
		}
		alpha -= 0.02f;
		Engine::Color color(1.0f, 1.0f, 1.0f, alpha);
		mSprite->SetModColor(color);
	}
}
void DestroyableObject::Shutdown()
{

}