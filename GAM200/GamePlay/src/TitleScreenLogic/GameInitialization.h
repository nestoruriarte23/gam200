#pragma once
#include "../../Engine/Engine.h"

class TitleScreenLogic : public ILogic
{
	RTTI_DECLARATION_INHERITED(TitleScreenLogic, ILogic);

public:
	LOGIC_COMPONENT(TitleScreenLogic);

	void Update();
	void Initialize();
	void Shutdown();
};
