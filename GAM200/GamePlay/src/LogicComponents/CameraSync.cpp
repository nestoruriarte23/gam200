#include "..//..//Engine/Engine.h"
#include "CameraSync.h"

BackgroundCamSync::BackgroundCamSync()
{
	auto space = Game->GetCurrScene()->FindSpace("Background");
	space != nullptr ? mOtherCam = space->FindObject("BackCamera") : nullptr;
}
void BackgroundCamSync::Initialize()
{
	auto space = Game->GetCurrScene()->FindSpace("Background");
	space != nullptr ? mOtherCam = space->FindObject("BackCamera") : nullptr;
}

void BackgroundCamSync::Update()
{
	if (mOtherCam)
	{
		mOtherCam->mPosition = mOwner->mPosition;
		mOtherCam->get_component_type<CameraComp>()->mViewSize = mOwner->get_component_type<CameraComp>()->mViewSize;
	}
}
