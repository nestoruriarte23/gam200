#pragma once

#include "../../Engine/src/Components/CompMngrs/LogicBase.h"

class BackgroundCamSync : public ILogic
{
	RTTI_DECLARATION_INHERITED(BackgroundCamSync, ILogic);

public:
	LOGIC_COMPONENT(BackgroundCamSync);

	BackgroundCamSync();

	void Initialize();
	void Update();

private:
	GameObject*		mOtherCam;
};