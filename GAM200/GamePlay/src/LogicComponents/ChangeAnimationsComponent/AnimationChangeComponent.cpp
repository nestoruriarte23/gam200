#include "AnimationChangeComponent.h"
#include "../../Engine/src/Utilities/MyDebug/MyDebug.h"

void AnimationChange::Initialize()
{
	//std::cout << "Animation Change Component Initialize" << std::endl; Vector2 current_position;

	mAnimationComp = mOwner->get_component_type<SpineAnimation>();
	ASSERT(mAnimationComp != nullptr);
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	std::string temp = mOwner->GetName();
	if(temp == "companion")
		current_animation = "Eguzkilore";

	west = true;

	current_direction = vec_directions[Directions::South];
}

void AnimationChange::Update()
{
	//std::cout << "Animation Change Component Update" << std::endl;

	go_direction = player->mPosition - mOwner->mPosition ;

	if (go_direction != Vector2(0, 0))
		if (west)
			DirectionChangedWest();
		else
			DirectionChangedEast();

	if (current_direction != previous_direction && !unique)
	{
		previous_direction = current_direction;
		ChangeAnimation(unique);
	}
}

void AnimationChange::Shutdown()
{
	//std::cout << "Animation Change Component Shut Down" << std::endl;
}

void AnimationChange::ChangeAnimation(bool _unique)
{
	unique = _unique;
	if(unique)
		mAnimationComp->SetCurrentAnim(current_animation);
	else
		mAnimationComp->SetCurrentAnim(current_animation + current_direction);
}

void AnimationChange::ChangeAnimation(const std::string& change, bool _unique)
{
	if (current_animation != std::string(change))
	{
		current_animation = change;
		ChangeAnimation(_unique);
	}
}

void AnimationChange::DirectionChangedWest()
{
	actual_angle = ToDegrees(static_cast<double>(go_direction.GetAngle()));
	if(actual_angle >= -22.5 && actual_angle < 22.5)
		current_direction = vec_directions[Directions::North];
	else if (actual_angle >= -67.5 && actual_angle < -22.5)
		current_direction = vec_directions[Directions::NorthWest];
	else if (actual_angle >= -112.5 && actual_angle < -67.5)
		current_direction = vec_directions[Directions::West];
	else if (actual_angle >= -157.5 && actual_angle < -112.5)
		current_direction = vec_directions[Directions::ShoutWest];
	else if ((actual_angle >= -180 && actual_angle < -157.5) || (actual_angle > 157.5 && (int)actual_angle <= 180))
		current_direction = vec_directions[Directions::South];
	else if (actual_angle >= 112.5 && actual_angle < 157.5)
		current_direction = vec_directions[Directions::ShoutWest];
	else if ((actual_angle > 67.5 && actual_angle <= 112.5))
		current_direction = vec_directions[Directions::West];
	else if (actual_angle >= 22.5 && actual_angle < 67.5)
			current_direction = vec_directions[Directions::NorthWest];

	mAnimationComp->SetFlipX(actual_angle >= 22.5 && actual_angle < 157.5);
}

void AnimationChange::DirectionChangedEast()
{
	actual_angle = ToDegrees(static_cast<double>(go_direction.GetAngle()));
	if (actual_angle >= -22.5 && actual_angle < 22.5)
		current_direction = vec_directions[Directions::North];
	else if (actual_angle >= -67.5 && actual_angle < -22.5)
		current_direction = vec_directions[Directions::NorthEast];
	else if (actual_angle >= -112.5 && actual_angle < -67.5)
		current_direction = vec_directions[Directions::East];
	else if (actual_angle >= -157.5 && actual_angle < -112.5)
		current_direction = vec_directions[Directions::SouthEast];
	else if ((actual_angle >= -180 && actual_angle < -157.5) || (actual_angle > 157.5 && (int)actual_angle <= 180))
		current_direction = vec_directions[Directions::South];
	else if (actual_angle >= 112.5 && actual_angle < 157.5)
		current_direction = vec_directions[Directions::SouthEast];
	else if ((actual_angle > 67.5 && actual_angle <= 112.5))
		current_direction = vec_directions[Directions::East];
	else if (actual_angle >= 22.5 && actual_angle < 67.5)
		current_direction = vec_directions[Directions::NorthEast];

	mAnimationComp->SetFlipX(!(actual_angle >= 22.5 && actual_angle < 157.5));
}

std::string AnimationChange::GetActualAnim()
{
	return current_animation + current_direction;
}
