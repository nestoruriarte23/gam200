#pragma once
#include "../../Engine/Engine.h"

class SpineAnimation;

enum Directions
{
	North,
	NorthEast,
	East,
	SouthEast,
	South,
	ShoutWest,
	West,
	NorthWest
};

class AnimationChange : public ILogic
{
	RTTI_DECLARATION_INHERITED(AnimationChange, ILogic);
	LOGIC_COMPONENT(AnimationChange);

public:

	void Update();
	void Initialize();

	void Shutdown();

	void ChangeAnimation(bool unique = false);
	void ChangeAnimation(const std::string& change, bool unique = false);
	void DirectionChangedWest();
	void DirectionChangedEast();
	std::string GetActualAnim();
	void SetActualAnim(const std::string& animation);

	bool west;

private:
	SpineAnimation* mAnimationComp;
	GameObject* player;
	bool unique;
	Vector2 go_direction;
	double actual_angle;
	std::string previous_direction;
	std::string current_direction;
	std::string current_animation;
	const std::vector<std::string> vec_directions { "_N", "_NE", "_E", "_SE", "_S", "_SW", "_W", "_NW" };
};
