#include "HealthComponent.h"
#include "../../HUDLogic/HUDLogic.h"

void Health::Initialize()
{
	if (mOwner->mTag == Tags::Player)
		health = 100;
	object_damaged = false;
	TimeDt = 0.0f;
	FullHealth = health;
}

void Health::Update()
{
	ObjectHitDetection();
}

void Health::Shutdown()
{
	//BUSCARLO EN EL LOGIC Y BORRAR
}

bool Health::is_full_hp()
{
	if (health >= FullHealth)
	{
		return true;
	}
	return false;
}


void Health::DamageObject(float damage)
{
	if (mOwner->mTag == Tags::Player && player_invulnerable || health < 0)
		return;
	health -= damage;
	object_damaged = true;
}
void Health::HealObject(float healing)
{
	if(!is_full_hp())
		health += healing;
}

void Health::ChangeHealth(float change)
{
	if (is_full_hp() && change > 0)
		return;
	health += change;
}

void Health::ObjectHitDetection()
{
	if (object_damaged)
	{
		if (reset_healing)
		{
			TimeDt = 0.0f;
			reset_healing = false;
		}

		TimeDt += TimeManager::Instance()->GetDt();
		if (TimeDt > 3.0f)
		{
			object_damaged = false;
			TimeDt = 0;
		}
	}
}

void Health::SetFullHealth(float max)
{
	if (max > FullHealth)
	{
		float add = max - FullHealth;
		FullHealth += add;
		health += add;
	}
	else
	{
		FullHealth = max;
		health = health > FullHealth ? max : health;
	}
}



#include "../../Engine/src/Graphics/Particles/ParticleProperties.h"

void DeathParticles::Update()
{
	mEllapsedTime += static_cast<float>(TimeManager::Instance()->GetDt());

	if (mEllapsedTime > mEmittingTime)
		part_sys->SetPause(true);
	if (mEllapsedTime > (mEmittingTime + mParticleLife))
		Game->DestroyObject(mOwner);
}

void DeathParticles::Initialize()
{
	std::cout << "Death created" << std::endl;

	mEllapsedTime = 0.0f;
	mEmittingTime = 1.2f;

	part_sys = mOwner->get_component_type<ParticleSystem>();
	part_sys->SetPosition(mOwner->mPosition);
	part_sys->SetDestination(Game->GetCurrScene()->FindObjectInAnySpace("companion"));
	mParticleLife = part_sys->GetMaxParticleLife();

	/*std::cout << "From: " << mOwner->mPosition << "  To: "
		<< Game->GetCurrScene()->FindObjectInAnySpace("companion")->mPosition;*/
}

void DeathParticles::Shutdown()
{
	std::cout << "Death dead" << std::endl;
}
