#pragma once
#include "../../Engine/Engine.h"
#include "../../Engine/src/Components/CompMngrs/LogicBase.h"
#include <vector>
#include <string>

class Health : public ILogic
{

	RTTI_DECLARATION_INHERITED(Health, ILogic);
	LOGIC_COMPONENT(Health);

public:

	void Update();
	void Initialize();
	void Shutdown();

	//void Edit();

	void ChangeHealth(float change);
	void SetHealth(float life) { health = life; FullHealth = life; }
	void HealObject(float healing);
	void DamageObject(float damage);
	bool is_full_hp();

	bool is_damaged() { return object_damaged; }
	float& CurrentHealth() { return health; }
	float& MaxHealth() { return FullHealth; }

	void SetFullHealth(float max);

	void ObjectHitDetection();
	bool player_invulnerable = false;
private:
	float health;
	float FullHealth;

	bool object_damaged = false;
	bool reset_healing = false;
	
	f64 TimeDt = 0.0f;
};

class ParticleSystem;

class DeathParticles : public ILogic
{

	RTTI_DECLARATION_INHERITED(DeathParticles, ILogic);
	LOGIC_COMPONENT(DeathParticles);

public:
	void Update();
	void Initialize();
	void Shutdown();

private:
	float mEllapsedTime;
	float mEmittingTime;
	float mParticleLife;
	ParticleSystem* part_sys;
};