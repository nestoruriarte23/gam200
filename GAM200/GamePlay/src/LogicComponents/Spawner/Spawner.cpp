#include "Spawner.h"

void GaltzagorriSpawner::Initialize()
{
	//std::cout << "Spawn appeared" << std::endl;
	mTimeEllapsed = 0.0f;
}

void GaltzagorriSpawner::Shutdown()
{
	//std::cout << "Spawn died" << std::endl;
}

void GaltzagorriSpawner::Update()
{
	mTimeEllapsed += static_cast<float>(TimeManager::Instance()->GetDt());

	if (mTimeEllapsed > 5.0f) 
	{
		GameObject * go = DBG_NEW GameObject();
		serializer->LoadArchetype("Galtzagorri", go);

		go->mPosition = mOwner->mPosition + Vector2(std::rand() % 700 - 350.0f, std::rand() % 700 - 350.0f);
		//go->Shutdown();
		//Game->CreateObject(go);
		//StMgr->attach(this);

		mTimeEllapsed = 0.0f;
	}
}