#pragma once
#include "../../Engine/Engine.h"

class GaltzagorriSpawner : public ILogic
{
	RTTI_DECLARATION_INHERITED(GaltzagorriSpawner, ILogic);

public:
	LOGIC_COMPONENT(GaltzagorriSpawner);

	void Update();
	void Initialize();
	void Shutdown();

private:
	float mTimeEllapsed = 0;
};
