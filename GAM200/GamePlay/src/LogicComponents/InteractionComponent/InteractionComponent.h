#include "..//..///Engine/src/EventSystem/Event.h"
#include "..//..///Engine/src/EventSystem/EventDispatcher.h"
#include "..//..///Engine/src/Input/InputEvent.h"

#ifndef	INTERACTION_COMPONENT_H_
#define INTERACTION_COMPONENT_H_
#include "../../Engine/Engine.h"
#include "../../PlayerLogic/PlayerLogic.h"

class TextComp;
class InteractionComponent: public ILogic{

	RTTI_DECLARATION_INHERITED(InteractionComponent, ILogic);
public:
	~InteractionComponent();

	void Update();
	void Initialize();
	void Shutdown();

	void Interact();
	void ShowInteractionPossibility();

	GameObject * mPlayer;
	GameObject * mInputButton;

	bool Edit();

	void FromJson(nlohmann::json& j);
	void ToJson(nlohmann::json& j);

	IComp* Clone();
	virtual void operator=(IComp&);
	bool are_the_same_type(IComp const& lhs);

protected:
	bool equal_to(IComp const& other) const;

private:
	bool mbShowText;
	double timer = 0.0;
	unsigned text_interactions = 0u;
	float range = 500;

	PlayerInfo* player_info;
	TextComp*	text_comp;
	std::vector<char*> mTexts;
	TResource<Texture>* break_texture;
};

#endif