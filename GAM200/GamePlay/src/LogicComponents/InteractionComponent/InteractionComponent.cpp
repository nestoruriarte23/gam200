#include "../../Engine/src/Utilities/MyDebug/LeakDetection.h"
#include "../../PlayerLogic/PlayerLogic.h"
#include "../../Engine/src/Physics/CollisionSystem.h"
#include "InteractionComponent.h"
#include "..//..//Engine/src/Graphics/Text/Text.h"

#define MAX_TEXT_SIZE 500

InteractionComponent::~InteractionComponent()
{}

void InteractionComponent::Initialize() {

	mPlayer = Game->GetCurrScene()->FindObjectInAnySpace("player");
	if(mPlayer) player_info = mPlayer->get_component_type<PlayerInfo>();
	text_comp = mOwner->get_component_type<TextComp>();
}

void InteractionComponent::Update() {
	
	Vector2 p(mPlayer->mPosition);
	Vector2 c(mOwner->mPosition.x + mOwner->mSortingPos.x, mOwner->mPosition.y + mOwner->mSortingPos.y);
	if (StaticPointToStaticCircle(&p, &c, range)) 
	{
		//ShowInteractionPossibility();
      	Interact();
	} 
	else 
	{
		//static_cast<Sprite*>(mInputButton->GetComp("Sprite"))->SetVisibility(false);
		mbShowText = false;
		if (text_comp) 
		{
			text_comp->mbVisible = mbShowText;
			text_interactions = 0;
		}
	}
}

void InteractionComponent::Shutdown() 
{
	for (char* str : mTexts)
		delete[] str;
	mTexts.clear();
}

void InteractionComponent::Interact() 
{
	switch (mOwner->mTag)
	{
	case Tags::Pickable:
	{
		if (KeyTriggered(Key::KeySpace))
		{
			std::string pickable_name(mOwner->GetName());
			player_info->Pickable_objects.push_back(pickable_name);
			Game->DestroyObject(mOwner);
		}
		break;
	}
	case Tags::Breakable:
	{
		if (KeyTriggered(Key::X) || ButtonTriggered(SDL_CONTROLLER_BUTTON_X))
			mOwner->get_component_type<Sprite>()->ChangeTexture(break_texture->GetFilename());
			
		break;
	}
	case Tags::TextObject:
		if (KeyTriggered(Key::KeySpace))
		{
			text_comp->mbVisible = true;
			text_comp->SetText(mTexts[text_interactions]);

			if (text_interactions++ >= mTexts.size())
				text_interactions = 0;
		}
		break;
	default:
		break;
	}
}

void InteractionComponent::ShowInteractionPossibility() {
 	dynamic_cast<Sprite*>(mInputButton->GetComp("Sprite"))->SetVisibility(true);
}



bool InteractionComponent::Edit()
{
	bool changed = false;
	ImGui::PushID("InteractionComponent");

	if (ImGui::TreeNode("InteractionComponent"))
	{
		changed = ImGui::DragFloat("Range", &range) || changed;
		RenderManager::Instance()->DrawCircleAt(mOwner->mPosition, 100, range*2, Engine::Color::yellow);

		switch (mOwner->mTag)
		{
		case Tags::Pickable:

			break;

		case Tags::Breakable:
		{
			static unsigned selected_item = -2;
			auto& all_textures = gActualResourceMgr.GetAllFilenamesOfType(FILETYPE::TextureType);

			if (selected_item == -2 && break_texture)
			{
				auto position = std::find(all_textures.begin(), all_textures.end(), break_texture->GetFilename());
				selected_item = std::distance(all_textures.begin(), position);
			}

			if (ImGui::BeginCombo(" Texture", break_texture ? all_textures[selected_item].data() : " - Default - "))
			{
				bool is_selected = (break_texture == nullptr);

				if (ImGui::Selectable(" - Default - ", is_selected))
				{
					selected_item = -1;
					break_texture = nullptr;
					changed = true;
				}

				if (is_selected)
				{
					selected_item = -1;
					ImGui::SetItemDefaultFocus();
				}

				for (unsigned n = 0; n < all_textures.size(); n++)
				{
					bool is_selected = (selected_item == n);
					if (ImGui::Selectable(all_textures[n].data(), is_selected))
					{
						selected_item = n;
						break_texture = gActualResourceMgr.GetResource<Texture>(all_textures[n]);
						changed = true;
					}
					if (ImGui::IsItemHovered())
					{
						ImGui::BeginTooltip();
						ImGui::Image((void*)(intptr_t)(gActualResourceMgr.GetResource<Texture>
							(all_textures[n])->Get()->mHandle), ImVec2(80, 80), ImVec2(0, 0),
							ImVec2(1, -1), ImVec4(1.0f, 1.0f, 1.0f, 1.0f), ImVec4(1.0f, 1.0f, 1.0f, 0.5f));
						ImGui::EndTooltip();
					}

					if (is_selected)
					{
						selected_item = n;
						ImGui::SetItemDefaultFocus();
					}
				}
				ImGui::EndCombo();
			}
			break;
		}
		case Tags::TextObject:
			ImGui::PushID("interaction text");
			if (ImGui::Button("Add new text"))
			{
				char* str = new char[MAX_TEXT_SIZE];
				memset(str, 0, MAX_TEXT_SIZE);
				mTexts.push_back(str);
			}
			for (unsigned i = 0; i < mTexts.size(); i++)
			{
				ImGui::PushID(i);
				changed = ImGui::InputText("", mTexts[i], MAX_TEXT_SIZE);
				ImGui::PopID();
			}
			ImGui::PopID();
			break;

		default:
			break;
		}
		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}
	ImGui::PopID();
	return changed;
}

void InteractionComponent::FromJson(nlohmann::json& j)
{
	if (j.find("mbShowText") != j.end())		mbShowText = j["mbShowText"];

	if (j.find("timer") != j.end())		timer = j["timer"];

	if (j.find("range") != j.end())		range = j["range"];

	if (j.find("text_interactions") != j.end())		text_interactions = j["text_interactions"];


	if (j.find("mTexts") != j.end())
	{
		for (std::string str : j["mTexts"])
		{
			char* string = new char[MAX_TEXT_SIZE];
			strcpy_s(string, MAX_TEXT_SIZE, str.c_str());
			mTexts.push_back(string);
		}
	}

	if (j.find("Break_Texture") != j.end() && j["Break_Texture"] != "")
		break_texture = gActualResourceMgr.GetResource<Texture>(j["Break_Texture"]);
	else
		break_texture = nullptr;

}
void InteractionComponent::ToJson(nlohmann::json& j)
{
	j["_type"] = "InteractionComponent";

	
	j["timer"] = timer;
	j["text_interactions"] = text_interactions;
	j["mbShowText"] = mbShowText;
	j["range"] = range;

	j["mTexts"];
	for (unsigned i = 0; i < mTexts.size(); i++)
		j["mTexts"].push_back(mTexts[i]);

	j["Break_Texture"] = break_texture ? break_texture->GetFilename() : "";
		
}

IComp* InteractionComponent::Clone()
{
	InteractionComponent* interactive_object =  DBG_NEW InteractionComponent(*this);
	interactive_object->mTexts.clear();

	for (char * str : mTexts)
	{
		char* new_str = new char[MAX_TEXT_SIZE];
		strcpy_s(new_str, MAX_TEXT_SIZE, str);
		interactive_object->mTexts.push_back(new_str);
	}
	return interactive_object;
}
void InteractionComponent::operator=(IComp& _comp)
{
	InteractionComponent* interactive_object = dynamic_cast<InteractionComponent*>(&_comp);

	for (char* str : interactive_object->mTexts)
	{
		char* new_str = new char[MAX_TEXT_SIZE];
		strcpy_s(new_str, MAX_TEXT_SIZE, str);
		mTexts.push_back(new_str);
	}
	timer = interactive_object->timer;
	text_interactions = interactive_object->text_interactions;
	mbShowText = interactive_object->mbShowText;
}

bool InteractionComponent::are_the_same_type(IComp const& lhs)
{
	if (dynamic_cast<const InteractionComponent*>(&lhs)) {
		return true;
	}
	return false;
}

bool InteractionComponent::equal_to(IComp const& other) const
{
	if (InteractionComponent const* p = dynamic_cast<InteractionComponent const*>(&other)) {
		return text_interactions == p->text_interactions && mbShowText == p->mbShowText;
	}
	else {
		return false;
	}
}