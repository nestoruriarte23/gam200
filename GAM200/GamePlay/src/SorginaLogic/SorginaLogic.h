#pragma once
#include "../../Engine/Engine.h"
#include "../LogicComponents/HealthComponent/HealthComponent.h"
#include "../LogicComponents/ChangeAnimationsComponent/AnimationChangeComponent.h"
#include "../../GamePlay/EnemyLogic.h"

//////////////////////////////////////////////////////////////////////////
//							SORGINA COMPONENT							//
//////////////////////////////////////////////////////////////////////////
class SorginaLogic : public ILogic
{
	RTTI_DECLARATION_INHERITED(SorginaLogic, ILogic);

public:
	LOGIC_COMPONENT(SorginaLogic);
	PropertyMap properties;

	void Update();
	void Initialize();
	void Shutdown();

	GameObject* mOwner;
	GameObject* mPlayer;

	float distance;
	Vector2 mDir;

	AnimationChange* mAnimation;

	SoundEmitter* mEmitter;

	Health* mLife;
	RigidBody* mRigidBody;
	EnemyLogic* mEnemyLogic;

	unsigned cycle;
	unsigned phase;

private:
};

//////////////////////////////////////////////////////////////////////////
//							SORGINA IDDLE STATE							//
//////////////////////////////////////////////////////////////////////////

struct SorginaIddleState : public State
{
	RTTI_DECLARATION_INHERITED(SorginaIddleState, State);
	SorginaIddleState(const char* name)
		:State(name), mLogic(NULL)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	SorginaIddleState* Clone();

	SorginaLogic * mLogic;

	float range;

	//AnimationChange* mAnimation;
};

//////////////////////////////////////////////////////////////////////////
//						SORGINA ATTACK STATE							//
//////////////////////////////////////////////////////////////////////////

struct SorginaAttackState : public State
{
	RTTI_DECLARATION_INHERITED(SorginaAttackState, State);
	SorginaAttackState(const char* name)
		:State(name), mLogic(NULL)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	SorginaAttackState* Clone();

	SorginaLogic * mLogic;

	float EllapsedTime;
	float EndTime;
	unsigned counter;

	float speed;
	float range;

	//AnimationChange* mAnimation;
};

//////////////////////////////////////////////////////////////////////////
//					SORGINA ATTACK TARGET STATE							//
//////////////////////////////////////////////////////////////////////////

struct SorginaAttackTargetState : public State
{
	RTTI_DECLARATION_INHERITED(SorginaAttackTargetState, State);
	SorginaAttackTargetState(const char* name)
		:State(name), mLogic(NULL)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	SorginaAttackTargetState* Clone();

	SorginaLogic * mLogic;

	float EllapsedTime;
	float ActivationTime;
	float EndTime;

	float speed;
	float range;

	//AnimationChange* mAnimation;
};

//////////////////////////////////////////////////////////////////////////
//							SORGINA BUFFER STATE						//
//////////////////////////////////////////////////////////////////////////

struct SorginaBufferState : public State
{
	RTTI_DECLARATION_INHERITED(SorginaBufferState, State);
	SorginaBufferState(const char* name)
		:State(name), mLogic(NULL), show_range(0)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	SorginaBufferState* Clone();

	SorginaLogic * mLogic;
	std::vector<GameObject*> mEnemiesNear;
	float range;
	bool show_range;

	float EllapsedTime;
	float EndTime;

	//AnimationChange* mAnimation;
};

////////////////////////////////////////////////////////////////////////////
////						ATTACKED WITH HAMMER HIT						//
////////////////////////////////////////////////////////////////////////////
//
//struct Sorgina_HammerHit : public State
//{
//	RTTI_DECLARATION_INHERITED(Sorgina_HammerHit, State);
//	Sorgina_HammerHit(const char* name)
//		:State(name)
//	{}
//
//	virtual void LogicEnter();
//	virtual void LogicUpdate();
//	virtual void LogicExit();
//	PropertyMap properties;
//
//	Sorgina_HammerHit* Clone();
//
//	SorginaLogic * mLogic;
//
//	Vector2 displacement;
//	float timer;
//	PROP_Val(float, displacement_time, 0.15f);
//	PROP_Val(float, stun_time, 0.25f);
//	PROP_Val(float, damage, 10.0f);
//	PROP_Val(float, pushback_velocity, 750.0f);
//};
//
////////////////////////////////////////////////////////////////////////////
////					ATTACKED WITH SUPER HAMMER HIT						//
////////////////////////////////////////////////////////////////////////////
//struct Sorgina_SuperHammerHit : public State
//{
//	RTTI_DECLARATION_INHERITED(Sorgina_SuperHammerHit, State);
//	Sorgina_SuperHammerHit(const char* name)
//		:State(name)
//	{}
//	PropertyMap properties;
//
//	virtual void LogicEnter();
//	virtual void LogicUpdate();
//	virtual void LogicExit();
//
//	Sorgina_SuperHammerHit* Clone();
//
//	SorginaLogic * mLogic;
//
//	Vector2 displacement;
//	float timer;
//	PROP_Val(float, damage, 20.0f);
//	PROP_Val(float, displacement_time, 0.15f);
//	PROP_Val(float, stun_time, 1.25f);
//	PROP_Val(float, pushback_velocity, 1000.0f);
//};
//
////////////////////////////////////////////////////////////////////////////
////						ATTACKED WITH HAMMER SPIN						//
////////////////////////////////////////////////////////////////////////////
//struct Sorgina_HammerSpin : public State
//{
//	RTTI_DECLARATION_INHERITED(Sorgina_HammerSpin, State);
//	Sorgina_HammerSpin(const char* name)
//		:State(name)
//	{}
//	PropertyMap properties;
//
//	virtual void LogicEnter();
//	virtual void LogicUpdate();
//	virtual void LogicExit();
//
//	Sorgina_HammerSpin* Clone();
//
//	SorginaLogic * mLogic;
//
//	Vector2 displacement;
//	float distance;
//	float timer;
//	PROP_Val(float, damage, 10.0f);
//	PROP_Val(float, attraction_vel, 250.0f);
//	PROP_Val(float, attack_distance, 200.0f);
//	PROP_Val(float, time_to_damage, 0.25f);
//};
//
////////////////////////////////////////////////////////////////////////////
////						ATTACKED WITH TACKLE							//
////////////////////////////////////////////////////////////////////////////
//struct Sorgina_Tackle : public State
//{
//	RTTI_DECLARATION_INHERITED(Sorgina_Tackle, State);
//	Sorgina_Tackle(const char* name)
//		:State(name)
//	{}
//	PropertyMap properties;
//
//	virtual void LogicEnter();
//	virtual void LogicUpdate();
//	virtual void LogicExit();
//	void OnCollisionStarted(const BoxCollider* coll);
//	Sorgina_Tackle* Clone();
//
//	SorginaLogic* mLogic;
//
//	Vector2 displacement;
//	float timer;
//	PROP_Val(float, displacement_time, 0.25f);
//	PROP_Val(float, mVelocity, 25.0f);
//};

