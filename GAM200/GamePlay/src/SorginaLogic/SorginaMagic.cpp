#include "SorginaMagic.h"
#include "../LogicComponents/HealthComponent/HealthComponent.h"
#include <iostream>


void MagicChaseLogic::Update()
{
	mDir = mPlayer->mPosition - mOwner->mPosition;
	mOwner->mPosition += mDir.Normalize() * mSpeed;

	/*Vector2 own_to_play = mPlayer->mPosition - mOwner->mPosition;
	float angle = acos(mDir.Dot(own_to_play)/(mDir.Length()*own_to_play.Length()));

	if (angle > 0.05)
		mDir = Matrix33().RotDeg(-5) * mDir;
	if (angle < 0.05)
		mDir = Matrix33().RotDeg(5) * mDir;

	std::cout << angle << std::endl;*/

	EllapsedTime += static_cast<float>(TimeManager::Instance()->GetDt());
	if (EllapsedTime > EndTime)
		Game->DestroyObject(mOwner);
}
