#pragma once
#include "../../Engine/Engine.h"
#include "../LamiaLogic/LamiaProjectile.h"

class MagicChaseLogic : public LamiaProjectileLogic
{
	RTTI_DECLARATION_INHERITED(MagicChaseLogic, ILogic);

public:
	LOGIC_COMPONENT(MagicChaseLogic);

	void Update();
};