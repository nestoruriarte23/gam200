#include "SorginaLogic.h"
#include "../GaltxaLogic/GaltxagorriLogic.h"
#include "../LamiaLogic/LamiaProjectile.h"

//////////////////////////////////////////////////////////////////////////
//							SORGINA COMPONENT							//
//////////////////////////////////////////////////////////////////////////

void SorginaLogic::Initialize()
{
	mOwner		= GetOwner();
	mPlayer		= Game->GetCurrScene()->FindObjectInAnySpace("player");

	distance	= mOwner->mPosition.Distance(mPlayer->mPosition);
	mDir		= mPlayer->mPosition - mOwner->mPosition;

	mAnimation	= mOwner->get_component_type<AnimationChange>();

	mEmitter	= mOwner->get_component_type<SoundEmitter>();

	mRigidBody = mOwner->get_component_type<RigidBody>();
	ASSERT(mRigidBody != nullptr);
	mEnemyLogic = mOwner->get_component_type<EnemyLogic>();
	ASSERT(mEnemyLogic != nullptr);
	mLife = mOwner->get_component_type<Health>();
	ASSERT(mLife != nullptr);
	mLife->SetHealth(50);

	cycle		= 0;
	phase		= 0;
}

void SorginaLogic::Update()
{
	distance = mOwner->mPosition.Distance(mPlayer->mPosition);
	if (mLife->CurrentHealth() <= 0)
		Game->DestroyObject(mOwner);
}

void SorginaLogic::Shutdown()
{

}

//////////////////////////////////////////////////////////////////////////
//							SORGINA IDDLE STATE							//
//////////////////////////////////////////////////////////////////////////

void SorginaIddleState::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<SorginaLogic>();

	ASSERT(mLogic->mAnimation != nullptr)
		mLogic->mAnimation->ChangeAnimation(std::string("Idle"), true);
}
void SorginaIddleState::LogicUpdate()
{
	if (mLogic->distance <= range)
		mOwnerStateMachine->ChangeState("SorginaAttackState");

	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerHit)
	{
		mLogic->mLife->DamageObject(10.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}

	if (mLogic->mEnemyLogic->attacked_with == AttackID::SuperHammerHit)
	{
		mLogic->mLife->DamageObject(20.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}
	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerSpin)
	{
		mLogic->mLife->DamageObject(10.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}
}
void SorginaIddleState::LogicExit()
{}

void SorginaIddleState::Edit()
{
	ImGui::PushID("SorginaIddleState");

	ImGui::DragFloat("Activation Distance", &range);

	ImGui::PopID();
}
void SorginaIddleState::Selected()
{
	if (mActor != NULL)
	{
		Vector2 pos(mActor->GetOwner()->mPosition);
		RenderManager::Instance()->DrawCircleAt(pos, 100, range * 2, Engine::Color::white);
	}
}
void SorginaIddleState::FromJson(nlohmann::json & j)
{
	range = j["range"];
}
void SorginaIddleState::ToJson(nlohmann::json & j)
{
	j["range"] = range;
}
SorginaIddleState* SorginaIddleState::Clone()
{
	SorginaIddleState* state = DBG_NEW SorginaIddleState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						SORGINA ATTACK STATE							//
//////////////////////////////////////////////////////////////////////////

void SorginaAttackState::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<SorginaLogic>();
	mLogic->mEmitter->SetCue(std::string("..//Resources//Sound//magic.wav"));
	ASSERT(mLogic->mAnimation != nullptr)
		mLogic->mAnimation->ChangeAnimation(std::string("Attack"), true);
	EllapsedTime = 0.0f;
	EndTime = 1.3f;
	counter = 0;
}
void SorginaAttackState::LogicUpdate()
{
	EllapsedTime += static_cast<float>(TimeManager::Instance()->GetDt());
	if (EllapsedTime >= EndTime && counter < 3)
	{
		EllapsedTime = 0.0f;
		EndTime = 0.5f;
		counter++;
		GameObject * projectile = Game->CreateArchetype("Magic");
		projectile->mPosition = mLogic->mOwner->mPosition;
		projectile->get_component_type<LamiaProjectileLogic>()->ReInit();
		mLogic->mEmitter->PlayCue();
	}
	if (mLogic->distance >= range)
		mOwnerStateMachine->ChangeState("SorginaIddleState");

	if (EllapsedTime >= 0.5f && counter == 3)
	{
		switch (mLogic->cycle)
		{
		case 0:
			mLogic->cycle++;
			mOwnerStateMachine->ChangeState("SorginaAttackTargetState");
			break;
		case 2:
			mLogic->cycle = 0;
			mOwnerStateMachine->ChangeState("SorginaBufferState");
			break;
		}

	}

	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerHit)
	{
		mLogic->mLife->DamageObject(10.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}

	if (mLogic->mEnemyLogic->attacked_with == AttackID::SuperHammerHit)
	{
		mLogic->mLife->DamageObject(20.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}
	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerSpin)
	{
		mLogic->mLife->DamageObject(10.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}
}
void SorginaAttackState::LogicExit()
{}

void SorginaAttackState::Edit()
{
	ImGui::PushID("SorginaIddleState");

	ImGui::DragFloat("Speed", &speed);
	ImGui::DragFloat("Deactivation Distance", &range);

	ImGui::PopID();
}
void SorginaAttackState::Selected()
{
	if (mActor != NULL)
	{
		Vector2 pos(mActor->GetOwner()->mPosition);
		RenderManager::Instance()->DrawCircleAt(pos, 100, range * 2, Engine::Color::blue);
	}
}
void SorginaAttackState::FromJson(nlohmann::json & j)
{
	range = j["range"];
	speed = j["speed"];
}
void SorginaAttackState::ToJson(nlohmann::json & j)
{
	j["range"] = range;
	j["speed"] = speed;
}
SorginaAttackState* SorginaAttackState::Clone()
{
	SorginaAttackState* state = DBG_NEW SorginaAttackState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//					SORGINA ATTACK TARGET STATE							//
//////////////////////////////////////////////////////////////////////////

void SorginaAttackTargetState::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<SorginaLogic>();
	mLogic->mEmitter->SetCue(std::string("..//Resources//Sound//magic.wav"));
	EllapsedTime = 0.0f;
	ActivationTime = 1.5f;
	EndTime = 11.5f;
}
void SorginaAttackTargetState::LogicUpdate()
{
	EllapsedTime += static_cast<float>(TimeManager::Instance()->GetDt());
	if (EllapsedTime >= ActivationTime && EllapsedTime < 10)
	{
		GameObject * projectile1 = Game->CreateArchetype("GreatMagic");
		GameObject * projectile2 = Game->CreateArchetype("GreatMagic");

		projectile1->mPosition = Vector2(mLogic->mOwner->mPosition.x+100, mLogic->mOwner->mPosition.y);
		projectile2->mPosition = Vector2(mLogic->mOwner->mPosition.x-100, mLogic->mOwner->mPosition.y);

		projectile1->get_component_type<LamiaProjectileLogic>()->ReInit();
		projectile2->get_component_type<LamiaProjectileLogic>()->ReInit();

		mLogic->mEmitter->PlayCue();
		EllapsedTime = 10;
	}
	if (EllapsedTime >= EndTime)
	{
		mLogic->cycle++;
		mOwnerStateMachine->ChangeState("SorginaAttackState");
	}
	if (mLogic->distance >= range)
		mOwnerStateMachine->ChangeState("SorginaIddleState");

	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerHit)
	{
		mLogic->mLife->DamageObject(10.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}

	if (mLogic->mEnemyLogic->attacked_with == AttackID::SuperHammerHit)
	{
		mLogic->mLife->DamageObject(20.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}
	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerSpin)
	{
		mLogic->mLife->DamageObject(10.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}
}
void SorginaAttackTargetState::LogicExit()
{}

void SorginaAttackTargetState::Edit()
{
	ImGui::PushID("SorginaIddleState");

	ImGui::DragFloat("Speed", &speed);
	ImGui::DragFloat("Deactivation Distance", &range);

	ImGui::PopID();
}
void SorginaAttackTargetState::Selected()
{
	if (mActor != NULL)
	{
		Vector2 pos(mActor->GetOwner()->mPosition);
		RenderManager::Instance()->DrawCircleAt(pos, 100, range * 2, Engine::Color::blue);
	}
}
void SorginaAttackTargetState::FromJson(nlohmann::json & j)
{
	range = j["range"];
	speed = j["speed"];
}
void SorginaAttackTargetState::ToJson(nlohmann::json & j)
{
	j["range"] = range;
	j["speed"] = speed;
}
SorginaAttackTargetState* SorginaAttackTargetState::Clone()
{
	SorginaAttackTargetState* state = DBG_NEW SorginaAttackTargetState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//							SORGINA BUFFER STATE						//
//////////////////////////////////////////////////////////////////////////

void SorginaBufferState::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<SorginaLogic>();
	mLogic->mEmitter->SetCue(std::string("..//Resources//Sound//magiceffect.wav"));
	ASSERT(mLogic->mAnimation != nullptr)
		mLogic->mAnimation->ChangeAnimation(std::string("Fury"), true);
	EllapsedTime = 0.0f;
	EndTime = 2.0f;
}
void SorginaBufferState::LogicUpdate()
{
	EllapsedTime += static_cast<float>(TimeManager::Instance()->GetDt());

	if (EllapsedTime >= EndTime + 9.0f)
	{
		mLogic->cycle = 0;
		mOwnerStateMachine->ChangeState("SorginaAttackState");
		return;
	}

	if (EllapsedTime >= EndTime && EllapsedTime < 10.0f)
	{
		std::vector<GameObject*> space_objects = Game->GetCurrScene()->scene_spaces[1]->space_objects;
		unsigned limit = space_objects.size();
		for (unsigned i = 0; i < limit; i++)
		{
			//if(space_objects[i]->GetName())
			if (space_objects[i]->mTag == Tags::Enemies && space_objects[i] != mLogic->mOwner)
				if (space_objects[i]->mPosition.Distance(mLogic->mOwner->mPosition) < range)
				{
					GaltzagorriLogic* mGal = space_objects[i]->get_component_type<GaltzagorriLogic>();
					if (mGal != nullptr && !mGal->buff)
						mEnemiesNear.push_back(space_objects[i]);
				}

		}
		EllapsedTime = 10.0f;
		mLogic->mEmitter->PlayCue();
		if (mEnemiesNear.size()) 
		{
			int index = rand() % mEnemiesNear.size();
			GaltzagorriLogic * lol = mEnemiesNear[index]->get_component_type<GaltzagorriLogic>();
			if (lol != nullptr)
			{
				lol->Buff();
				mEnemiesNear.clear();
				mOwnerStateMachine->ChangeState("SorginaAttackState");
			}
		}
	}

	if (mLogic->distance >= range)
		mOwnerStateMachine->ChangeState("SorginaIddleState");

	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerHit)
	{
		mLogic->mLife->DamageObject(10.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}
		
	if (mLogic->mEnemyLogic->attacked_with == AttackID::SuperHammerHit)
	{
		mLogic->mLife->DamageObject(20.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}
	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerSpin)
	{
		mLogic->mLife->DamageObject(10.0f);
		mLogic->mEnemyLogic->attacked_with = AttackID::Nothing;
	}
}
void SorginaBufferState::LogicExit()
{
	mEnemiesNear.clear();
}

void SorginaBufferState::Edit()
{
	ImGui::PushID("SorginaIddleState");

	ImGui::DragFloat("Area of buffing", &range);
	ImGui::SameLine();
	ImGui::Checkbox("Show Range", &show_range);

	ImGui::PopID();
}
void SorginaBufferState::Selected()
{
	if (mActor != NULL && show_range == true)
	{
		Vector2 pos(mActor->GetOwner()->mPosition);
		RenderManager::Instance()->DrawCircleAt(pos, 100, range * 2, Engine::Color::red);
	}
}
void SorginaBufferState::FromJson(nlohmann::json & j)
{
	range = j["range"];
	//speed = j["speed"];
}
void SorginaBufferState::ToJson(nlohmann::json & j)
{
	j["range"] = range;
	//j["speed"] = speed;
}
SorginaBufferState* SorginaBufferState::Clone()
{
	SorginaBufferState* state = DBG_NEW SorginaBufferState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////

//void Sorgina_HammerHit::LogicEnter()
//{
//	mLogic = mActor->GetOwner()->get_component_type<SorginaLogic>();
//	timer = 0;
//
//	/* DAMAGE */
//	mLogic->mLife->DamageObject(damage);
//	mLogic->mEnemyLogic->attacked(AttackID::Nothing);
//	/* PUSH BACK */
//	displacement = mLogic->mDir.Normalize();
//	//mOwner->mPosition += displacement * pushback_velocity;
//	mLogic->mRigidBody->AddVelocity(displacement * pushback_velocity);
//}
//void Sorgina_HammerHit::LogicUpdate()
//{
//	timer += static_cast<float>(TimeManager::Instance()->GetDt());
//	if (timer >= displacement_time)
//	{
//		mLogic->mRigidBody->mVelocity = Vector2(0, 0);
//		if (mLogic->mLife->CurrentHealth() <= 0)
//			mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
//	}
//	if (timer >= stun_time)
//		mOwnerStateMachine->ChangeState("SorginaRunState");
//}
//void Sorgina_HammerHit::LogicExit()
//{}
//
//Sorgina_HammerHit* Sorgina_HammerHit::Clone()
//{
//	Sorgina_HammerHit* state = DBG_NEW Sorgina_HammerHit(mName.c_str());
//	state->mLogic = mLogic;
//	state->displacement = displacement;
//	state->timer = timer;
//
//	return state;
//}

//////////////////////////////////////////////////////////////////////////
//					ATTACKED WITH SUPER HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////

//void Sorgina_SuperHammerHit::LogicEnter()
//{
//	mLogic = mActor->GetOwner()->get_component_type<SorginaLogic>();
//	timer = 0.0f;
//
//	mLogic->mLife->DamageObject(damage);
//	mLogic->mEnemyLogic->attacked(AttackID::Nothing);
//	/* PUSH BACK */
//	displacement = mLogic->mDir.Normalize();
//	//mOwner->mPosition += displacement * pushback_velocity;
//	mLogic->mRigidBody->AddVelocity(displacement * pushback_velocity);
//}
//void Sorgina_SuperHammerHit::LogicUpdate()
//{
//	timer += static_cast<float>(TimeManager::Instance()->GetDt());
//	if (timer >= displacement_time)
//	{
//		mLogic->mRigidBody->mVelocity = Vector2(0, 0);
//		if (mLogic->mLife->CurrentHealth() <= 0)
//			mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
//	}
//	if (timer >= stun_time)
//		mOwnerStateMachine->ChangeState("SorginaRunState");
//	if (mLogic->mLife->CurrentHealth() <= 0)
//		mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
//}
//void Sorgina_SuperHammerHit::LogicExit()
//{}
//
//Sorgina_SuperHammerHit* Sorgina_SuperHammerHit::Clone()
//{
//	Sorgina_SuperHammerHit* state = DBG_NEW Sorgina_SuperHammerHit(mName.c_str());
//	state->mLogic = mLogic;
//	state->displacement = displacement;
//	state->timer = timer;
//
//	return state;
//}

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER SPIN						//
//////////////////////////////////////////////////////////////////////////

/*void Sorgina_HammerSpin::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<SorginaLogic>();
	timer = time_to_damage;

	mLogic->mRigidBody->AddVelocity(mLogic->mDir * attraction_vel);
}
void Sorgina_HammerSpin::LogicUpdate()
{
	//TORNADO
	if (mLogic->distance <= attack_distance && timer >= time_to_damage)
	{
		mLogic->mLife->DamageObject(damage);
		timer = 0;
	}
	timer += static_cast<float>(TimeManager::Instance()->GetDt());
	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerSpinFinished)
	{
		mLogic->mEnemyLogic->attacked_with == AttackID::Nothing;
		mOwnerStateMachine->ChangeState("GaltzagorriRunState");
	}
	if (mLogic->mLife->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
}
void Sorgina_HammerSpin::LogicExit()
{}

Sorgina_HammerSpin* Sorgina_HammerSpin::Clone()
{
	Sorgina_HammerSpin* state = DBG_NEW Sorgina_HammerSpin(mName.c_str());
	state->mLogic = mLogic;
	state->displacement = displacement;

	return state;
}*/

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH TACKLE							//
//////////////////////////////////////////////////////////////////////////

/*void Sorgina_Tackle::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<SorginaLogic>();

	timer = 0;
}
void Sorgina_Tackle::LogicUpdate()
{
	if (timer <= displacement_time)
		mLogic->mOwner->mPosition += mLogic->mDir * mVelocity;
	//mRigidbody->AddVelocity(displacement * mVelocity);
	else
		mOwnerStateMachine->ChangeState("SorginaRunState");
	timer += static_cast<float>(TimeManager::Instance()->GetDt());
}
void Sorgina_Tackle::LogicExit()
{}
void Sorgina_Tackle::OnCollisionStarted(const BoxCollider* coll)
{
	if (coll->mOwner->mTag == Tags::Miscellaneous)
		mOwnerStateMachine->ChangeState("SorginaRunState");
}

Sorgina_Tackle* Sorgina_Tackle::Clone()
{
	Sorgina_Tackle* state = DBG_NEW Sorgina_Tackle(mName.c_str());
	state->mLogic = mLogic;
	state->timer = timer;
	state->displacement_time = displacement_time;
	state->mVelocity = mVelocity;

	return state;
}*/
