#include "SDL.h"
#include "..//..//Engine/Engine.h"
#include "..//../Engine/src/Level/Demo/Demo.h"
#include "..//../Engine/src/Level/EngineDemoLevel/EngineDemoLevel.h"
#include "..//../Engine/src/Level/Editor/Editor.h"
#include "../../Engine/src/Utilities/MyDebug/LeakDetection.h"

EventDispatcher & event_dispatcher = EventDispatcher::get_instance();

//#define LEAK_CHECKING

int main(int argc, char *argv[])
{
#ifdef LEAK_CHECKING
	_CrtMemState s1, s2, s3;
	_CrtMemCheckpoint(&s1);
#endif

	auto GameWindow = Game->GetWindow();

	if (SystemInit(GameWindow, "Erroak", 1600, 900, false) == false)
		return 0;

	for (int i = 0; i < SDL_NumJoysticks(); i++)
	{
		if (SDL_IsGameController(i))
		{
			SDL_GameControllerOpen(i);
			break;
		}
	}
	
	SystemHideCursor(false);

	GSM->AddLevel(EDITOR_LEVEL, DBG_NEW Editor);
	GSM->AddLevel(ENGINE_PROOF_LEVEL, DBG_NEW EngineDemoLevel);
	
	GSM->Initialize(EDITOR_LEVEL);

	SystemGameLoop();



	SystemExit(GameWindow, GSM);

	SDL_Quit();

#ifdef LEAK_CHECKING
	_CrtDumpMemoryLeaks();
	_CrtMemCheckpoint(&s2);

	if (_CrtMemDifference(&s3, &s1, &s2))
		_CrtMemDumpStatistics(&s3);
#endif

	return 0;
}