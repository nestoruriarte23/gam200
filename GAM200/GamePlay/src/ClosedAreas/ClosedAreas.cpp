#include "ClosedAreas.h"
#include "../../Engine/src/Level/Editor/Editor.h"
#include "../LogicComponents/HealthComponent/HealthComponent.h"
#include <string>
//////////////////////////////////////////////////////////////////////
//							 CLOSED AREAS							//
//////////////////////////////////////////////////////////////////////

void ClosedAreas::Initialize()
{
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	triggered = true;
	playerinfo = player->get_component_type<PlayerInfo>();
	ASSERT(playerinfo != nullptr)
	initial_enemies = static_cast<float>(playerinfo->enemies_slayed);
}
void ClosedAreas::Update()
{
	if (playerinfo->enemies_slayed - initial_enemies == enemy_count)
	{
		while(mBarrier.size() > 0)
		{
			GameObject* barrier = mBarrier.back(); 
			mBarrier.pop_back();
			Game->DestroyObject(barrier);
		}
			
		mTransforms.clear();
	}

	if (!triggered)
		return;

	float length = (player->mPosition - mOwner->mPosition).Length();
	if (length < range)
	{
		for (Transform2D& tran : mTransforms)
		{
			GameObject* barrier = Game->CreateArchetype("barrier");
			barrier->mPosition = tran.mPosition;
			barrier->mScale = tran.mScale;
			barrier->get_component_type<BoxCollider>()->mCollSize = tran.mScale;
			barrier->get_component_type<BoxCollider>()->mRotation = tran.mOrientation;
			barrier->mRotation = tran.mOrientation;
			mBarrier.push_back(barrier);
		}
		triggered = false;
	}
}
void ClosedAreas::Shutdown()
{
	mTransforms.clear();
}

bool ClosedAreas::Edit()
{
	if (!MouseDown(MouseInput::LEFT)) Editor::not_moveable_object = false;
	bool changed = false;

	ImGuiIO& io = ImGui::GetIO();
	ImGui::PushID("ClosedAreas");

	if (ImGui::TreeNode("ClosedAreas"))
	{
		changed = ImGui::DragFloat("Initial Enemies", &enemy_count) || changed;
		changed = ImGui::DragFloat("Range", &range) || changed;
		RenderManager::Instance()->DrawCircleAt(mOwner->mPosition, 100, range*2, Engine::Color::yellow);

		if (ImGui::Button("Add Barrier"))
		{
			Transform2D new_trans(mOwner->mPosition, mOwner->mScale*2, 0);
			mTransforms.push_back(new_trans);
		}
		 
		int i = 1;
		for (Transform2D& tran : mTransforms)
		{
			ImGui::PushID(9874 + i);
			RenderMgr->DrawOrientedRectangleAt(tran.mPosition, tran.mScale.x, tran.mScale.y, tran.mOrientation, Engine::Color::rosa);

			ImGui::Text((std::string("BARRIER  ") + std::to_string(i++)).data());

			ImGui::SameLine(ImGui::GetWindowWidth() - 35);
			if (ImGui::Button("", ImVec2(14, 14))) 
			{
				mTransforms.remove(tran);
				changed = true;
				ImGui::PopID();
				break;
			}

			if (StaticPointToOrientedRect(&InputManager->GetMouse(), &tran.mPosition, tran.mScale.x, tran.mScale.y, tran.mOrientation))
			{
				selected = i;
				Editor::not_moveable_object = true;
			}
				
			if (Editor::not_moveable_object && MouseDown(MouseInput::LEFT) && (selected == i))
			{
				tran.mPosition = InputManager->GetMouse();
				ImGui::PopID();
				break;
			}
				

			changed = ImGui::DragFloat2("Barrier Pos", tran.mPosition.v) || changed;
			changed = ImGui::DragFloat2("Barrier Scale", tran.mScale.v) || changed;
			changed = ImGui::SliderFloat("Barrier Rotation", &tran.mOrientation, 0.0f, 2 * PI) || changed;
			ImGui::PopID();
		}
		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}
	
	ImGui::PopID();
	return changed;
}

void ClosedAreas::OnCollisionStarted(const BoxCollider* coll)
{
	if (coll->mOwner->mTag == Tags::Player && !triggered)
	{
		
		triggered = true;
	}
}

void ClosedAreas::FromJson(nlohmann::json& j)
{
	if (j.find("enemy_count") != j.end())	enemy_count = j["enemy_count"];
	if (j.find("initial_enemies") != j.end())	initial_enemies = j["initial_enemies"];
	if (j.find("range") != j.end())	range = j["range"];

	json & barriers = *j.find("Barriers");
	for (auto it = barriers.begin(); it != barriers.end() && barriers.size() != 0; ++it) 
	{
		Transform2D new_tran;
		if ((*it).find("mPosition") != (*it).end())
		{
			new_tran.mPosition.x = (*it)["mPosition"]["x"];
			new_tran.mPosition.y = (*it)["mPosition"]["y"];
		}
		if ((*it).find("mScale") != (*it).end())
		{
			new_tran.mScale.x = (*it)["mScale"]["x"];
			new_tran.mScale.y = (*it)["mScale"]["y"];
		}
		if ((*it).find("mOrientation") != (*it).end())
			new_tran.mOrientation = (*it)["mOrientation"];
		mTransforms.push_back(new_tran);
	}
}
void ClosedAreas::ToJson(nlohmann::json& j)
{
	j["_type"] = "ClosedAreas";

	j["enemy_count"] = enemy_count;
	j["initial_enemies"] = initial_enemies;
	j["range"] = range;

	j["Barriers"];
	for (Transform2D& trans : mTransforms) 
	{
		json _j;

		_j["mPosition"];
		_j["mPosition"]["x"] = trans.mPosition.x;
		_j["mPosition"]["y"] = trans.mPosition.y;

		_j["mScale"];
		_j["mScale"]["x"] = trans.mScale.x;
		_j["mScale"]["y"] = trans.mScale.y;

		_j["mOrientation"] = trans.mOrientation;

		j["Barriers"].push_back(_j);
	}
}

IComp* ClosedAreas::Clone()
{
   ClosedAreas* closed = DBG_NEW ClosedAreas(*this);
   closed->mTransforms.clear();

   for (Transform2D& trans : mTransforms)
	   closed->mTransforms.push_back(trans);
   return closed;
}
void ClosedAreas::operator=(IComp& _comp)
{
	ClosedAreas* closed = dynamic_cast<ClosedAreas*>(&_comp);

	for (Transform2D& trans : closed->mTransforms)
		mTransforms.push_back(trans);
}
bool ClosedAreas::are_the_same_type(IComp const& lhs)
{
	if (dynamic_cast<const ClosedAreas*>(&lhs)) {
		return true;
	}
	return false;
}

bool ClosedAreas::equal_to(IComp const& other) const
{
	if (ClosedAreas const* p = dynamic_cast<ClosedAreas const*>(&other)) {
		return this->enemy_count == p->enemy_count && initial_enemies == p->initial_enemies;
	}
	else {
		return false;
	}
}