#pragma once
#include "../../Engine/Engine.h"
#include "../PlayerLogic/PlayerLogic.h"

//////////////////////////////////////////////////////////////////////
//							 CLOSED AREAS							//
//////////////////////////////////////////////////////////////////////

class ClosedAreas : public ILogic
{
	RTTI_DECLARATION_INHERITED(ClosedAreas, ILogic);

public:
	ClosedAreas() : ILogic() {}

	void Update();
	void Initialize();
	void Shutdown();

	bool Edit();

	void FromJson(nlohmann::json& j);
	void ToJson(nlohmann::json& j);

	IComp* Clone();
	virtual void operator=(IComp&);
	bool are_the_same_type(IComp const& lhs);

protected:
	bool equal_to(IComp const& other) const;

public:
	GameObject* player;
	PlayerInfo* playerinfo;
	float range = 100;
	float enemy_count;
	float slayed_enemies;
	float initial_enemies;
	bool triggered;

	std::vector<GameObject* > mBarrier;
	std::list<Transform2D> mTransforms;
	int selected = 0;


	void OnCollisionStarted(const BoxCollider* coll);
};