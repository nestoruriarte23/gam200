#pragma once
#include "../../Engine/Engine.h"

struct ShakeInfo
{
	float amplitude = 20;
	float frequency = 5;
	float ellapsed_time = 0;
	float delay = 1;
	float value = 0;
};


class CameraJoint : public IComp
{
	RTTI_DECLARATION_INHERITED(CameraJoint, IComp);

public:
	void Initialize();
	void Update();
	void Shutdown();

	bool Edit();

	void FromJson(nlohmann::json& j);
	void ToJson(nlohmann::json& j);

	IComp* Clone();
	virtual void operator=(IComp&);
	bool are_the_same_type(IComp const& lhs);

protected:
	bool equal_to(IComp const& other) const;

public:
	GameObject*		player;
	CameraComp*		main_camera;
	Vector2			end_joint;
	Vector2			init_joint;
	Vector2			position;

	float			scale_speed = 0.02f;
	float			move_speed = 0.02f;
	float			range	= 0;
	float			scalar	= 1;

	bool			multiple_joints = false;
	bool			cinematic	= false;
	bool			edit = false;
};



class CameraLogic : public ISystem
{
	MAKE_SINGLETON(CameraLogic);
	RTTI_DECLARATION_INHERITED(CameraLogic, ISystem);

public:
	virtual bool Initialize();
	virtual void Update();
	virtual void ShutDown();

	void AddJoint(CameraJoint* joint);
	void RemoveJoint(CameraJoint* joint);

	void UpdateCameraJoint();
	void FollowPlayer();
	void CheckWithJoints();

	void InitShake(float _frequency = 1.0f, float _amplitude = 1.0f, float _delay = 0.5f);

private:
	GameObject*					player;
	CameraComp*					main_camera;
	CameraComp*					hud_camera;
	CameraComp*					back_camera;

	bool						attached;
	Vector2						init_scale;
	Vector2						end_scale;
	
	std::list<CameraJoint*>		mJoints;
	CameraJoint*				CurrentJoint = nullptr;

	void						ShakeCamera();
	ShakeInfo					mShakeInfo;
	bool						shaking = false;
};

#define CameraMgr (CameraLogic::Instance())