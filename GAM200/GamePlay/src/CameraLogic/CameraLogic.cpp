#include "CameraLogic.h"
#include "../../Engine/src/Level/Editor/Editor.h"
#include <cmath>
#include <algorithm>

CameraLogic::CameraLogic() {}

bool CameraLogic::Initialize()
{
	auto space = Game->GetCurrScene()->FindSpace("Background");
	space != nullptr ? back_camera = RenderManager::Instance()->GetCamera(Game->GetCurrScene()->FindSpace("Background")) : nullptr;
	main_camera = RenderManager::Instance()->GetCamera(Game->GetCurrScene()->FindSpace("MainArea"));
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	
	init_scale = main_camera->mViewSize;
	return true;
}

void CameraLogic::Update()
{
	CheckWithJoints();

	if (CurrentJoint)
		UpdateCameraJoint();
	else
		FollowPlayer();

	if(shaking)
		ShakeCamera();

	if (back_camera)
	{
		back_camera->mOwner->mPosition = main_camera->mOwner->mPosition;
		back_camera->mViewSize = main_camera->mViewSize;
	}
}

void CameraLogic::ShutDown()
{
	mJoints.clear();
}

void CameraLogic::AddJoint(CameraJoint* joint)
{
	mJoints.push_back(joint);
}
void CameraLogic::RemoveJoint(CameraJoint* joint)
{
	mJoints.remove(joint);
}

void CameraLogic::UpdateCameraJoint()
{
	CurrentJoint->Update();
	main_camera->mOwner->mPosition = Vector2::Lerp(main_camera->mOwner->mPosition, CurrentJoint->position, CurrentJoint->move_speed);
	main_camera->mViewSize = Vector2::Lerp(main_camera->mViewSize, end_scale, CurrentJoint->scale_speed);
}

void CameraLogic::FollowPlayer()
{
	if (main_camera->mViewSize != init_scale)
	{
		Vector2 current_scale(floor(main_camera->mViewSize.x), floor(main_camera->mViewSize.y));
		if (current_scale != init_scale)
			main_camera->mViewSize = Vector2::Lerp(main_camera->mViewSize, init_scale, 0.05f);
		else
			main_camera->mViewSize = init_scale;
	}
	
	main_camera->mOwner->mPosition = Vector2::Lerp(main_camera->mOwner->mPosition, player->mPosition, 0.05f);
}

void CameraLogic::CheckWithJoints()
{
	if (CurrentJoint && (CurrentJoint->position - player->mPosition).Length() < CurrentJoint->range)
		return;

	CurrentJoint = nullptr;
	std::for_each(mJoints.begin(), mJoints.end(),
		[&](CameraJoint* joint)
		{
			Vector2 disntace(joint->position - player->mPosition);
			if (disntace.Length() < joint->range)
			{
				CurrentJoint = joint;
				end_scale = init_scale*joint->scalar;
			}
		});
}

void CameraLogic::InitShake(float _frequency, float _amplitude, float _delay)
{
	shaking = true;
	mShakeInfo.ellapsed_time = 0.0f;
	mShakeInfo.delay = _delay;
	mShakeInfo.frequency = _frequency;
	mShakeInfo.amplitude = _amplitude;
}

void CameraLogic::ShakeCamera()
{
	mShakeInfo.ellapsed_time += static_cast<float>(TimeManager::Instance()->GetDt());
	if (mShakeInfo.ellapsed_time >= mShakeInfo.delay)
		shaking = false;

	main_camera->mOwner->mPosition.x += sin(mShakeInfo.frequency*mShakeInfo.value)*mShakeInfo.amplitude;
	main_camera->mOwner->mPosition.y += cos(mShakeInfo.frequency*mShakeInfo.value)*mShakeInfo.amplitude;
	mShakeInfo.value += 1;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////											CameraJoint													////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void CameraJoint::Initialize()
{
	main_camera = RenderManager::Instance()->GetCamera(Game->GetCurrScene()->FindSpace("MainArea"));
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");

	if(end_joint == Vector2(0,0))
		end_joint = mOwner->mPosition;
	if (init_joint == Vector2(0, 0))
		init_joint = mOwner->mPosition;
	CameraMgr->AddJoint(this);
}

void CameraJoint::Update()
{
	if (multiple_joints)
	{
		position = player->mPosition;
		position.Clamp(init_joint, end_joint);
	}
	else
		position = mOwner->mPosition;
}

void CameraJoint::Shutdown()
{
	CameraMgr->RemoveJoint(this);
}


bool CameraJoint::Edit()
{
	if(!MouseDown(MouseInput::LEFT)) Editor::not_moveable_object = false;
	bool changed = false;
	
	ImGuiIO& io = ImGui::GetIO();
	ImGui::PushID("CameraJoint");

	if (ImGui::TreeNode("CameraJoint"))
	{
		
		changed = ImGui::DragFloat("Range", &range) || changed;
		changed = ImGui::DragFloat("Scalar", &scalar, 0.05f) || changed;
		changed = ImGui::DragFloat("Move Speed", &move_speed, 0.002f, 0.0f, 1.0f) || changed;
		changed = ImGui::DragFloat("Scale Speed", &scale_speed, 0.002f, 0.0f, 1.0f) || changed;
		changed = ImGui::Checkbox("Cinematic ", &cinematic) || changed;
		ImGui::SameLine();
		changed = ImGui::Checkbox("Multiple joints", &multiple_joints) || changed;

		if (multiple_joints)
		{
			changed = ImGui::DragFloat2("Second joint", end_joint.v) || changed;
			RenderManager::Instance()->DrawCircleAt(end_joint, 50, 70, Engine::Color::yellow);
			RenderManager::Instance()->DrawCircleAt(end_joint, 50, 50, Engine::Color::yellow);
			RenderManager::Instance()->DrawCircleAt(end_joint, 50, 30, Engine::Color::yellow);

			changed = ImGui::DragFloat2("init joint", init_joint.v) || changed;
			RenderManager::Instance()->DrawCircleAt(init_joint, 50, 70, Engine::Color::yellow);
			RenderManager::Instance()->DrawCircleAt(init_joint, 50, 50, Engine::Color::yellow);
			RenderManager::Instance()->DrawCircleAt(init_joint, 50, 30, Engine::Color::yellow);
			RenderManager::Instance()->DrawLineAt(init_joint, end_joint, Engine::Color::yellow);

			if (StaticPointToStaticCircle(&InputManager->GetMouse(), &end_joint, 70))
			{
				Editor::not_moveable_object = true;
				edit = true;
			}
			else if (StaticPointToStaticCircle(&InputManager->GetMouse(), &init_joint, 70))
			{
				Editor::not_moveable_object = true;
				edit = false;
			}

			if (Editor::not_moveable_object && edit && MouseDown(MouseInput::LEFT))
				end_joint = InputManager->GetMouse();

			if (Editor::not_moveable_object && !edit && MouseDown(MouseInput::LEFT))
				init_joint = InputManager->GetMouse();
		}
		else 
			Editor::not_moveable_object = false;
			
		Vector2 result_scale(main_camera->mViewSize * scalar);
		RenderManager::Instance()->DrawCircleAt(mOwner->mPosition, 50, range*2, Engine::Color::white);
		RenderManager::Instance()->DrawRectangleAt(mOwner->mPosition, result_scale, Engine::Color::white);

		ImGui::TreePop();
	}
	else
	{
		Editor::not_moveable_object = false;
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}
	ImGui::PopID();
	return changed;
}

void CameraJoint::FromJson(nlohmann::json& j)
{
	if (j.find("end_joint") != j.end())
	{
		end_joint.x = j["end_joint"]["x"];
		end_joint.y = j["end_joint"]["y"];
	}
	if (j.find("init_joint") != j.end())
	{
		init_joint.x = j["init_joint"]["x"];
		init_joint.y = j["init_joint"]["y"];
	}
	
	if(j.find("range") != j.end())		range = j["range"];

	if (j.find("scalar") != j.end())	scalar = j["scalar"];

	if (j.find("move_speed") != j.end()) move_speed = j["move_speed"];

	if (j.find("scale_speed") != j.end())scale_speed = j["scale_speed"];

	if (j.find("cinematic") != j.end()) cinematic = j["cinematic"];

	if (j.find("multiple_joints") != j.end()) multiple_joints = j["multiple_joints"];
}
void CameraJoint::ToJson(nlohmann::json& j)
{
	j["_type"] = "CameraJoint";

	j["end_joint"];
	j["end_joint"]["x"] = end_joint.x;
	j["end_joint"]["y"] = end_joint.y;

	j["init_joint"];
	j["init_joint"]["x"] = init_joint.x;
	j["init_joint"]["y"] = init_joint.y;

	j["range"] = range;
	j["scalar"] = scalar;
	j["move_speed"] = move_speed;
	j["scale_speed"] = scale_speed;

	j["cinematic"] = cinematic;
	j["multiple_joints"] = multiple_joints;
}

void CameraJoint::operator=(IComp& _comp)
{
	CameraJoint& camera_joint = *dynamic_cast<CameraJoint*>(&_comp);

	end_joint = camera_joint.end_joint;
	range = camera_joint.range;
	scalar = camera_joint.scalar;
	move_speed = camera_joint.move_speed;
	scale_speed = camera_joint.scale_speed;
	cinematic = camera_joint.cinematic;
	multiple_joints = camera_joint.multiple_joints;
}

IComp* CameraJoint::Clone()
{
	return DBG_NEW CameraJoint(*this);
}

bool CameraJoint::equal_to(IComp const& other)const
{
	if (CameraJoint const* p = dynamic_cast<CameraJoint const*>(&other)) {
		return main_camera == p->main_camera && range == p->range ;
	}
	else {
		return false;
	}
}

bool CameraJoint::are_the_same_type(IComp const& lhs)
{
	if (dynamic_cast<const CameraJoint*>(&lhs)) {
		return true;
	}
	return false;
}