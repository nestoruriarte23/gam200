#pragma once
#include "../../Engine/Engine.h"
#include "..//..//..//Engine/src/Input/InputEvent.h"
#include "../../Engine/src/Graphics/Particles/ParticleProperties.h"

class PlayerAnimationChange;
class EnemyLogic;
class Health;
class TextComp;

//////////////////////////////////////////////////////////////////////
//							 Player Info							//
//////////////////////////////////////////////////////////////////////

class PlayerInfo : public ILogic
{
	RTTI_DECLARATION_INHERITED(PlayerInfo, ILogic);

public:
	PlayerInfo() : ILogic() {}

	void Update();
	void Initialize();
	void Shutdown();

	PropertyMap properties;

	bool ComboStarted = false;
	bool PlayerHeal = false;
	bool reset;
	bool HH;
	int  ComboCount = 0;
	int  ObjectCount = 0;
	int	 comboid;
	unsigned enemies_slayed;
	float timer;
	PROP_Val(float, cancellation_time, 2.0f);

	
	std::vector<std::string> Pickable_objects;

	bool Edit();

	void FromJson(nlohmann::json& j);
	void ToJson(nlohmann::json& j);

	IComp* Clone();
	virtual void operator=(IComp&);
	bool are_the_same_type(IComp const& lhs);

protected:
	bool equal_to(IComp const& other) const;
};

//////////////////////////////////////////////////////////////////////////
//						PLAYER WITH KEYBOARD							//
//////////////////////////////////////////////////////////////////////////

class PlayerMovement : public ILogic
{
	RTTI_DECLARATION_INHERITED(PlayerMovement, ILogic);
	LOGIC_COMPONENT(PlayerMovement);

public:
	void Update();
	void Initialize();
	void Shutdown();

	Vector2 direction;
	bool BlockMovement;
	Vector2 movement;

private:
	Vector2 movementKeyboard;
};

//////////////////////////////////////////////////////////////////////////
//							PLAYER IDLE STATE							//
//////////////////////////////////////////////////////////////////////////

struct PlayerIdleState : public State
{
	RTTI_DECLARATION_INHERITED(PlayerIdleState, State);
	PlayerIdleState(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	PlayerIdleState* Clone();

	GameObject* mOwner;
	PlayerMovement* mDirection;
	Health* mHealth;
	PlayerAnimationChange* mAnimation;
	float HH_timer;
	PROP_Val(float, HH_cooldown, 0.05f);
};


//////////////////////////////////////////////////////////////////////////
//							PLAYER RUN STATE							//
//////////////////////////////////////////////////////////////////////////

struct PlayerRunState : public State
{
	RTTI_DECLARATION_INHERITED(PlayerRunState, State);
	PlayerRunState(const char* name)
		:State(name)
	{
	}

	~PlayerRunState();
	PlayerRunState* Clone();

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	GameObject* mOwner;
	PlayerAnimationChange* mAnimation = NULL;
	PlayerMovement* mDirection = NULL;
	Health* mHealth;
	PROP_Val(float, mVelocity, 10.0f);
	float HH_timer;
	PROP_Val(float, HH_cooldown, 0.025f);
};

//////////////////////////////////////////////////////////////////////////
//						PLAYER HAMMER HIT STATE							//
//////////////////////////////////////////////////////////////////////////

struct PlayerHammerHitState : public State
{
	RTTI_DECLARATION_INHERITED(PlayerHammerHitState, State);
	PlayerHammerHitState(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit(); 
	PropertyMap properties;

	PlayerHammerHitState* Clone();

	GameObject* mOwner;
	PlayerMovement* mDirection = NULL;
	PlayerInfo* mPlayerInfo = NULL;
	RigidBody* mRigidBody = NULL;
	BoxCollider* mBox = NULL;
	EnemyLogic* enemy = NULL;
	std::vector<GameObject*> targets;
	Space* mSpace;
	Ray mAttack;
	Matrix33 rotMatrix;
	bool attacked;
	PlayerAnimationChange* mAnimation;

	PROP_Val(float, AttackRange, 300.0f);
	PROP_Val(float, mVelocity, 25.0f);
	float inertia_time;
	PROP_Val(float, timer, 0.05f);
	float reload_time;
	PROP_Val(float, reload_cooldown, 0.35f);

};

//////////////////////////////////////////////////////////////////////////
//						PLAYER TACKLE LOGIC								//
//////////////////////////////////////////////////////////////////////////

struct TackleLogic : public ILogic
{
	RTTI_DECLARATION_INHERITED(TackleLogic, ILogic);
	LOGIC_COMPONENT(TackleLogic);

public:
	TackleLogic() : ILogic() {}

	void Initialize();
	void Update();
	void ShutDown();

	void OnCollisionStarted(const BoxCollider* coll);
	GameObject* player;
	EnemyLogic* enemy;
	bool tackling;
};

//////////////////////////////////////////////////////////////////////////
//						PLAYER TACKLE STATE								//
//////////////////////////////////////////////////////////////////////////

struct PlayerTackleState : public State
{
	RTTI_DECLARATION_INHERITED(PlayerTackleState, State);
	PlayerTackleState(const char* name)
		:State(name)
	{}
	
	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	PlayerTackleState* Clone();

	GameObject* mOwner;
	PlayerInfo* mPlayerInfo;
	GameObject* mTackle;
	TackleLogic* mTackleLogic;
	PlayerMovement* mDirection;
	RigidBody* mRigidBody = NULL;
	Health* mHealth;

	PROP_Val(float, mVelocity, 50.0f);
	float timer;
	PROP_Val(float, inertia_time, 0.075f);
};

//////////////////////////////////////////////////////////////////////////
//					PLAYER SUPER HAMMER HIT STATE						//
//////////////////////////////////////////////////////////////////////////

struct PlayerSuperHammerHitState : public State
{
	RTTI_DECLARATION_INHERITED(PlayerSuperHammerHitState, State);
	PlayerSuperHammerHitState(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	PlayerSuperHammerHitState* Clone();

	GameObject* mOwner;
	PlayerInfo* mPlayerInfo;
	Space* mSpace;
	Health* mHealth;
	std::vector<GameObject*> targets;
	Ray mAttack;
	Matrix33 rotMatrix;
	PlayerMovement* mDirection;
	EnemyLogic* enemy;
	PlayerAnimationChange* mAnimation;
	bool animation_chaned;

	bool attacked;
	PROP_Val(float, mVelocity, 2.5f);
	PROP_Val(float, knockvelocity, 50.0f);
	PROP_Val(float, AttackRange, 400.0f);

	float recover_time;
	PROP_Val(float, recover_cooldown, 0.5f);

	float charge_timer;
	PROP_Val(float, charge_needed, 1.0f);
};

//////////////////////////////////////////////////////////////////////////
//						PLAYER HAMMER SPIN STATE						//
//////////////////////////////////////////////////////////////////////////

struct PlayerHammerSpinState : public State
{
	RTTI_DECLARATION_INHERITED(PlayerHammerSpinState, State);
	PlayerHammerSpinState(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	PlayerHammerSpinState* Clone();

	PlayerAnimationChange* mAnimation;
	Health* mHealth;	
	ParticleSystem* tornado_particles;
	PlayerMovement* mDirection;
	GameObject* mOwner;
	GameObject* tornado;
	BoxCollider* tCollider;
	PROP_Val(float, mVelocity, 3.0f);
	PROP_Val(float, increment, 1.5f);
	PROP_Val(float, recoil, 0.15f);
};

//////////////////////////////////////////////////////////////////////////
//							  TORNADO LOGIC						    	//
//////////////////////////////////////////////////////////////////////////

struct TornadoLogic : public ILogic
{
	RTTI_DECLARATION_INHERITED(TornadoLogic, ILogic);
	LOGIC_COMPONENT(TornadoLogic);

public:
	TornadoLogic() : ILogic() {}

	void Initialize();
	void Update();
	void ShutDown();

	void OnCollisionStarted(const BoxCollider* coll);
	EnemyLogic* enemy;
};

//////////////////////////////////////////////////////////////////////////
//							PLAYER DEATH STATE							//
//////////////////////////////////////////////////////////////////////////

struct PlayerDeathState : public State
{
	RTTI_DECLARATION_INHERITED(PlayerDeathState, State);
	PlayerDeathState(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	float alpha;
	Engine::Color mColor;
	GameObject* deathscreen;
	GameObject* deathtext;
	Sprite* screenSprite;
	TextComp* textSprite;
	GameObject* mOwner;
	PlayerAnimationChange* mAnimation;
};

//////////////////////////////////////////////////////////////////////
//						Combo Player State							//
//////////////////////////////////////////////////////////////////////

struct ComboSys : public State
{
	RTTI_DECLARATION_INHERITED(ComboSys, State);
	ComboSys(const char* name)
		:State(name)
	{}

	ComboSys* Clone();

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	PlayerInfo* mPlayerInfo = NULL;
	f64 TimeEllapsed = 0.0f;
	int TempComboCount = 0;
};