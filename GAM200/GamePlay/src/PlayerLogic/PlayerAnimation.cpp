#include "PlayerAnimation.h"
#include "../../Engine/src/Utilities/MyDebug/MyDebug.h"
#include "../../spine/include/spine/spine.h"

void PlayerAnimationChange::Initialize()
{
	mAnimationComp = mOwner->get_component_type<SpineAnimation>();
	ASSERT(mAnimationComp != nullptr);
	mPlayerDirection = mOwner->get_component_type<PlayerMovement>();
	ASSERT(mPlayerDirection != nullptr);

	current_animation = "PatxiRun";
	current_direction = vec_directions[Directions::West];
	anim_time = 0;
}

void PlayerAnimationChange::Update()
{
	mDirection = mPlayerDirection->direction;

	if (mDirection != Vector2(0, 0))
		DirectionChanged();

	if (current_direction != previous_direction)
	{
		anim_time = mAnimationComp->GetAnimationState()->getCurrent(0)->getTrackTime();
		ChangeAnimation(mbUnique);
		anim_time += static_cast<float>(TimeManager::Instance()->GetDt()) * mAnimationComp->mRelSpeed;
		mAnimationComp->GetAnimationState()->getCurrent(0)->setTrackTime(anim_time);
	}
	previous_direction = current_direction;
}

void PlayerAnimationChange::Shutdown()
{}

void PlayerAnimationChange::ChangeAnimation(bool unique)
{
	if (unique)
		mAnimationComp->SetCurrentAnim(current_animation);
	else
		mAnimationComp->SetCurrentAnim(current_animation + current_direction);
}

void PlayerAnimationChange::ChangeAnimation(const std::string& change, bool unique)
{
	current_animation = change;
	mbUnique = unique;
	ChangeAnimation(unique);
}

void PlayerAnimationChange::DirectionChanged()
{
	actual_angle = ToDegrees(static_cast<double>(mDirection.GetAngle()));
	if (actual_angle >= -22.5 && actual_angle < 22.5)
		current_direction = vec_directions[Directions::North];
	else if (actual_angle >= -67.5 && actual_angle < -22.5)
		current_direction = vec_directions[Directions::NorthWest];
	else if (actual_angle >= -112.5 && actual_angle < -67.5)
		current_direction = vec_directions[Directions::West];
	else if (actual_angle >= -157.5 && actual_angle < -112.5)
		current_direction = vec_directions[Directions::ShoutWest];
	else if ((actual_angle >= -180 && actual_angle < -157.5) || (actual_angle > 157.5 && (int)actual_angle <= 180))
		current_direction = vec_directions[Directions::South];
	else if (actual_angle >= 112.5 && actual_angle < 157.5)
		current_direction = vec_directions[Directions::ShoutWest];
	else if ((actual_angle > 67.5 && actual_angle <= 112.5))
		current_direction = vec_directions[Directions::West];
	else if (actual_angle >= 22.5 && actual_angle < 67.5)
		current_direction = vec_directions[Directions::NorthWest];

	/* Set it to true in that range of the angle */
	mAnimationComp->SetFlipX(actual_angle >= 22.5 && actual_angle < 157.5);
}

std::string PlayerAnimationChange::GetActualAnimName()
{
	return current_animation + current_direction;
}
