#include "PlayerLogic.h"
#include "PlayerAnimation.h"
#include "../../EnemyLogic.h"
#include "../LogicComponents/HealthComponent/HealthComponent.h"
#include "../../Engine/src/Graphics/Text/Text.h"

//////////////////////////////////////////////////////////////////////////
//							PLAYER IDLE STATE							//
//////////////////////////////////////////////////////////////////////////

void PlayerIdleState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	mDirection = mOwner->get_component_type<PlayerMovement>();
	ASSERT(mDirection != nullptr);
	HH_timer = 0.0f;

	mHealth = mOwner->get_component_type<Health>();
	ASSERT(mHealth != nullptr)
	mAnimation = mOwner->get_component_type<PlayerAnimationChange>();
	ASSERT(mAnimation != nullptr)
	mAnimation->ChangeAnimation(std::string("PatxiIddle"));
}
void PlayerIdleState::LogicUpdate()
{
	/* MOVEMENT */
	if(mDirection->movement != Vector2(0, 0))
		mOwnerStateMachine->ChangeState("PlayerRunState");
	//SUPER HAMMER HIT
	if (KeyDown(Key::X) || ButtonDown(SDL_CONTROLLER_BUTTON_X))
		mOwnerStateMachine->ChangeState("PlayerSuperHammerHitState");
	//HAMMER HIT
	if (HH_timer > HH_cooldown)
		if (KeyTriggered(Key::X) || ButtonTriggered(SDL_CONTROLLER_BUTTON_X))
			mOwnerStateMachine->ChangeState("PlayerHammerHitState");
	//HAMMER TACKLE
	if (KeyTriggered(Key::C) || ButtonTriggered(SDL_CONTROLLER_BUTTON_A))
		mOwnerStateMachine->ChangeState("PlayerTackleState");
	//HAMMER SPIN
	if (ButtonDown(SDL_CONTROLLER_BUTTON_B) || KeyDown(Key::V))
		mOwnerStateMachine->ChangeState("PlayerHammerSpinState");
	//COOL DOWNS
	if (HH_timer <= HH_cooldown)
		HH_timer += static_cast<float>(TimeManager::Instance()->GetDt());
	//DEATH
	if (mHealth->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("PlayerDeathState");
}
void PlayerIdleState::LogicExit()
{}
PlayerIdleState* PlayerIdleState::Clone()
{
	PlayerIdleState* state = DBG_NEW PlayerIdleState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//							PLAYER RUN STATE							//
//////////////////////////////////////////////////////////////////////////

void PlayerRunState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	HH_timer = 0.0f;

	mHealth = mOwner->get_component_type<Health>();
	ASSERT(mHealth != nullptr)
	mAnimation = mOwner->get_component_type<PlayerAnimationChange>();
	ASSERT(mAnimation != nullptr)
	mDirection = mOwner->get_component_type<PlayerMovement>();
	ASSERT(mDirection != nullptr)

	mAnimation->ChangeAnimation(std::string("PatxiRun"));
}
void PlayerRunState::LogicUpdate()
{
	//IDLE
	if (mDirection->movement == Vector2(0, 0))
		mOwnerStateMachine->ChangeState("PlayerIdleState");
	//SUPER HAMMER HIT
	if (KeyDown(Key::X) || ButtonDown(SDL_CONTROLLER_BUTTON_X))
		mOwnerStateMachine->ChangeState("PlayerSuperHammerHitState");
	//HAMMER HIT
	if (HH_timer > HH_cooldown)
		if (KeyTriggered(Key::X) || ButtonTriggered(SDL_CONTROLLER_BUTTON_X))
			mOwnerStateMachine->ChangeState("PlayerHammerHitState");
	//HAMMER TACKLE
	if (KeyTriggered(Key::C) || ButtonTriggered(SDL_CONTROLLER_BUTTON_A))
		mOwnerStateMachine->ChangeState("PlayerTackleState");
	//HAMMER SPIN
	if (ButtonDown(SDL_CONTROLLER_BUTTON_B) || KeyDown(Key::V))
		mOwnerStateMachine->ChangeState("PlayerHammerSpinState");
	//MOVEMENT
	//mOwner->mPosition += mDirection->movement * mVelocity;
	mOwner->Move(mDirection->movement, mVelocity);

	//COOLDOWNS
	if (HH_timer <= HH_cooldown)
		HH_timer += static_cast<float>(TimeManager::Instance()->GetDt());
	//DEATH
	if (mHealth->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("PlayerDeathState");
}
void PlayerRunState::LogicExit()
{}
PlayerRunState::~PlayerRunState()
{
}
PlayerRunState* PlayerRunState::Clone()
{
	PlayerRunState* state = DBG_NEW PlayerRunState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						PLAYER HAMMER HIT STATE							//
//////////////////////////////////////////////////////////////////////////

const static float SpeedMultiplier = 4.0f;

void PlayerHammerHitState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	mSpace = mOwner->GetSpace();
	inertia_time = 0;
	reload_time = 0;
	attacked = false;

	mDirection = mOwner->get_component_type<PlayerMovement>();
	ASSERT(mDirection != nullptr);
	mPlayerInfo = mActor->mOwner->get_component_type<PlayerInfo>();
	ASSERT(mPlayerInfo != nullptr);
	mRigidBody = mOwner->get_component_type<RigidBody>();
	ASSERT(mRigidBody != nullptr);
	mBox = mOwner->get_component_type<BoxCollider>();
	ASSERT(mBox != nullptr);
	//mPlayerInfo->ComboStarted = true;
	//mPlayerInfo->ComboCount += 1;
	mPlayerInfo->HH = true;

	mAnimation = mOwner->get_component_type<PlayerAnimationChange>();
	ASSERT(mAnimation != nullptr)
	if(mPlayerInfo->comboid == 0)
		mAnimation->ChangeAnimation(std::string("PatxiHit"));
	else
		mAnimation->ChangeAnimation(std::string("PatxiHit2"));
	mAnimation->GetAnimation()->mRelSpeed *= SpeedMultiplier;
}
void PlayerHammerHitState::LogicUpdate()
{
	mDirection->BlockMovement = true;
	//ATTACK
	if (!attacked)
	{
		for (float rot = -35.0f; rot <= 35.0f; rot += 5.0f)
		{
			mAttack = Ray(Vector2(mOwner->mPosition.x + mBox->mOffset.x, mOwner->mPosition.y + mBox->mOffset.y), rotMatrix.RotDeg(rot) * mDirection->direction, AttackRange, 0);
			mAttack.DrawRay();
			CastRay(mAttack, mSpace, Tags::Enemies, targets);
		}
		for (auto it = targets.begin(); it != targets.end(); it++)
		{
			enemy = (*it)->get_component_type<EnemyLogic>();
			ASSERT(enemy != nullptr);
			enemy->attacked(AttackID::HammerHit);
		}
		targets.clear();
		attacked = true;
	}
	inertia_time += static_cast<float>(TimeManager::Instance()->GetDt());

	//INERTIA
	if (inertia_time < timer)
		mOwner->mPosition += mDirection->direction * mVelocity;
	else
		reload_time += static_cast<float>(TimeManager::Instance()->GetDt());
	//COOLDOWN AFTER ATTACK
	if (reload_time >= reload_cooldown)
	{
		mDirection->BlockMovement = false;
		mOwnerStateMachine->ChangeState("PlayerRunState");
	}
}
void PlayerHammerHitState::LogicExit()
{
	mAnimation->GetAnimation()->mRelSpeed /= SpeedMultiplier;
}
PlayerHammerHitState* PlayerHammerHitState::Clone()
{
	PlayerHammerHitState* state = DBG_NEW PlayerHammerHitState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						PLAYER TACKLE LOGIC								//
//////////////////////////////////////////////////////////////////////////

void TackleLogic::Initialize()
{
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	tackling = false;
	subscribe_collision_event();
}
void TackleLogic::Update()
{}
void TackleLogic::ShutDown()
{}
void TackleLogic::OnCollisionStarted(const BoxCollider* coll)
{
	if (coll->mOwner->mTag == Tags::Enemies && tackling)
	{
		enemy = coll->mOwner->get_component_type<EnemyLogic>();
		ASSERT(enemy != NULL)
		enemy->attacked(AttackID::Tackle);
	}
}

//////////////////////////////////////////////////////////////////////////
//						PLAYER TACKLE STATE								//
//////////////////////////////////////////////////////////////////////////

void PlayerTackleState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	mTackle = Game->CreateArchetype("tackle");
	timer = 0;

	mDirection = mOwner->get_component_type<PlayerMovement>();
	ASSERT(mDirection != nullptr);
	if(mTackle)
		mTackleLogic = mTackle->get_component_type<TackleLogic>();
	ASSERT(mTackleLogic != nullptr); 
	mHealth = mOwner->get_component_type<Health>();
	ASSERT(mHealth != nullptr);
	mPlayerInfo = mActor->mOwner->get_component_type<PlayerInfo>();
	ASSERT(mPlayerInfo != nullptr);
	mPlayerInfo->reset = true;
}
void PlayerTackleState::LogicUpdate()
{
	mHealth->player_invulnerable = true;
	mTackleLogic->tackling = true;
	mDirection->BlockMovement = true;
	if(mTackle)
		mTackle->mPosition = mOwner->mPosition + 
			Vector2(mDirection->direction.x, mDirection->direction.y) * 100;
	if (timer < inertia_time)
		mOwner->mPosition += mDirection->direction * mVelocity;
	else
	{
		mHealth->player_invulnerable = false;
		mTackleLogic->tackling = false;
		mDirection->BlockMovement = false;
		mOwnerStateMachine->ChangeState("PlayerRunState");
	}
	timer += static_cast<float>(TimeManager::Instance()->GetDt());
}
void PlayerTackleState::LogicExit()
{
	Game->DestroyObject(mTackle);
}
PlayerTackleState* PlayerTackleState::Clone()
{
	PlayerTackleState* state = DBG_NEW PlayerTackleState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//					PLAYER SUPER HAMMER HIT STATE						//
//////////////////////////////////////////////////////////////////////////

void PlayerSuperHammerHitState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	mSpace = mOwner->GetSpace();
	charge_timer = 0;
	recover_time = 0;
	attacked = false;
	animation_chaned = false;

	mHealth = mOwner->get_component_type<Health>();
	ASSERT(mHealth != nullptr)
	mDirection = mOwner->get_component_type<PlayerMovement>();
	ASSERT(mDirection != nullptr);
	mAnimation = mOwner->get_component_type<PlayerAnimationChange>();
	ASSERT(mAnimation != nullptr)
	mAnimation->ChangeAnimation(std::string("PatxiSuperHammerHitAnticip"));

	mPlayerInfo = mActor->mOwner->get_component_type<PlayerInfo>();
	ASSERT(mPlayerInfo != nullptr);
	//mPlayerInfo->ComboStarted = true;
	//mPlayerInfo->ComboCount += 1;
}
void PlayerSuperHammerHitState::LogicUpdate()
{
	BoxCollider* mBox = mOwner->get_component_type<BoxCollider>();
	//CHARGE THE ATTACK
	if (ButtonDown(SDL_CONTROLLER_BUTTON_X) || KeyDown(Key::X))
	{
		charge_timer += static_cast<float>(TimeManager::Instance()->GetDt());

		//MOVEMENT
		mOwner->mPosition += mDirection->movement * mVelocity;
	}

	//CHARGE CANCELLED
	if ((!ButtonDown(SDL_CONTROLLER_BUTTON_X) && !KeyDown(Key::X)) && charge_timer < charge_needed)
		//mOwnerStateMachine->ChangeState("PlayerRunState");
		mOwnerStateMachine->ChangeState("PlayerHammerHitState");

	if (charge_timer >= charge_needed && !animation_chaned)
	{
		mAnimation->ChangeAnimation(std::string("PatxiSuperHammerHitFullyCharged"));
		animation_chaned = true;
	}
	//ATTACK CHARGED
	if ((!ButtonDown(SDL_CONTROLLER_BUTTON_X) && !KeyDown(Key::X)) && charge_timer >= charge_needed)
	{
		mPlayerInfo->reset = true;

		if (!attacked)
		{
		mAnimation->ChangeAnimation(std::string("PatxiSuperHammerHit"));
			mDirection->BlockMovement = true;
			//ATTACK
			for (float rot = -25.0f; rot <= 25.0f; rot += 5.0f)
			{
				mAttack = Ray(Vector2(mOwner->mPosition.x + mBox->mOffset.x, mOwner->mPosition.y + mBox->mOffset.y), rotMatrix.RotDeg(rot) * mDirection->direction, AttackRange, 0);
				mAttack.DrawRay();
				CastRay(mAttack, mSpace, Tags::Enemies, targets);
			}

			//RESOLVE ENEMIES
			for (auto it = targets.begin(); it != targets.end(); it++)
			{
				enemy = (*it)->get_component_type<EnemyLogic>();
				ASSERT(enemy != nullptr);
				enemy->attacked(AttackID::SuperHammerHit);
			}
			targets.clear();
			attacked = true;
		}
		//RECOVERING TIME AFTER ATTACK
		recover_time += static_cast<float>(TimeManager::Instance()->GetDt());
		if (recover_time >= recover_cooldown)
		{
			mDirection->BlockMovement = false;
			mOwnerStateMachine->ChangeState("PlayerRunState");
		}
	}
	//DEATH
	if (mHealth->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("PlayerDeathState");
}
void PlayerSuperHammerHitState::LogicExit()
{}
PlayerSuperHammerHitState* PlayerSuperHammerHitState::Clone()
{
	PlayerSuperHammerHitState* state = DBG_NEW PlayerSuperHammerHitState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						PLAYER HAMMER SPIN STATE						//
//////////////////////////////////////////////////////////////////////////

void PlayerHammerSpinState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	tornado = Game->CreateArchetype("tornado");

	mAnimation = mOwner->get_component_type<PlayerAnimationChange>();
	ASSERT(mAnimation != nullptr)
	mDirection = mOwner->get_component_type<PlayerMovement>();
	ASSERT(mDirection != nullptr)
	mHealth = mOwner->get_component_type<Health>();
	ASSERT(mHealth != nullptr)
	mAnimation->ChangeAnimation(std::string("PatxiSpin"));
	tCollider = tornado->get_component_type<BoxCollider>();
	ASSERT(tCollider != nullptr)
	tornado_particles = mOwner->get_component_type<ParticleSystem>();
	ASSERT(tornado_particles != nullptr)
}
void PlayerHammerSpinState::LogicUpdate()
{
	if (ButtonDown(SDL_CONTROLLER_BUTTON_B) || KeyDown(Key::V))
	{
		mOwner->mPosition += mDirection->movement * mVelocity;
		tCollider->mCollSize += Vector2(increment, increment);
		tornado->mScale += Vector2(increment, increment);
		tornado->mPosition = mOwner->mPosition;
		tornado_particles->SetPosition(mOwner->mPosition);
		tornado_particles->SetDestination(mOwner->mPosition);
		tornado_particles->SetTargetPoint(mOwner->mPosition);
		tornado_particles->SetPause(false);
		tornado_particles->SetRadius(tornado->mScale.x);
		mHealth->DamageObject(recoil);
	}
	else
	{
		tornado_particles->SetPause(true);
		mOwnerStateMachine->ChangeState("PlayerRunState");
	}
	//DEATH
	if (mHealth->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("PlayerDeathState");
}
void PlayerHammerSpinState::LogicExit()
{
	Game->DestroyObject(tornado);
}
PlayerHammerSpinState* PlayerHammerSpinState::Clone()
{
	PlayerHammerSpinState* state = DBG_NEW PlayerHammerSpinState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//							  TORNADO LOGIC						    	//
//////////////////////////////////////////////////////////////////////////

void TornadoLogic::Initialize()
{
	subscribe_collision_event();
}
void TornadoLogic::Update()
{}
void TornadoLogic::ShutDown()
{}
void TornadoLogic::OnCollisionStarted(const BoxCollider* coll)
{
	if (coll->mOwner->mTag == Tags::Enemies)
	{
		enemy = coll->mOwner->get_component_type<EnemyLogic>();
		ASSERT(enemy != NULL)
		enemy->attacked(AttackID::HammerSpin);
	}
}

//////////////////////////////////////////////////////////////////////////
//							PLAYER DEATH STATE							//
//////////////////////////////////////////////////////////////////////////

void PlayerDeathState::LogicEnter()
{
	deathscreen = Game->GetCurrScene()->FindObjectInAnySpace("cover");
	ASSERT(deathscreen != nullptr)
	screenSprite = deathscreen->get_component_type<Sprite>();
	ASSERT(screenSprite != nullptr)
	deathtext = Game->GetCurrScene()->FindObjectInAnySpace("deathtext");
	ASSERT(deathtext != nullptr)
	textSprite = deathtext->get_component_type<TextComp>();
	ASSERT(textSprite != nullptr)
	alpha = 0;
	mOwner = mActor->GetOwner();
	mAnimation = mOwner->get_component_type<PlayerAnimationChange>();
	ASSERT(mAnimation != nullptr)
	//mAnimation->ChangeAnimation(std::string("PatxiHammerSpin"), true);
}
void PlayerDeathState::LogicUpdate()
{
	if (alpha <= 1.0f)
	{
		mColor = Engine::Color(1.0f, 1.0f, 1.0f, alpha);
		screenSprite->SetModColor(mColor);
		alpha += 0.02f;
	}
	else
	{
		textSprite->mbVisible = true;
		GSM->setPause(true);
		if (KeyTriggered(Key::KeySpace))
		{
			GSM->setPause(false);
			GSM->GoToLevel("LevelWithSpaces");
		}
	}
}
void PlayerDeathState::LogicExit()
{}

//////////////////////////////////////////////////////////////////////////
//						PLAYER WITH KEYBOARD							//
//////////////////////////////////////////////////////////////////////////

void PlayerMovement::Initialize()
{
	direction = Vector2(1, 0);
	BlockMovement = false;
}
void PlayerMovement::Update()
{
	if (BlockMovement)
		return;

	//MOVEMENT WITH GAMEPAD
	if (InputHandler::Instance()->GetJoystickState() != Vector2(0, 0))
	{
		movement = InputHandler::Instance()->GetJoystickState();
		direction = movement.Normalize();
	}

	//MOVEMENT WITH KEYBOARD
	else
	{
		if (KeyDown(Key::Left))
			movement.x = -1;
		if (KeyDown(Key::Up))
			movement.y = 1;
		if (KeyDown(Key::Down))
			movement.y = -1;
		if (KeyDown(Key::Right))
			movement.x = 1;

		if (!KeyDown(Key::Left) && !KeyDown(Key::Right))
			movement.x = 0;
		if (!KeyDown(Key::Up) && !KeyDown(Key::Down))
			movement.y = 0;

		if (KeyDown(Key::Left) && KeyDown(Key::Right))
			movement.x = 0;
		if (KeyDown(Key::Up) && KeyDown(Key::Down))
			movement.y = 0;

		if (movement != Vector2(0, 0))
			direction = movement.Normalize();
	}
}
void PlayerMovement::Shutdown()
{}

//////////////////////////////////////////////////////////////////////
//						Combo Player State							//
//////////////////////////////////////////////////////////////////////

void ComboSys::LogicEnter()
{
	mPlayerInfo = mActor->mOwner->get_component_type<PlayerInfo>();
	TimeEllapsed = 0.0f;
}
void ComboSys::LogicUpdate()
{
	if (mPlayerInfo && mPlayerInfo->ComboCount != 0)
	{
		if (mPlayerInfo->ComboStarted)
		{
			TimeEllapsed = 0.0f;
			mPlayerInfo->ComboStarted = false;
		}
			
		TimeEllapsed += static_cast<float>(TimeManager::Instance()->GetDt());

		if (TimeEllapsed > 2)
		{
			mPlayerInfo->ComboCount = 0;
			TimeEllapsed = 0.0f;
		}
	}
	//std::cout << mPlayerInfo->ComboCount << std::endl;
}
void ComboSys::LogicExit()
{

}
ComboSys* ComboSys::Clone()
{
	ComboSys* state = DBG_NEW ComboSys(*this);
	return state;
}

//////////////////////////////////////////////////////////////////////
//							 Player Info							//
//////////////////////////////////////////////////////////////////////

void PlayerInfo::Initialize()
{
	comboid = 0;
	timer = 0.0f;
	reset = false;
	HH = false;
	enemies_slayed = 0;
}
void PlayerInfo::Update()
{
	//std::cout << comboid << std::endl;
	if (reset)
	{
		comboid = 0;
		reset = false;
	}
		if (HH)
	{
		comboid += 1;
		timer = 0.0f;
		HH = false;
	}
	if (comboid != 0 && timer >= cancellation_time)
		comboid = 0;
	if (comboid > 1)
		comboid = 0;
	timer += static_cast<float>(TimeManager::Instance()->GetDt());

	/* WIN CONDITION */
	if (ObjectCount >= 2)
	{
		ObjectCount = 0;
		GSM->GoToLevel("Testing");
		Game->onTitle = true;
	}
}
void PlayerInfo::Shutdown() 
{
	Pickable_objects.clear();
}


bool PlayerInfo::Edit()
{
	bool changed = false;
	ImGui::PushID("PlayerInfo");

	if (ImGui::TreeNode("PlayerInfo"))
	{
		ImGui::TreePop();
	}
	else
	{
		ImGui::SameLine(ImGui::GetWindowWidth() - 35);
		if (ImGui::Button("", ImVec2(14, 14))) {
			mOwner->RemoveComp(this);
			changed = true;
		}
	}
	ImGui::PopID();
	return changed;
}

void PlayerInfo::FromJson(nlohmann::json& j)
{
	if (j.find("Pickable_objects") != j.end())
	{
		for (std::string str : j["Pickable_objects"])
		{
			std::string string(str);
			//strcpy_s(string, 100, str.c_str());
			Pickable_objects.push_back(string);
		}
	}
}

void PlayerInfo::ToJson(nlohmann::json& j)
{
	j["_type"] = "PlayerInfo";

	j["Pickable_objects"];
	for (unsigned i = 0; i < Pickable_objects.size(); i++)
		j["Pickable_objects"].push_back(Pickable_objects[i].c_str());
}

IComp* PlayerInfo::Clone()
{
	PlayerInfo* player_info = DBG_NEW PlayerInfo(*this);
	player_info->Pickable_objects.clear();

	for (std::string str : Pickable_objects)
	{
		std::string new_str(str);
		//strcpy_s(new_str, 100, str);
		player_info->Pickable_objects.push_back(new_str);
	}
	return player_info;
}

void PlayerInfo::operator=(IComp& _comp)
{
	PlayerInfo* player_info = dynamic_cast<PlayerInfo*>(&_comp);

	for (std::string str : Pickable_objects)
	{
		std::string new_str(str);
		//strcpy_s(new_str, 100, str);
		player_info->Pickable_objects.push_back(new_str);
	}
}

bool PlayerInfo::are_the_same_type(IComp const& lhs)
{
	if (dynamic_cast<const PlayerInfo*>(&lhs)) {
		return true;
	}
	return false;
}

bool PlayerInfo::equal_to(IComp const& other) const
{
	if (PlayerInfo const* p = dynamic_cast<PlayerInfo const*>(&other)) {
		return this->ComboCount == p->ComboCount && this->ComboStarted == p->ComboStarted;
	}
	else {
		return false;
	}
}