#pragma once
#include "../../Engine/Engine.h"
#include "../LogicComponents/ChangeAnimationsComponent/AnimationChangeComponent.h"
#include "PlayerLogic.h"

class SpineAnimation;

class PlayerAnimationChange : public ILogic
{
	RTTI_DECLARATION_INHERITED(PlayerAnimationChange, ILogic);
	LOGIC_COMPONENT(PlayerAnimationChange);

public:

	void Update();
	void Initialize();

	void Shutdown();

	void ChangeAnimation(bool unique = false);
	void ChangeAnimation(const std::string& change, bool unique = false);
	void DirectionChanged();
	std::string GetActualAnimName();
	void SetActualAnim(const std::string& animation);

	SpineAnimation* GetAnimation() { return mAnimationComp; }

private:
	SpineAnimation* mAnimationComp;
	PlayerMovement* mPlayerDirection;
	Vector2 mDirection;
	double actual_angle;
	float anim_time;

	bool mbUnique{ false };

	std::string previous_direction;
	std::string current_direction;
	std::string current_animation;
	const std::vector<std::string> vec_directions{ "_N", "_NE", "_E", "_SE", "_S", "_SW", "_W", "_NW" };
};
#pragma once
