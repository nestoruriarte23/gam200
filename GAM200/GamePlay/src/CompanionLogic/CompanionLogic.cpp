#include "CompanionLogic.h"
#include "../PlayerLogic/PlayerLogic.h"
#include "../LogicComponents/HealthComponent/HealthComponent.h"
#include "../../Engine/src/Graphics/Particles/ParticleProperties.h"
#include "../HUDLogic/HUDLogic.h"



void CompanionMoveState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	distance = 70000;
	mVelocity = 5;
	lerp_direction = Vector2(0, 0);
}

void CompanionMoveState::LogicUpdate()
{
	movement = Vector2(player->mPosition.x, player->mPosition.y) - Vector2(mOwner->mPosition.x, mOwner->mPosition.y-80);

	if (movement.LengthSq() > distance)
		lerp_direction = player->mPosition - (movement/2);

	Vector2 tempPos = Vector2::Lerp(mOwner->mPosition, lerp_direction, 0.05f);

	mOwner->Move(mOwner->mPosition, tempPos);

	mOwner->mPosition = tempPos;

	if((KeyTriggered(Key::Z) || ButtonTriggered(SDL_CONTROLLER_BUTTON_LEFTSHOULDER)) && CompanionBar::ExplosionReady)
		mOwnerStateMachine->ChangeState("CompanionExplosion");

}

void CompanionMoveState::LogicExit()
{

}

CompanionMoveState* CompanionMoveState::Clone()
{
	CompanionMoveState* state = DBG_NEW CompanionMoveState(*this);

	return state;
}



void CompanionHeal::LogicEnter()
{
	mPlayer = Game->GetCurrScene()->FindObjectInAnySpace("player");
	player_info = mPlayer->get_component_type<PlayerInfo>();
	player_health = mPlayer->get_component_type<Health>();
	Healing = 1.0f;
	mParticleSys = mActor->mOwner->get_component_type<ParticleSystem>();
}

void CompanionHeal::LogicUpdate()
{
	if (!player_health->is_damaged())
	{
		if (!player_health->is_full_hp())
		{
			mParticleSys->SetPause(false);
			healing_delay += TimeManager::Instance()->GetDt();
			if (healing_delay > 0.1f)
			{
				player_health->ChangeHealth(Healing);
				healing_delay = 0.0f;
			}
		}
		else
			mParticleSys->SetPause(true);
	}
	else 
		mParticleSys->SetPause(true);
}

void CompanionHeal::LogicExit()
{

}

CompanionHeal* CompanionHeal::Clone()
{
	CompanionHeal* state = DBG_NEW CompanionHeal(*this);

	return state;
}



void CompanionExplosion::LogicEnter()
{
	mInitPos = Vector2(mActor->mOwner->mPosition.x, mActor->mOwner->mPosition.y);
	Range = 1300.0f;

	Explosion = Game->CreateArchetype("Explosion");
	Explosion->mPosition = mActor->mOwner->mPosition;
}

void CompanionExplosion::LogicUpdate()
{
	if(Explosion->mScale.x > Range)
	{
		Game->DestroyObject(Explosion);
		mOwnerStateMachine->ChangeState("CompanionMoveState");
	}
}


void CompanionExplosion::LogicExit()
{
	CompanionBar::ExplosionReady = false;
	CompanionBar::ExplosionCounter = 0;
}

CompanionExplosion* CompanionExplosion::Clone()
{
	CompanionExplosion* state = DBG_NEW CompanionExplosion(*this);

	return state;
}


void ExplosionLogic::Initialize()
{
	explosion_shape = mOwner->get_component_type<BoxCollider>();
	mSprite = mOwner->get_component_type<Sprite>();
	subscribe_collision_event();
	DeadTime = 0.0f;
	alpha = 1.0f;
}
void ExplosionLogic::Update()
{
	mOwner->mScale += Vector2(55, 50);
	explosion_shape->mCollSize += Vector2(55, 50);

	DeadTime += TimeManager::Instance()->GetDt();
	if (DeadTime > 0.3f)
	{
		alpha -= 0.25f;
		Engine::Color color(1.0f, 1.0f, 1.0f, alpha);
		mSprite->SetModColor(color);
	}
}
void ExplosionLogic::ShutDown()
{

}

void ExplosionLogic::OnCollisionStarted(const BoxCollider* coll)
{
	if (coll->mOwner->mTag == Tags::Enemies)
	{
		Game->DestroyObject(coll->mOwner);
	}
}

