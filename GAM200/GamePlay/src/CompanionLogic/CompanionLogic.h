#pragma once
#include "../../Engine/Engine.h"

class PlayerInfo;
class Health;



struct CompanionMoveState : public State
{
	RTTI_DECLARATION_INHERITED(CompanionMoveState, State);
	CompanionMoveState(const char* name)
		:State(name)
	{}

	CompanionMoveState* Clone();

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	GameObject* mOwner;
	GameObject* player;
	Vector2 movement;
	Vector2 lerp_direction;
	float mVelocity;
	float distance;
};

class ParticleSystem;
struct CompanionHeal : public State
{
	RTTI_DECLARATION_INHERITED(CompanionHeal, State);
	CompanionHeal(const char* name)
		:State(name)
	{}

	CompanionHeal* Clone();

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	GameObject* mPlayer;
	PlayerInfo* player_info;
	Health* player_health;
	ParticleSystem* mParticleSys;

	float Healing;
	f64 healing_delay = 0.0f;
};

struct CompanionExplosion : public State
{
	RTTI_DECLARATION_INHERITED(CompanionExplosion, State);
	CompanionExplosion(const char* name)
		:State(name)
	{}

	CompanionExplosion* Clone();

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	Vector2 mInitPos;
	GameObject* Explosion;
	float Range;

};


struct ExplosionLogic : public ILogic
{
	RTTI_DECLARATION_INHERITED(ExplosionLogic, ILogic);
	LOGIC_COMPONENT(ExplosionLogic);

public:
	ExplosionLogic() : ILogic() {}

	void Initialize();
	void Update();
	void ShutDown();

	void OnCollisionStarted(const BoxCollider* coll);

	BoxCollider* explosion_shape;
	Sprite* mSprite;
	float alpha = 1.0f;
	f64 DeadTime = 0.0f;
};



