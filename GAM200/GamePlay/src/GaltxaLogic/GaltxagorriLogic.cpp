#include "GaltxagorriLogic.h"
#include "../HUDLogic/HUDLogic.h"

//////////////////////////////////////////////////////////////////////////
//						GALTZAGORRI COMPONENT							//
//////////////////////////////////////////////////////////////////////////

void GaltzagorriLogic::Initialize()
{
	
	mOwner = GetOwner();
	mPlayer = Game->GetCurrScene()->FindObjectInAnySpace("player");
	player_collider = mPlayer->get_component_type<BoxCollider>();
	owner_collider = mOwner->get_component_type<BoxCollider>();

	mAnimation = mOwner->get_component_type<AnimationChange>();

	mRigidBody = mOwner->get_component_type<RigidBody>();
	ASSERT(mRigidBody != nullptr);
	mEnemyLogic = mOwner->get_component_type<EnemyLogic>();
	ASSERT(mEnemyLogic != nullptr);

	mLife = mOwner->get_component_type<Health>();
	ASSERT(mLife != nullptr);

	distance = mOwner->mPosition.Distance(mPlayer->mPosition);
	mDir = mPlayer->mPosition - mOwner->mPosition;

	buff = false;
}

void GaltzagorriLogic::Update()
{
	mDir = mPlayer->mPosition - mOwner->mPosition;
	distance = (mOwner->mPosition + owner_collider->mOffset).Distance(mPlayer->mPosition + player_collider->mOffset);
}

void GaltzagorriLogic::Shutdown()
{

}

void GaltzagorriLogic::Buff()
{
	buff = true;
	mOwner->mScale *= 3;
	BoxCollider* box = mOwner->get_component_type<BoxCollider>();
	box->mCollSize *= 3;
	box->mOffset -= (box->mOffset * 3) / 2;
	mLife->SetFullHealth(30);
}

//////////////////////////////////////////////////////////////////////////
//						GALTZAGORRI IDDLE STATE							//
//////////////////////////////////////////////////////////////////////////

void GaltzagorriIddleState::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<GaltzagorriLogic>();
	mSpace = mLogic->mOwner->GetSpace();
	
	mLogic->mLife->SetHealth(20);

	ASSERT(mLogic->mAnimation != nullptr)
		mLogic->mAnimation->ChangeAnimation(std::string("Galtz_Iddle"));
	mLogic->mAnimation->west = false;

	//range = 1492;
}
void GaltzagorriIddleState::LogicUpdate()
{
	//TRIGGER
	if (mLogic->distance < activation || mLogic->mEnemyLogic->trigger)
	{
		trigger = Game->CreateArchetype("galtzatrigger");
		trigger->mPosition = mActor->GetOwner()->mPosition;
		//Game->DestroyObject(trigger);
		mOwnerStateMachine->ChangeState("GaltzagorriRunState");
	}
	//RECEIVE ATTACK
	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerHit)
		mOwnerStateMachine->ChangeState("Galtzagorri_HammerHit");
	else if (mLogic->mEnemyLogic->attacked_with == AttackID::SuperHammerHit)
		mOwnerStateMachine->ChangeState("Galtzagorri_SuperHammerHit");
	else if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerSpin)
		mOwnerStateMachine->ChangeState("Galtzagorri_HammerSpin");
	else if (mLogic->mEnemyLogic->attacked_with == AttackID::Tackle)
		mOwnerStateMachine->ChangeState("Galtzagorri_Tackle");
	//DIE
	if(mLogic->mLife->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
}
void GaltzagorriIddleState::LogicExit()
{}
/*void GaltzagorriIddleState::Edit()
{
	ImGui::PushID("GaltzagorriRunState");

	ImGui::DragFloat("Activation Distance", &range);
	//ImGui::DragFloat("Velocity", &mVelocity);

	ImGui::PopID();
}
void GaltzagorriIddleState::Selected()
{
	if (mActor != NULL)
	{
		Vector2 pos(mActor->GetOwner()->mPosition.x, mActor->GetOwner()->mPosition.y);
		RenderManager::Instance()->DrawCircleAt(pos, 100, range * 2, Engine::Color::white);
	}
}
void GaltzagorriIddleState::FromJson(nlohmann::json & j)
{
	range = j["range"];
}
void GaltzagorriIddleState::ToJson(nlohmann::json & j)
{
	j["range"] = range;
}*/
GaltzagorriIddleState* GaltzagorriIddleState::Clone()
{
	GaltzagorriIddleState* state = DBG_NEW GaltzagorriIddleState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//					GALTZAGORRI TRIGGER LOGIC							//
//////////////////////////////////////////////////////////////////////////

void TriggerLogic::Initialize() { subscribe_collision_event(); }
void TriggerLogic::Update() {}
void TriggerLogic::ShutDown() {}
void TriggerLogic::OnCollisionStarted(const BoxCollider* coll)
{
	if (coll->mOwner->mTag == Tags::Enemies)
	{
		enemy = coll->mOwner->get_component_type<EnemyLogic>();
		ASSERT(enemy != NULL)
		enemy->trigger = true;
	}
}

//////////////////////////////////////////////////////////////////////////
//						GALTZAGORRI RUN STATE							//
//////////////////////////////////////////////////////////////////////////

void GaltzagorriRunState::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<GaltzagorriLogic>();

	ASSERT(mLogic->mAnimation != nullptr)
		mLogic->mAnimation->ChangeAnimation(std::string("Galtz_Run"));

	EllapsedTime = 0;
}
void GaltzagorriRunState::LogicUpdate()
{
	float range = 150.0f;
	if (mLogic->buff)
		range *= 2.0f;
	//RUN
	if(mLogic->distance >= range)
		if(!mLogic->buff)
			mLogic->mOwner->mPosition += mLogic->mDir.Normalize() * mVelocity;
		else
			mLogic->mOwner->mPosition += mLogic->mDir.Normalize() * mVelocity * 0.75f;
	//ATTACK
	else// if (EllapsedTime > ChangeTime)
		mOwnerStateMachine->ChangeState("GaltzagorriAttackState");
	//RECEIVE ATTACK
	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerHit)
		mOwnerStateMachine->ChangeState("Galtzagorri_HammerHit");
	else if (mLogic->mEnemyLogic->attacked_with == AttackID::SuperHammerHit)
		mOwnerStateMachine->ChangeState("Galtzagorri_SuperHammerHit");
	else if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerSpin)
		mOwnerStateMachine->ChangeState("Galtzagorri_HammerSpin");
	else if (mLogic->mEnemyLogic->attacked_with == AttackID::Tackle)
		mOwnerStateMachine->ChangeState("Galtzagorri_Tackle");
	//DIE
	if (mLogic->mLife->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
	//COOL DOWNS
	EllapsedTime += static_cast<float>(TimeManager::Instance()->GetDt());
}
void GaltzagorriRunState::LogicExit()
{}

GaltzagorriRunState* GaltzagorriRunState::Clone()
{
	GaltzagorriRunState* state = DBG_NEW GaltzagorriRunState(*this);

	return state;
}
//////////////////////////////////////////////////////////////////////////
//						GALTZAGORRI ATTACK STATE						//
//////////////////////////////////////////////////////////////////////////

void GaltzagorriAttackState::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<GaltzagorriLogic>();
	if(!mLogic->buff)
		mLogic->mPlayer->get_component_type<Health>()->DamageObject(damage);
	else
		mLogic->mPlayer->get_component_type<Health>()->DamageObject(damage*3);

	ASSERT(mLogic->mAnimation != nullptr)
		mLogic->mAnimation->ChangeAnimation(std::string("Galtz_Attack"));

	EllapsedTime = 0.0f;
}
void GaltzagorriAttackState::LogicUpdate()
{
	EllapsedTime += static_cast<float>(TimeManager::Instance()->GetDt());
	//RECEIVE ATTACK
	if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerHit)
		mOwnerStateMachine->ChangeState("Galtzagorri_HammerHit");
	else if (mLogic->mEnemyLogic->attacked_with == AttackID::SuperHammerHit)
		mOwnerStateMachine->ChangeState("Galtzagorri_SuperHammerHit");
	else if (mLogic->mEnemyLogic->attacked_with == AttackID::HammerSpin)
		mOwnerStateMachine->ChangeState("Galtzagorri_HammerSpin");
	else if (mLogic->mEnemyLogic->attacked_with == AttackID::Tackle)
		mOwnerStateMachine->ChangeState("Galtzagorri_Tackle");
	//DIE
	if (mLogic->mLife->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
	if(EllapsedTime > 1.0f)
		mOwnerStateMachine->ChangeState("GaltzagorriRunState");
}
void GaltzagorriAttackState::LogicExit()
{}

GaltzagorriAttackState* GaltzagorriAttackState::Clone()
{
	GaltzagorriAttackState* state = DBG_NEW GaltzagorriAttackState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						GALTZAGORRI DEATH STATE							//
//////////////////////////////////////////////////////////////////////////

void GaltzagorriDeathState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	playerinfo = player->get_component_type<PlayerInfo>();
	ASSERT(playerinfo != nullptr)
	playerinfo->enemies_slayed += 1;
}
void GaltzagorriDeathState::LogicUpdate()
{
	Game->DestroyObject(mOwner);
	CompanionBar::ExplosionCounter++;
}
void GaltzagorriDeathState::LogicExit()
{}

GaltzagorriDeathState* GaltzagorriDeathState::Clone()
{
	GaltzagorriDeathState* state = DBG_NEW GaltzagorriDeathState(*this);
	return state;
}

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////

void Galtzagorri_HammerHit::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<GaltzagorriLogic>();
	timer = 0;
	
	/* DAMAGE */
	mLogic->mLife->DamageObject(damage);
	mLogic->mEnemyLogic->attacked(AttackID::Nothing);
	/* PUSH BACK */
	displacement = mLogic->mDir.Normalize();
	//mOwner->mPosition += displacement * pushback_velocity;
	mLogic->mRigidBody->AddVelocity(displacement * pushback_velocity);
}
void Galtzagorri_HammerHit::LogicUpdate()
{
	timer += static_cast<float>(TimeManager::Instance()->GetDt());
	if (timer >= displacement_time)
	{
		mLogic->mRigidBody->mVelocity = Vector2(0, 0);
		if(mLogic->mLife->CurrentHealth() <= 0)
			mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
	}
	if (timer >= stun_time)
		mOwnerStateMachine->ChangeState("GaltzagorriRunState");
}
void Galtzagorri_HammerHit::LogicExit()
{}

Galtzagorri_HammerHit* Galtzagorri_HammerHit::Clone()
{
	Galtzagorri_HammerHit* state = DBG_NEW Galtzagorri_HammerHit(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//					ATTACKED WITH SUPER HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////

void Galtzagorri_SuperHammerHit::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<GaltzagorriLogic>();
	timer = 0.0f;

	mLogic->mLife->DamageObject(damage);
	mLogic->mEnemyLogic->attacked(AttackID::Nothing);
	/* PUSH BACK */
	displacement = mLogic->mDir.Normalize();
	//mOwner->mPosition += displacement * pushback_velocity;
	mLogic->mRigidBody->AddVelocity(displacement * pushback_velocity);
}
void Galtzagorri_SuperHammerHit::LogicUpdate()
{
	timer += static_cast<float>(TimeManager::Instance()->GetDt());
	if (timer >= displacement_time)
	{
		mLogic->mRigidBody->mVelocity = Vector2(0, 0);
		if (mLogic->mLife->CurrentHealth() <= 0)
			mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
	}
	if (timer >= stun_time)
		mOwnerStateMachine->ChangeState("GaltzagorriRunState");
	if(mLogic->mLife->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
}
void Galtzagorri_SuperHammerHit::LogicExit()
{}

Galtzagorri_SuperHammerHit* Galtzagorri_SuperHammerHit::Clone()
{
	Galtzagorri_SuperHammerHit* state = DBG_NEW Galtzagorri_SuperHammerHit(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER SPIN						//
//////////////////////////////////////////////////////////////////////////

void Galtzagorri_HammerSpin::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<GaltzagorriLogic>();
	timer = time_to_damage;
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	tornado = Game->GetCurrScene()->FindObjectInAnySpace("tornado");
}
void Galtzagorri_HammerSpin::LogicUpdate()
{
	if (tornado != nullptr)
		attack_distance = tornado->mScale.x / 2;
	displacement = (player->mPosition - mActor->mOwner->mPosition);
	distance = displacement.Length();
	//TORNADO
	mActor->mOwner->mPosition += displacement.Normalize() * attraction_vel;
	if (mLogic->distance <= attack_distance && timer >= time_to_damage)
	{
		mLogic->mLife->DamageObject(damage);
		timer = 0;
		std::cout << "Tornado hit" << std::endl;
	}
	timer += static_cast<float>(TimeManager::Instance()->GetDt());
	//Tornado finished
	if (tornado == nullptr)
		mOwnerStateMachine->ChangeState("GaltzagorriRunState");
	//died
	if (mLogic->mLife->CurrentHealth() <= 0)
		mOwnerStateMachine->ChangeState("GaltzagorriDeathState");
}
void Galtzagorri_HammerSpin::LogicExit()
{}

Galtzagorri_HammerSpin* Galtzagorri_HammerSpin::Clone()
{
	Galtzagorri_HammerSpin* state = DBG_NEW Galtzagorri_HammerSpin(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH TACKLE							//
//////////////////////////////////////////////////////////////////////////

void Galtzagorri_Tackle::LogicEnter()
{
	mLogic = mActor->GetOwner()->get_component_type<GaltzagorriLogic>();
	ASSERT(mLogic != nullptr)
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	displacement = (player->mPosition - mActor->mOwner->mPosition).Normalize();
	timer = 0;
	mLogic->mEnemyLogic->attacked(AttackID::Nothing);
}
void Galtzagorri_Tackle::LogicUpdate()
{
	if (timer <= displacement_time)
		mLogic->mOwner->mPosition -= displacement * mVelocity;
		//mRigidbody->AddVelocity(displacement * mVelocity);
	else
		mOwnerStateMachine->ChangeState("GaltzagorriRunState");
	timer += static_cast<float>(TimeManager::Instance()->GetDt());
}
void Galtzagorri_Tackle::LogicExit()
{}
void Galtzagorri_Tackle::OnCollisionStarted(const BoxCollider* coll)
{
	if (coll->mOwner->mTag == Tags::Miscellaneous)
		mOwnerStateMachine->ChangeState("GaltzagorriRunState");
}

Galtzagorri_Tackle* Galtzagorri_Tackle::Clone()
{
	Galtzagorri_Tackle* state = DBG_NEW Galtzagorri_Tackle(*this);

	return state;
}