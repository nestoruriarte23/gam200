#pragma once
#include "../../Engine/Engine.h"
#include "../LogicComponents/HealthComponent/HealthComponent.h"
#include "../LogicComponents/ChangeAnimationsComponent/AnimationChangeComponent.h"
#include "../../GamePlay/EnemyLogic.h"
#include "../PlayerLogic/PlayerLogic.h"

//////////////////////////////////////////////////////////////////////////
//						GALTZAGORRI COMPONENT							//
//////////////////////////////////////////////////////////////////////////
class GaltzagorriLogic : public ILogic
{
	RTTI_DECLARATION_INHERITED(GaltzagorriLogic, ILogic);

public:
	LOGIC_COMPONENT(GaltzagorriLogic);
	PropertyMap properties;

	void Update();
	void Initialize();
	void Shutdown();

	void Buff();

	GameObject* mOwner;
	GameObject* mPlayer;

	BoxCollider* owner_collider;
	BoxCollider* player_collider;

	AnimationChange* mAnimation;

	EnemyLogic* mEnemyLogic;
	RigidBody* mRigidBody;
	Health* mLife;

	float distance;
	Vector2 mDir;

	bool buff;

private:
};

//////////////////////////////////////////////////////////////////////////
//						GALTZAGORRI IDDLE STATE							//
//////////////////////////////////////////////////////////////////////////

struct GaltzagorriIddleState : public State
{
	RTTI_DECLARATION_INHERITED(GaltzagorriIddleState, State);
	GaltzagorriIddleState(const char* name)
		:State(name), mLogic(NULL)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	/*void Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);*/
	GaltzagorriIddleState* Clone();

	GameObject* trigger;
	GaltzagorriLogic* mLogic;

	std::vector<GameObject*> targets;
	EnemyLogic* otherLogic;
	Space* mSpace;
	Ray triggerAlly;
	Matrix33 rotMatrix;

	float distance; 
	PROP_Val(float, activation, 500.0f);

	AnimationChange* mAnimation;
};

//////////////////////////////////////////////////////////////////////////
//					GALTZAGORRI TRIGGER LOGIC							//
//////////////////////////////////////////////////////////////////////////

struct TriggerLogic : public ILogic
{
	RTTI_DECLARATION_INHERITED(TriggerLogic, ILogic);
	LOGIC_COMPONENT(TriggerLogic);

public:
	TriggerLogic() : ILogic() {}

	void Initialize();
	void Update();
	void ShutDown();

	void OnCollisionStarted(const BoxCollider* coll);
	EnemyLogic* enemy;
};

//////////////////////////////////////////////////////////////////////////
//						GALTZAGORRI RUN STATE							//
//////////////////////////////////////////////////////////////////////////

struct GaltzagorriRunState : public State
{
	RTTI_DECLARATION_INHERITED(GaltzagorriRunState, State);
	GaltzagorriRunState(const char* name)
		:State(name), mLogic(NULL)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	GaltzagorriRunState* Clone();
	GaltzagorriLogic* mLogic;
	
	float fdistance;
	Vector2 distance;
	PROP_Val(float, mVelocity, 7.0f);
	PROP_Val(float, range, 70);
	PROP_Val(float, attack_distance, 20);
	PROP_Val(float, ChangeTime, 1.0f);
	float EllapsedTime;
	AnimationChange* mAnimation;
};

//////////////////////////////////////////////////////////////////////////
//							GALTZAGORRI DEATH							//
//////////////////////////////////////////////////////////////////////////

struct GaltzagorriDeathState : public State
{
	RTTI_DECLARATION_INHERITED(GaltzagorriDeathState, State);
	GaltzagorriDeathState(const char* name)
		:State(name), mOwner(NULL)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	GaltzagorriDeathState* Clone();
	PropertyMap properties;

	GameObject* mOwner;
	GameObject* player;
	PlayerInfo* playerinfo;
	AnimationChange* mAnimation;
};

//////////////////////////////////////////////////////////////////////////
//						GALTZAGORRI ATTACK STATE						//
//////////////////////////////////////////////////////////////////////////

struct GaltzagorriAttackState : public State
{
	RTTI_DECLARATION_INHERITED(GaltzagorriAttackState, State);
	GaltzagorriAttackState(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	GaltzagorriAttackState* Clone();
	GaltzagorriLogic* mLogic;
	PROP_Val(float, damage, 5.0f);
	float EllapsedTime;
};

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////

struct Galtzagorri_HammerHit: public State
{
	RTTI_DECLARATION_INHERITED(Galtzagorri_HammerHit, State);
	Galtzagorri_HammerHit(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	PropertyMap properties;

	Galtzagorri_HammerHit* Clone();

	GaltzagorriLogic * mLogic;

	Vector2 displacement;
	float timer;
	PROP_Val(float, displacement_time, 0.15f);
	PROP_Val(float, stun_time, 0.25f);
	PROP_Val(float, damage, 10.0f);
	PROP_Val(float, pushback_velocity, 750.0f);
};

//////////////////////////////////////////////////////////////////////////
//					ATTACKED WITH SUPER HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////
struct Galtzagorri_SuperHammerHit : public State
{
	RTTI_DECLARATION_INHERITED(Galtzagorri_SuperHammerHit, State);
	Galtzagorri_SuperHammerHit(const char* name)
		:State(name)
	{}
	PropertyMap properties;

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	Galtzagorri_SuperHammerHit* Clone();

	GaltzagorriLogic * mLogic;

	Vector2 displacement;
	float timer;
	PROP_Val(float, damage, 20.0f); 
	PROP_Val(float, displacement_time, 0.15f);
	PROP_Val(float, stun_time, 1.25f);
	PROP_Val(float, pushback_velocity, 1000.0f);
};

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER SPIN						//
//////////////////////////////////////////////////////////////////////////
struct Galtzagorri_HammerSpin : public State
{
	RTTI_DECLARATION_INHERITED(Galtzagorri_HammerSpin, State);
	Galtzagorri_HammerSpin(const char* name)
		:State(name)
	{}
	PropertyMap properties;

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	Galtzagorri_HammerSpin* Clone();

	GaltzagorriLogic * mLogic;

	GameObject* player;
	GameObject* tornado;
	Vector2 displacement;
	float distance;
	float timer;
	PROP_Val(float, damage, 10.0f);
	PROP_Val(float, attraction_vel, 5.0f);
	PROP_Val(float, attack_distance, 500.0f);
	PROP_Val(float, time_to_damage, 1.5f);
};

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH TACKLE							//
//////////////////////////////////////////////////////////////////////////
struct Galtzagorri_Tackle : public State
{
	RTTI_DECLARATION_INHERITED(Galtzagorri_Tackle, State);
	Galtzagorri_Tackle(const char* name)
		:State(name)
	{}
	PropertyMap properties;

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	void OnCollisionStarted(const BoxCollider* coll);
	Galtzagorri_Tackle* Clone();

	GaltzagorriLogic* mLogic;

	GameObject* player;
	Vector2 displacement;
	float timer;
	PROP_Val(float, displacement_time, 0.2f);
	PROP_Val(float, mVelocity, 30.0f);
};

