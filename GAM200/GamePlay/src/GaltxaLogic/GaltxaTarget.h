#pragma once
#include "../../Engine/Engine.h"

const static int enemy_count = 100;
class GaltxaTarget 
{
	MAKE_SINGLETON(GaltxaTarget);
public:
	Vector2 get_target(const Vector2 enemy_pos);
	bool is_full();


private:
	Vector2	Positions[enemy_count];
	bool available_positions[enemy_count] = { false };
	Vector2 PlayerPos;

};

#define EnemyTarget (GaltxaTarget::Instance())