#pragma once
#include "../../Engine/Engine.h"

class LamiaProjectileLogic : public ILogic
{
	RTTI_DECLARATION_INHERITED(LamiaProjectileLogic, ILogic);

public:
	LOGIC_COMPONENT(LamiaProjectileLogic);

	void Update();
	void Initialize();
	void Shutdown();

	void ReInit();

	void OnCollisionStarted(const BoxCollider* coll);
	void PlayerHit();

	GameObject* mPlayer;
	GameObject* mOwner;
	Vector2 mDir;
	float mDistance;
	float mSpeed;
	float mDamage;

	float EllapsedTime;
	float EndTime;

	EventHandler handler;
};

//////////////////////////////////////////////////////////////////////////
//						LAMIA PROJECTILE STATE							//
//////////////////////////////////////////////////////////////////////////

/*struct LamiaProjectileState : public State
{
	RTTI_DECLARATION_INHERITED(LamiaProjectileState, State);
	LamiaProjectileState(const char* name)
		:State(name), mOwner(NULL)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);

	GameObject* mOwner;
	GameObject* player;
	Vector2 movement;
	float mVelocity;
	float range;
};*/