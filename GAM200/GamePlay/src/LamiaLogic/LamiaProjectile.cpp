#include "LamiaProjectile.h"
#include "../LogicComponents/HealthComponent/HealthComponent.h"
#include <iostream>

void LamiaProjectileLogic::Initialize()
{
	ReInit();

	subscribe_collision_event();
}

void LamiaProjectileLogic::ReInit()
{
	mOwner = GetOwner();
	mPlayer = Game->GetCurrScene()->FindObjectInAnySpace("player");
	mName = mOwner->GetName();
	if (mPlayer != NULL)
	{
		mDir = Vector2(mPlayer->mPosition.x, mPlayer->mPosition.y + 150) - Vector2(mOwner->mPosition.x, mOwner->mPosition.y);
		mOwner->mRotation = -mDir.GetAngle() - PI / 2;
	}
	std::string lamia = "Lamia_Projectile";
	if (mName == lamia)
		mSpeed = 30;
	else
		mSpeed = 15;
	mDamage = 10;

	EllapsedTime = 0.0f;
	EndTime = 3.0f;
}

void LamiaProjectileLogic::Update()
{
	mOwner->mPosition += mDir.Normalize() * mSpeed;

	EllapsedTime += static_cast<float>(TimeManager::Instance()->GetDt());
	if (EllapsedTime > EndTime)
		Game->DestroyObject(mOwner);
}

void LamiaProjectileLogic::Shutdown()
{}

void LamiaProjectileLogic::OnCollisionStarted(const BoxCollider* coll)
{
	if(coll == mPlayer->get_component_type<BoxCollider>())
		PlayerHit();
	if(coll->GetOwner()->mTag == Tags::Miscellaneous)
		Game->DestroyObject(mOwner);
}

void LamiaProjectileLogic::PlayerHit()
{
	mPlayer->get_component_type<Health>()->ChangeHealth(-mDamage);
	Game->DestroyObject(mOwner);
}