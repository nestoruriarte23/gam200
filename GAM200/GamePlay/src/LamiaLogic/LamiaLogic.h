#pragma once
#include "../../Engine/Engine.h"
#include "../LogicComponents/HealthComponent/HealthComponent.h"
#include "../../GamePlay/EnemyLogic.h"
#include "../LogicComponents/ChangeAnimationsComponent/AnimationChangeComponent.h"

//////////////////////////////////////////////////////////////////////////
//							LAMIA CHASE STATE							//
//////////////////////////////////////////////////////////////////////////

struct LamiaChaseState : public State
{
	RTTI_DECLARATION_INHERITED(LamiaChaseState, State);
	LamiaChaseState(const char* name)
		:State(name), mOwner(NULL)
	{}

	virtual ~LamiaChaseState() {}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	LamiaChaseState* Clone();

	GameObject* mOwner;
	GameObject* player;
	Vector2 movement;
	float mVelocity;
	float range;
};

//////////////////////////////////////////////////////////////////////////
//							LAMIA ATTACK STATE							//
//////////////////////////////////////////////////////////////////////////

struct LamiaAttackState : public State
{
	RTTI_DECLARATION_INHERITED(LamiaAttackState, State);
	LamiaAttackState(const char* name)
		:State(name), mOwner(NULL)
	{}

	virtual ~LamiaAttackState() {}
	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	LamiaAttackState* Clone();

	EnemyLogic* mLogic;
	GameObject* mOwner;
	GameObject* player;
	AnimationChange* mAnimation;
	Vector2 movement;
	Vector2 projectile_spawn_offset;
	float displacement;
	float EllapsedTime;
	float SpawnProjectileTime;
	bool spawned;
	float ResetTime;
};

//////////////////////////////////////////////////////////////////////////
//						LAMIA ENTER WATER STATE							//
//////////////////////////////////////////////////////////////////////////

struct LamiaEnterWaterState : public State
{
	RTTI_DECLARATION_INHERITED(LamiaEnterWaterState, State);
	LamiaEnterWaterState(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	LamiaEnterWaterState* Clone();

	float EllapsedTime;
	float EnsStateTime;
};

//////////////////////////////////////////////////////////////////////////
//						LAMIA LEAVE WATER STATE							//
//////////////////////////////////////////////////////////////////////////

struct LamiaLeaveWaterState : public State
{
	RTTI_DECLARATION_INHERITED(LamiaLeaveWaterState, State);
	LamiaLeaveWaterState(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	LamiaLeaveWaterState* Clone();

	float EllapsedTime;
	float EnsStateTime;
};


//////////////////////////////////////////////////////////////////////////
//							LAMIA RANGE STATE							//
//////////////////////////////////////////////////////////////////////////

struct LamiaRangeState : public State
{
	RTTI_DECLARATION_INHERITED(LamiaRangeState, State);
	LamiaRangeState(const char* name)
		:State(name), mOwner(NULL)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	void Edit();
	void Selected();
	void FromJson(nlohmann::json & j);
	void ToJson(nlohmann::json & j);
	LamiaRangeState* Clone();

	GameObject* mOwner;
	GameObject* player;
	Vector2 mOwnerPos, mPlayerPos;
	StateMachine * main_machine;
	float range_attack;
	float range_flee;
};

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////

struct Lamia_HammerHit : public State
{
	RTTI_DECLARATION_INHERITED(Lamia_HammerHit, State);
	Lamia_HammerHit(const char* name)
		:State(name)
	{}
	PropertyMap properties;

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	Lamia_HammerHit* Clone();

	EnemyLogic* mLogic;
	GameObject* mOwner;
	Health* mLife;

	float timer;
	PROP_Val(float, damage, 10.0f);
};

//////////////////////////////////////////////////////////////////////////
//					ATTACKED WITH SUPER HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////
struct Lamia_SuperHammerHit : public State
{
	RTTI_DECLARATION_INHERITED(Lamia_SuperHammerHit, State);
	Lamia_SuperHammerHit(const char* name)
		:State(name)
	{}
	PropertyMap properties;

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	Lamia_SuperHammerHit* Clone();

	EnemyLogic* mLogic;
	GameObject* mOwner;
	Health* mLife;

	PROP_Val(float, damage, 20.0f);
};

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER SPIN						//
//////////////////////////////////////////////////////////////////////////
struct Lamia_HammerSpin : public State
{
	RTTI_DECLARATION_INHERITED(Lamia_HammerSpin, State);
	Lamia_HammerSpin(const char* name)
		:State(name)
	{}
	PropertyMap properties;

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();
	Lamia_HammerSpin* Clone();

	EnemyLogic* mLogic;
	GameObject* mOwner;
	Health* mLife;

	PROP_Val(float, damage, 20.0f);
};

/*//////////////////////////////////////////////////////////////////////////
//							ATTACKED WITH TACKLE						//
//////////////////////////////////////////////////////////////////////////
struct Lamia_Tackle : public State
{
	RTTI_DECLARATION_INHERITED(Lamia_Tackle, State);
	Lamia_Tackle(const char* name)
		:State(name)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	EnemyLogic* mLogic;
	GameObject* mOwner;
};*/