#include "LamiaLogic.h"
#include "../../Engine/src/StateMachines/StateMachine.h"
#include "../../GamePlay/src/LogicComponents/HealthComponent/HealthComponent.h"

//////////////////////////////////////////////////////////////////////////
//							LAMIA CHASE STATE							//
//////////////////////////////////////////////////////////////////////////

void LamiaChaseState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
}

void LamiaChaseState::LogicUpdate()
{
	movement = Vector2(player->mPosition.x, player->mPosition.y) - Vector2(mOwner->mPosition.x, mOwner->mPosition.y);
	//if (movement.Length() > range)
		//mOwner->mPosition += movement.Normalize() * mVelocity;     COMMENT FOR VERTICAL SLICE ONLY
	//else
	//	mOwnerStateMachine->ChangeState("LamiaAttackState");
}

void LamiaChaseState::LogicExit()
{}

void LamiaChaseState::Edit()
{
	ImGui::PushID("GaltzagorriRunState");

	ImGui::DragFloat("Range to Attack", &range);
	ImGui::DragFloat("Speed", &mVelocity);

	ImGui::PopID();
}

void LamiaChaseState::Selected()
{
	if (mActor != NULL)
	{
		Vector2 pos(mActor->GetOwner()->mPosition.x, mActor->GetOwner()->mPosition.y);
		RenderManager::Instance()->DrawCircleAt(pos, 100, range * 2, Engine::Color::white);
	}
}

void LamiaChaseState::FromJson(nlohmann::json & j)
{
	mVelocity = j["mVelocity"];
	range = j["range"];
}

void LamiaChaseState::ToJson(nlohmann::json & j)
{
	j["mVelocity"] = mVelocity;
	j["range"] = range;
}

LamiaChaseState* LamiaChaseState::Clone()
{
	LamiaChaseState* state = DBG_NEW LamiaChaseState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//							LAMIA ATTACK STATE							//
//////////////////////////////////////////////////////////////////////////

void LamiaAttackState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	movement = Vector2(player->mPosition.x, player->mPosition.y) - projectile_spawn_offset+Vector2(mOwner->mPosition.x, mOwner->mPosition.y);
	movement.NormalizeThis();
	mLogic = mOwner->get_component_type<EnemyLogic>();

	//EnemyDirectionState* dirState = dynamic_cast<EnemyDirectionState*>(mOwnerStateMachine[1].GetState("EnemyDirectionState"));
	//movement = dirState->movement;

	displacement = 100;

	EllapsedTime = SpawnProjectileTime;
	spawned = false;

	mAnimation = mOwner->get_component_type<AnimationChange>();
	ASSERT(mAnimation != nullptr)
	mAnimation->ChangeAnimation(std::string("LamiaAttack"));
	std::cout << "Entered\n";
}

void LamiaAttackState::LogicUpdate()
{
	EllapsedTime += static_cast<float>(TimeManager::Instance()->GetDt());
	if (EllapsedTime > SpawnProjectileTime && !spawned)
	{
		spawned = true;
		GameObject * projectile = Game->CreateArchetype("Lamia_Projectile");
		movement = Vector2(player->mPosition.x, player->mPosition.y) - projectile_spawn_offset + Vector2(mOwner->mPosition.x, mOwner->mPosition.y);
		movement.NormalizeThis();
		projectile->mPosition = Vector3(projectile_spawn_offset.x+movement.x+mOwner->mPosition.x, projectile_spawn_offset.y+movement.y+mOwner->mPosition.y, 0.0f);
		//projectile->Initialize();
		std::cout << "Shot projectile" << std::endl;
	}
	if (EllapsedTime > SpawnProjectileTime)
	{
		EllapsedTime = 0.0f;
		spawned = false;
	}

	if (mLogic->attacked_with == AttackID::HammerHit)
		mOwnerStateMachine->ChangeState("Lamia_HammerHit");
	else if (mLogic->attacked_with == AttackID::SuperHammerHit)
		mOwnerStateMachine->ChangeState("Lamia_SuperHammerHit");
	else if (mLogic->attacked_with == AttackID::HammerSpin)
		mOwnerStateMachine->ChangeState("Lamia_HammerSpin");
	else if (mLogic->attacked_with == AttackID::Tackle)
		mOwnerStateMachine->ChangeState("Lamia_Tackle");
}

void LamiaAttackState::LogicExit()
{
	mAnimation->ChangeAnimation(std::string("LamiaAttackAnticip"));
	std::cout << "Exited\n";
}

void LamiaAttackState::Edit()
{
	ImGui::PushID("GaltzagorriRunState");

	ImGui::DragFloat2("Proj. Spawn Point", projectile_spawn_offset.v);
	ImGui::DragFloat("Proj. Spawn Time", &SpawnProjectileTime);
	ImGui::DragFloat("Reset Timer At", &ResetTime);

	ImGui::PopID();
}

void LamiaAttackState::Selected()
{
	if (mActor != NULL)
	{
		Vector2 pos(mActor->GetOwner()->mPosition.x, mActor->GetOwner()->mPosition.y);
		RenderManager::Instance()->DrawCircleAt(pos+projectile_spawn_offset, 100, 2, Engine::Color::white);
	}
}

void LamiaAttackState::FromJson(nlohmann::json & j)
{
	projectile_spawn_offset.x = j["Projectile Spawn Offset"]["x"];
	projectile_spawn_offset.y = j["Projectile Spawn Offset"]["y"];
	SpawnProjectileTime = j["Spawn Projectile Time"];
	ResetTime = j["Reset Timer At"];
}

void LamiaAttackState::ToJson(nlohmann::json & j)
{
	j["Projectile Spawn Offset"];
	j["Projectile Spawn Offset"]["x"] = projectile_spawn_offset.x;
	j["Projectile Spawn Offset"]["y"] = projectile_spawn_offset.y;
	j["Spawn Projectile Time"] = SpawnProjectileTime;
	j["Reset Timer At"] = ResetTime;
}

LamiaAttackState* LamiaAttackState::Clone()
{
	LamiaAttackState* state = DBG_NEW LamiaAttackState(*this);

	return state;
}


//////////////////////////////////////////////////////////////////////////
//							LAMIA RANGE STATE							//
//////////////////////////////////////////////////////////////////////////

void LamiaRangeState::LogicEnter()
{
	mOwner = mActor->GetOwner();
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	main_machine = mOwner->get_component_type<Actor>()->mBrain[0];
}

void LamiaRangeState::LogicUpdate()
{
	mOwnerPos = Vector2(mOwner->mPosition.x, mOwner->mPosition.y);
	mPlayerPos = Vector2(player->mPosition.x, player->mPosition.y);
	float distance = mOwnerPos.Distance(mPlayerPos);
	if (distance < range_attack && main_machine->mCurrentState == main_machine->GetState("LamiaChaseState"))
	{
		main_machine->ChangeState("LamiaAttackState");
	}
	if (distance > range_attack && main_machine->mCurrentState == main_machine->GetState("LamiaAttackState"))
	{
		main_machine->ChangeState("LamiaChaseState");
	}
}

void LamiaRangeState::LogicExit()
{}

void LamiaRangeState::Edit()
{
	ImGui::PushID("GaltzagorriRangeState");

	ImGui::DragFloat("Range of Attack", &range_attack);
	ImGui::DragFloat("Range to Flee", &range_flee);

	ImGui::PopID();
}

void LamiaRangeState::Selected()
{
	if (mActor != NULL)
	{
		mOwnerPos = Vector2(mActor->GetOwner()->mPosition.x, mActor->GetOwner()->mPosition.y);
		RenderManager::Instance()->DrawCircleAt(mOwnerPos, 100, 2*range_attack, Engine::Color::white);
		RenderManager::Instance()->DrawCircleAt(mOwnerPos, 100, 2*range_flee, Engine::Color::white);
	}
}

void LamiaRangeState::FromJson(nlohmann::json & j)
{
	range_attack = j["Range of attack"];
	range_flee = j["Range to flee"];
}

void LamiaRangeState::ToJson(nlohmann::json & j)
{
	j["Range of attack"] = range_attack;
	j["Range to flee"] = range_flee;
}

LamiaRangeState* LamiaRangeState::Clone()
{
	LamiaRangeState* state = DBG_NEW LamiaRangeState(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////

void Lamia_HammerHit::LogicEnter()
{
	mOwner = mActor->GetOwner();

	mLife = mOwner->get_component_type<Health>();
	ASSERT(mLife != nullptr);
	mLogic = mOwner->get_component_type<EnemyLogic>();
	ASSERT(mLogic != nullptr);
}

void Lamia_HammerHit::LogicUpdate()
{
	/* DAMAGE */
	mLife->DamageObject(damage);
	mLogic->attacked(AttackID::Nothing);
}

void Lamia_HammerHit::LogicExit()
{

}

Lamia_HammerHit* Lamia_HammerHit::Clone()
{
	Lamia_HammerHit* state = DBG_NEW Lamia_HammerHit(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//					ATTACKED WITH SUPER HAMMER HIT						//
//////////////////////////////////////////////////////////////////////////

void Lamia_SuperHammerHit::LogicEnter()
{
	mOwner = mActor->GetOwner();

	mLife = mOwner->get_component_type<Health>();
	ASSERT(mLife != nullptr);
	mLogic = mOwner->get_component_type<EnemyLogic>();
	ASSERT(mLogic != nullptr);

	mLife->DamageObject(damage);
	mLogic->attacked(AttackID::Nothing);
}

void Lamia_SuperHammerHit::LogicUpdate()
{
	mOwnerStateMachine->ChangeState("LamiaAttackState");
}

void Lamia_SuperHammerHit::LogicExit()
{

}

Lamia_SuperHammerHit* Lamia_SuperHammerHit::Clone()
{
	Lamia_SuperHammerHit* state = DBG_NEW Lamia_SuperHammerHit(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//						ATTACKED WITH HAMMER SPIN						//
//////////////////////////////////////////////////////////////////////////

void Lamia_HammerSpin::LogicEnter()
{
	mOwner = mActor->GetOwner();

	mLife = mOwner->get_component_type<Health>();
	ASSERT(mLife != nullptr);
	mLogic = mOwner->get_component_type<EnemyLogic>();
	ASSERT(mLogic != nullptr);
}

void Lamia_HammerSpin::LogicUpdate()
{
	mLife->DamageObject(damage);
	mLogic->attacked(AttackID::Nothing);
}

void Lamia_HammerSpin::LogicExit()
{

}

Lamia_HammerSpin* Lamia_HammerSpin::Clone()
{
	Lamia_HammerSpin* state = DBG_NEW Lamia_HammerSpin(*this);

	return state;
}

//////////////////////////////////////////////////////////////////////////
//							ATTACKED WITH TACKLE						//
//////////////////////////////////////////////////////////////////////////

/*void Lamia_Tackle::LogicEnter()
{
	mOwner = mActor->GetOwner();
	mLogic = mOwner->get_component_type<EnemyLogic>();
	if (mLogic == nullptr)
	{
		std::cout << "Enemy logic component not found" << std::endl;
		return;
	}
}

void Lamia_Tackle::LogicUpdate()
{}

void Lamia_Tackle::LogicExit()
{}*/