#pragma once
#include "../../Engine/Engine.h"

class Health;

class HealthBar : public ILogic
{
	RTTI_DECLARATION_INHERITED(HealthBar, ILogic);
	LOGIC_COMPONENT(HealthBar);

public:

	void Initialize();
	void Update();
	void Shutdown();

	Health* player_health;
	Vector2 InitialPos;
	Vector2 InitialScale;

};

class CompanionBar : public ILogic
{
	RTTI_DECLARATION_INHERITED(CompanionBar, ILogic);
	LOGIC_COMPONENT(CompanionBar);

public:

	void Initialize();
	void Update();
	void Shutdown();

	Vector2 InitialPos;
	Vector2 InitialScale;

	int MaxCount = 20;
	static int ExplosionCounter;
	static bool ExplosionReady;
};


class FaceSprite : public ILogic
{
	RTTI_DECLARATION_INHERITED(FaceSprite, ILogic);
	LOGIC_COMPONENT(FaceSprite);

public:

	void Initialize();
	void Update();
	void Shutdown();

	Sprite* mSprite;
	GameObject* player;
	Health* player_health;
};