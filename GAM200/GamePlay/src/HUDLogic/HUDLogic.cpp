#include "../LogicComponents/HealthComponent/HealthComponent.h"
#include "HUDLogic.h"


void HealthBar::Initialize()
{
	player_health = Game->GetCurrScene()->FindObjectInAnySpace("player")->get_component_type<Health>();
	InitialPos = mOwner->mPosition;
	InitialScale = mOwner->mScale;
}

void HealthBar::Update()
{
	float CurrentHalth = player_health->CurrentHealth();
	float FullHealth = player_health->MaxHealth();

	float scaleH = (CurrentHalth / FullHealth);
	float transx = (InitialScale.x - (scaleH * InitialScale.x)) / 2.0f;

	if (CurrentHalth < 0.0f)
		scaleH = 0;

	mOwner->mPosition.x = -transx + InitialPos.x;
	mOwner->mScale.x = scaleH * InitialScale.x;
}

void HealthBar::Shutdown()
{

}


int CompanionBar::ExplosionCounter = 0;
bool CompanionBar::ExplosionReady = false;

void CompanionBar::Initialize()
{
	InitialPos = mOwner->mPosition;
	InitialScale = mOwner->mScale;
	CompanionBar::ExplosionCounter = 0;
}

void CompanionBar::Update()
{
	if (ExplosionCounter >= MaxCount)
	{
		ExplosionReady = true;
		ExplosionCounter = MaxCount;
	}
		
	float scale = ((float)ExplosionCounter / (float)MaxCount);
	float transx = (InitialScale.x - (scale * InitialScale.x)) / 2.0f;

	if (ExplosionCounter < 0.0f)
		scale = 0;

	mOwner->mPosition.x = -transx + InitialPos.x;
	mOwner->mScale.x = scale * InitialScale.x;


}
void CompanionBar::Shutdown()
{

}

void FaceSprite::Initialize()
{
	mSprite = mOwner->get_component_type<Sprite>();
	ASSERT(mSprite != nullptr);
	player = Game->GetCurrScene()->FindObjectInAnySpace("player");
	ASSERT(player != nullptr);
	player_health = player->get_component_type<Health>();
	ASSERT(player_health != nullptr)
}

void FaceSprite::Update()
{
	if (player_health->CurrentHealth() <= 100 && player_health->CurrentHealth() > 80)
	{
		mSprite->ChangeTexture("..//Resources//Textures//HUD//BAR_face_100.png");
	}
	else if (player_health->CurrentHealth() <= 75 && player_health->CurrentHealth() > 50)
	{
		mSprite->ChangeTexture("..//Resources//Textures//HUD//BAR_face_80.png");
	}
	else if (player_health->CurrentHealth() <= 50 && player_health->CurrentHealth() > 30)
	{
		mSprite->ChangeTexture("..//Resources//Textures//HUD//BAR_face_50.png");
	}
	else if (player_health->CurrentHealth() <= 30 && player_health->CurrentHealth() > 15)
	{
		mSprite->ChangeTexture("..//Resources//Textures//HUD//BAR_face_25.png");
	}
	else if (player_health->CurrentHealth() <= 15 && player_health->CurrentHealth() >= 0)
	{
		mSprite->ChangeTexture("..//Resources//Textures//HUD//BAR_face_5.png");
	}
}

void FaceSprite::Shutdown()
{

}