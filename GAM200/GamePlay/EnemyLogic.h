#pragma once
#include "../Engine/Engine.h"

//////////////////////////////////////////////////////////////////////////
//							DESTROYABLE OBJECT							//
//////////////////////////////////////////////////////////////////////////

class DestroyableObject : public ILogic
{
	RTTI_DECLARATION_INHERITED(DestroyableObject, ILogic);
	LOGIC_COMPONENT(DestroyableObject);

public:
	void Initialize();
	void Update();
	void Shutdown();

	void Hit() { damaged = true; }

private:
	Sprite* mSprite;
	bool damaged = false;
	f64 Timer = 0.0f;
	float alpha = 1.0f;
};

//////////////////////////////////////////////////////////////////////////
//						DESTROY OBJECT AFTER TIME						//
//////////////////////////////////////////////////////////////////////////

class DestroyObjectTime : public ILogic
{
	RTTI_DECLARATION_INHERITED(DestroyObjectTime, ILogic);
	LOGIC_COMPONENT(DestroyObjectTime);

public:
	void Initialize() {}
	void Update() { Timer += static_cast<float>(TimeManager::Instance()->GetDt()); if(Timer > destroy_time) Game->DestroyObject(mOwner); }
	void Shutdown() {}
	PropertyMap properties;

private:
	f64 Timer = 0.0f;
	PROP_Val(float, destroy_time, 1.0f);
};

//////////////////////////////////////////////////////////////////////////
//						ENEMY LOGIC										//
//////////////////////////////////////////////////////////////////////////

enum AttackID {Nothing, HammerHit, Tackle, SuperHammerHit, HammerSpin};

class EnemyLogic : public ILogic
{
	RTTI_DECLARATION_INHERITED(EnemyLogic, ILogic);

public:
	LOGIC_COMPONENT(EnemyLogic);
	PropertyMap properties;

	void Update();
	void Initialize();
	void Shutdown();

	void attacked(AttackID id);
	AttackID attacked_with;
	bool trigger;

private:
	DestroyableObject* destroyable_object = nullptr;
	float timer;
	PROP_Val(float, inmunity_time, 0.45f);
};

//////////////////////////////////////////////////////////////////////////
//						ENEMY DIRECTION STATE							//
//////////////////////////////////////////////////////////////////////////

struct EnemyDirectionState : public State
{
	RTTI_DECLARATION_INHERITED(EnemyDirectionState, State);
	EnemyDirectionState(const char* name)
		:State(name), mOwner(NULL)
	{}

	virtual void LogicEnter();
	virtual void LogicUpdate();
	virtual void LogicExit();

	GameObject* mOwner;
	GameObject* player;
	Vector2 movement;
};



