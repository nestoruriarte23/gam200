#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 texture;
layout (location = 2) in vec4 color;

out vec4 VtxColor;

uniform mat4 mtxWorldToView;

void main()
{
    gl_Position = mtxWorldToView * vec4(aPos.x, aPos.y, 0.999f, 1.0f);
	VtxColor = color;
}