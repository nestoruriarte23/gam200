#version 330 core

in vec4 VtxColor;

out vec4 FragColor;

void main()
{
   FragColor = VtxColor;
}