#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 texture;
layout (location = 2) in vec4 color;

out vec2 TextureCoord;
out vec4 VtxColor;

uniform mat4 mtxModel;
uniform mat4 mtxWorldToView;

void main()
{
  gl_Position = mtxWorldToView * mtxModel * vec4(aPos.x, aPos.y, aPos.z, 1.0f);
	TextureCoord = texture;
	VtxColor = color;
}