#version 330 core

in vec2 TextureCoord;
in vec4 VtxColor;

out vec4 FragColor;

uniform vec4      modColor;
uniform sampler2D ourTexture;  // No need to change this uniform since it has the default location (0), this means, the texture currently binded

void main()
{
  FragColor = texture(ourTexture, TextureCoord) * modColor * VtxColor;
	if(FragColor.a < 0.1)
		discard;// discard the pixel to avoid writing to the depth buffer. 
}