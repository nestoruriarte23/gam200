#version 330 core

layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 texture;
layout (location = 2) in vec2 offPos;
layout (location = 3) in vec2 offScale;
layout (location = 4) in vec4 offColor;

out vec2 TextureCoord;
out vec4 VtxColor;

uniform float highest;
uniform mat4 mtxWorldToView;

void main()
{
    gl_Position = mtxWorldToView * vec4(aPos.x * offScale.x 
      + offPos.x, aPos.y * offScale.y + offPos.y, offPos.y / highest, 1.0f);
	VtxColor = offColor;
  TextureCoord = texture;
}